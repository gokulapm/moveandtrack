import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardOptionModalPage } from './card-option-modal.page';

describe('CardOptionModalPage', () => {
  let component: CardOptionModalPage;
  let fixture: ComponentFixture<CardOptionModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardOptionModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardOptionModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
