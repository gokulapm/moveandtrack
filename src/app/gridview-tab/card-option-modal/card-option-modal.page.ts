import { Component, Input, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ModalController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { app, storageVariable } from 'src/environments/environment';
import { ActionModePage } from '../action-mode/action-mode.page';
import { AlertsModalPage } from '../alerts-modal/alerts-modal.page';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-card-option-modal',
  templateUrl: './card-option-modal.page.html',
  styleUrls: ['./card-option-modal.page.scss'],
})
export class CardOptionModalPage implements OnInit {
@Input() cardData;
  vehicleActivity =  ['Live Track', 'Track History', 'Reports', 'Nearby', 'Alerts', 'GeoFence', 'Detail View', 'Immobilize', 'Settings', 'Share', 'Link', 'Navigation'];
  plateNo: any;
  selectedVin;
  constructor(
    private modalController: ModalController,
    private socialSharing: SocialSharing,
    public commonService: CommonService,
    private clipboard: Clipboard,
  ) { }

  closeModal(){
    this.modalController.dismiss();
  }
  async openAlertsModal() {
    this.closeModal()
    const modal = await this.modalController.create({
      component: AlertsModalPage
    });
    return await modal.present();
  }
  async openActionModal(mode: string, selectedVin) {
    this.closeModal()
    if (selectedVin) {
      this.selectedVin = selectedVin;
      localStorage.setItem('selectedVin', JSON.stringify(selectedVin))
    }
    const modal = await this.modalController.create({
      component: ActionModePage,
      componentProps: {
        'mode': mode,
        'vin': this.selectedVin
      }
    });
    return await modal.present();
  }

  copyLinkClip() {
    const latlng = storageVariable.upDatedJsonData["liveDatas"][this.selectedVin.vin]
    var copyText = "http://www.google.com/maps/place/" + latlng.latitude + "," + latlng.longitude;
    this.clipboard.copy(copyText)
    this.commonService.presentToast("Link copied")
  }

  navigateToGoogle() {
    const latlng = storageVariable.upDatedJsonData["liveDatas"][this.selectedVin.vin]
    window.open("http://www.google.com/maps/place/" + latlng.latitude + "," + latlng.longitude)
    // window.open('geo://'+this.selectedVin.latitude+', '+this.selectedVin.longitude+'system');
  }

  shareLocation() {
    let company;
    if (app.appName == "Upcot-mvt") {
      company = "Upcot"
    } else {
      company = app.appName
    }
    var options = {
      message: company + ' - Location Share', // not supported on some apps (Facebook, Instagram)
      url: "http://www.google.com/maps/place/" + this.selectedVin.latitude + "," + this.selectedVin.longitude,
    };
    this.socialSharing.shareWithOptions(options);
  }
  ngOnInit() {
    // this.vin = this.vin
    this.selectedVin = this.cardData
    let gridCardOption = JSON.parse(JSON.parse(localStorage.getItem('loginData'))[1].cardmenu);
    if (gridCardOption[this.cardData.icon]) {
      this.vehicleActivity = gridCardOption[this.cardData.icon];
    } else {
      this.vehicleActivity = gridCardOption['ASSETS'];
    }
    console.log(this.cardData)
  }

}
