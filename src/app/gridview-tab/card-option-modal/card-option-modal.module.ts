import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

// import { CardOptionModalPage } from './card-option-modal.page';

// const routes: Routes = [
//   {
//     path: '',
//     component: CardOptionModalPage
//   }
// ];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // RouterModule.forChild(routes)
  ],
  declarations: []
})
export class CardOptionModalPageModule {}
