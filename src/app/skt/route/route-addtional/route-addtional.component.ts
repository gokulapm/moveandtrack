import { Component, OnInit,Input} from '@angular/core';
import { FormGroup,FormBuilder,FormControlName, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { AjaxService } from 'src/app/services/ajax.service';
import { CommonService } from 'src/app/services/common.service';
import { serverUrl } from 'src/environments/environment';

@Component({
  selector: 'app-route-addtional',
  templateUrl: './route-addtional.component.html',
  styleUrls: ['./route-addtional.component.scss'],
})
export class RouteAddtionalComponent implements OnInit {
  @Input() value;

  data:any;
  stopDetails:FormGroup;
  companyDetail: { branchID: string; companyID: string; userId: string; };
  url: string;
  serviceName: string;
  busstop:any;
  constructor(private modalController: ModalController,
  private formBuilder: FormBuilder, 
  private ajaxService:AjaxService,
  private commonService:CommonService

  ) { }
   async closeModal() {
     this.modalController.dismiss();
   }
    getStopNames(){

      var url = serverUrl.web + `/routetrip/getstopName?schoolId=${this.companyDetail.companyID}&branchId=${this.companyDetail.branchID}`;
      this.ajaxService.ajaxGetPerference(url)
        .subscribe(res => {
          this.busstop = res;
        })


    }
   submit(){
     if(this.serviceName != 'available'){
       var data ={
          "stopName":this.stopDetails.value.busStop,
          "arrivalTime":this.stopDetails.value.arrivalTime,
          "orderNo":"1",
          "lastUpdBy":this.companyDetail.userId,
          "SchoolId":this.companyDetail.companyID,
          "branchId":this.companyDetail.branchID,
}
        Object.keys(data).forEach((key) => (data[key] == null || data[key] == "") && delete data[key])
        data["latlng"]='';
        data["tripId"]=this.value;
        const url = serverUrl.web + "/routetrip/addBusstop"
        this.ajaxService.ajaxPostWithString(url,data).subscribe(res =>{
          if(res == '{"message":"Added Successfully"}'){
           this.commonService.presentToast(" Added successfully.")
           this.stopDetails.reset();
           this.modalController.dismiss();
          }
          else{
            this.commonService.presentToast("Please check the values.")
          }
        })
        
      }
     
      else if(this.serviceName == 'available'){
        var datas =
        {
          "stopPointId":this.value.stopPointId,
          "stopName":this.stopDetails.value.busStop,
          "arrivalTime":this.stopDetails.value.arrivalTime,
          "orderNo":"1",
          "latlng":"13.0103, 80.246",
          "lastUpdBy":"demo-ca",
          "schoolId":this.companyDetail.companyID,
          "branchId":this.companyDetail.branchID,
        }
       
        Object.keys(datas).forEach((key) => (datas[key] == null || datas[key] == "") && delete datas[key])
        const url = serverUrl.web + "/routetrip/updateBusstop"
        this.ajaxService.ajaxPostWithString(url,datas).subscribe(res =>{
          if(res == '{"message":"Updated Successfully"}'){
            this.commonService.presentToast('Updated successfully.');
            this.stopDetails.reset();
            this.modalController.dismiss();
          }
          else{
            this.commonService.presentToast('Try again later.')
          }
        })
      }
      
      
    }
    createForm(){
      this.stopDetails = this.formBuilder.group({
        busStop: ['', Validators.required],
      arrivalTime: ['00:00:00', Validators.required]
      });
     }
    editForm(){
      if (this.value) {
        if (this.value.submit == "available") {
          this.serviceName = "available";
        this.stopDetails.patchValue({
          busStop: this.value.stopName,
          arrivalTime:this.value.arrivalTime
          
           });
          }
        }
      }
   ngOnInit(){
     this.companyDetail = {
       branchID: localStorage.getItem('corpId'),
       companyID: localStorage.getItem('corpId'),
       userId: localStorage.getItem('userName')
     }
     this.getStopNames();
     this.createForm();
  this.editForm();
  }

}
