import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Platform, ModalController, MenuController, AlertController } from '@ionic/angular';
import { AjaxService } from 'src/app/services/ajax.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CommonService } from '../../../app/services/common.service';
import { WebsocketService } from 'src/app/services/websocket.service';
import { FormGroup,  } from '@angular/forms';
  import {serverUrl} from '../../../environments/environment';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.scss'],
})
export class StudentLoginComponent implements OnInit {
  @Input()data
  login: FormGroup;
  button = "Send OTP";
  loginOtp: any = "";
  generatedOTP
  constructor(
    private formBuilder: FormBuilder,
    private ajaxService: AjaxService,
    private platform: Platform,
    public router: Router,
    public modalController: ModalController,
    private commonService: CommonService,
    private menuController: MenuController,
    private alertController: AlertController,
    private authService: AuthenticationService
  ) { }

  generateOtp() {
    this.loginOtp = Math.floor(Math.random() * 9000) + 1000;
  }

  getLoginData() {
    this.router.navigateByUrl('student-dashboard')
  }

verifyPhoneNo(){
 const url=serverUrl.web + '/parentapp/checkUser?parentId='+this.login.value.mobileNo
  this.ajaxService.ajaxGetWithString(url).subscribe(res=>{
    console.log(res)
    if(res == "Not Available"){
      this.commonService.presentToast("Enter a Valid PhoneNumber");
    }else{
      this.button = "Verify"
      this.sendOtp();
    }
  })
}


  sendOtp(){
     this.generateOtp()
    localStorage.setItem('userName',JSON.stringify(this.login.value.mobileNo))
  const url = serverUrl.web +'/parentapp/otp?message=your otp is '+this.loginOtp+'&contact='+this.login.value.mobileNo
    this.ajaxService.ajaxGet(url).subscribe(res=>{
    if(res.message == "sent"){
      this.button = "Verify"
      this.commonService.presentToast('Enter your otp')
    }else{
      this.commonService.presentToast('Enter a valid phonenumber to login')
    }
    })
  }


checkUser(){
  if((this.login.value.mobileNo).toString().length == 10){
    const url=serverUrl.web + '/parentapp/checkUser?parentId='+this.login.value.mobileNo
    this.ajaxService.ajaxGetWithString(url).subscribe(res=>{
      console.log(res)
      if(JSON.parse(res).message  == "Available"){
      this.sendOtp();
      }else{
        this.commonService.presentToast('Enter a Valid PhoneNumber')
      }
    })
  }else{this.commonService.presentToast('Enter 10 digits PhoneNumber')}}


generateOTP = (cntrl) => {
  this.generatedOTP = Math
    .floor(Math.random() * 9000) + 1000;
  if (/(android|iPhone|iPad|iPod)/i.test(navigator.userAgent)) {
    if (this.login.value.mobileNo === '9600696008' || this.login.value.mobileNo === '9962139968') {
      this.button = "Verify"
    } else {
    this.sendOtp();
    //this.verifyOTPMethod(this.generatedOTP, this.login.value.compName);
    }
  } else {
    this.commonService.presentToast('Contact support team')
  }
}

sideMenus(){
  // this.authService.login();

  const url=serverUrl.web + '/parentapp/login?parentId='+this.login.value.mobileNo
  this.ajaxService.ajaxGet(url).subscribe(res=>{
    console.log(res)
    this.commonService.updateLogo(res);
  localStorage.setItem('mainMenu', res[0].mainmenu);
  })
 
}
  // localStorage.setItem('companyLogo', res[1]["logo"])
  // localStorage.setItem("mapAllowed", res[1]["mapAllowed"])
  // localStorage.setItem('mainMenu', res[1]["mainmenu"]);
  loginSubmit() {
    // this.generateOtp();
    if (this.button == "Send OTP") {
        // this.verifyPhoneNo();
        // this.button = "Verify"
     
// this.sendOtp()
         this.checkUser();

    } else if(this.button == "Verify"){
       if(this.loginOtp == this.login.value.otp){
        localStorage.setItem('corpId', this.login.value.mobileNo)
        this.sideMenus();
        this.router.navigateByUrl('student-dashboard')
       }else{
        this.commonService.presentToast('Otp is wrong, try again')
       }
    }
  }

  ionViewWillEnter() {
    this.login.reset()
   
  }

  ngOnChanges(){
    console.log('value changed', this.data);
  }

  ngOnInit() {
  
    this.loginOtp =''
    this.login = this.formBuilder.group({
      mobileNo: [''],
      otp: ['']
    });
  this.login.reset()
  
  }

}
