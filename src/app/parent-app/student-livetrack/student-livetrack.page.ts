import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-livetrack',
  templateUrl: './student-livetrack.page.html',
  styleUrls: ['./student-livetrack.page.scss'],
})
export class StudentLivetrackPage implements OnInit {

  constructor(
    private location: Location
  ) { }
  locationBack() {
    this.location.back()
  }

  ngOnInit() {
    
  }

}
