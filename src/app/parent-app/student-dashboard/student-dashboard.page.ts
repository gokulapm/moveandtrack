import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { serverUrl } from 'src/environments/environment';
import {AjaxService} from '../../services/ajax.service';
@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student-dashboard.page.html',
  styleUrls: ['./student-dashboard.page.scss'],
})
export class StudentDashboardPage implements OnInit {

  constructor(
    private menuController: MenuController,
    private router: Router,
    private ajaxService:AjaxService
  ) { }

//   studentsData={
//     studName:' ',
//     driverNo:'',
//     driverName:'',
//     routeName:'',
//     std:"",
//     sec:"",
//     routes:[{name:"",value:""},{name:"",value:""},
//     { name:" ",value:""}],
// }

studentsData=[{"sec":"B","std":"STD XII","routesDatas":[{"name":"Stared at Home","value":"current"}],
"studentName":"GOKUL","driverName":"demo","driverNo":"12345678910","routeName":"GT45"}]
  studentDetail(data){
    localStorage.setItem('selectedVin',JSON.stringify(data))
    
    this.router.navigateByUrl('/parent-tab/routmap')
  }

  getStudentDetails(){
    const url =serverUrl.web+  '/parentapp/dashboard?parentId='+localStorage.getItem('userName')
    this.ajaxService.ajaxGet(url).subscribe(res=>{
      console.log(res)
      this.studentsData = res;
// localStorage.setItem('selectedVin',JSON.stringify({vin:''}))
localStorage.setItem('appSettings',JSON.stringify("asas"));
localStorage.setItem('staticIOData',JSON.stringify({paramVin:''}));

    })
  }


 
  ngOnInit() {
    this.getStudentDetails();
    this.menuController.enable(true)
  }

}
