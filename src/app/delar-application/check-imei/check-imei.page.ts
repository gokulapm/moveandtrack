import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AjaxService } from '../../services/ajax.service';
import { CommonService } from '../../services/common.service';
import { serverUrl } from 'src/environments/environment';
import { Platform } from '@ionic/angular';

import { FormGroup,FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-check-imei',
  templateUrl: './check-imei.page.html',
  styleUrls: ['./check-imei.page.scss'],
})
export class CheckImeiPage implements OnInit {
  numberSearch:FormGroup;
  imeiMobileSearch = ['Imei Number','Mobile Number'];
  imeino:"";
  reportData: any;
  myPlatform: any;
  holder = "Enter the Imei Number ";
  maxNum = 15;
  data: any;
  disabled= true;
  mobileNumber:any;

  constructor(
    private commonService: CommonService,
    private ajaxService: AjaxService,
    private location : Location,
    public platform: Platform,
    private formBuilder:FormBuilder
  ) { }
  
  closePage() {
    this.location.back()
  }


  submit(){
  if(this.numberSearch.value.commonNumber.length  == 15){  
      const url = serverUrl.web + '/global/searchimei?imeiNo='+ this.numberSearch.value.commonNumber;
      this.ajaxService.ajaxGet(url).subscribe(res => {
        this.reportData=res;
        this.numberSearch.reset();
        if(res.length == 0){
        this.commonService.presentToast("Imei not available")
        }
      })
    }else{
      const url2 = serverUrl.web + '/global/searchcontact?contactNo='+ this.numberSearch.value.commonNumber;
      this.ajaxService.ajaxGet(url2).subscribe(res => {
        if(res.contact == '-'){
          this.commonService.presentToast("Mobile number not available")
          this.mobileNumber = ''
        }
        else {
          this.mobileNumber = res;
        }
        this.numberSearch.reset();
      })
      }
      }

   getImeiMobileNumber(data){
   this.numberSearch.reset();
   if(data.target.value == 'Imei Number'){
    this.holder="Enter the Imei Number ";
     this.maxNum = 15;
     this.mobileNumber = '';
     }
   else if (data.target.value == 'Mobile Number'){
    this.holder="Enter the Mobile Number ";
    this.maxNum = 10;
   this.reportData = '';
  }
  }
      
      ngOnInit() {
    this.myPlatform = this.platform.platforms()[0];
    if (this.myPlatform == 'tablet') {
      this.myPlatform = 'desktop';
    }
    this.numberSearch = this.formBuilder.group({
      commonNumber :['',Validators.required]
    })
    }
}
