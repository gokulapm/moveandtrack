import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { PipesModule } from 'pipes-module';
import { IonicModule } from '@ionic/angular';
import { SearchFilterService } from '../../services/search-filter.service';
import { DashboardPageRoutingModule } from './dashboard-routing.module';
import { DashboardPage } from './dashboard.page';
// import { AddImeiCompanyPageModule } from './add-imei-company/add-imei-company.module';
// import { AddCompanyVehiclePage } from '../company-vehicle/add-company-vehicle/add-company-vehicle.page';
import { Routes, RouterModule } from '@angular/router';
import { AddImeiCompanyPage } from './add-imei-company/add-imei-company.page';
const addImeiCompanyPage: Routes = [
  {
    path: '',
    component: AddImeiCompanyPage
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // PipesModule,
    DashboardPageRoutingModule,
    RouterModule.forChild(addImeiCompanyPage)
    
  ],
  declarations: [DashboardPage, AddImeiCompanyPage, SearchFilterService] //SearchFilterService
})
export class DashboardPageModule {}
