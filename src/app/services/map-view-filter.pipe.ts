import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapViewFilter'
})
export class MapViewFilterPipe implements PipeTransform {

  transform(items: any[], terms: string, count: number): any[] {
    if (!items) return [];
    if (terms) {
        let data: Array<Object>;
        if (terms !== "All" && terms !== "Vin") {
          data = items.filter(it => {
            return it.status === terms;
          });
        } else if (terms === "All") {
          data = items;
        }

        localStorage.setItem('maxLength', data.length.toString());

        let data1: Array<Object> = [];
        for (let i = 0; i < count; i++) {
          if (data[i]) {
            data1.push(data[i]);
          } else {
            break;
          }
        }
        if (localStorage.gridDataLength !== data1.length.toString()) {
          localStorage.setItem('updateGridData', 'true');
        }
        localStorage.setItem('gridDataLength', data1.length.toString());
        localStorage.setItem('gridData', JSON.stringify(data1));

        return data1;
      
    }
  }

}
