import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-executive-summary-card',
  templateUrl: './executive-summary-card.component.html',
  styleUrls: ['./executive-summary-card.component.scss'],
})
export class ExecutiveSummaryCardComponent implements OnInit {
@Input() commonData: any;
@Input() searchTerm:any;
x;
  constructor(
    private commonService: CommonService
  ) { }
  ngOnChanges(){
  this.x = this.commonData;
  this.x["runningDuration"] = this.commonService.timeConverter(this.x.runningDuration, "display");
  this.x["stopDuration"] = this.commonService.timeConverter(this.x.stopDuration, "display");
  this.x["idleDuration"] = this.commonService.timeConverter(this.x.idleDuration, "display");
  this.x["towedDuration"] = this.commonService.timeConverter(this.x.towedDuration, "display");

}
  ngOnInit() {
    //console.log("hit"+this.commonData)
    this.x = this.commonData;
  }

}
