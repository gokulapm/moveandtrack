import { Component, OnInit } from '@angular/core';
import {  Platform, MenuController } from '@ionic/angular';
import { app } from 'src/environments/environment';

@Component({
  selector: 'app-expense-maintenance',
  templateUrl: './expense-maintenance.page.html',
  styleUrls: ['./expense-maintenance.page.scss'],
})
export class ExpenseMaintenancePage implements OnInit {
  myPlatform;
  isDeleteShow: any = false;
  appName: string;
  constructor(
    private platform:Platform,
  ) { }

  ngOnInit() {
    this.myPlatform = this.platform.platforms()[0];
    this.appName = app.appName;
    if(this.myPlatform == 'tablet'){
      this.myPlatform = 'desktop';
    }
    let localMainMenu = JSON.parse(localStorage.mainMenu)
    this.isDeleteShow = localMainMenu.includes("Delete")
  }

}
