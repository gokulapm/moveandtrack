(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["student-livetrack-student-livetrack-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/parent-app/student-livetrack/student-livetrack.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/parent-app/student-livetrack/student-livetrack.page.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class='header'>\n  <ion-toolbar class=\"header-style\">\n    <ion-row style=\"align-items: center;\">\n      <ion-icon (click)=\"locationBack()\" class=\"icon-size\" name=\"arrow-back\"></ion-icon>\n      <ion-label> Live Track </ion-label>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<app-vtslivetrack></app-vtslivetrack>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/parent-app/student-livetrack/student-livetrack.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/parent-app/student-livetrack/student-livetrack.module.ts ***!
  \**************************************************************************/
/*! exports provided: StudentLivetrackPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentLivetrackPageModule", function() { return StudentLivetrackPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _student_livetrack_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./student-livetrack.page */ "./src/app/parent-app/student-livetrack/student-livetrack.page.ts");








const routes = [
    {
        path: '',
        component: _student_livetrack_page__WEBPACK_IMPORTED_MODULE_7__["StudentLivetrackPage"]
    }
];
let StudentLivetrackPageModule = class StudentLivetrackPageModule {
};
StudentLivetrackPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_student_livetrack_page__WEBPACK_IMPORTED_MODULE_7__["StudentLivetrackPage"]]
    })
], StudentLivetrackPageModule);



/***/ }),

/***/ "./src/app/parent-app/student-livetrack/student-livetrack.page.scss":
/*!**************************************************************************!*\
  !*** ./src/app/parent-app/student-livetrack/student-livetrack.page.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-style {\n  --background: #1aba7e;\n  color: white;\n}\n\n.icon-size {\n  font-size: 18px;\n  padding: 9px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvcGFyZW50LWFwcC9zdHVkZW50LWxpdmV0cmFjay9zdHVkZW50LWxpdmV0cmFjay5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhcmVudC1hcHAvc3R1ZGVudC1saXZldHJhY2svc3R1ZGVudC1saXZldHJhY2sucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFyZW50LWFwcC9zdHVkZW50LWxpdmV0cmFjay9zdHVkZW50LWxpdmV0cmFjay5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLXN0eWxlIHtcbiAgICAtLWJhY2tncm91bmQ6ICMxYWJhN2U7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4uaWNvbi1zaXplIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZzogOXB4O1xufVxuIiwiLmhlYWRlci1zdHlsZSB7XG4gIC0tYmFja2dyb3VuZDogIzFhYmE3ZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaWNvbi1zaXplIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBwYWRkaW5nOiA5cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/parent-app/student-livetrack/student-livetrack.page.ts":
/*!************************************************************************!*\
  !*** ./src/app/parent-app/student-livetrack/student-livetrack.page.ts ***!
  \************************************************************************/
/*! exports provided: StudentLivetrackPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentLivetrackPage", function() { return StudentLivetrackPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



let StudentLivetrackPage = class StudentLivetrackPage {
    constructor(location) {
        this.location = location;
    }
    locationBack() {
        this.location.back();
    }
    ngOnInit() {
    }
};
StudentLivetrackPage.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"] }
];
StudentLivetrackPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-student-livetrack',
        template: __webpack_require__(/*! raw-loader!./student-livetrack.page.html */ "./node_modules/raw-loader/index.js!./src/app/parent-app/student-livetrack/student-livetrack.page.html"),
        styles: [__webpack_require__(/*! ./student-livetrack.page.scss */ "./src/app/parent-app/student-livetrack/student-livetrack.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]])
], StudentLivetrackPage);



/***/ })

}]);
//# sourceMappingURL=student-livetrack-student-livetrack-module-es2015.js.map