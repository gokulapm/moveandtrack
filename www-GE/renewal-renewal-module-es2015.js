(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["renewal-renewal-module"],{

/***/ "./src/app/delar-application/subscription/renewal/renewal-routing.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/delar-application/subscription/renewal/renewal-routing.module.ts ***!
  \**********************************************************************************/
/*! exports provided: RenewalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenewalPageRoutingModule", function() { return RenewalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _renewal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./renewal.page */ "./src/app/delar-application/subscription/renewal/renewal.page.ts");




const routes = [
    {
        path: '',
        component: _renewal_page__WEBPACK_IMPORTED_MODULE_3__["RenewalPage"]
    }
];
let RenewalPageRoutingModule = class RenewalPageRoutingModule {
};
RenewalPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RenewalPageRoutingModule);



/***/ }),

/***/ "./src/app/delar-application/subscription/renewal/renewal.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/delar-application/subscription/renewal/renewal.module.ts ***!
  \**************************************************************************/
/*! exports provided: RenewalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenewalPageModule", function() { return RenewalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _renewal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./renewal-routing.module */ "./src/app/delar-application/subscription/renewal/renewal-routing.module.ts");
/* harmony import */ var _renewal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./renewal.page */ "./src/app/delar-application/subscription/renewal/renewal.page.ts");







let RenewalPageModule = class RenewalPageModule {
};
RenewalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _renewal_routing_module__WEBPACK_IMPORTED_MODULE_5__["RenewalPageRoutingModule"]
        ],
        declarations: [_renewal_page__WEBPACK_IMPORTED_MODULE_6__["RenewalPage"]]
    })
], RenewalPageModule);



/***/ })

}]);
//# sourceMappingURL=renewal-renewal-module-es2015.js.map