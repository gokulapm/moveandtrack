(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["mapview-tab-mapview-tab-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/mapview-tab/mapview-tab.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/mapview-tab/mapview-tab.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar mode=\"md\" class=\"appHeader\" *ngIf=\"myPlatform !='desktop'\">\r\n    <ion-grid class=\"appHeaderTitle ion-padding\" style=\"padding-bottom:8px;\">\r\n      <ion-row *ngIf='myPlatform !=\"desktop\"' >\r\n        <ion-col  size=\"2\">\r\n          <ion-menu-button></ion-menu-button>\r\n        </ion-col>\r\n        <ion-col style=\"align-self: center;\">\r\n          <ion-searchbar *ngIf='search' size=\"10\"  showCancelButton=\"never\" [(ngModel)]=\"plateNo\"  (ionChange) =\"searchVehicle(plateNo)\" placeholder=\"Search\">\r\n          </ion-searchbar>\r\n        </ion-col>\r\n        <!-- <ion-col size=1 style=\"   font-size: 20px; align-self: center;\">\r\n          <ion-icon (click) =\"showOrHideSearch()\"  [name]=\"iconHeader\"></ion-icon>\r\n        </ion-col> -->\r\n      </ion-row>\r\n      <ion-row *ngIf='myPlatform ==\"desktop\"' >\r\n        <ion-col size='2'>\r\n          <ion-menu-button></ion-menu-button>\r\n      </ion-col>\r\n      <ion-col size='8.5' style='align-self: center;'>\r\n          <ion-row>\r\n              <ion-label> Map view </ion-label>\r\n          </ion-row>\r\n      </ion-col>\r\n      <!-- <ion-col size='1.5'>\r\n              <ion-row><ion-icon style='font-size: 25px;' ios=\"md-search\" (click) ='searchStatus()' md=\"md-search\"></ion-icon></ion-row>\r\n          </ion-col> -->\r\n      <ion-col size='1.5' style=\"padding: 0px;\">\r\n          <ion-row >\r\n              <ion-col size='3' style=\"align-self: center;\"> \r\n                 \r\n              </ion-col>\r\n             <ion-col *ngIf='myPlatform ==\"desktop\"' size='9'>\r\n              <ion-img [src]=\"app.logo\"></ion-img>\r\n             </ion-col>\r\n          </ion-row>\r\n      </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"myPlatform !='desktop'\">\r\n  <div class=\"map\" #mapElement></div>\r\n \r\n  <div id=\"popup\">\r\n    <div id=\"popup1\"></div>\r\n    <div id=\"popup2\"></div>\r\n  </div>\r\n\r\n   <ion-fab *ngIf='fabButton' (click)=\"switchMap()\" style=\"top: 1%;\" horizontal=\"end\" vertical=\"top\" slot=\"fixed\" edge>\r\n    <ion-fab-button size ='medium' color='light'>\r\n        <ion-icon  center name='map'></ion-icon>\r\n    </ion-fab-button>\r\n</ion-fab>\r\n\r\n  <div id=\"modal\" class=\"modal\">\r\n\r\n    <ion-grid style='padding:0px'>\r\n      <div *ngIf=\"markerInfo.show\">\r\n        <ion-card style=\"background:white\">\r\n          <ion-card-content style=\"padding:0px; justify-content: flex-end; background:white\">\r\n            <ion-row class=\"ion-align-items-right ion-justify-content-right\" style=\"justify-content: flex-end;\"\r\n              (click)=\"closeInfo()\">\r\n              <ion-icon name=\"backspace\"></ion-icon>\r\n            </ion-row>\r\n          </ion-card-content>\r\n          <ion-card-content style=\"padding:0px;\">\r\n          <app-gridview-card [commonData]='clickMarker'></app-gridview-card>\r\n          <ion-row (click)='showLocation(clickMarker)' class=\"address\">\r\n            <ion-col size=12 style=\"padding-top: 0px; padding-bottom: 0px;\">\r\n                <ion-row *ngIf=\"clickMarker.location\">\r\n                    {{clickMarker.location}}\r\n                </ion-row>\r\n                <ion-row *ngIf=\"!clickMarker.location\">\r\n                    <!-- <ion-spinner name=\"dots\"></ion-spinner> -->\r\n                    Click for location \r\n                </ion-row>\r\n                </ion-col>\r\n                </ion-row>\r\n    </ion-card-content>\r\n        </ion-card>\r\n      </div>\r\n    </ion-grid>\r\n  </div>\r\n</div>\r\n<div *ngIf=\"myPlatform == 'desktop'\">\r\n  <ion-row style=\"height: 35px;\">\r\n      <ion-col size=10.5 style=\"text-align: center;     background: white;\r\n          position: fixed;\r\n          z-index: 11;\r\n          height: 43px;\">\r\n          <!-- <span style=\"font-weight: bold;font-size: 28px;\">{{plateNum}}</span> -->\r\n\r\n      </ion-col>\r\n      <ion-col style=\"align-self: center; position: fixed;\r\n          right: 117px; z-index: 12;\" size=0.5>\r\n          <ion-row (click)=\"bellRouter()\" style=\"align-self: center;     float: right;\">\r\n              <ion-img src=\"assets/dashboard_background/bell.png\" style=\"width:22px; height:22px;\"></ion-img>\r\n              <ion-badge color=\"danger\"\r\n                  style=\"position: absolute; align-self: center;margin-left: -35px; font-size: 9px; left: 76px; color: white; font-weight: bold;  top: 0px;\">\r\n                  {{alertDataLength}}\r\n              </ion-badge>\r\n          </ion-row>\r\n      </ion-col>\r\n      <ion-col style=\"text-align: -webkit-center; position: fixed; right: 5px; z-index: 15;\" size=1>\r\n          <ion-img class=\"logo\" [src]=\"app.logo\"></ion-img>\r\n      </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row style=\"    margin-top: 7px;\">\r\n      <ion-col size=\"2.5\" style=\"   margin-top: 7px;max-width: 20%; margin: 0px;padding: 0px;\">\r\n          <ion-row>\r\n              <div style=\"position:absolute;top: 2%;left: 0px;font-size: 25px;z-index: 16;\"\r\n                  (click)=\"swipePrevious()\">\r\n                  <div class=\"arrowPrevious\">\r\n                  </div>\r\n              </div>\r\n              <div (click)=\"swipeNext()\">\r\n                  <div class=\"arrowNext\">\r\n                  </div>\r\n              </div>\r\n              <!---Important*****-->\r\n              <div class=\"emptyClearleft\">\r\n              </div>\r\n              <div class=\"emptyClear\">\r\n              </div>\r\n              <ion-slides [options]=\"slideOpts\" #mySlider\r\n                  style=\"text-align: -webkit-center;\r\n            position: fixed;left: 13px;z-index: 12;background: white;width: 240px;border-top:5px solid rgb(108, 42, 132)\">\r\n                  <ion-slide class=\"tooltip statusBar\" style=\"background-color:#989aa2 ; \"\r\n                      (click)=\"changeGridView({'type':'All', 'count': vehicleCount.Total})\">\r\n                      {{vehicleCount.Total}}\r\n                      <span class=\"tooltiptextinitial\">Total</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Running')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#1eb15d;\"\r\n                      (click)=\"changeGridView({'type':'Running', 'count': vehicleCount.Running})\">\r\n                      {{vehicleCount.Running}}\r\n                      <span class=\"tooltiptext\">Running</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Stop')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#FF0000 ;\"\r\n                      (click)=\"changeGridView({'type':'Stop', 'count': vehicleCount.Stop})\">\r\n                      {{vehicleCount.Stop}}\r\n                      <span class=\"tooltiptext\">Stop</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('HighTemp')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#FF0000 ;\"\r\n                      (click)=\"changeGridView({'type':'HighTemp', 'count': vehicleCount.HighTemp})\">\r\n                      {{vehicleCount.HighTemp}}\r\n                      <span class=\"tooltiptext\">High temperature</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Idle')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#1f5baa ;\"\r\n                      (click)=\"changeGridView({'type':'Idle', 'count': vehicleCount.Idle})\">\r\n                      {{vehicleCount.Idle}}\r\n                      <span class=\"tooltiptext\">Idle</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Geofence')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#1f5baa ;\"\r\n                      (click)=\"changeGridView({'type':'Geofence', 'count': vehicleCount.Geofence})\">\r\n                      {{vehicleCount.Geofence}}\r\n                      <span class=\"tooltiptext\">Geofence</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('PowerFail')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#724040 ;\"\r\n                      (click)=\"changeGridView({'type':'PowerFail', 'count': vehicleCount['PowerFail']})\">\r\n                      {{vehicleCount['PowerFail']}}\r\n                      <span class=\"tooltiptext\">Power fail</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('No Transmission')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#000000 ;\"\r\n                      (click)=\"changeGridView({'type':'No Transmission', 'count': vehicleCount['No Transmission']})\">\r\n                      {{vehicleCount['No Transmission']}}\r\n                      <span class=\"tooltiptext\">No Transmit</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Overspeed')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#f28918 ;\"\r\n                      (click)=\"changeGridView({'type':'Overspeed', 'count': vehicleCount.Overspeed})\">\r\n                      {{vehicleCount.Overspeed}}\r\n                      <span class=\"tooltiptext\">Overspeed</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('DoorOpen')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#FF851B ;\"\r\n                      (click)=\"changeGridView({'type':'DoorOpen', 'count': vehicleCount.DoorOpen})\">\r\n                      {{vehicleCount.DoorOpen}}\r\n                      <span class=\"tooltiptext\">Door open</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Towed')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#c200b8;\"\r\n                      (click)=\"changeGridView({'type':'Towed', 'count': vehicleCount.Towed})\">\r\n                      {{vehicleCount.Towed}}\r\n                      <span class=\"tooltiptext\">Towed</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Yet_to_transmit')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#7d410f;\"\r\n                      (click)=\"changeGridView({'type':'Yet_to_transmit', 'count': vehicleCount.Yet_to_transmit})\">\r\n                      {{vehicleCount.Yet_to_transmit}}\r\n                      <span class=\"tooltiptext\">Yet to transmit</span>\r\n                  </ion-slide>\r\n                  <ion-slide *ngIf=\"loginData.includes('Online')\" class=\"tooltip statusBar\"\r\n                      style=\"background-color:#00E1BC;\"\r\n                      (click)=\"changeGridView({'type':'Online', 'count': vehicleCount.Online})\">\r\n                      {{vehicleCount.Online}}\r\n                      <span class=\"tooltiptext\">Online</span>\r\n                  </ion-slide>\r\n              </ion-slides>\r\n          </ion-row>\r\n\r\n          <ion-row style=\"width: 20.9%;\r\n                  position: fixed;\r\n                  padding: 2px;\r\n                  z-index: 1;\r\n                  top: 86px;\r\n                  background: white;\">\r\n              <ion-col style=\"padding: 0px; \" size=10>\r\n                  <ion-searchbar [(ngModel)]=\"plateNo\"  (ionChange) =\"searchVehicle(plateNo)\"  placeholder=\"Search\">\r\n                  </ion-searchbar>\r\n              </ion-col>\r\n              <ion-col size=1 style=\"align-self: center;\r\n                      text-align: center; padding: 0px;\">\r\n                  <ion-icon style=\"font-size: 25px;\" (click)=\"openFilter()\" [name]=\"filterIcon\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col (click)=\"doRefresh($event)\" size=1 style=\"align-self: center;\r\n                      text-align: center; padding: 0px;\">\r\n                  <ion-icon style=\"\r\n                               font-size: 25px;\" name=\"md-refresh\"></ion-icon>\r\n              </ion-col>\r\n          </ion-row>\r\n          <ion-row style=\"width: 21%;\r\n                  position: fixed;\r\n                  z-index: 1;\r\n                  top: 129px;\r\n                  background: white;\" *ngIf=\"filterIcon == 'md-close'\">\r\n\r\n              <ion-col color=\"light\" (click)=\"filterPage('watchMode')\">\r\n                  <svg width=\"35\" height=\"35\" viewBox=\"0 0 800 800\"\r\n                      style=\"shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd\">\r\n                      <path attr.fill=\"{{watchFilter==true ? '#1d93ed' : 'black'}}\"\r\n                          d=\"M430 586.53c-38.76,0 -72.57,-8.35 -101.64,-30.26 -23.48,-17.69 -42.12,-38.46 -54.23,-65.66 -25.5,-57.32 -16.5,-119.47 21.29,-169.22 5.71,-7.52 14.31,-14.96 20.32,-20.98 3.66,-3.66 6.82,-5.64 11.1,-8.87 45.82,-34.58 114.25,-42.08 166.23,-15.87 26.01,13.11 22.96,12.83 41.25,28.02 2.68,2.23 3.36,2.15 5.71,4.95 21.61,25.86 34.09,41.43 42.37,77.52 6.04,26.29 6.13,44.87 0.72,71.06 -5.44,26.3 -15.66,47.2 -32.06,67.85 -3.66,4.61 -6.09,6.85 -9.91,11.41 -4.97,5.94 -14.96,13.85 -21.32,18.65 -8.52,6.42 -15.1,9.99 -24.99,14.97 -16.11,8.1 -41.32,16.43 -64.84,16.43zm-99.92 -203.83c6.3,3.33 13.53,5.52 20.45,8.87 6.9,3.34 13.46,5.7 19.95,9.35l38.2 21.74c-7.98,10.9 -69.47,36.48 -78.6,41.3 4.87,20.89 32.99,45.12 56.14,54.44 25.57,10.31 56.37,9.23 80.65,-2.91 50.26,-25.13 71.85,-78.41 50.51,-132.25 -9.01,-22.73 -28.38,-40.94 -49.62,-51.62 -42.27,-21.25 -92.62,-9.51 -123.98,27.48 -3.87,4.57 -12.27,17.49 -13.7,23.6zm-247.79 41.3l14.63 18.67c4.77,6.26 10.17,11.51 15.24,18.07 22.98,29.66 86.65,83.79 113.99,101.83l53.31 30.62c34.19,17.15 88.84,34.64 131.89,34.64 55.33,0 83.37,-4.63 132.85,-24.35 23.09,-9.21 55.22,-26.56 75.53,-40.37l47.71 -36.22c28.49,-25.39 34.46,-31.2 58.63,-57.28l30.51 -36.1c2.23,-2.99 5.13,-7.56 7.8,-9.51 -2.05,-3.07 -4.1,-6.53 -6.46,-9.53 -15.48,-19.68 -54.17,-64.69 -72.14,-78.4l-36.15 -30.46c-6.52,-5.21 -11.83,-9.35 -19,-14.3 -22.47,-15.51 -37.38,-24.8 -62.53,-37.39 -34.95,-17.51 -88.82,-35.08 -132.77,-35.08 -55.33,0 -83.37,4.62 -132.86,24.34 -8.3,3.32 -15.62,6.58 -23.01,10.29 -32.56,16.33 -73.09,42.11 -100.23,66.3 -43.92,39.15 -55.7,51.86 -89.88,93.97 -2.86,3.52 -4.72,6.76 -7.06,10.26z\" />\r\n                  </svg>\r\n              </ion-col>\r\n              <ion-col color=\"light\" (click)=\"filterPage('battery')\" style=\"align-self: center;\">\r\n                  <svg width=\"25\" height=\"25\" viewBox=\"0 0 800 800\"\r\n                      style=\"shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd\">\r\n                      <path attr.fill=\"{{batteryStatusFilter == true ? '#1d93ed' : 'black'}}\"\r\n                          d=\"M-0 248.48l0 486.31 846.66 -0.01 0 -486.3 -846.66 0zm87.77 -55.22l205.61 0 0 -81.39 -205.61 0 0 81.39zm465.53 0l205.61 0 0 -81.39 -205.61 0 0 81.39zm-297.64 259.53l0 0 -37.49 0 0 37.48 -55.21 0.01 0 -37.48 -37.49 -0.01 0 -55.22 37.49 0 0 -37.48 55.21 0 0 37.48 37.49 0 0 55.22zm193.28 199.43l-51.22 -20.62 37.76 -93.8 -106.05 0 68.29 -169.63 51.22 20.62 -37.76 93.8 106.05 -0.01 -68.29 169.64zm272.25 -199.43l-130.19 0 0 -55.22 130.19 0 0 55.22z\" />\r\n                  </svg>\r\n              </ion-col>\r\n              <ion-col color=\"light\" (click)=\"filterPage('gsm')\" style=\"align-self: center;\">\r\n                  <svg xmlns=\"http://www.w3.org/2000/svg\" xml:space=\"preserve\" width=\"7mm\" height=\"7mm\"\r\n                      version=\"1.1\"\r\n                      style=\"shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd\"\r\n                      viewBox=\"0 0 846.66 846.66\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n                      <path attr.fill=\"{{gsmStatusFilter == true ? '#1d93ed' : 'black'}}\"\r\n                          d=\"M273.84 700.03c0,10.28 3.11,13.39 13.38,13.39l124.95 0 0 -98.17 -138.33 0 0 84.78zm182.96 -232.05l0 232.05c0,10.28 3.1,13.39 13.39,13.39l124.95 0 0 -258.82 -124.95 0c-10.29,0 -13.39,3.1 -13.39,13.38zm-254.36 245.44l40.16 0 0 -374.85c0,-24.95 99.44,-157.07 124.95,-205.27l-298.98 0 104.46 163.29c21.32,30.97 16.17,25.19 16.03,73.22l-0.39 312.76c-0.13,16.63 -3.44,30.85 13.77,30.85zm21.17 -403.47l-111.3 -159.72 203.51 0 -92.21 159.72zm420.61 403.47l133.87 0 0 -401.62c0,-10.29 -3.1,-13.39 -13.37,-13.39l-120.5 0 0 415.01z\" />\r\n                  </svg>\r\n              </ion-col>\r\n              <ion-col color=\"light\" (click)=\"filterPage('gps')\" style=\"align-self: center;\">\r\n                  <ion-icon style='font-size: 25px;'\r\n                      [ngClass]=\"{selecedFabIcon : gpsFilter === true, notSelecedFabIcon : gpsFilter === false}\"\r\n                      name=\"md-locate\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col color=\"light\" (click)=\"filterPage('subscription')\" style=\"align-self: center;\">\r\n                  <ion-icon style='font-size: 25px;'\r\n                      [ngClass]=\"{selecedFabIcon : subscriptionFilter === true, notSelecedFabIcon : subscriptionFilter === false}\"\r\n                      name=\"logo-usd\"></ion-icon>\r\n              </ion-col>\r\n\r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-virtual-scroll style=\"top: 88px;\"\r\n                  [items]=\"dashboardData | mapViewSearch:plateNo |mapViewFilter: filterValue : count\"\r\n                  [trackBy]=\"identify\"  [itemHeight]=\"itemHeightFn\"\r\n                 >\r\n                  <ion-card *virtualItem=\"let data;\" style=\"padding:0px; margin: 10px 3px 12px 2px;\"\r\n                      disabled='{{data.warrantyExpiry}}'\r\n                      [ngClass]='{\"runningCardBorder\": data.status === \"Running\" || data.status === \"Good\", \"stopCardBorder\":data.status === \"Stop\" || data.status === \"HighTemp\", \"idleCardBorder\":data.status === \"Idle\" || data.status === \"Geofence\" ,\"doorOpen\": data.status === \"DoorOpen\", \"noTransCardBorder\":data.status === \"No Transmission\",\"powerFail\": data.status === \"PowerFail\" , \"overspeedCardBorder\": data.status === \"Overspeed\" , \"towedCardBorder\": data.status === \"Towed\",\"yetToTransmitCardBorder\": data.status === \"Yet_to_transmit\", \"onlineCardBorder\": data.status === \"Online\", \"active\": selectedArrayVin.includes(data.vin)}'>\r\n                      <ion-card-content style=\"padding:0px;\">\r\n                          <app-gridview-card (click)=\"showModal(data)\" [commonData]='data'></app-gridview-card>\r\n\r\n                          <ion-row>\r\n\r\n                              <ion-row (click)='showLocation(data)' class=\"address\">\r\n                                  <ion-col size=12 style=\"padding-top: 0px; padding-bottom: 0px;\">\r\n                                      <ion-row *ngIf=\"data.location\">\r\n                                          {{data.location}}\r\n                                      </ion-row>\r\n                                      <ion-row *ngIf=\"!data.location\">\r\n                                          Click For Location\r\n                                      </ion-row>\r\n                                  </ion-col>\r\n                              </ion-row>\r\n                          </ion-row>\r\n                      </ion-card-content>\r\n\r\n                  </ion-card>\r\n              </ion-virtual-scroll>\r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadMoreData($event)\">\r\n                  <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\r\n                  </ion-infinite-scroll-content>\r\n              </ion-infinite-scroll>\r\n          </ion-row>\r\n\r\n\r\n      </ion-col>\r\n\r\n     \r\n\r\n      <ion-col style=\"max-width: 80%; overflow-y: hidden; height: 100%;\r\n              position: fixed; max-width: 80%;left: 20.8%;\" size=\"9.4\" id=\"mapv\">\r\n                <ion-fab (click)=\"filterRefreshMap()\" style=\"top: 5%;\" vertical=\"top\" horizontal=\"end\" >\r\n                    <ion-fab-button size=\"small\" color=\"light\">\r\n                      <ion-icon name=\"refresh\"></ion-icon>\r\n                    </ion-fab-button>\r\n                  </ion-fab>\r\n                  <!-- <ion-fab (click)=\"filterDesktopModal()\" style=\"top: 12%;\" vertical=\"top\" horizontal=\"end\" >\r\n                    <ion-fab-button size=\"small\" color=\"light\">\r\n                      <ion-icon *ngIf=\"!desktopFilterModal\" name=\"funnel\"></ion-icon>\r\n                      <ion-icon *ngIf=\"desktopFilterModal\" name=\"close\"></ion-icon>\r\n                    </ion-fab-button>\r\n                  </ion-fab> -->\r\n                  <!-- //top19 -->\r\n                  <ion-fab  (click)=\"switchMap()\" style=\"top: 12%;\" horizontal=\"end\" vertical=\"top\" >\r\n                    <ion-fab-button size ='small' color='light'>\r\n                        <ion-icon  center name='map'></ion-icon>\r\n                    </ion-fab-button>\r\n                </ion-fab>\r\n                  <!-- <div style=\"    top: 11%; position: absolute; right: 2%;  background-color: white;\">\r\n                    <ion-item >\r\n                        <ion-label>Plate no</ion-label>\r\n                        <ion-select placeholder=\"Select One\">\r\n                          <ion-select-option value=\"f\">Female</ion-select-option>\r\n                          <ion-select-option value=\"m\">Male</ion-select-option>\r\n                        </ion-select>\r\n                      </ion-item> \r\n                  </div> -->\r\n              <ion-row class=\"heading\">\r\n                <span class=\"margin\">Overall map view</span>\r\n            </ion-row>\r\n            <ion-row style=\"height: 100%;\">\r\n           <div class=\"map\" #mapElement></div>\r\n          </ion-row>\r\n\r\n      </ion-col>\r\n  </ion-row>\r\n</div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/mapview-tab/mapview-tab.module.ts":
/*!***************************************************!*\
  !*** ./src/app/mapview-tab/mapview-tab.module.ts ***!
  \***************************************************/
/*! exports provided: MapviewTabPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapviewTabPageModule", function() { return MapviewTabPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _mapview_tab_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mapview-tab.page */ "./src/app/mapview-tab/mapview-tab.page.ts");
/* harmony import */ var _services_map_view_filter_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/map-view-filter.pipe */ "./src/app/services/map-view-filter.pipe.ts");
/* harmony import */ var _services_map_view_search_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/map-view-search.pipe */ "./src/app/services/map-view-search.pipe.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");










var routes = [
    {
        path: '',
        component: _mapview_tab_page__WEBPACK_IMPORTED_MODULE_6__["MapviewTabPage"]
    }
];
var MapviewTabPageModule = /** @class */ (function () {
    function MapviewTabPageModule() {
    }
    MapviewTabPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _components_components_module__WEBPACK_IMPORTED_MODULE_9__["ComponentsModule"]
            ],
            declarations: [_mapview_tab_page__WEBPACK_IMPORTED_MODULE_6__["MapviewTabPage"], _services_map_view_filter_pipe__WEBPACK_IMPORTED_MODULE_7__["MapViewFilterPipe"], _services_map_view_search_pipe__WEBPACK_IMPORTED_MODULE_8__["MapViewSearchPipe"]]
        })
    ], MapviewTabPageModule);
    return MapviewTabPageModule;
}());



/***/ }),

/***/ "./src/app/mapview-tab/mapview-tab.page.scss":
/*!***************************************************!*\
  !*** ./src/app/mapview-tab/mapview-tab.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\n.address {\n  font-size: 10px;\n}\n#popup {\n  opacity: 1;\n  margin-left: 0px;\n  height: 38px;\n  font-size: 85%;\n  background-color: white;\n}\n.overlay {\n  background: white;\n  color: #494848;\n}\n.modal {\n  z-index: 12;\n  position: fixed;\n  bottom: 0px;\n  right: 0px;\n  width: 100%;\n  -webkit-transition: 0.5s;\n  transition: 0.5s;\n}\n.modalIos {\n  z-index: 12;\n  position: fixed;\n  bottom: 50px;\n  right: 0px;\n  width: 100%;\n  -webkit-transition: 0.5s;\n  transition: 0.5s;\n}\n.ion-padding,\n[padding] {\n  padding-left: unset;\n  padding-right: unset;\n  -webkit-padding-start: var(--ion-padding, 16px);\n  padding-inline-start: var(--ion-padding, 0px);\n  -webkit-padding-end: var(--ion-padding, -16px);\n  padding-inline-end: var(--ion-padding, 13px);\n  padding-bottom: 8px;\n}\n#gridView {\n  height: 124vh !important;\n  overflow-y: scroll;\n}\n#alert {\n  height: 53vh !important;\n}\n.active {\n  --background: rgb(211, 211, 211);\n}\n.botmRow {\n  position: fixed;\n  left: 21%;\n  bottom: 0px;\n  right: 0;\n}\n#vehicleInfo {\n  height: 53vh !important;\n}\n#yesterdayStatus {\n  height: 33vh !important;\n}\n#executiveSummary {\n  height: 33vh !important;\n}\n.executiveGreater {\n  overflow-y: scroll;\n  height: 30vh;\n}\n#odometer {\n  height: 33vh !important;\n}\n.address {\n  font-size: 10px;\n}\n.statusBar {\n  font-size: 14px;\n  color: white;\n  font-weight: bold;\n  -webkit-box-align: center;\n  align-items: center;\n  height: 29px;\n  width: 29px;\n  border-radius: 54%;\n  box-shadow: 0 0 0 3px #00000024;\n  place-content: center;\n}\n.gridviewMargin {\n  margin-top: 8px;\n}\n.livetrackMargin {\n  margin-top: 85px;\n}\n.cardStyle {\n  height: 342px;\n  margin: 0px;\n}\n.blockDisplay {\n  display: block;\n}\n.noneDisplay {\n  display: none;\n}\n.statusLabel {\n  font-size: 11px;\n  text-align: center;\n}\n.filterPad {\n  padding: 5px 1px;\n}\n.filterPad .ion-activatable .grayBackground {\n  background-color: #cacaca;\n}\n.filter {\n  position: fixed;\n  width: 100%;\n  z-index: 12;\n}\n.searchBackground {\n  background: #32384b;\n}\n.card {\n  top: 10vh;\n}\n.modal {\n  background-color: white;\n  color: #202020;\n  z-index: 12;\n  position: fixed;\n  right: 0px;\n  width: 100%;\n  -webkit-transition: 0.5s;\n  transition: 0.5s;\n  border-top-left-radius: 40px;\n  border-top-right-radius: 40px;\n}\n.dragDown {\n  -webkit-transition: 0.5s;\n  transition: 0.5s;\n  bottom: -500px;\n  display: none;\n}\n.dragUp {\n  -webkit-transition: 0.5s;\n  transition: 0.5s;\n  bottom: 0px;\n}\n.dragUpIos {\n  -webkit-transition: 0.5s;\n  transition: 0.5s;\n  bottom: 50px;\n}\n.displayType {\n  display: grid;\n}\n.semiCircle {\n  background: black;\n  width: 100%;\n  padding: 2%;\n  border-radius: 0px 0px 20px 18px;\n}\n.alertsStyle {\n  color: white;\n  font-size: 16px;\n  font-weight: 700;\n}\n.borderRight {\n  border-right: 1px solid #dedede;\n}\n.imgSize {\n  height: 30px;\n}\n.cardContentPad {\n  padding-top: 6px;\n  padding-bottom: 0px;\n}\n.cardMargin {\n  margin-top: 6px;\n  margin-bottom: 6px;\n  border-radius: 10px;\n}\n.running {\n  background-color: #1eb15d;\n}\n.towed {\n  background-color: #c200b8;\n}\n.yettotransmit {\n  background-color: #7d410f;\n}\n.online {\n  background-color: #00e1bc;\n}\n.idle {\n  background-color: #1f5baa;\n}\n.stop {\n  background-color: #ff0000;\n}\n.overspeed {\n  background-color: #f28918;\n}\n.notransmission {\n  background-color: #000000;\n}\n.all {\n  background-color: #989aa2;\n}\n/*******New Design ****/\n/*******Side Border****/\n.runningCardBorder {\n  border-left: 6px solid #1eb15d;\n}\n.towedCardBorder {\n  border-left: 6px solid #c200b8;\n}\n.yetToTransmitCardBorder {\n  border-left: 6px solid #7d410f;\n}\n.onlineCardBorder {\n  border-left: 6px solid #00e1bc;\n}\n.runningCardBorder {\n  border-left: 6px solid #1eb15d;\n}\n.idleCardBorder {\n  border-left: 6px solid #1f5baa;\n}\n.noTransCardBorder {\n  border-left: 6px solid #000000;\n}\n.powerFail {\n  border-left: 6px solid #724040;\n}\n.overspeedCardBorder {\n  border-left: 6px solid #f28918;\n}\n.stopCardBorder {\n  border-left: 6px solid red;\n}\n.doorOpen {\n  border-left: 6px solid #ff851b;\n}\n/*******driver bg****/\n.runningDriverBg {\n  background-color: #1eb15d;\n}\n.idleDriverBg {\n  background-color: #1f5baa;\n}\n.noTransDriverBg {\n  background-color: #000000;\n}\n.overspeedDriverBg {\n  background-color: #f28918;\n}\n.stopDriverBg {\n  background-color: red;\n}\n/*******Address broder****/\n.runningAddressBorder {\n  border: 2px solid #1eb15d;\n}\n.idleAddressBorder {\n  border: 2px solid #1f5baa;\n}\n.noTransAddressBorder {\n  border: 2px solid #000000;\n}\n.overspeedAddressBorder {\n  border: 2px solid #f28918;\n}\n.stopAddressBorder {\n  border: 2px solid red;\n}\n/*******text cooor****/\n.runningText {\n  color: #1eb15d;\n}\n.idleText {\n  color: #1f5baa;\n}\n.noTransText {\n  color: #000000;\n}\n.overspeedText {\n  color: #f28918;\n}\n.stopText {\n  color: red;\n}\n.selecedFabIcon {\n  color: #1d93ed;\n}\n.notSelecedFabIcon {\n  color: #000000;\n}\n.heading {\n  height: 22px;\n  background-color: #6c2a84;\n  color: white;\n  font-size: 12px;\n  font-weight: bold;\n  -webkit-box-pack: center;\n          justify-content: center;\n  width: 100%;\n  border-top-left-radius: 12px;\n  border-top-right-radius: 12px;\n  padding: 6px;\n}\n.map {\n  width: 100%;\n  height: 91.2%;\n}\n.cardDesign::-webkit-scrollbar {\n  width: 12px;\n  /* width of the entire scrollbar */\n}\n.cardDesign::-webkit-scrollbar-track {\n  background: white;\n  /* color of the tracking area */\n}\n.cardDesign::-webkit-scrollbar-thumb {\n  background-color: white;\n  /* color of the scroll thumb */\n  border-radius: 20px;\n  /* roundness of the scroll thumb */\n  border: 3px solid lightgray;\n  /* creates padding around scroll thumb */\n}\n.alertCardGreater {\n  height: 48vh;\n  overflow-y: scroll;\n}\n::-webkit-scrollbar {\n  width: 3px;\n}\n/* Track */\n::-webkit-scrollbar-track {\n  background: #ffffff;\n}\n/* Handle */\n::-webkit-scrollbar-thumb {\n  background: #cacaca;\n}\n/* Handle on hover */\n::-webkit-scrollbar-thumb:hover {\n  background: #cacaca;\n}\n.margin {\n  margin-top: -2px;\n}\ndiv.scroll {\n  margin: 4px, 4px;\n  padding: 4px;\n  background-color: green;\n  width: 500px;\n  height: 110px;\n  overflow-x: hidden;\n  overflow-y: auto;\n  text-align: justify;\n}\n.timediff {\n  text-align: center;\n  color: white;\n  position: relative;\n  font-size: 3vw;\n  bottom: 7px;\n}\n.col {\n  -webkit-box-flex: 1;\n  flex: 1;\n  display: block;\n  padding: 5px;\n  width: 100%;\n}\n.DashboardButtons {\n  color: #ffda44;\n  font-size: 3vw;\n  background-color: transparent;\n  text-align: center;\n}\n.imgbuttons {\n  width: 30%;\n  position: relative;\n}\n/*Start Scss*/\n.header-background-color {\n  --background: #f6b221;\n}\n.a.white {\n  color: white;\n}\n.a.red {\n  color: red;\n}\n.arrowNext::after {\n  content: \"\";\n  font: normal normal normal 17px/0 FontAwesome;\n  color: #6c2a84;\n  position: fixed;\n  top: 9.3%;\n  left: 20.1%;\n  font-size: 13px;\n  z-index: 33;\n}\n.arrowPrevious::after {\n  content: \"\";\n  font: normal normal normal 17px/0 FontAwesome;\n  color: #6c2a84;\n  right: 0px;\n  position: fixed;\n  top: 9.3%;\n  left: 0%;\n  font-size: 13px;\n}\n.emptyClear, .emptyClearleft {\n  height: 51px;\n  width: 23px;\n  display: block;\n  background-color: white;\n  position: fixed;\n  top: 5.8%;\n  left: 19.2%;\n  z-index: 1;\n}\n.emptyClearleft {\n  height: 70px;\n  left: 0%;\n}\n.statusBar {\n  font-size: 14px;\n  color: white;\n  font-weight: bold;\n  height: 29px;\n  width: 29px !important;\n  border-radius: 50%;\n  margin: 4px 12px 16px 7px;\n  box-shadow: 0 0 0 3px #00000024;\n  display: grid !important;\n  place-content: center;\n}\n.tooltip {\n  position: relative;\n  display: inline-block;\n}\n.tooltip .tooltiptextinitial {\n  visibility: hidden;\n  width: 120px;\n  color: black;\n  text-align: center;\n  border-radius: 6px;\n  padding: 13px 0;\n  bottom: 100%;\n  font-size: 10px;\n  position: absolute;\n  top: 20px;\n  left: -47px;\n  z-index: 1;\n}\n.tooltip .tooltiptext {\n  visibility: hidden;\n  width: 120px;\n  color: #1d1c1c;\n  text-align: center;\n  border-radius: 6px;\n  padding: 13px 0;\n  bottom: 100%;\n  font-size: 10px;\n  position: absolute;\n  top: 20px;\n  left: -45px;\n  z-index: 1;\n}\n.tooltip:hover .tooltiptext {\n  visibility: visible;\n}\n.tooltip:hover .tooltiptextinitial {\n  visibility: visible;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwdmlldy10YWIvbWFwdmlldy10YWIucGFnZS5zY3NzIiwiL1VzZXJzL3JhZmlxL0Rlc2t0b3AvQVRTIENhcGFjaXRvciBOZXcgYXBpIGJhY2tlbmQvc3JjL2FwcC9tYXB2aWV3LXRhYi9tYXB2aWV3LXRhYi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQ2hCO0VBQ0ksZUFBQTtBRENKO0FDRUE7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0FEQ0o7QUNDQTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtBREVKO0FDQUE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHdCQUFBO0VBQUEsZ0JBQUE7QURHSjtBQ0RBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtFQUFBLGdCQUFBO0FESUo7QUNGQTs7RUFFRSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsK0NBQUE7RUFDQSw2Q0FBQTtFQUNBLDhDQUFBO0VBQ0EsNENBQUE7RUFDQSxtQkFBQTtBREtGO0FDWUE7RUFDRSx3QkFBQTtFQUNBLGtCQUFBO0FEVEY7QUNXQTtFQUNFLHVCQUFBO0FEUkY7QUNjQTtFQUNFLGdDQUFBO0FEWEY7QUNjQTtFQUNFLGVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7QURYRjtBQ2FBO0VBQ0UsdUJBQUE7QURWRjtBQ1lBO0VBQ0UsdUJBQUE7QURURjtBQ1dBO0VBQ0UsdUJBQUE7QURSRjtBQ1VBO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0FEUEY7QUNTQTtFQUNFLHVCQUFBO0FETkY7QUNRQTtFQUNFLGVBQUE7QURMRjtBQ09BO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsK0JBQUE7RUFDQSxxQkFBQTtBREpGO0FDT0E7RUFDRSxlQUFBO0FESkY7QUNNQTtFQUNFLGdCQUFBO0FESEY7QUNLQTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FERkY7QUNJQTtFQUNFLGNBQUE7QURERjtBQ0dBO0VBQ0UsYUFBQTtBREFGO0FDRUE7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QURDRjtBQ0VBO0VBQ0UsZ0JBQUE7QURDRjtBQ0VBO0VBQ0UseUJBQUE7QURDRjtBQ0VBO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FEQ0Y7QUNFQTtFQUNFLG1CQUFBO0FEQ0Y7QUNFQTtFQUNFLFNBQUE7QURDRjtBQ0VBO0VBQ0UsdUJBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUVBLHdCQUFBO0VBQUEsZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FEQUY7QUNHQTtFQUNFLHdCQUFBO0VBQUEsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtBREFGO0FDR0E7RUFDRSx3QkFBQTtFQUFBLGdCQUFBO0VBQ0EsV0FBQTtBREFGO0FDRUE7RUFDRSx3QkFBQTtFQUFBLGdCQUFBO0VBQ0EsWUFBQTtBRENGO0FDQ0E7RUFDRSxhQUFBO0FERUY7QUNDQTtFQUNFLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxnQ0FBQTtBREVGO0FDQ0E7RUFDRSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FERUY7QUNDQTtFQUNFLCtCQUFBO0FERUY7QUNDQTtFQUNFLFlBQUE7QURFRjtBQ0NBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBREVGO0FDQ0E7RUFFRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBRENGO0FDRUE7RUFDRSx5QkFBQTtBRENGO0FDQ0E7RUFDRSx5QkF4S007QUQwS1I7QUNBQTtFQUNFLHlCQTFLVztBRDZLYjtBQ0RBO0VBQ0UseUJBNUtPO0FEZ0xUO0FDRkE7RUFDRSx5QkFBQTtBREtGO0FDSEE7RUFDRSx5QkFBQTtBRE1GO0FDSkE7RUFDRSx5QkFBQTtBRE9GO0FDTEE7RUFDRSx5QkFBQTtBRFFGO0FDTkE7RUFDRSx5QkFBQTtBRFNGO0FDUEEsdUJBQUE7QUFDQSx1QkFBQTtBQUNBO0VBQ0UsOEJBQUE7QURVRjtBQ1JBO0VBQ0UsOEJBQUE7QURXRjtBQ1RBO0VBQ0UsOEJBQUE7QURZRjtBQ1ZBO0VBQ0UsOEJBQUE7QURhRjtBQ1hBO0VBQ0UsOEJBQUE7QURjRjtBQ1pBO0VBQ0UsOEJBQUE7QURlRjtBQ2JBO0VBQ0UsOEJBQUE7QURnQkY7QUNkQTtFQUNFLDhCQUFBO0FEaUJGO0FDZEE7RUFDRSw4QkFBQTtBRGlCRjtBQ2ZBO0VBQ0UsMEJBQUE7QURrQkY7QUNoQkE7RUFDRSw4QkFBQTtBRG1CRjtBQ2pCQSxxQkFBQTtBQUNBO0VBQ0UseUJBM09RO0FEK1BWO0FDbEJBO0VBQ0UseUJBN09LO0FEa1FQO0FDbkJBO0VBQ0UseUJBOU9lO0FEb1FqQjtBQ3BCQTtFQUNFLHlCQWxQVTtBRHlRWjtBQ3JCQTtFQUNFLHFCQXhQSztBRGdSUDtBQ3JCQSwwQkFBQTtBQUNBO0VBQ0UseUJBQUE7QUR3QkY7QUN0QkE7RUFDRSx5QkFBQTtBRHlCRjtBQ3ZCQTtFQUNFLHlCQUFBO0FEMEJGO0FDeEJBO0VBQ0UseUJBQUE7QUQyQkY7QUN6QkE7RUFDRSxxQkFBQTtBRDRCRjtBQ3pCQSxzQkFBQTtBQUNBO0VBQ0UsY0E3UVE7QUR5U1Y7QUMxQkE7RUFDRSxjQS9RSztBRDRTUDtBQzNCQTtFQUNFLGNBaFJlO0FEOFNqQjtBQzVCQTtFQUNFLGNBcFJVO0FEbVRaO0FDN0JBO0VBQ0UsVUExUks7QUQwVFA7QUM5QkE7RUFDRSxjQUFBO0FEaUNGO0FDL0JBO0VBQ0UsY0FBQTtBRGtDRjtBQy9CQTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSxZQUFBO0FEa0NGO0FDaENBO0VBQ0UsV0FBQTtFQUNBLGFBQUE7QURtQ0Y7QUNqQ0E7RUFDRSxXQUFBO0VBQWEsa0NBQUE7QURxQ2Y7QUNuQ0E7RUFDRSxpQkFBQTtFQUFnQywrQkFBQTtBRHVDbEM7QUNyQ0E7RUFDRSx1QkFBQTtFQUF5Qiw4QkFBQTtFQUN6QixtQkFBQTtFQUFxQixrQ0FBQTtFQUNyQiwyQkFBQTtFQUFzQyx3Q0FBQTtBRDJDeEM7QUN4Q0E7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7QUQyQ0Y7QUN4Q0E7RUFDRSxVQUFBO0FEMkNGO0FDeENBLFVBQUE7QUFDQTtFQUNFLG1CQUFBO0FEMkNGO0FDeENBLFdBQUE7QUFDQTtFQUNFLG1CQUFBO0FEMkNGO0FDeENBLG9CQUFBO0FBQ0E7RUFDRSxtQkFBQTtBRDJDRjtBQ3ZDQTtFQUNFLGdCQUFBO0FEMENGO0FDeENBO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBRDJDRjtBQ3RDQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUR5Q0Y7QUN0Q0E7RUFDRSxtQkFBQTtFQUNBLE9BQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUR5Q0Y7QUN0Q0E7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7QUR5Q0Y7QUN0Q0E7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7QUR5Q0Y7QUN0Q0EsYUFBQTtBQUNBO0VBQ0UscUJBQUE7QUR5Q0Y7QUN0Q0E7RUFDRSxZQUFBO0FEeUNGO0FDdkNBO0VBQ0UsVUFBQTtBRDBDRjtBQ3hDQTtFQUNFLFlBQUE7RUFDQSw2Q0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBRDJDRjtBQ3pDQTtFQUNFLFlBQUE7RUFDQSw2Q0FBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsZUFBQTtBRDRDRjtBQzFDQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBRDZDRjtBQzNDQTtFQUNFLFlBQUE7RUFDQSxRQUFBO0FEOENGO0FDNUNBO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSwrQkFBQTtFQUNBLHdCQUFBO0VBQ0EscUJBQUE7QUQrQ0Y7QUM3Q0E7RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0FEZ0RGO0FDOUNBO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBRGlERjtBQy9DQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7QURrREY7QUMvQ0E7RUFDRSxtQkFBQTtBRGtERjtBQ2hEQTtFQUNFLG1CQUFBO0FEbURGIiwiZmlsZSI6InNyYy9hcHAvbWFwdmlldy10YWIvbWFwdmlldy10YWIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLmFkZHJlc3Mge1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbiNwb3B1cCB7XG4gIG9wYWNpdHk6IDE7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIGhlaWdodDogMzhweDtcbiAgZm9udC1zaXplOiA4NSU7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ub3ZlcmxheSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBjb2xvcjogIzQ5NDg0ODtcbn1cblxuLm1vZGFsIHtcbiAgei1pbmRleDogMTI7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICB0cmFuc2l0aW9uOiAwLjVzO1xufVxuXG4ubW9kYWxJb3Mge1xuICB6LWluZGV4OiAxMjtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDUwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICB0cmFuc2l0aW9uOiAwLjVzO1xufVxuXG4uaW9uLXBhZGRpbmcsXG5bcGFkZGluZ10ge1xuICBwYWRkaW5nLWxlZnQ6IHVuc2V0O1xuICBwYWRkaW5nLXJpZ2h0OiB1bnNldDtcbiAgLXdlYmtpdC1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiB2YXIoLS1pb24tcGFkZGluZywgMHB4KTtcbiAgLXdlYmtpdC1wYWRkaW5nLWVuZDogdmFyKC0taW9uLXBhZGRpbmcsIC0xNnB4KTtcbiAgcGFkZGluZy1pbmxpbmUtZW5kOiB2YXIoLS1pb24tcGFkZGluZywgMTNweCk7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XG59XG5cbiNncmlkVmlldyB7XG4gIGhlaWdodDogMTI0dmggIWltcG9ydGFudDtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xufVxuXG4jYWxlcnQge1xuICBoZWlnaHQ6IDUzdmggIWltcG9ydGFudDtcbn1cblxuLmFjdGl2ZSB7XG4gIC0tYmFja2dyb3VuZDogcmdiKDIxMSwgMjExLCAyMTEpO1xufVxuXG4uYm90bVJvdyB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbGVmdDogMjElO1xuICBib3R0b206IDBweDtcbiAgcmlnaHQ6IDA7XG59XG5cbiN2ZWhpY2xlSW5mbyB7XG4gIGhlaWdodDogNTN2aCAhaW1wb3J0YW50O1xufVxuXG4jeWVzdGVyZGF5U3RhdHVzIHtcbiAgaGVpZ2h0OiAzM3ZoICFpbXBvcnRhbnQ7XG59XG5cbiNleGVjdXRpdmVTdW1tYXJ5IHtcbiAgaGVpZ2h0OiAzM3ZoICFpbXBvcnRhbnQ7XG59XG5cbi5leGVjdXRpdmVHcmVhdGVyIHtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICBoZWlnaHQ6IDMwdmg7XG59XG5cbiNvZG9tZXRlciB7XG4gIGhlaWdodDogMzN2aCAhaW1wb3J0YW50O1xufVxuXG4uYWRkcmVzcyB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLnN0YXR1c0JhciB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgaGVpZ2h0OiAyOXB4O1xuICB3aWR0aDogMjlweDtcbiAgYm9yZGVyLXJhZGl1czogNTQlO1xuICBib3gtc2hhZG93OiAwIDAgMCAzcHggIzAwMDAwMDI0O1xuICBwbGFjZS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5ncmlkdmlld01hcmdpbiB7XG4gIG1hcmdpbi10b3A6IDhweDtcbn1cblxuLmxpdmV0cmFja01hcmdpbiB7XG4gIG1hcmdpbi10b3A6IDg1cHg7XG59XG5cbi5jYXJkU3R5bGUge1xuICBoZWlnaHQ6IDM0MnB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLmJsb2NrRGlzcGxheSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4ubm9uZURpc3BsYXkge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uc3RhdHVzTGFiZWwge1xuICBmb250LXNpemU6IDExcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZpbHRlclBhZCB7XG4gIHBhZGRpbmc6IDVweCAxcHg7XG59XG5cbi5maWx0ZXJQYWQgLmlvbi1hY3RpdmF0YWJsZSAuZ3JheUJhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2FjYWNhO1xufVxuXG4uZmlsdGVyIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogMTI7XG59XG5cbi5zZWFyY2hCYWNrZ3JvdW5kIHtcbiAgYmFja2dyb3VuZDogIzMyMzg0Yjtcbn1cblxuLmNhcmQge1xuICB0b3A6IDEwdmg7XG59XG5cbi5tb2RhbCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBjb2xvcjogIzIwMjAyMDtcbiAgei1pbmRleDogMTI7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgcmlnaHQ6IDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRyYW5zaXRpb246IDAuNXM7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDQwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA0MHB4O1xufVxuXG4uZHJhZ0Rvd24ge1xuICB0cmFuc2l0aW9uOiAwLjVzO1xuICBib3R0b206IC01MDBweDtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLmRyYWdVcCB7XG4gIHRyYW5zaXRpb246IDAuNXM7XG4gIGJvdHRvbTogMHB4O1xufVxuXG4uZHJhZ1VwSW9zIHtcbiAgdHJhbnNpdGlvbjogMC41cztcbiAgYm90dG9tOiA1MHB4O1xufVxuXG4uZGlzcGxheVR5cGUge1xuICBkaXNwbGF5OiBncmlkO1xufVxuXG4uc2VtaUNpcmNsZSB7XG4gIGJhY2tncm91bmQ6IGJsYWNrO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMiU7XG4gIGJvcmRlci1yYWRpdXM6IDBweCAwcHggMjBweCAxOHB4O1xufVxuXG4uYWxlcnRzU3R5bGUge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLmJvcmRlclJpZ2h0IHtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2RlZGVkZTtcbn1cblxuLmltZ1NpemUge1xuICBoZWlnaHQ6IDMwcHg7XG59XG5cbi5jYXJkQ29udGVudFBhZCB7XG4gIHBhZGRpbmctdG9wOiA2cHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbi5jYXJkTWFyZ2luIHtcbiAgbWFyZ2luLXRvcDogNnB4O1xuICBtYXJnaW4tYm90dG9tOiA2cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5ydW5uaW5nIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFlYjE1ZDtcbn1cblxuLnRvd2VkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyMDBiODtcbn1cblxuLnlldHRvdHJhbnNtaXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjN2Q0MTBmO1xufVxuXG4ub25saW5lIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwZTFiYztcbn1cblxuLmlkbGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWY1YmFhO1xufVxuXG4uc3RvcCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZjAwMDA7XG59XG5cbi5vdmVyc3BlZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjI4OTE4O1xufVxuXG4ubm90cmFuc21pc3Npb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xufVxuXG4uYWxsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzk4OWFhMjtcbn1cblxuLyoqKioqKipOZXcgRGVzaWduICoqKiovXG4vKioqKioqKlNpZGUgQm9yZGVyKioqKi9cbi5ydW5uaW5nQ2FyZEJvcmRlciB7XG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgIzFlYjE1ZDtcbn1cblxuLnRvd2VkQ2FyZEJvcmRlciB7XG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgI2MyMDBiODtcbn1cblxuLnlldFRvVHJhbnNtaXRDYXJkQm9yZGVyIHtcbiAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAjN2Q0MTBmO1xufVxuXG4ub25saW5lQ2FyZEJvcmRlciB7XG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgIzAwZTFiYztcbn1cblxuLnJ1bm5pbmdDYXJkQm9yZGVyIHtcbiAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAjMWViMTVkO1xufVxuXG4uaWRsZUNhcmRCb3JkZXIge1xuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICMxZjViYWE7XG59XG5cbi5ub1RyYW5zQ2FyZEJvcmRlciB7XG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgIzAwMDAwMDtcbn1cblxuLnBvd2VyRmFpbCB7XG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgIzcyNDA0MDtcbn1cblxuLm92ZXJzcGVlZENhcmRCb3JkZXIge1xuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICNmMjg5MTg7XG59XG5cbi5zdG9wQ2FyZEJvcmRlciB7XG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgcmVkO1xufVxuXG4uZG9vck9wZW4ge1xuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICNmZjg1MWI7XG59XG5cbi8qKioqKioqZHJpdmVyIGJnKioqKi9cbi5ydW5uaW5nRHJpdmVyQmcge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWViMTVkO1xufVxuXG4uaWRsZURyaXZlckJnIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFmNWJhYTtcbn1cblxuLm5vVHJhbnNEcml2ZXJCZyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG59XG5cbi5vdmVyc3BlZWREcml2ZXJCZyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMjg5MTg7XG59XG5cbi5zdG9wRHJpdmVyQmcge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG59XG5cbi8qKioqKioqQWRkcmVzcyBicm9kZXIqKioqL1xuLnJ1bm5pbmdBZGRyZXNzQm9yZGVyIHtcbiAgYm9yZGVyOiAycHggc29saWQgIzFlYjE1ZDtcbn1cblxuLmlkbGVBZGRyZXNzQm9yZGVyIHtcbiAgYm9yZGVyOiAycHggc29saWQgIzFmNWJhYTtcbn1cblxuLm5vVHJhbnNBZGRyZXNzQm9yZGVyIHtcbiAgYm9yZGVyOiAycHggc29saWQgIzAwMDAwMDtcbn1cblxuLm92ZXJzcGVlZEFkZHJlc3NCb3JkZXIge1xuICBib3JkZXI6IDJweCBzb2xpZCAjZjI4OTE4O1xufVxuXG4uc3RvcEFkZHJlc3NCb3JkZXIge1xuICBib3JkZXI6IDJweCBzb2xpZCByZWQ7XG59XG5cbi8qKioqKioqdGV4dCBjb29vcioqKiovXG4ucnVubmluZ1RleHQge1xuICBjb2xvcjogIzFlYjE1ZDtcbn1cblxuLmlkbGVUZXh0IHtcbiAgY29sb3I6ICMxZjViYWE7XG59XG5cbi5ub1RyYW5zVGV4dCB7XG4gIGNvbG9yOiAjMDAwMDAwO1xufVxuXG4ub3ZlcnNwZWVkVGV4dCB7XG4gIGNvbG9yOiAjZjI4OTE4O1xufVxuXG4uc3RvcFRleHQge1xuICBjb2xvcjogcmVkO1xufVxuXG4uc2VsZWNlZEZhYkljb24ge1xuICBjb2xvcjogIzFkOTNlZDtcbn1cblxuLm5vdFNlbGVjZWRGYWJJY29uIHtcbiAgY29sb3I6ICMwMDAwMDA7XG59XG5cbi5oZWFkaW5nIHtcbiAgaGVpZ2h0OiAyMnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNmMyYTg0O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTJweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEycHg7XG4gIHBhZGRpbmc6IDZweDtcbn1cblxuLm1hcCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDkxLjIlO1xufVxuXG4uY2FyZERlc2lnbjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgLyogd2lkdGggb2YgdGhlIGVudGlyZSBzY3JvbGxiYXIgKi9cbn1cblxuLmNhcmREZXNpZ246Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIC8qIGNvbG9yIG9mIHRoZSB0cmFja2luZyBhcmVhICovXG59XG5cbi5jYXJkRGVzaWduOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAvKiBjb2xvciBvZiB0aGUgc2Nyb2xsIHRodW1iICovXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIC8qIHJvdW5kbmVzcyBvZiB0aGUgc2Nyb2xsIHRodW1iICovXG4gIGJvcmRlcjogM3B4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgLyogY3JlYXRlcyBwYWRkaW5nIGFyb3VuZCBzY3JvbGwgdGh1bWIgKi9cbn1cblxuLmFsZXJ0Q2FyZEdyZWF0ZXIge1xuICBoZWlnaHQ6IDQ4dmg7XG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcbn1cblxuOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAzcHg7XG59XG5cbi8qIFRyYWNrICovXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLyogSGFuZGxlICovXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NhY2FjYTtcbn1cblxuLyogSGFuZGxlIG9uIGhvdmVyICovXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2NhY2FjYTtcbn1cblxuLm1hcmdpbiB7XG4gIG1hcmdpbi10b3A6IC0ycHg7XG59XG5cbmRpdi5zY3JvbGwge1xuICBtYXJnaW46IDRweCwgNHB4O1xuICBwYWRkaW5nOiA0cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xuICB3aWR0aDogNTAwcHg7XG4gIGhlaWdodDogMTEwcHg7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbn1cblxuLnRpbWVkaWZmIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZm9udC1zaXplOiAzdnc7XG4gIGJvdHRvbTogN3B4O1xufVxuXG4uY29sIHtcbiAgLXdlYmtpdC1ib3gtZmxleDogMTtcbiAgZmxleDogMTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDVweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5EYXNoYm9hcmRCdXR0b25zIHtcbiAgY29sb3I6ICNmZmRhNDQ7XG4gIGZvbnQtc2l6ZTogM3Z3O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaW1nYnV0dG9ucyB7XG4gIHdpZHRoOiAzMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLypTdGFydCBTY3NzKi9cbi5oZWFkZXItYmFja2dyb3VuZC1jb2xvciB7XG4gIC0tYmFja2dyb3VuZDogI2Y2YjIyMTtcbn1cblxuLmEud2hpdGUge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5hLnJlZCB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5hcnJvd05leHQ6OmFmdGVyIHtcbiAgY29udGVudDogXCLvgZRcIjtcbiAgZm9udDogbm9ybWFsIG5vcm1hbCBub3JtYWwgMTdweC8wIEZvbnRBd2Vzb21lO1xuICBjb2xvcjogIzZjMmE4NDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDkuMyU7XG4gIGxlZnQ6IDIwLjElO1xuICBmb250LXNpemU6IDEzcHg7XG4gIHotaW5kZXg6IDMzO1xufVxuXG4uYXJyb3dQcmV2aW91czo6YWZ0ZXIge1xuICBjb250ZW50OiBcIu+Bk1wiO1xuICBmb250OiBub3JtYWwgbm9ybWFsIG5vcm1hbCAxN3B4LzAgRm9udEF3ZXNvbWU7XG4gIGNvbG9yOiAjNmMyYTg0O1xuICByaWdodDogMHB4O1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogOS4zJTtcbiAgbGVmdDogMCU7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLmVtcHR5Q2xlYXIsIC5lbXB0eUNsZWFybGVmdCB7XG4gIGhlaWdodDogNTFweDtcbiAgd2lkdGg6IDIzcHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDUuOCU7XG4gIGxlZnQ6IDE5LjIlO1xuICB6LWluZGV4OiAxO1xufVxuXG4uZW1wdHlDbGVhcmxlZnQge1xuICBoZWlnaHQ6IDcwcHg7XG4gIGxlZnQ6IDAlO1xufVxuXG4uc3RhdHVzQmFyIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBoZWlnaHQ6IDI5cHg7XG4gIHdpZHRoOiAyOXB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbWFyZ2luOiA0cHggMTJweCAxNnB4IDdweDtcbiAgYm94LXNoYWRvdzogMCAwIDAgM3B4ICMwMDAwMDAyNDtcbiAgZGlzcGxheTogZ3JpZCAhaW1wb3J0YW50O1xuICBwbGFjZS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi50b29sdGlwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi50b29sdGlwIC50b29sdGlwdGV4dGluaXRpYWwge1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIHdpZHRoOiAxMjBweDtcbiAgY29sb3I6IGJsYWNrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgcGFkZGluZzogMTNweCAwO1xuICBib3R0b206IDEwMCU7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDIwcHg7XG4gIGxlZnQ6IC00N3B4O1xuICB6LWluZGV4OiAxO1xufVxuXG4udG9vbHRpcCAudG9vbHRpcHRleHQge1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIHdpZHRoOiAxMjBweDtcbiAgY29sb3I6ICMxZDFjMWM7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBwYWRkaW5nOiAxM3B4IDA7XG4gIGJvdHRvbTogMTAwJTtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMjBweDtcbiAgbGVmdDogLTQ1cHg7XG4gIHotaW5kZXg6IDE7XG59XG5cbi50b29sdGlwOmhvdmVyIC50b29sdGlwdGV4dCB7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG59XG5cbi50b29sdGlwOmhvdmVyIC50b29sdGlwdGV4dGluaXRpYWwge1xuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xufSIsIlxyXG4uYWRkcmVzc3tcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICB9XHJcbiAgXHJcbiNwb3B1cHtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgaGVpZ2h0OiAzOHB4O1xyXG4gICAgZm9udC1zaXplOiA4NSU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufVxyXG4ub3ZlcmxheSB7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgICBjb2xvcjogcmdiKDczLCA3MiwgNzIpO1xyXG59XHJcbi5tb2RhbCB7XHJcbiAgICB6LWluZGV4OiAxMjtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGJvdHRvbTogMHB4O1xyXG4gICAgcmlnaHQ6IDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgdHJhbnNpdGlvbjogMC41cztcclxufVxyXG4ubW9kYWxJb3N7XHJcbiAgICB6LWluZGV4OiAxMjtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGJvdHRvbTogNTBweDtcclxuICAgIHJpZ2h0OiAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRyYW5zaXRpb246IDAuNXM7XHJcbn1cclxuLmlvbi1wYWRkaW5nLFxyXG5bcGFkZGluZ10ge1xyXG4gIHBhZGRpbmctbGVmdDogdW5zZXQ7XHJcbiAgcGFkZGluZy1yaWdodDogdW5zZXQ7XHJcbiAgLXdlYmtpdC1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XHJcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAwcHgpO1xyXG4gIC13ZWJraXQtcGFkZGluZy1lbmQ6IHZhcigtLWlvbi1wYWRkaW5nLCAtMTZweCk7XHJcbiAgcGFkZGluZy1pbmxpbmUtZW5kOiB2YXIoLS1pb24tcGFkZGluZywgMTNweCk7XHJcbiAgcGFkZGluZy1ib3R0b206IDhweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuJHN0b3A6IHJlZDtcclxuJHJ1bm5pbmc6ICMxZWIxNWQ7XHJcbiRpZGxlOiAjMWY1YmFhO1xyXG4kb3ZlcnNwZWVkOiAjZjI4OTE4O1xyXG4kbm9UcmFuc21pc3Npb246ICMwMDAwMDA7XHJcbiRwb3dlckZhaWw6ICM3MjQwNDA7XHJcbiRkb29yT3BlbjogI2ZmODUxYjtcclxuJHRvd2VkOiAjYzIwMGI4O1xyXG4keWV0dG90cmFuczogIzdkNDEwZjtcclxuJG9ubGluZTogIzAwZTFiYztcclxuXHJcbiNncmlkVmlldyB7XHJcbiAgaGVpZ2h0OiAxMjR2aCAhaW1wb3J0YW50O1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxufVxyXG4jYWxlcnQge1xyXG4gIGhlaWdodDogNTN2aCAhaW1wb3J0YW50O1xyXG59XHJcbi8vICAgI21hcHZ7XHJcbi8vICAgICBoZWlnaHQ6IDMwdmggIWltcG9ydGFudDtcclxuLy8gICAgIH1cclxuXHJcbi5hY3RpdmUge1xyXG4gIC0tYmFja2dyb3VuZDogcmdiKDIxMSwgMjExLCAyMTEpO1xyXG59XHJcblxyXG4uYm90bVJvdyB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGxlZnQ6IDIxJTtcclxuICBib3R0b206IDBweDtcclxuICByaWdodDogMDtcclxufVxyXG4jdmVoaWNsZUluZm8ge1xyXG4gIGhlaWdodDogNTN2aCAhaW1wb3J0YW50O1xyXG59XHJcbiN5ZXN0ZXJkYXlTdGF0dXMge1xyXG4gIGhlaWdodDogMzN2aCAhaW1wb3J0YW50O1xyXG59XHJcbiNleGVjdXRpdmVTdW1tYXJ5IHtcclxuICBoZWlnaHQ6IDMzdmggIWltcG9ydGFudDtcclxufVxyXG4uZXhlY3V0aXZlR3JlYXRlciB7XHJcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gIGhlaWdodDogMzB2aDtcclxufVxyXG4jb2RvbWV0ZXIge1xyXG4gIGhlaWdodDogMzN2aCAhaW1wb3J0YW50O1xyXG59XHJcbi5hZGRyZXNzIHtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbn1cclxuLnN0YXR1c0JhciB7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgaGVpZ2h0OiAyOXB4O1xyXG4gIHdpZHRoOiAyOXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDU0JTtcclxuICBib3gtc2hhZG93OiAwIDAgMCAzcHggIzAwMDAwMDI0O1xyXG4gIHBsYWNlLWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmdyaWR2aWV3TWFyZ2luIHtcclxuICBtYXJnaW4tdG9wOiA4cHg7XHJcbn1cclxuLmxpdmV0cmFja01hcmdpbiB7XHJcbiAgbWFyZ2luLXRvcDogODVweDtcclxufVxyXG4uY2FyZFN0eWxlIHtcclxuICBoZWlnaHQ6IDM0MnB4O1xyXG4gIG1hcmdpbjogMHB4O1xyXG59XHJcbi5ibG9ja0Rpc3BsYXkge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbi5ub25lRGlzcGxheSB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uc3RhdHVzTGFiZWwge1xyXG4gIGZvbnQtc2l6ZTogMTFweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5maWx0ZXJQYWQge1xyXG4gIHBhZGRpbmc6IDVweCAxcHg7XHJcbn1cclxuXHJcbi5maWx0ZXJQYWQgLmlvbi1hY3RpdmF0YWJsZSAuZ3JheUJhY2tncm91bmQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjYWNhY2E7XHJcbn1cclxuXHJcbi5maWx0ZXIge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB3aWR0aDogMTAwJTtcclxuICB6LWluZGV4OiAxMjtcclxufVxyXG5cclxuLnNlYXJjaEJhY2tncm91bmQge1xyXG4gIGJhY2tncm91bmQ6ICMzMjM4NGI7XHJcbn1cclxuXHJcbi5jYXJkIHtcclxuICB0b3A6IDEwdmg7XHJcbn1cclxuXHJcbi5tb2RhbCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgY29sb3I6IHJnYigzMiwgMzIsIDMyKTtcclxuICB6LWluZGV4OiAxMjtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgcmlnaHQ6IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICAvL2JvcmRlci1ib3R0b206IDFweCBzb2xpZDtcclxuICB0cmFuc2l0aW9uOiAwLjVzO1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDQwcHg7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDQwcHg7XHJcbn1cclxuXHJcbi5kcmFnRG93biB7XHJcbiAgdHJhbnNpdGlvbjogMC41cztcclxuICBib3R0b206IC01MDBweDtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uZHJhZ1VwIHtcclxuICB0cmFuc2l0aW9uOiAwLjVzO1xyXG4gIGJvdHRvbTogMHB4O1xyXG59XHJcbi5kcmFnVXBJb3Mge1xyXG4gIHRyYW5zaXRpb246IDAuNXM7XHJcbiAgYm90dG9tOiA1MHB4O1xyXG59XHJcbi5kaXNwbGF5VHlwZSB7XHJcbiAgZGlzcGxheTogZ3JpZDtcclxufVxyXG5cclxuLnNlbWlDaXJjbGUge1xyXG4gIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBhZGRpbmc6IDIlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDBweCAwcHggMjBweCAxOHB4O1xyXG59XHJcblxyXG4uYWxlcnRzU3R5bGUge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLmJvcmRlclJpZ2h0IHtcclxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZGVkZWRlO1xyXG59XHJcblxyXG4uaW1nU2l6ZSB7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG59XHJcblxyXG4uY2FyZENvbnRlbnRQYWQge1xyXG4gIHBhZGRpbmctdG9wOiA2cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG5cclxuLmNhcmRNYXJnaW4ge1xyXG4gIC8vIGJvcmRlci1sZWZ0OiA2cHggc29saWQgJHN0b3A7XHJcbiAgbWFyZ2luLXRvcDogNnB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDZweDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4ucnVubmluZyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFlYjE1ZDtcclxufVxyXG4udG93ZWQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICR0b3dlZDtcclxufVxyXG4ueWV0dG90cmFuc21pdCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJHlldHRvdHJhbnM7XHJcbn1cclxuLm9ubGluZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJG9ubGluZTtcclxufVxyXG4uaWRsZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFmNWJhYTtcclxufVxyXG4uc3RvcCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmMDAwMDtcclxufVxyXG4ub3ZlcnNwZWVkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjI4OTE4O1xyXG59XHJcbi5ub3RyYW5zbWlzc2lvbiB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcclxufVxyXG4uYWxsIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTg5YWEyO1xyXG59XHJcbi8qKioqKioqTmV3IERlc2lnbiAqKioqL1xyXG4vKioqKioqKlNpZGUgQm9yZGVyKioqKi9cclxuLnJ1bm5pbmdDYXJkQm9yZGVyIHtcclxuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICRydW5uaW5nO1xyXG59XHJcbi50b3dlZENhcmRCb3JkZXIge1xyXG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgJHRvd2VkO1xyXG59XHJcbi55ZXRUb1RyYW5zbWl0Q2FyZEJvcmRlciB7XHJcbiAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAkeWV0dG90cmFucztcclxufVxyXG4ub25saW5lQ2FyZEJvcmRlciB7XHJcbiAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAkb25saW5lO1xyXG59XHJcbi5ydW5uaW5nQ2FyZEJvcmRlciB7XHJcbiAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAkcnVubmluZztcclxufVxyXG4uaWRsZUNhcmRCb3JkZXIge1xyXG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgJGlkbGU7XHJcbn1cclxuLm5vVHJhbnNDYXJkQm9yZGVyIHtcclxuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICRub1RyYW5zbWlzc2lvbjtcclxufVxyXG4ucG93ZXJGYWlsIHtcclxuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICRwb3dlckZhaWw7XHJcbiAgLy8gJHBvd2VyRmFpbDojNDEyNTI1O1xyXG59XHJcbi5vdmVyc3BlZWRDYXJkQm9yZGVyIHtcclxuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICRvdmVyc3BlZWQ7XHJcbn1cclxuLnN0b3BDYXJkQm9yZGVyIHtcclxuICBib3JkZXItbGVmdDogNnB4IHNvbGlkICRzdG9wO1xyXG59XHJcbi5kb29yT3BlbiB7XHJcbiAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAkZG9vck9wZW47XHJcbn1cclxuLyoqKioqKipkcml2ZXIgYmcqKioqL1xyXG4ucnVubmluZ0RyaXZlckJnIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkcnVubmluZztcclxufVxyXG4uaWRsZURyaXZlckJnIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkaWRsZTtcclxufVxyXG4ubm9UcmFuc0RyaXZlckJnIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkbm9UcmFuc21pc3Npb247XHJcbn1cclxuLm92ZXJzcGVlZERyaXZlckJnIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkb3ZlcnNwZWVkO1xyXG59XHJcbi5zdG9wRHJpdmVyQmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICRzdG9wO1xyXG59XHJcblxyXG4vKioqKioqKkFkZHJlc3MgYnJvZGVyKioqKi9cclxuLnJ1bm5pbmdBZGRyZXNzQm9yZGVyIHtcclxuICBib3JkZXI6IDJweCBzb2xpZCAkcnVubmluZztcclxufVxyXG4uaWRsZUFkZHJlc3NCb3JkZXIge1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICRpZGxlO1xyXG59XHJcbi5ub1RyYW5zQWRkcmVzc0JvcmRlciB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgJG5vVHJhbnNtaXNzaW9uO1xyXG59XHJcbi5vdmVyc3BlZWRBZGRyZXNzQm9yZGVyIHtcclxuICBib3JkZXI6IDJweCBzb2xpZCAkb3ZlcnNwZWVkO1xyXG59XHJcbi5zdG9wQWRkcmVzc0JvcmRlciB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgJHN0b3A7XHJcbn1cclxuXHJcbi8qKioqKioqdGV4dCBjb29vcioqKiovXHJcbi5ydW5uaW5nVGV4dCB7XHJcbiAgY29sb3I6ICRydW5uaW5nO1xyXG59XHJcbi5pZGxlVGV4dCB7XHJcbiAgY29sb3I6ICRpZGxlO1xyXG59XHJcbi5ub1RyYW5zVGV4dCB7XHJcbiAgY29sb3I6ICRub1RyYW5zbWlzc2lvbjtcclxufVxyXG4ub3ZlcnNwZWVkVGV4dCB7XHJcbiAgY29sb3I6ICRvdmVyc3BlZWQ7XHJcbn1cclxuLnN0b3BUZXh0IHtcclxuICBjb2xvcjogJHN0b3A7XHJcbn1cclxuLnNlbGVjZWRGYWJJY29uIHtcclxuICBjb2xvcjogIzFkOTNlZDtcclxufVxyXG4ubm90U2VsZWNlZEZhYkljb24ge1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG59XHJcblxyXG4uaGVhZGluZyB7XHJcbiAgaGVpZ2h0OiAyMnB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM2YzJhODQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxMnB4O1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMnB4O1xyXG4gIHBhZGRpbmc6IDZweDtcclxufVxyXG4ubWFwIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDkxLjIlO1xyXG59XHJcbi5jYXJkRGVzaWduOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7IC8qIHdpZHRoIG9mIHRoZSBlbnRpcmUgc2Nyb2xsYmFyICovXHJcbn1cclxuLmNhcmREZXNpZ246Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7IC8qIGNvbG9yIG9mIHRoZSB0cmFja2luZyBhcmVhICovXHJcbn1cclxuLmNhcmREZXNpZ246Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTsgLyogY29sb3Igb2YgdGhlIHNjcm9sbCB0aHVtYiAqL1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7IC8qIHJvdW5kbmVzcyBvZiB0aGUgc2Nyb2xsIHRodW1iICovXHJcbiAgYm9yZGVyOiAzcHggc29saWQgcmdiKDIxMSwgMjExLCAyMTEpOyAvKiBjcmVhdGVzIHBhZGRpbmcgYXJvdW5kIHNjcm9sbCB0aHVtYiAqL1xyXG59XHJcblxyXG4uYWxlcnRDYXJkR3JlYXRlciB7XHJcbiAgaGVpZ2h0OiA0OHZoO1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxufVxyXG5cclxuOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDNweDtcclxufVxyXG5cclxuLyogVHJhY2sgKi9cclxuOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XHJcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxufVxyXG5cclxuLyogSGFuZGxlICovXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6IHJnYigyMDIsIDIwMiwgMjAyKTtcclxufVxyXG5cclxuLyogSGFuZGxlIG9uIGhvdmVyICovXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWI6aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6IHJnYigyMDIsIDIwMiwgMjAyKTtcclxufVxyXG4vLyBuZXcgY2hhbmdlc1xyXG5cclxuLm1hcmdpbiB7XHJcbiAgbWFyZ2luLXRvcDogLTJweDtcclxufVxyXG5kaXYuc2Nyb2xsIHtcclxuICBtYXJnaW46IDRweCwgNHB4O1xyXG4gIHBhZGRpbmc6IDRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcclxuICB3aWR0aDogNTAwcHg7XHJcbiAgaGVpZ2h0OiAxMTBweDtcclxuICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgb3ZlcmZsb3cteTogYXV0bztcclxuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG59XHJcblxyXG4vLyBBcm1vcm9uXHJcblxyXG4udGltZWRpZmYge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogM3Z3O1xyXG4gIGJvdHRvbTogN3B4O1xyXG59XHJcblxyXG4uY29sIHtcclxuICAtd2Via2l0LWJveC1mbGV4OiAxO1xyXG4gIGZsZXg6IDE7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uRGFzaGJvYXJkQnV0dG9ucyB7XHJcbiAgY29sb3I6ICNmZmRhNDQ7XHJcbiAgZm9udC1zaXplOiAzdnc7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uaW1nYnV0dG9ucyB7XHJcbiAgd2lkdGg6IDMwJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi8qU3RhcnQgU2NzcyovXHJcbi5oZWFkZXItYmFja2dyb3VuZC1jb2xvciB7XHJcbiAgLS1iYWNrZ3JvdW5kOiAjZjZiMjIxO1xyXG59XHJcblxyXG4uYS53aGl0ZSB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi5hLnJlZCB7XHJcbiAgY29sb3I6IHJlZDtcclxufVxyXG4uYXJyb3dOZXh0OjphZnRlcntcclxuICBjb250ZW50OiBcIu+BlFwiO1xyXG4gIGZvbnQ6IG5vcm1hbCBub3JtYWwgbm9ybWFsIDE3cHgvMCBGb250QXdlc29tZTtcclxuICBjb2xvcjogIzZjMmE4NDtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdG9wOiA5LjMlO1xyXG4gIGxlZnQ6IDIwLjElO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICB6LWluZGV4OiAzMztcclxufVxyXG4uYXJyb3dQcmV2aW91czo6YWZ0ZXJ7XHJcbiAgY29udGVudDogJ1xcZjA1Myc7XHJcbiAgZm9udDogbm9ybWFsIG5vcm1hbCBub3JtYWwgMTdweC8wIEZvbnRBd2Vzb21lO1xyXG4gIGNvbG9yOiAjNmMyYTg0O1xyXG4gIHJpZ2h0OiAwcHg7IFxyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDkuMyU7XHJcbiAgbGVmdDogMCU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcbi5lbXB0eUNsZWFyLC5lbXB0eUNsZWFybGVmdHtcclxuICBoZWlnaHQ6IDUxcHg7XHJcbiAgd2lkdGg6IDIzcHg7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRvcDogNS44JTtcclxuICBsZWZ0OiAxOS4yJTtcclxuICB6LWluZGV4OiAxO1xyXG59XHJcbi5lbXB0eUNsZWFybGVmdHtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgbGVmdDowJTtcclxufVxyXG4uc3RhdHVzQmFye1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgaGVpZ2h0OiAyOXB4O1xyXG4gIHdpZHRoOiAyOXB4ICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIG1hcmdpbjo0cHggMTJweCAxNnB4IDdweDtcclxuICBib3gtc2hhZG93OiAwIDAgMCAzcHggIzAwMDAwMDI0O1xyXG4gIGRpc3BsYXk6IGdyaWQgIWltcG9ydGFudDtcclxuICBwbGFjZS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLnRvb2x0aXAge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuLnRvb2x0aXAgLnRvb2x0aXB0ZXh0aW5pdGlhbCB7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICBwYWRkaW5nOiAxM3B4IDA7XHJcbiAgYm90dG9tOiAxMDAlO1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAyMHB4O1xyXG4gIGxlZnQ6IC00N3B4O1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuLnRvb2x0aXAgLnRvb2x0aXB0ZXh0IHtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIGNvbG9yOiAjMWQxYzFjO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgcGFkZGluZzogMTNweCAwO1xyXG4gIGJvdHRvbTogMTAwJTtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMjBweDtcclxuICBsZWZ0OiAtNDVweDtcclxuICB6LWluZGV4OiAxO1xyXG59XHJcblxyXG4udG9vbHRpcDpob3ZlciAudG9vbHRpcHRleHQge1xyXG4gIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbn1cclxuLnRvb2x0aXA6aG92ZXIgLnRvb2x0aXB0ZXh0aW5pdGlhbCB7XHJcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/mapview-tab/mapview-tab.page.ts":
/*!*************************************************!*\
  !*** ./src/app/mapview-tab/mapview-tab.page.ts ***!
  \*************************************************/
/*! exports provided: MapviewTabPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapviewTabPage", function() { return MapviewTabPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_map_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth-map.service */ "./src/app/services/auth-map.service.ts");
/* harmony import */ var _services_openlayer_map_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/openlayer-map.service */ "./src/app/services/openlayer-map.service.ts");
/* harmony import */ var _services_google_map_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/google-map.service */ "./src/app/services/google-map.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_websocket_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/websocket.service */ "./src/app/services/websocket.service.ts");











var MapviewTabPage = /** @class */ (function () {
    function MapviewTabPage(mapService, GMap, olMap, activatedRoute, ajaxService, menuController, platform, commonService, websocketService, router) {
        var _this = this;
        this.mapService = mapService;
        this.GMap = GMap;
        this.olMap = olMap;
        this.activatedRoute = activatedRoute;
        this.ajaxService = ajaxService;
        this.menuController = menuController;
        this.platform = platform;
        this.commonService = commonService;
        this.websocketService = websocketService;
        this.router = router;
        this.count = 20;
        this.layerModal = false;
        this.icon = "assets/vtsicon/";
        this.iconHeader = 'search';
        this.search = false;
        this.desktopFilterModal = false;
        this.fabButton = false;
        this.slideOpts = {
            slidesPerView: 5,
            speed: 400
        };
        this.vehicleModel = {
            'BIKE': 'M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm195.16 -440.19c0,5.53 -2.73,15.07 -6.02,19.5 -21.01,28.31 -59.59,11.69 -61.75,-14.22 3.69,1.95 22.84,10.26 26.4,10.56l11.44 -28.17 -28.17 -11.44c9.51,-14.19 46.22,-15.78 56.06,12.61 0.83,2.37 2.04,8.44 2.04,11.16zm30.81 -2.63l0 7c-0.66,12 -4.67,23.9 -12.44,34.24 -1.53,2.04 -1.98,2.45 -3.6,4.32l-11.99 10.9c-26.93,19.95 -65.2,12.72 -86.61,-11.37 -9.98,-11.22 -20.43,-38.55 -14.76,-50.39 -5.52,-2.65 -11.34,-5.58 -17.6,-7.03 -2.75,4.11 -11.51,11.96 -15.62,16.06 -7.32,7.33 -10.87,13.86 -19.58,13.86 -2.85,0 -30.18,-19.32 -34.08,-22.25 -5.5,-4.13 -11.05,-7.54 -16.8,-11.37l-25.08 -17.18c-8.49,-6.35 -16.75,-11.52 -27.71,-14.53 -17.02,-4.67 -28.07,-5.97 -50.37,-5.97 2.8,4.19 7.84,7.9 12.13,10.76 12.93,8.64 29.98,14.73 45.86,14.41 8.15,-0.17 3.56,-1.47 20.73,11.43 3.03,2.28 5.21,3.68 8.18,5.89 2.55,1.92 6.42,3.55 7.28,6.81l-78.33 0 0 8.8c0,52.54 -62.88,81.58 -103.24,51.31 -14.98,-11.23 -27.03,-28.95 -27.03,-48.67 0,-26.19 3.41,-20.76 6.16,-32.57l-5.28 -0.88c-1.67,-7.17 1.46,-10.72 8.57,-17.83 12.44,-12.43 28.55,-20.9 48.64,-20.9l20.25 0.89c-2.57,-2.24 -4.73,-4.27 -7.27,-6.83 -2.8,-2.83 -4.13,-3.67 -7.03,-6.15 -8.69,-7.48 -3.15,-11.92 -6.83,-19.6l14.08 0c8.65,-16.35 -1.83,-30.47 -9.68,-32.56 -0.67,2.51 -1.15,2.69 -2.63,4.4 0,-10.75 5.49,-21.74 9.03,-29.68 9.23,-20.66 26.96,-37.77 46.71,-48.37 2.71,-1.45 9.96,-5.57 12.91,-5.57 3.81,0 8.8,2.16 8.8,7.04 0,6.13 -13.18,23.49 -14.08,34.34 4.14,-0.35 7.68,-1.76 12.32,-1.76 9.38,0 13.14,16.72 -12.32,16.72 0.33,3.99 2.24,4.74 5.47,5.96 9.71,3.67 16.19,0.06 26.91,-2.62l34.85 -7.42c29.02,-4.72 56.03,9.11 69.2,34 16.35,-1.36 31.46,-7.9 43.64,-17.09 3.58,-2.7 6.82,-7.56 11.81,-7.56l34.32 0c9.69,0 15.95,0.89 25.53,0.89 17.32,-0.01 28.83,-3.79 33.35,7.14 4.2,10.13 6.89,13.39 -3.43,19.26l-2.63 1.76c6.66,0 13.2,-0.26 13.2,6.16 0,4.79 -4.22,6.67 -8.8,7.05 0.85,3.2 0.75,1.9 2.81,4.22 1.47,1.65 1.7,2.23 3.15,3.89l9.69 11.44c3.6,4.14 13.48,12.43 6.82,17.87 -3.32,2.7 -8.07,0.76 -10.31,-2.05l-17.22 -20.64c-1.82,-2.05 -2.54,-3.19 -4.39,-5.28 -7.37,-8.27 -4.85,-9.45 -12.56,-9.45 -4.86,0 -11.72,26.51 -15.32,30.43 -2.8,3.04 -27.12,11.73 -31.96,13.83 -3.82,1.63 -6.94,2.71 -10.7,4.27 -3.09,1.26 -7.12,3.45 -10.67,4.27l0 10.57c2.89,0.67 6.31,2.39 8.82,3.5 2.38,1.04 6.37,2.98 8.78,3.54 3.97,-14.9 58.34,-49.72 98.37,-9.46 11.89,11.97 18.71,27.73 19.58,43.8zm-397.85 -1.78l28.16 13.2 12.33 -26.4c6.59,4.83 6.42,18.68 4.62,26.63 -0.98,4.37 -1.58,4.63 -3.42,8.02 -10.65,19.71 -39.85,25.14 -57.28,2.96 -6.95,-8.84 -8.24,-24.29 -3.04,-34.23 2.11,-4.06 4.25,-7.14 7.38,-10.23 6.24,-6.12 14.19,-8.94 25.34,-9.87 -0.88,3.76 -5.23,11.33 -7.05,14.96 -2.45,4.88 -4.51,10.18 -7.04,14.96zm262.3 -67.77l0 -7.92 35.19 -16.73c2.02,-0.91 3.62,-1.81 5.88,-2.93l6.6 -2.49 -1.66 3.02c-1.63,3.37 -3.38,9.09 -5.21,10.64l-26.6 11.26c-3.57,1.42 -10.09,4.81 -14.2,5.15zm-124.73 -249.29c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z',
            'AUTO': "M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm-130.92 -618.95c-4.98,7.59 -19.8,38.59 -23.37,50.15 -1.67,5.4 -3.96,14 -3.96,21.03 -2.51,5.85 -0.61,25.66 0.34,32.26 2.69,18.62 9.99,34.38 18.58,50.32 2.16,4 3.82,6.82 5.95,11.08 2.11,4.21 3.86,7.96 5.94,11.89 -1.76,2.63 -10.13,11.39 -12.97,12.15 -1.35,-5.03 -5.38,-6.15 -7.92,-9.9 -2.81,-4.13 1.06,-6.15 -2.71,-15.94 -7.43,-19.35 -25.46,-16.27 -38.82,-16.31 -7.19,-0.02 -12.45,-1.46 -12.99,5.66 -0.39,5.19 -0.1,13.3 -0.03,18.72 0.11,8.17 3.55,7.28 11.4,7.22 -1.77,2.44 -9.17,5.58 -12.99,13.78 -1.53,3.28 -2.17,6.48 -2.41,9.95l0 14.39 25.94 -0.11c-2.19,3.56 -4.2,4.95 -5.79,9.71 -1.29,3.84 -2.23,9.64 -1.79,13.32 1.25,10.44 4.91,15.94 10.37,21.67 5.99,3.7 12.6,8.66 22.01,7.78 8.73,-0.82 14.87,-3.08 21.18,-8.83 8.76,-7.96 11.56,-23.05 7.54,-33.83 -1.99,-5.37 -3.17,-5.74 -5.69,-9.81 4.75,0.23 6.03,0.15 9.73,-0.72 0,-5.58 -4.6,-9.76 -8.92,-16.21l11.17 -10.71c7.17,12.89 13.45,22.18 29.33,26.15 12.61,3.16 89.35,1.59 108.66,1.59 17.24,0 38.36,1.09 55.12,-0.16 0,8.8 -0.65,15.93 4.69,19.79 4.8,3.47 15.1,2.25 22.96,2.25 -0.08,4.37 2.15,10.03 3.83,13.11 2.57,4.71 4.79,6 7.66,9.6 9.22,7.58 22.6,10.44 35.19,4.6 5.99,-2.78 8.73,-5.27 12.62,-10.07 3.07,-3.79 6.36,-10.62 6.24,-17.24l20.75 0c18.7,0 21.83,1.22 22.25,-3.73l0 -15.74c-0.54,-4.34 -3.57,-3.22 -17.83,-3.23 5.33,-4.62 5.72,-5.49 5.67,-15.39l0 -149.18c-3.47,-159.14 -86.22,-97.26 -265.08,-97.26 -7.28,0 -17.94,-1.34 -22.91,1.61 -4.88,0.47 -12.9,5.41 -16.14,7.98 -3.91,3.11 -9.2,9.48 -11.21,14.27 -2.05,2.86 -3.75,7.42 -5.84,10.83 -1.87,3.09 -4.41,7.93 -5.75,11.51zm186.94 131.32c-3.81,0 -6.5,-0.44 -9.19,0.8 -2.17,1 -4.12,2.89 -5.36,5.17 -2.08,3.83 -3.05,12.91 -1.56,18.36 1.5,2.62 2.48,2.46 6.08,2.49 2.68,0.03 5.47,-0.06 8.15,-0.06 5.61,0 11.54,0.48 17.04,-0.3 0,35.69 2.69,31.98 -11.36,31.92l-32.46 0.03c-5.62,0.04 -5.61,-0.27 -8.88,-2.46l0 -65.66c-3.64,-2.45 -3.15,-2.44 -9.22,-2.44l0.3 -98.9 42.68 15.08c15.15,4.51 20.39,12.05 19.01,27.58l0.22 67.59 -13.87 0.16c-2.1,0.46 -0.63,-0.1 -1.58,0.64zm127.62 84.52l-0.03 0 0.01 0 0.02 0zm-214.36 -84.52c-9.82,0 -19.64,0.02 -29.47,-0.02 -5.61,-0.03 -7.98,0.82 -10.74,3.84 -3.19,3.48 -3,5.9 -3,12.16l0 9.97c4.84,1.13 8.24,0.45 13.72,1.03 3.31,-2.32 43.62,-0.7 47.35,-0.7 -0.01,7.36 1.92,26.91 -1.9,31.37 -2.62,1.12 -6.18,0.72 -9.4,0.72l0 -31.59 -36.05 0.2 0.06 31.39c-9.19,0.02 -18.4,-0.06 -27.59,0.02 -11.81,0.12 -9.7,-1.94 -9.7,-18.66 0,-55.08 0.86,-113.49 -0.04,-167.8 17.85,0 35.75,-0.15 53.58,-0.04 5.59,0.04 34.97,10.36 40.49,13 0,14.17 -0.98,96.44 0.47,102.96 -10.49,0 -10.45,0.78 -10.13,11.35l-16.06 0.16c-2.11,0.46 -0.64,-0.1 -1.59,0.64zm-3.59 -274.03c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            'BOBCAT': "M303.39 269.89l149.29 53.73c11.81,10.17 -3.56,12.67 11.24,28.75 6.94,7.55 13.51,11.34 16.56,23.43 4.41,17.49 -0.53,18.92 7.47,35.58 30.63,7.14 79.07,-9.22 113.81,-9.22 -13.89,-20.74 -19.77,-5.62 -38.43,-16.94l-67.94 -61.25c-7.7,-21.6 17.45,-12.44 -62.45,-39.06 -14.18,-4.71 -27.45,-10.17 -43.06,-15.37l-86.49 -30.41 0 30.76zm119.93 -269.89c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm-89.19 139.71l3.08 0c11.15,0 12.06,16.63 12.31,27.68l-27.69 0c0.91,-10.85 1.21,-27.68 12.3,-27.68zm58.45 0l39.99 0c0.31,13.87 3.48,16.24 6.15,27.68l-39.99 0c-0.45,-5.52 -4.48,-23.95 -6.15,-27.68zm-36.91 0l24.61 0c3.21,6.69 5.93,17.69 6.15,27.68l-24.6 0c-0.65,-7.65 -3.53,-22.2 -6.16,-27.68zm-18.46 -30.77c2.11,9.05 3.08,9.39 3.08,21.54l-12.31 0c6.92,-14.42 -2.72,-12.78 9.23,-21.54zm49.22 0l36.91 0c1.7,7.33 3.24,15.47 6.15,21.54l-39.98 0c-0.24,-10.49 -1.86,-12.05 -3.08,-21.54zm-36.91 0l24.61 0c2.11,9.05 3.07,9.39 3.07,21.54l-24.6 0c-0.23,-10.49 -1.87,-12.05 -3.08,-21.54zm-46.14 61.52c11.04,5.3 18.02,8.38 33.95,9.11 34.7,1.58 4.24,10.23 30.65,12.42l0 -12.3 21.53 0c7.94,16.53 -1.28,23.09 18.46,27.69 -0.65,-7.65 -3.53,-22.19 -6.16,-27.69l39.99 0c9.32,40.04 7.26,48.69 30.75,49.22l-27.27 -99.13c-12.32,-48.89 56.49,-42.37 -117.3,-42.37 -17.4,0 -11.22,55.18 -24.6,83.05zm218.39 61.53c0,9.85 -2.78,11.68 -6.62,17.98 -3.64,5.95 -5.63,9.94 -8.75,15.86 9.69,6.48 62.5,58.43 67.67,58.43 16.34,0 10.04,-7.58 25.55,5.06 12.94,10.54 11.36,7.54 11.36,28.77 28.52,0 20.11,1.68 36.91,3.09 12,-51.52 2.68,-23.53 2.04,-55.72 -0.11,-5.54 7.78,-28.12 -18.18,-48.11 -7.18,-5.53 -18.2,-6.91 -30,-6.91 0,-11.49 1.81,-39.99 -15.38,-39.99 -13.3,0 -12.4,6.15 -21.53,6.15 -17.95,0 -26.51,-5.77 -43.07,-6.15l0 21.54zm-93.67 81.46c10.75,0 19.46,8.7 19.46,19.45 0,10.74 -8.71,19.45 -19.46,19.45 -10.73,0 -19.44,-8.71 -19.44,-19.45 0,-10.75 8.71,-19.45 19.44,-19.45zm0 -26c-25.09,0 -45.44,20.35 -45.44,45.45 0,25.1 20.35,45.45 45.44,45.45 25.11,0 45.46,-20.35 45.46,-45.45 0,-25.1 -20.35,-45.45 -45.46,-45.45zm-136.81 26c10.73,0 19.44,8.7 19.44,19.45 0,10.74 -8.71,19.45 -19.44,19.45 -10.74,0 -19.45,-8.71 -19.45,-19.45 0,-10.75 8.71,-19.45 19.45,-19.45zm-49.44 -139.91l9.24 0 0 9.23 -9.24 0 0 -9.23zm-49.21 123.05c19.17,12.83 12.71,18.75 46.14,21.53 5.44,-23.37 26.92,-39.36 55.38,-39.99l0 -107.67c-23.88,-5.56 -38.14,-18.45 -67.68,-18.45 0,11.28 -2.13,25.86 -3.44,39.62 -4.63,48.47 -20.51,-13.91 -30.4,104.96zm98.65 -9.14c-25.1,0 -45.46,20.35 -45.46,45.45 0,25.1 20.36,45.45 45.46,45.45 25.1,0 45.45,-20.35 45.45,-45.45 0,-25.1 -20.35,-45.45 -45.45,-45.45zm12.09 -6.25c43.87,10.22 39.99,46.2 39.99,70.74l33.83 0c-5.67,-24.36 -4.34,-46.39 20.1,-62.95 23.68,-16.05 27.12,-7.79 53.72,-7.79 -1.26,-15.19 -2.44,-11.7 -31.16,-21.12 -19.98,-6.54 -104.31,-39.39 -116.48,-40.39l0 61.51z",
            'BUS': "M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm63.41 -409.51c0,-6.2 -0.22,-7.7 2.56,-13.73l3.75 -5.56c9.77,-10.77 24.55,-11.59 35.92,-2 17.22,14.53 6.13,44.56 -16.63,44.56 -13.79,0 -25.6,-11.98 -25.6,-23.27zm-171.51 100.04l-4.67 0c-5.77,-0.43 -11.37,-2.5 -15.6,-6.75 -8.25,-8.26 -5.98,-23.7 -5.98,-38.23 -14.51,0 -27.97,3.19 -30.71,-12.71 -0.61,-3.45 -1.12,-7.82 -1.11,-11.32 0.02,-4.95 -0.55,-6.98 -0.75,-10.86 -1.49,-28.31 -2.32,-66.82 -2.33,-95.4 0,-8.63 0.79,-14.44 0.77,-23.27 0,-8.01 0,-16.03 0,-24.04 -4.68,-2.48 -7.32,-4.05 -10.06,-8.54 -4.75,-7.78 -3.11,-32.99 -3.11,-44.19 0,-26.23 16.99,-20.6 18.47,-27.28l2.91 -23.46c3.34,-19.91 5.15,-39.89 21.57,-49.77 24.79,-14.9 81.02,-21.25 121.45,-21.25l3.02 0 9.49 0 0.8 0c12.1,0.03 26.68,0.17 34.73,0.81 2.96,0.25 3.24,0.75 7,0.76l29.19 2.63c9.24,1.28 18.47,2.63 27.27,4.52 16.04,3.44 32.77,7.35 46.42,16.4 17.52,11.62 19.66,43.42 22.34,63.74 0.1,0.79 0.29,2.55 0.41,3.46 0.17,1.12 0.16,0.98 0.38,1.94l0.42 1.13c1.49,2.88 3.38,1.67 7.74,3.9 2.19,1.13 4.34,2.81 5.94,4.93 7.29,9.76 4.94,31.53 4.94,47.02 0,10.45 -3.86,20.9 -13.96,23.25l0 120.98c0,8.66 -0.82,15.52 -0.77,23.27 0.02,6.74 0.64,17.72 -0.06,23.99 -1.24,11.08 -16.66,9.36 -25.54,9.36l0 24.81c0,27.19 -48.85,26.28 -48.85,0.79l0 -25.6 -179.16 0 0 27.92c0,9.12 -10.94,16.21 -22.6,17.06zm-6.09 -100.04l0 -3.88c0,-17.85 23.28,-33.39 42.23,-17.41 17.5,14.75 5.91,44.56 -16.63,44.56 -13.79,0 -25.6,-11.98 -25.6,-23.27zm233.43 -89.19l-241.95 0 0 -127.95 244.28 0 0 125.62c0,1.79 -0.54,2.33 -2.33,2.33zm-210.16 -170.61l180.7 0 0 26.37 -180.7 0 0 -26.37zm56.61 -92.35c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            'CAR': "M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm-41.63 -541.9c0,-16.27 29.79,-12.43 34.38,-8.45 5.14,4.45 5.14,12.44 0,16.89 -5.18,4.47 -25.16,4.47 -30.34,0 -1.69,-1.47 -4.04,-5.83 -4.04,-8.44zm165.21 76.84c0,-18.58 23.93,-31.38 39.18,-16.12 21.22,21.21 -11.03,53.45 -32.25,32.24 -3.11,-3.11 -6.93,-10.35 -6.93,-16.12zm-26.89 -2.88l0 5.76c0,23.93 23.13,47.06 47.06,47.06 10.43,0 15.35,-0.3 24.98,-4.79 14.26,-6.66 27.85,-24.87 27.85,-41.31 0,-16.72 -2.45,-26.96 -14.64,-39.15 -3.95,-3.94 -8.05,-7.43 -13.21,-9.84 -13.42,-6.26 -31.09,-6.8 -44.22,-0.03 -14.12,7.29 -27.82,23.83 -27.82,42.3zm-249.74 2.88c0,-18.58 23.93,-31.38 39.18,-16.12 21.21,21.21 -11.03,53.45 -32.24,32.24 -3.12,-3.11 -6.94,-10.35 -6.94,-16.12zm-26.9 -2.88l0 5.76c0,23.93 23.14,47.06 47.07,47.06 16.34,0 26.54,-2.99 38.18,-14.64 33.9,-33.9 4.36,-85.25 -31.45,-85.25 -16.84,0 -26.87,2.37 -39.15,14.64 -7.22,7.23 -14.65,18.38 -14.65,32.43zm134.48 -118.15c0,-46.73 -7.38,-39.38 65.32,-39.38 7.87,0 18.69,10.6 24.74,15.59l34.43 27.05c5.18,4.93 0.72,10.19 -2.5,10.19l-108.54 0c-8.41,0 -13.45,-5.05 -13.45,-13.45zm-40.35 13.45l-47.06 0c-7.07,0 -9.67,-5.85 -2.64,-13.22l26.51 -31.11c7.7,-9.66 11.2,-8.5 23.19,-8.5 17.49,0 13.46,18.83 13.46,39.38 0,8.4 -5.04,13.45 -13.46,13.45zm-155.6 30.74l0 48.98c0,19.73 19.66,39.38 39.38,39.38l11.53 0c0,-10.09 -3.48,-19.88 4.79,-36.51 3.27,-6.59 7,-13.29 12.22,-18.51 3.96,-3.97 13.02,-10.2 18.36,-12.38 20.82,-8.51 41.73,-7.06 60.14,5.52 2.29,1.57 2.36,1.9 4.26,3.41l11.11 11.96c6.06,8.85 11.11,23.22 11.11,34.99 0,4.81 -0.96,5.91 -0.96,11.52l155.6 0c0,-10.09 -3.47,-19.88 4.79,-36.51 10.83,-21.8 29.55,-36.49 55.72,-36.49 15.4,0 28.32,5.85 39.26,14.53 13.51,10.71 22.22,29.3 22.22,46.95 0,4.81 -0.96,5.91 -0.96,11.52 20.72,0 35.54,-15.16 35.54,-32.65l0 -38.43c0,-11.84 -8.19,-21.91 -15.52,-26.74 -7.63,-5 -22.61,-7.01 -33.47,-8.79 -6.4,-1.06 -67.08,-11.17 -71.52,-13.97l-69.27 -57.52c-8.48,-6.67 -16.82,-14 -29.22,-14l-119.11 0c-8.24,0 -17.56,4.54 -22.33,8.4 -6.36,5.15 -13.51,13.95 -18.83,20.55l-25.34 29.41c-18.16,20.39 -3.36,13.6 -47.84,14.6 -16.47,0.36 -31.66,16.88 -31.66,30.78zm207.11 -219.76c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            'CRANE': "M349.18 465.04l42.28 0 0 11.8 -42.28 0 0 -11.8zm74.14 -465.04c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm-204.95 182.34c0,33.42 -6.47,27.54 67.86,27.54 14.68,0 11.81,-13.83 11.81,-28.51 0,-6.9 -5.68,-10.83 -12.79,-10.83l-54.09 0c-7.93,0 -12.79,3.74 -12.79,11.8zm173.08 180.97l0 27.53 -34.4 0c2.76,-2.4 4.98,-4.38 7.99,-6.75 3.05,-2.42 5.62,-4.74 8.59,-7.16l13.12 -10.47c2.59,-2.31 1.14,-2.2 4.7,-3.15zm-42.28 13.77l0 -30.48 37.39 0c-1.65,2.45 -1.67,2.21 -3.93,3.93 -2,1.53 -2.84,2.48 -4.79,4.06 -3.25,2.66 -6.05,4.63 -9.25,7.48 -2.44,2.18 -16.49,14.23 -19.42,15.01zm42.28 -73.77l0 28.53 -34.4 0 34.4 -28.53zm-42.28 14.75l0 -31.47 38.37 0 -38.37 31.47zm42.28 -73.75l0 27.53 -34.4 0c4.55,-3.97 30.36,-26.45 34.4,-27.53zm-42.28 13.77l0 -30.49 37.39 0 -8.72 8c-3.77,3.07 -25.21,21.57 -28.67,22.49zm249.82 -91.47l8.84 0 0 51.14 -8.84 0 0 -51.14zm-207.54 17.7l0 28.53 -34.4 0c1.65,-2.47 1.65,-2.22 3.91,-3.96 7.97,-6.11 23.35,-19.78 30.49,-24.57zm-42.28 14.74l0 -32.44 39.34 0c-1.36,2.05 -6.88,6.34 -9.41,8.3 -8,6.25 -22.61,19.26 -29.93,24.14zm21.64 -123.91l18.69 66.88 -38.35 0c1.28,-5.52 17.67,-64.01 19.66,-66.88zm-38.35 66.88l-81.63 0c3.07,-4.58 68.43,-55.54 78.9,-64.69l26.27 -18.98c-0.8,1.95 -0.31,0.69 -0.95,2.99l-22.59 80.68zm50.16 -91.47l25.15 11.24c40.01,16.62 87.14,39.31 127.99,56.91l48.49 21.35c1.64,0.83 1.71,0.91 2.93,1.97l-178.99 0 -12.68 -45.35c-2.28,-7.86 -4.26,-14.86 -6.56,-22.94 -1.23,-4.31 -2.18,-7.75 -3.27,-11.49 -1.09,-3.76 -2.57,-7.77 -3.06,-11.69zm-164.25 99.34c0,6.64 -1.75,16.72 7.88,16.72l103.25 0 0 250.79 -45.22 0c-2.01,0 -5.05,2.55 -5.92,3.93 -1.69,2.65 -7.3,35.41 7.87,35.41l168.19 0c11.82,0 9.83,-15.61 9.83,-25.57 0,-7.47 -2.56,-13.77 -9.83,-13.77l-43.29 0 0 -250.79 179.99 0 0 50.16c0,6.04 -6.88,-2.57 -6.88,20.66 0,6.25 2.37,15.34 4.3,20.28 1.58,4.04 2.84,4.41 4.34,7.45 2.1,4.28 1.02,5.56 7.09,5.7l0.98 9.82c7.7,1.81 12.78,3.61 12.78,11.82 0,6.85 -3.49,11.47 -10.27,10.33 -11.56,-1.93 -5.9,-10.33 -12.34,-10.33 -9.16,0 0.01,17.7 10.82,17.7 5.91,0 11.19,0.6 15.8,-5.83 5.42,-7.55 4.96,-17.56 -1.29,-24.42 -4.63,-5.09 -8.43,-1.48 -8.62,-9.09 8.08,-0.18 3.21,-1.99 8.1,-7.65 7.87,-9.06 6.66,-22.88 6.66,-35.62 0,-4.71 -3.13,-7.86 -6.88,-8.86l0 -52.12c3,0 4.49,0.33 6.91,-1.94 2.5,-2.36 4.98,-20.25 -3.21,-22.42 -10.25,-2.71 0.64,5.07 -26.04,-6.42 -10.65,-4.58 -20.42,-8.88 -30.75,-13.49l-60.58 -26.97c-23.75,-9.85 -51.54,-23.55 -75.58,-33.57l-30.73 -13.54c-3.54,-1.67 -12.57,-6.27 -16.06,-6.56 -0.62,-7.48 -12.33,-15.56 -18.81,-1.09 -2.27,5.07 -22.44,19.77 -26.74,23.41 -3.23,2.74 -6.04,4.75 -9.3,7.41l-64.46 52.58c-6.17,5.22 -12.77,9.56 -18.57,14.87 -5.26,4.82 -4.58,2.98 -13.65,3.07 -6.07,0.08 -9.77,2.01 -9.77,7.94z",
            'FOCKLIFTS': "M638.29 388.46c0,3.16 -2.56,5.73 -5.72,5.73l-82.05 0c-3.17,0 -5.73,-2.57 -5.73,-5.73l0 -82.04c0,-3.16 2.56,-5.73 5.73,-5.73l82.05 0c3.16,0 5.72,2.57 5.72,5.73l0 82.04zm-214.97 -388.46c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm46.05 274.06c-24.9,-40.69 -90.36,-149.72 -91.02,-150.8 -3.33,-5.58 -9.44,-9.05 -15.94,-9.05l-106.04 0c-10.23,0 -18.56,8.33 -18.56,18.56l0 102.19 -29.84 0c-7.07,0 -12.83,5.75 -12.83,12.82l0 118.38c0,7.07 5.76,12.83 12.83,12.83l13.43 0c1.75,0 3.16,-1.41 3.16,-3.15 0,-24.36 19.81,-44.17 44.16,-44.17 24.35,0 44.16,19.81 44.16,44.17 0,0.83 0.33,1.63 0.93,2.22 0.59,0.59 1.38,0.93 2.22,0.93l35.41 0c1.74,0 3.15,-1.41 3.15,-3.15 0,-24.36 19.81,-44.17 44.16,-44.17 24.35,0 44.16,19.81 44.16,44.17 0,1.74 1.41,3.15 3.15,3.15l9.61 0c9.19,0 16.66,-7.48 16.66,-16.66l0 -79.89c0,-3.27 -1.53,-6.04 -2.96,-8.38zm-105.36 -0.2l0 -17.26c0,-2.97 -2.41,-5.37 -5.37,-5.37l-30.42 0 0 -50.81c0,-2.97 -2.41,-5.37 -5.38,-5.37l-17 0c-2.97,0 -5.37,2.4 -5.37,5.37l0 73.44 -25.52 0 -0.01 -122.51 76.93 0 73.21 122.51 -61.07 0 0 0zm-67.67 101.97c0,-15.25 -12.36,-27.62 -27.62,-27.62 -15.25,0 -27.62,12.37 -27.62,27.62 0,15.26 12.37,27.63 27.62,27.63 15.26,0 27.62,-12.37 27.62,-27.63zm130.02 0c0,-15.25 -12.36,-27.62 -27.61,-27.62 -15.26,0 -27.62,12.37 -27.62,27.62 0,15.26 12.36,27.63 27.62,27.63 15.25,0 27.61,-12.37 27.61,-27.63zm199.89 1.87l-111.52 0c-6.64,0 -12.03,-5.39 -12.03,-12.04l0 -258.39c0,-6.65 5.39,-12.04 12.03,-12.04 6.65,0 12.04,5.39 12.04,12.04l0 246.35 99.48 0c6.65,0 12.04,5.4 12.04,12.04 -0.01,6.65 -5.39,12.04 -12.04,12.04zm12.03 -165.63c0,3.16 -2.56,5.72 -5.72,5.72l-82.05 0c-3.17,0 -5.73,-2.56 -5.73,-5.72l0 -82.05c0,-3.17 2.56,-5.73 5.73,-5.73l82.05 0c3.16,0 5.72,2.56 5.72,5.73l0 82.05z",
            'LOADER': "M301.98 146.22c-11.14,1.99 -19.4,9.58 -22.69,18.71 -3.18,8.84 -1.77,66.71 -1.8,80.4 -0.07,25.86 12.66,29.05 32.67,49.55 9.54,9.76 11.57,11.7 29.45,11.66 13.7,-0.03 27.4,-0.01 41.1,-0.01 13.47,0 26.99,0.57 35.19,-6.85l9.8 -10.27c10.24,-10.24 25.62,-19.98 19.79,-40.36 -1.97,-6.88 -34.7,-70.51 -40.72,-82.59 -4.9,-9.84 -9.5,-17.04 -21.23,-19.83 -5.81,-1.39 -74.97,-1.6 -81.56,-0.41zm121.34 -146.22c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm-203 349.85c7.42,-65.86 102.19,-105.8 155.64,-39.56 13.64,16.9 20.53,40.57 17.29,68.33l31.75 0c-2.03,-15.95 -1.65,-28.33 3.28,-42.85 5.9,-17.32 10.7,-19.72 17.66,-30.62 -15.26,-1.36 -20.61,-4.85 -28.25,-12.85 -7.34,-7.65 -9.95,-19.05 -8.06,-32.13 -17.2,2.16 -79.16,3.99 -93.19,-1.81 -11.63,-4.78 -29.04,-24.36 -39.29,-34.69 -7.67,-7.73 -7.31,-10.1 -10.62,-13.69 -26.6,4.93 -58.29,21.05 -61.81,46.62 -1.81,13.32 -0.33,38.91 -0.32,53.51 0.01,16.76 -0.05,28.38 15.92,39.74zm290.72 -151.53c-39.47,6.48 -31.11,5.25 -61.22,34.76 -5.32,5.21 -20.55,18.83 -23.67,24.75 -7.48,14.14 1.16,27.58 11.99,31.41 16.01,5.65 25.81,-8.69 32.03,-14.8 8.35,-8.22 16.09,-15.74 24.42,-23.99 8.34,-8.25 9.66,-5.9 26.13,-9.41 6.26,-1.33 5.39,-2 8.86,1.45 23.36,23.16 44.94,19.22 76.74,19.22 15.03,0 33.9,3.07 36.3,-12.12 1.23,-7.73 -3.28,-13.14 -7.83,-15.48 -7.2,-3.69 -12.97,-0.2 -18.02,-6.59 -3.08,-3.92 -28.02,-55.3 -32.51,-64.3 -6.09,-12.19 -9.24,-24.2 -26.86,-15.31 -16.92,8.55 -31.01,15.78 -40.26,31.92 -4.03,7.03 -3.73,10.74 -6.1,18.49zm-8.63 93.24c-37.61,4.78 -69.01,38.32 -63.55,81.95 4.63,37.07 38.96,68.35 81.96,62.95 36.73,-4.6 68.66,-39.03 62.99,-81.73 -4.93,-37.04 -38.45,-68.63 -81.4,-63.17zm3.02 44.04c-39.2,9.21 -24.61,65.02 12.42,56.84 13.85,-3.06 25.88,-17.44 21.85,-34.97 -3.1,-13.57 -17.22,-25.87 -34.27,-21.87zm-207.63 -44.04c-37.45,4.77 -69.05,38.34 -63.58,81.73 4.7,37.23 38.83,68.59 82,63.17 36.98,-4.64 68.39,-38.91 62.98,-81.73 -4.66,-37.01 -38.69,-68.6 -81.4,-63.17zm3.03 44.04c-14.31,3.37 -26.82,16.35 -22.56,35 8.48,37.13 65.56,24.73 56.89,-13.19 -3.1,-13.53 -17.2,-25.83 -34.33,-21.81zm5.9 -197.2l-0.02 50.24 105.93 -0.07 -36.16 -72.97 -69.74 -0.04 -0.01 22.84z",
            'SMALL TRUCK': "M1235.06 1548.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm2.54 -613.73c0,-15.21 1.35,-17.34 16.73,-17.34 48.51,0 40.51,-3.21 56.17,23.1l13.29 24.37c1.32,9.35 0.48,4.03 -2.5,6.34 -4.88,5.04 -39.28,2.42 -49.26,2.44 -35.58,0.05 -34.43,6.21 -34.43,-38.91zm246.86 106.36c-4.4,-3.41 -6.98,-2.33 -13.7,-2.1l-1.92 -45.1c-1.84,-21.6 -20.83,-24.8 -44.15,-24.7 -22.05,0.09 -44.1,-0.09 -66.15,0.07 -9.91,-11.39 -23.07,-35.25 -32.26,-49 -24.39,-36.52 -14.51,-30.07 -89.65,-30.64 -55.88,-0.43 -27.47,32.38 -35.87,84.15l-198.64 -0.13c0,25.8 -3.2,86.87 3.21,105.71 10.12,2.86 20.16,2.06 32.19,2.06 6.88,-2.07 2.84,-9.63 10.56,-23.21 33.05,-58.07 121.59,-39.12 128.38,22.96l144.92 0.02c2.83,-32.66 32.9,-57.38 67.91,-57.51 19.34,-0.07 34.6,7.27 46.13,17.54 16.6,14.79 13.55,15.42 22.46,39.11 8.24,1.21 15.97,0.25 22.88,-1.43 5.3,-12.46 3.78,-21.74 3.7,-37.8zm-101.23 34.94c20.55,-9.23 31.88,23.84 10.54,31.4 -20.12,7.14 -32.96,-21.33 -10.54,-31.4zm-44.54 24.63c10.03,61.01 109.84,51.72 99.57,-17.63 -3.69,-24.85 -29.33,-47.24 -59.3,-41.32 -24.65,4.87 -45.44,27.45 -40.27,58.95zm-235.06 -25.2c20.61,-6.79 31.45,25.04 11.32,31.95 -21.16,7.28 -34.04,-24.48 -11.32,-31.95zm-45.14 20.76c2.24,26.69 23.98,47.35 53.63,45.65 65.73,-3.76 60.77,-106.85 -8.06,-100.95 -26.65,2.29 -48.06,25.64 -45.57,55.3zm142.26 -309.42c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            'TANKER': "M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm147.88 -437.47c29.14,0 31.78,43.53 -0.93,43.53 -26.22,0 -26.38,-43.53 0.93,-43.53zm-280.56 -118.51l22.23 0 0 -23.15 -22.23 0 0 23.15zm0 -37.04l22.23 0 0 -23.15 -22.23 0 0 23.15zm0 74.07l22.23 0 0 -23.15 -22.23 0 0 23.15zm66.67 -120.37c0,5.05 1.08,7.78 7.41,9.26l0 13.89 -37.04 0 0 133.33 -14.81 0 0 -22.22 -22.23 0 0 22.22 -14.81 0 0 -133.33c-26.81,0 -38.33,3.91 -54.88,21.04 -38.19,39.56 -17.52,128.03 44.7,128.03l161.1 0c10.42,0 19.66,-2.8 26.68,-6.66 14.04,-7.69 25.05,-20.28 31.15,-35.53 11.11,-27.84 7.19,-60.02 -10.98,-83.35 -8.08,-10.38 -24.21,-22.6 -42.22,-22.6l0 -14.82c11.08,-2.58 9.84,-15.74 -2.77,-15.74l-61.11 0c-4.45,0 -10.19,2.54 -10.19,6.48zm13.89 201.85c29.14,0 31.78,43.53 -0.93,43.53 -26.22,0 -26.37,-43.53 0.93,-43.53zm-88.89 0c13.67,0 22.54,10.06 22.8,21.41 0.68,29.45 -44.42,29.45 -43.74,0 0.24,-10.87 9.5,-21.41 20.94,-21.41zm275 -99.07l0 -56.48c13.02,0 14.03,4.22 19.83,11.65l42.21 54.09 -52.83 0.06c-6.81,-0.11 -9.21,-2.65 -9.21,-9.32zm-51.85 -65.74l0 119.44c-23.76,0 -18.36,-1.56 -23.85,4.85 -5.03,5.86 -11.15,10.69 -17.76,14.66 -7.31,4.38 -20.81,10.12 -32.47,10.12l-170.36 0c-18.98,0 -36.16,-10.66 -48.4,-22.9 -2.04,-2.05 -3.54,-4.5 -5.3,-5.8 -2.48,0.65 -5.56,2.6 -5.56,5.55l0 54.64c0,3.37 4.51,6.47 8.33,6.47l36.11 0c0,6.75 1.07,10.37 3.37,15.15 12.26,25.43 43.03,27.88 59.86,11.06 6.62,-6.63 10.84,-16.15 10.84,-26.21l14.82 0c0,38 52.75,52.43 70.7,15.15 2.31,-4.78 3.37,-8.4 3.37,-15.15l125.93 0c0,38 52.74,52.43 70.7,15.15 2.31,-4.78 3.37,-8.4 3.37,-15.15l17.59 0c10.08,0 19.45,-9.36 19.45,-19.44l0 -75.93c0,-9.9 -10.71,-20.79 -17.2,-29.09l-46.18 -59.39c-5.91,-6.45 -16.27,-9.66 -26.43,-9.66l-40.74 0c-4.46,0 -10.19,2.54 -10.19,6.48zm-116.45 -159.38c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            'TRUCK': "M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm-195.33 -646.31l0 181.15c0,3.84 1.92,5.76 5.76,5.76l205.03 0c3.39,0 4.95,-4.53 4.95,-8.24l0 -175.37c0,-10.47 -7.12,-19.76 -17.3,-19.76l-180.33 0c-5.79,0 -9.41,1.78 -12.57,4.71 -2.68,2.47 -5.54,6.88 -5.54,11.75zm-14.83 229.62c0.67,3.72 2.41,5.88 6.59,5.88l65.05 0c3.6,0 1.69,-5.52 7.4,-16.49 1.44,-2.79 3.15,-5.06 4.94,-7.41 1.94,-2.57 4.07,-4.03 5.77,-6.57l-82.4 -0.07c-3.31,0.06 -2.64,-0.36 -4.73,1.03 -0.98,0.66 -2.62,2.46 -2.62,3.98l0 19.65zm317.84 31.4c-23.7,0 -24.85,-37.06 0.82,-37.06 23.19,0 23.71,37.06 -0.82,37.06zm-0.83 18.94c29.23,0 45.73,-28.23 35.67,-52.15 -9.43,-22.44 -40.22,-32.45 -61.8,-11.06 -6.24,6.18 -10.47,15.09 -10.34,26.03 0.24,20.2 16.49,37.18 36.47,37.18zm-194.32 -18.94c-24.89,0 -25.24,-37.06 0,-37.06 23.5,0 23.47,37.06 0,37.06zm-37.87 -23.06c0,8.37 -0.44,13.77 3.84,21.7 11.77,21.86 39.92,25.96 55.65,13.09 20.98,-17.13 20.58,-43.51 0.44,-60.14 -13.5,-11.15 -35.61,-10.06 -48.63,2.89 -5.35,5.34 -11.3,14.83 -11.3,22.46zm270.08 -130.93l-73.28 0c-3.01,0 -6.59,-2.27 -6.59,-4.94l0 -41.97c0,-4.91 3.33,-6.59 8.23,-6.59l40.34 0c8.57,0 13.61,11.74 20.93,21.88 3.2,4.42 16.96,22.97 16.96,25.87 0,3.22 -3.07,5.75 -6.59,5.75zm66.69 102.94l0 19.04c-0.83,8.51 -5.29,6.49 -25.53,6.49 -9.87,0 -19.76,0 -29.64,0 -0.97,-11.72 -7.51,-21.04 -15.01,-28.62 -17.86,-18.06 -49.1,-17.83 -67.13,-0.02 -8.61,8.51 -11.85,15.05 -15.01,28.64l-98.81 0c0,-14.31 -10.91,-23.39 -15.65,-30.47l92.22 0 0 -171.26c0,-7.44 5.74,-13.18 13.18,-13.18l55.15 0c14.62,0 25.82,6.27 33.91,13.03 1.65,1.38 3.72,4.28 5.28,6.26 1.95,2.48 3.02,4.06 4.8,6.73 8.53,12.89 32.92,47.57 37,56.02 2.39,4.98 4.65,11.43 4.65,18.4l0 84c7.43,0 20.59,-1.97 20.59,4.94zm-245.74 -325.32c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            'COMPRESSORS': "M365.57 176.98c0,-7.96 -6.47,-14.44 -14.44,-14.44 -7.98,0 -14.44,6.48 -14.44,14.44 0,7.98 6.46,14.45 14.44,14.45 7.97,0 14.44,-6.47 14.44,-14.45zm57.75 -176.98c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm195.98 215.08c-1.74,-10.43 -10.74,-18.09 -21.32,-18.11l-349.32 0c-10.56,0.02 -19.56,7.65 -21.32,18.06 -4.02,25.07 -6.09,50.41 -6.19,75.81 0.1,25.38 2.17,50.71 6.19,75.76 1.74,10.43 10.74,18.09 21.32,18.1l23.39 0c3.68,-40.93 37.98,-72.29 79.07,-72.29 41.1,0 75.39,31.36 79.06,72.29l50.9 0 0 -7.22c0,-3.98 3.23,-7.22 7.22,-7.22 3.99,0 7.22,3.24 7.22,7.22l0 50.54 57.77 0 0 -50.54c0,-3.98 3.23,-7.22 7.21,-7.22 4,0 7.23,3.24 7.23,7.22l0 7.22 30.25 0c10.56,-0.01 19.56,-7.64 21.32,-18.05 4.02,-25.07 6.08,-50.43 6.19,-75.82 -0.1,-25.37 -2.17,-50.7 -6.19,-75.75zm-318.72 17.99l-21.67 0c-3.99,0 -7.22,-3.23 -7.22,-7.21 0,-3.99 3.23,-7.23 7.22,-7.23l21.67 0c3.98,0 7.21,3.24 7.21,7.23 0,3.98 -3.23,7.21 -7.21,7.21zm267.15 0l-238.28 0c-3.98,0 -7.21,-3.23 -7.21,-7.21 0,-3.99 3.23,-7.23 7.21,-7.23l238.28 0c3.99,0 7.22,3.24 7.22,7.23 0,3.98 -3.23,7.21 -7.22,7.21zm-216.61 93.87c-26.29,0 -49.99,15.83 -60.04,40.12 -10.05,24.29 -4.5,52.23 14.09,70.82 18.58,18.58 46.54,24.15 70.82,14.08 24.29,-10.05 40.11,-33.75 40.11,-60.04 -0.04,-35.88 -29.11,-64.95 -64.98,-64.98zm0 101.08c-19.94,0 -36.1,-16.16 -36.1,-36.1 0,-19.94 16.16,-36.11 36.1,-36.11 19.93,0 36.1,16.17 36.1,36.1 -0.02,19.95 -16.17,36.09 -36.1,36.11zm144.4 21.66c0,4 3.23,7.23 7.23,7.23l43.31 0c4,0 7.23,-3.23 7.23,-7.23l0 -7.21 -57.77 0 0 7.21zm-122.74 -57.76c0,-11.96 -9.7,-21.65 -21.66,-21.65 -11.96,0 -21.66,9.69 -21.66,21.65 0,11.97 9.7,21.66 21.66,21.66 11.96,0 21.66,-9.69 21.66,-21.66zm-100.72 -267.15l-14.81 0 0 57.76 48.38 0c-19.15,-13.37 -31.43,-34.51 -33.57,-57.76zm223.46 57.76c35.89,0 64.98,-29.09 64.98,-64.99 0,-35.89 -29.09,-64.98 -64.98,-64.98 -35.89,0 -64.98,29.09 -64.98,64.98 0.04,35.88 29.11,64.96 64.98,64.99zm41.51 -79.33c1.89,-0.32 3.83,0.13 5.39,1.23 1.57,1.12 2.62,2.8 2.94,4.69 0.47,2.78 0.7,5.6 0.7,8.42 0,3.99 -3.23,7.22 -7.21,7.22 -4,0 -7.23,-3.23 -7.23,-7.22 0,-2.02 -0.16,-4.02 -0.49,-6.01 -0.67,-3.93 1.97,-7.65 5.9,-8.33zm-7.53 -19.64c2.83,2.82 2.83,7.38 0,10.21l-14.48 14.5c1.42,2.89 2.15,6.05 2.17,9.27 0,11.97 -9.71,21.67 -21.67,21.67 -11.96,0 -21.66,-9.7 -21.66,-21.67 0,-11.96 9.7,-21.65 21.66,-21.65 3.22,0.01 6.4,0.75 9.28,2.16l14.51 -14.5c2.81,-2.82 7.38,-2.82 10.19,0.01zm-84.52 33.98c0.01,-14.86 6.56,-28.98 17.92,-38.58 11.37,-9.6 26.38,-13.7 41.05,-11.24 3.63,0.68 6.18,4.01 5.87,7.7 -0.32,3.7 -3.37,6.56 -7.07,6.62 -0.41,0 -0.81,-0.03 -1.22,-0.09 -10.47,-1.76 -21.19,1.19 -29.31,8.04 -8.1,6.86 -12.79,16.93 -12.8,27.55 0,4 -3.23,7.23 -7.22,7.23 -3.99,0 -7.22,-3.23 -7.22,-7.23zm96.06 64.99l48.35 0 0 -57.76 -14.81 0c-2.13,23.25 -14.4,44.38 -33.54,57.76zm-40.47 -70.04l-0.05 -0.08c-2.86,-2.79 -7.43,-2.75 -10.25,0.1 -2.81,2.84 -2.8,7.43 0.03,10.25 2.83,2.83 7.41,2.84 10.25,0.04 2.85,-2.82 2.9,-7.4 0.1,-10.26l-0.08 -0.05zm-103.92 70.04l53.38 0c-11.59,-8.13 -20.82,-19.21 -26.71,-32.08 -5.88,12.87 -15.1,23.94 -26.67,32.08zm-45.53 0c35.89,0 64.98,-29.09 64.98,-64.99 0,-35.89 -29.09,-64.98 -64.98,-64.98 -35.89,0 -64.98,29.09 -64.98,64.98 0.03,35.88 29.11,64.96 64.98,64.99zm-49.77 -61.71l0 -0.02c-0.87,-1.71 -1.01,-3.69 -0.41,-5.52 0.6,-1.82 1.9,-3.32 3.61,-4.19 6.18,-2.75 12.9,-4.07 19.67,-3.86 0.97,-2.49 2.28,-4.84 3.9,-6.97 -2.85,-2.79 -6.22,-5 -9.93,-6.49 -3.79,-1.27 -5.83,-5.36 -4.57,-9.15 1.27,-3.79 5.36,-5.84 9.15,-4.57 6.3,2.41 11.98,6.22 16.6,11.12 2.45,-1.06 5.03,-1.78 7.67,-2.12 -0.04,-3.99 -0.85,-7.94 -2.42,-11.62 -1.74,-3.56 -0.29,-7.86 3.26,-9.64 3.55,-1.77 7.86,-0.35 9.66,3.19 2.77,6.17 4.09,12.9 3.87,19.67 2.48,0.97 4.83,2.27 6.95,3.88 2.83,-2.77 5.04,-6.11 6.49,-9.8 0.79,-2.46 2.82,-4.31 5.34,-4.86 2.52,-0.55 5.15,0.29 6.89,2.2 1.73,1.9 2.32,4.58 1.54,7.05 -2.36,6.36 -6.18,12.06 -11.18,16.66 1.08,2.45 1.81,5.05 2.17,7.7 3.99,0 7.94,-0.82 11.61,-2.39 3.55,-1.8 7.91,-0.36 9.7,3.2 1.8,3.57 0.36,7.92 -3.2,9.71 -5.72,2.59 -11.92,3.94 -18.19,3.95 -0.48,0 -1,-0.06 -1.5,-0.07 -0.96,2.49 -2.27,4.82 -3.89,6.94 2.85,2.8 6.22,5 9.94,6.5 3.34,1.14 5.38,4.53 4.8,8.01 -0.58,3.5 -3.6,6.05 -7.13,6.05 -0.77,0 -1.54,-0.13 -2.26,-0.37 -6.3,-2.41 -11.98,-6.21 -16.61,-11.11 -2.44,1.06 -5.01,1.76 -7.66,2.11 0.03,4 0.84,7.96 2.42,11.65 1.73,3.56 0.28,7.86 -3.27,9.64 -3.54,1.77 -7.86,0.35 -9.66,-3.19 -2.76,-6.18 -4.08,-12.9 -3.86,-19.67 -2.49,-0.97 -4.84,-2.28 -6.97,-3.89 -2.8,2.84 -5.01,6.22 -6.5,9.93 -1.27,3.79 -5.36,5.83 -9.15,4.56 -3.79,-1.26 -5.83,-5.36 -4.56,-9.15 2.41,-6.29 6.21,-11.97 11.11,-16.6 -1.06,-2.44 -1.77,-5.02 -2.12,-7.66 -3.98,0.01 -7.93,0.83 -11.6,2.4 -1.72,0.85 -3.69,1 -5.52,0.4 -1.82,-0.6 -3.32,-1.9 -4.19,-3.61z",
            'BATTERY': "M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm-184.36 -579.92l370 0 0 -53.48 -370 0 0 53.48zm303.51 -67.95c0,1.67 0.51,2.18 2.18,2.18l36.11 0c1.67,0 2.18,-0.51 2.18,-2.18l0 -18.04 -40.47 0 0 18.04zm-92.5 0c0,1.67 0.5,2.18 2.16,2.18l36.14 0c1.67,0 2.17,-0.51 2.17,-2.18l0 -18.04 -40.47 0 0 18.04zm-92.49 0c0,1.67 0.5,2.18 2.17,2.18l36.14 0c1.66,0 2.16,-0.51 2.16,-2.18l0 -18.04 -40.47 0 0 18.04zm-92.5 0c0,1.67 0.51,2.18 2.18,2.18l36.11 0c1.67,0 2.18,-0.51 2.18,-2.18l0 -18.04 -40.47 0 0 18.04zm183.81 158.61c8.41,0 33.79,-1.77 37.78,3 5.09,6.1 -0.95,12.25 -5.32,18.81l-53.56 80.34c-2.67,3.99 -5.95,11.35 -11.71,11.35 -4.2,0 -7.98,-3.77 -7.98,-7.98 0,-2.09 13.23,-54.76 14.19,-56.76 -18.33,0 -38.14,4.45 -38.14,-8.85 0,-3.58 12.77,-24.11 15.92,-29.31l34.02 -58.22c1.93,-3.47 6.48,-12.05 8.74,-14.3 5.13,-5.1 13.16,-1.51 13.16,4.28 0,5.87 -6.03,55.4 -7.1,57.64zm-211.93 -78.93l0 206.62c0,2.04 0.62,2.65 2.66,2.65l368.88 0c2.04,0 2.66,-0.61 2.66,-2.65l0 -206.62c0,-2.03 -0.62,-2.65 -2.66,-2.65l-368.88 0c-2.04,0 -2.66,0.62 -2.66,2.65zm168.47 111.73c12.82,0 34.59,-2.79 34.59,7.11 0,6.76 -4.28,14.21 -4.42,21.27 2.45,-1.81 12.59,-18.01 15.24,-21.99 2.64,-3.96 4.75,-7.13 7.45,-11.18 2.06,-3.09 6.62,-8.93 7.44,-12.05 -6.49,0 -13,-0.04 -19.51,0 -14.6,0.11 -14.6,-2.02 -12.24,-21.12 0.38,-3.22 1.53,-9.16 1.62,-12.58 -2.71,1.98 -26.59,43.8 -30.17,50.54zm-16.32 -305.2c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            'WELDING MACHINES': 'M336.41 377.24l-12.79 0c-16.05,0 -29.47,13.47 -31.99,27.51 -2.29,12.78 2.55,49.66 -2.19,57.3 -4.36,7.03 -13.98,7.07 -18.7,0.44 -3.59,-5.04 -2.11,-19.69 -2.11,-28.02 0,-19.84 0,-39.67 0,-59.49 5.82,-0.49 12.44,-7.03 15.62,-10.76 3.64,-4.29 6.98,-11.64 6.98,-19.37l0 -57.99 -67.78 0 0 57.99c0,15.1 13.58,29.37 22.6,30.13 0,10.85 -0.99,83.57 0.88,90.24 4.12,14.86 17.06,23.88 30.63,24.98l5.54 0c7.41,-0.6 14.69,-3.65 20.49,-9.48 6.8,-6.85 10.28,-13.13 10.24,-25.91 -0.05,-11.3 0,-22.6 0,-33.9 -0.01,-20.56 2.54,-21.09 22.58,-21.09l0 67.78 22.6 0 0 22.6 67.78 0 0 -22.6 90.38 0 0 22.6 67.78 0 0 -22.6 24.1 0 0 -248.52 -272.64 0 0 158.16zm86.91 -377.24c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm-84.66 67.76c-12.04,0 -24.1,0.03 -36.15,0 -12.31,-0.05 -20.82,3.05 -28.58,7.55 -15,8.68 -27.89,27.41 -27.89,45.16l0 39.17 -22.6 0 0 45.18 67.78 0 0 -45.18 -22.6 0c0,-16.21 -2.21,-41.3 5.11,-52.9 3.11,-4.93 8.65,-10.4 14.25,-12.86 11.98,-5.27 36.33,-3.54 50.68,-3.54 0.8,9.45 16.07,22.6 29.39,22.6 13.65,0 17.1,-3.34 30.02,-7.63l61.32 -21.52c3.05,-1.21 9.32,-3.71 12.58,-3.98 -2.08,-2.85 -19.26,-7.61 -23.78,-9.36l-49.56 -17.47c-8.46,-2.79 -18.01,-7.15 -27.68,-7.82l-4.84 0c-5.65,0.42 -11.28,2.39 -16.69,6.99 -3.73,3.17 -10.27,9.79 -10.76,15.61zm65.53 259.82c0,18.92 22.59,14.43 22.59,3.01 0,-18.92 -22.59,-14.42 -22.59,-3.01zm0 -77.57l90.37 0 0 -22.59 -90.37 0 0 22.59z',
            'TOWED STREET SWEEPER': 'M237.28 439.23c24.7,2.37 82.42,39.97 109.99,32.9 27.29,-7.02 17.01,-4.11 41.77,1.32 -7.49,-25.63 -13.6,-19.39 -7.6,-54.95l-12.15 -7.35c7.84,-20.91 19.55,-50.13 39.12,-51.2 -9.44,10.26 -32.47,31.5 -29.95,49.44l22.82 3.75c14.56,-20.24 11.72,-38.99 40.28,-47.53 38.59,-11.53 50.64,24.62 57.76,31.32 30.03,-13.15 -6.26,-17.58 50.91,-29.84 8.26,-1.76 24.69,-3.8 34.55,-7.81l6.65 -5.18c0.35,-0.35 0.75,-0.97 1.12,-1.32 6.73,-6.53 12.79,-7.9 20.3,-2.74l-13.11 2.64c-6.38,0.48 -4.28,1.91 -5.23,9.38 -10.05,1.06 -5.15,0.13 -8.85,3.63 -12.38,-0.8 -40.53,6 -52.92,10.33 -24.43,8.55 -1.33,12.61 -33.56,27.06l3.51 24.64c7.32,-1.41 5.67,-3.08 11.81,-4.18 18.97,-3.45 9.02,6.37 32.74,-10.25l2.75 -2.57c9.22,-6.69 -7.89,3.5 3.22,-2.11 10.08,5.06 6.68,5.95 16,12.24 14.2,-5.29 26.92,-15.05 31.99,-29.84 31.21,3.13 28.84,-13.56 9.16,-22.47l2.15 -6.08 2.36 -2.37c5.52,-13.47 -9.36,-8.99 4.76,-17.74 -9.65,-2.49 -6.48,-0.23 -10.32,-9.39 -2.18,-5.23 -3.3,-10.61 -4.39,-15.85l-73.99 12.77c-7.94,-14.78 -3.82,-18.22 -26.62,-18.12 -19.29,0.1 -54.28,10.29 -69.53,20.19 -10.28,6.68 -17.01,15.05 -25.46,20.16l-0.34 -82.61c-8.78,2.98 -87.74,41.59 -92.91,51.58 -21.4,41.35 -16.52,111.38 -17.83,119.42 -2.78,17 3.99,-11.49 -1.16,4.2l-1.95 3.78c-6.81,-33.38 2.66,-102.88 16.47,-129.03 -11.3,-1.85 -75.82,2.37 -83.15,6.43 -8.68,5.92 -13.81,34.44 -15.11,44.95 -0.58,0.76 -3.8,0.44 -5.43,13.26l2.36 33.48c0.1,-0.39 0.79,1.51 1.49,1.99 1.55,3.63 -6.56,6.02 5.73,2.28l0.83 -0.05 -0.48 0.24c-0.74,0.34 -1.69,1.02 -2.35,1.39 -0.81,0.46 -1.91,2.93 -2.54,2.22 -0.72,-0.81 -1.83,1.7 -2.66,2.65 19,4.67 7.41,6.59 26.99,10.94zm186.04 -439.23c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm143.38 186.47l0.49 -7.19c-5.4,0.92 -5.45,-0.34 -3.81,7.05l-10.07 -0.95 -0.77 12.72 23.83 0.82 0.35 -12.75 -10.02 0.3zm-42.8 -84.05l8.19 -1.56 -0.92 6.76 -16.76 57.87c-27.88,-1.33 -40.14,-5.95 -67.87,1.11 -18.83,4.79 -39.71,11.14 -58.33,14.26 2.31,-12.32 7.12,-35.97 13.63,-44.02 5.89,-6.12 107.87,-32.42 122.06,-34.42zm-2.25 289.07l1.75 -0.02c-15.88,-0.22 -3.72,2.01 -22.06,-0.81l-4.59 22.21c14.14,-4.58 5.12,-5.48 5.12,-5.48 2.23,11.58 8.51,8.37 17.64,7.24l0.7 -0.41 3.41 1.44 -1.97 -24.17zm16.84 23.31c4.36,1.44 0.94,0.64 1.84,-1.68l1.85 1.13 8.86 -1.17c2.23,-4.39 4.36,-0.29 0.21,-7.49 -0.53,-5.23 0.81,-4.79 -2.17,-5.25l0.63 2.82 -1.15 -5.14 0.72 -1.84 5.96 14.95 4.94 -2.57c6.04,-0.09 9.07,-1.38 14.68,-6.31l-5.49 -11.39c3.68,3.79 5.14,5.75 10.14,10.66 -0.16,-18.16 -21.91,-20.5 -14.51,-24.89 6.8,7.67 10.19,10.9 19.04,15.65 -5.77,-15.51 -17.59,-15.58 -17.42,-18.21l3.33 -2.05c2.97,3.36 5.05,5.15 8.84,8.21 5.52,4.46 5.66,4.56 10.13,4.67 -2.94,-10.5 -15.05,-10.79 -16.14,-18.03l-10.41 9.89c-6.24,2.64 -9.69,5.43 -16.36,6.79l2.95 -2.81c0.21,-1.05 0.95,-1.46 1.69,-1.87 5.97,-5.48 5.93,-3.75 9.93,-11.53l-0.09 -5.32 -1.57 -3.75c-3.93,0.6 -3.11,1.63 -3.37,-1.91l-3.92 0.79 -4.05 0.84c-0.02,3.97 0.77,2.4 -4.17,3.02 -16.43,20.69 -39.93,-5.18 -39.79,22.54 10.06,5.52 25.33,1.48 36.71,0.01 9.14,-0.2 3.36,-1.24 6.78,3.79 -9.67,-1.39 -4.26,-1.45 -6.04,2.08l-0.59 -0.59 -6.67 2.19 -4.47 -0.01 -0.39 -0.48 -7.3 0.95 16.84 23.31zm-10.62 0.63l3.36 -0.2 1.88 -0.31 5.38 -0.12 -10.62 0.63zm18.96 -24.14l0.75 0.74 0.08 2 -0.83 -2.74zm0 0l0.83 2.74 -0.83 -2.74zm2.88 11.88l1.33 9.91 -1.33 -9.91zm-326.9 -41.06c-3.06,1.36 -2.65,0.9 -1.54,0.09l1.54 -0.09zm236.28 -1.63c2.64,6.18 6.67,15.39 1.75,23.69 -3.07,5.18 -10.99,8.32 -17.52,4.13 -10.03,-6.45 -11.04,-34.23 15.77,-27.82zm-22.96 42.07c33.94,2.88 42.5,-53.32 6.83,-60.2 -13.28,19.39 -17.33,33.33 -6.83,60.2zm-201.67 -143.87c25.91,0.09 74.05,-1.23 94.85,-13.24 12.65,-7.3 23.3,-11.79 35.36,-18.46 8.87,-4.91 28.16,-13.19 34.79,-20.22 -22.07,0.17 -73.83,9.04 -92.25,16.46 -13.88,5.58 -65.06,28.69 -72.75,35.46zm378.38 31.92c3.48,5.6 2.4,1.03 2.02,10.05l-2.36 2.37 0.34 -12.42zm-278.34 -78.72c27.22,-3.07 47.81,-10.5 79.93,-10.9l0.02 78.85c28.92,-17.44 29.07,-22.33 75.69,-32.55l-8.32 -73.62c-29.44,-4.56 -130.01,25.12 -147.32,38.22zm110.35 124.12c39.7,-17.59 50.65,62 15.59,77.31 -37.54,16.38 -51.82,-61.27 -15.59,-77.31zm-58.02 22.99c-13.43,73.59 52.86,89.53 86.72,72.28 39.2,-19.95 27.85,-108.89 -5.43,-119.3 -37.27,-11.64 -52.97,24.18 -62.25,49.06l-19.04 -2.04zm143.11 -184.76l65.91 -0.33c2.41,11.99 6.79,58.38 4.84,68.46 -14.81,2.63 -51.18,9.76 -63.79,8.51 -2.99,-23.09 -10.37,-53.89 -6.96,-76.64zm13.43 -72.63c30.94,0.85 42.42,8.14 42.56,8.16l9.66 46.89 -64.77 -0.39 12.55 -54.66zm-152.3 31.04c-5.56,46.99 -5.01,24.94 -10.34,55.25 29.69,-4.63 76.39,-25.29 105.4,-18.83l9.81 76.53c21.38,5.57 30.07,-3.81 37.24,18.59 14.05,-1.34 66.96,-10.21 76.42,-15.35 1.74,-23.47 -2.46,-58.79 -6.26,-82.03l-12.53 -54.04c-3.25,-18.67 1.93,-2.03 3.01,-17.44 -15.12,-6.83 -36.05,-8.1 -55.04,-9.49 -43.25,-3.17 -19.53,6.83 -48.42,14.36 -18.58,4.84 -33.85,9.87 -50.81,15.52 -29.21,9.72 -32.89,6.59 -48.48,16.93z',
            'COMPACTOR': 'M316.51 237.37c-8.45,4.69 -42.76,20.05 -50.36,20.61 0.79,-28.98 4.05,-45.36 -6.16,-66.5 -4.32,-8.96 -4.47,-13.54 -14.96,-8.26 -5.87,11.55 6.33,15.87 6.8,33.43 0.39,15.03 -0.07,30.96 -0.06,46.12 -5.15,10.84 -27.59,7.33 -38.66,28.74 -9.35,18.09 -7.08,39.31 -6.95,61.65 6.41,-3.6 8.55,-7.28 17.12,-12.04 47.22,-26.3 105.28,-3.62 118.87,52.86l3.36 10.5c0.2,0.42 0.52,1.16 0.86,1.7l79.08 -0.04c-10.38,-39.79 -13.17,-63.01 5.42,-100.49 12.81,-25.82 37.82,-51.12 64.7,-60.13 -0.39,-0.4 -1.12,-1.46 -1.32,-1.07 -0.01,0.01 -7.29,-2.32 -9,-15.51 -15.08,-116.42 1.25,-111.86 -75.37,-111.86 -35.12,0.01 -75.2,-9.38 -81.85,24.48 -5.84,29.7 -6.03,65.03 -11.52,95.81zm106.81 -237.37c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm-171.07 288.03c-88.74,16.21 -61.8,144.23 23.72,127.71 82.37,-15.91 59.99,-143.02 -23.72,-127.71zm274.54 -96.12c-18.16,3.4 -26.73,6.03 -41,15.09 -103.03,65.37 -42.77,231.82 83.3,207.98 54.21,-10.25 103.54,-64.86 89.19,-133.69 -11.33,-54.27 -63.65,-102.09 -131.49,-89.38zm-90.57 -15.54l34.08 -0.22c1.07,-13.24 -4.72,-46.17 -6.58,-61.53 -3.45,-28.45 2.38,-42.11 -27.23,-42.48l-0.27 104.23zm-153.97 48.33c-2.59,8.86 -3.14,23.39 1.21,30.76 12.4,2.17 12.83,-5.86 12.51,-17.15 -0.34,-12.05 -2.32,-15.51 -13.72,-13.61zm-25.81 -0.04c-9.58,4.74 -7.9,41.9 7.49,30.31 3.97,-2.99 6.72,-37.31 -7.49,-30.31zm151.95 95.38c4.04,-15.91 -5.58,-14.46 -20.37,-14.31 -9.15,0.1 -29.46,-3.29 -35.44,2.91 -18.08,18.73 48.42,12.66 55.81,11.4zm-76.42 -144.02l88 0.23 0.84 -104.05c-83.32,0.24 -75.86,-8.71 -81.15,34.71 -2.33,19.14 -8.21,52.05 -7.69,69.11z',
            'GENERATOR': 'M206 206.15l0 226.74c0.01,3.48 3.48,6.3 7.77,6.31l7.76 0 0 -239.34 -7.76 0c-4.29,0 -7.76,2.83 -7.77,6.29zm217.32 -206.15c-171.86,0 -311.7,139.83 -311.7,311.71 0,44.83 9.17,87.64 27.26,127.24 77.94,170.53 227.35,350.58 271.3,401.69 3.3,3.83 8.1,6.02 13.15,6.02 5.05,0 9.84,-2.19 13.14,-6.02 43.93,-51.1 193.35,-231.14 271.31,-401.69 18.1,-39.6 27.26,-82.41 27.26,-127.24 -0.02,-171.88 -139.85,-311.71 -311.72,-311.71zm0.01 59.44c141.83,0 256.81,114.98 256.81,256.81 0,141.83 -114.98,256.81 -256.81,256.81 -141.83,0 -256.81,-114.98 -256.81,-256.81 0,-141.83 114.98,-256.81 256.81,-256.81zm139.71 341.96c-8.57,0 -15.52,-5.64 -15.52,-12.59l0 -18.9c0,-6.95 6.95,-12.58 15.52,-12.59l0 -75.58c-8.57,-0.01 -15.52,-5.64 -15.52,-12.6l0 -18.89c0,-6.96 6.95,-12.59 15.52,-12.6l0 -12.6 -131.94 0 0 188.95 131.94 0 0 -12.6 0 0zm-38.81 0l-69.85 0c-4.29,0 -7.76,-2.81 -7.76,-6.29 0,-3.49 3.47,-6.3 7.76,-6.3l69.85 0c4.28,0 7.76,2.81 7.76,6.3 0,3.48 -3.48,6.29 -7.76,6.29zm0 -25.19l-69.85 0c-4.29,0 -7.76,-2.82 -7.76,-6.29 0,-3.48 3.47,-6.31 7.76,-6.31l69.85 0c4.28,0 7.76,2.83 7.76,6.31 0,3.47 -3.48,6.29 -7.76,6.29zm0 -25.18l-69.85 0c-4.29,0 -7.76,-2.83 -7.76,-6.31 0,-3.48 3.47,-6.3 7.76,-6.3l69.85 0c4.28,0 7.76,2.82 7.76,6.3 0,3.48 -3.48,6.31 -7.76,6.31zm-186.28 -62.99l38.81 0c4.29,0 7.76,-2.82 7.76,-6.3l0 -25.19c0,-3.48 -3.47,-6.3 -7.76,-6.3l-38.81 0c-4.28,0 -7.76,2.82 -7.76,6.3l0 25.19c0,3.48 3.48,6.3 7.76,6.3zm294.94 -88.18l-7.76 0 0 239.34 7.76 0c4.29,-0.01 7.76,-2.83 7.77,-6.31l0 -226.74c-0.01,-3.46 -3.48,-6.29 -7.77,-6.29zm-395.84 239.34l372.56 0 0 -239.34 -372.56 0 0 239.34zm15.52 -69.28c0.01,-6.96 6.96,-12.59 15.53,-12.6l0 -75.58c-8.57,-0.01 -15.52,-5.64 -15.53,-12.6l0 -18.89c0.01,-6.96 6.96,-12.59 15.53,-12.6l0 -18.89c0,-1.67 0.82,-3.28 2.27,-4.45 1.45,-1.19 3.43,-1.85 5.49,-1.85l294.94 0c2.06,0 4.04,0.66 5.49,1.85 1.45,1.17 2.27,2.78 2.27,4.45l0 18.89c8.57,0.01 15.52,5.64 15.53,12.6l0 18.89c-0.01,6.96 -6.96,12.59 -15.53,12.6l0 75.58c8.57,0.01 15.52,5.64 15.53,12.6l0 18.89c-0.01,6.95 -6.96,12.59 -15.53,12.59l0 18.9c0,1.67 -0.82,3.27 -2.27,4.46 -1.45,1.18 -3.43,1.84 -5.49,1.84l-294.94 0c-2.06,0 -4.04,-0.66 -5.49,-1.84 -1.45,-1.19 -2.27,-2.79 -2.27,-4.46l0 -18.9c-8.57,0 -15.52,-5.64 -15.53,-12.59l0 -18.89zm15.53 18.89l15.52 0 0 -18.89 -15.52 0 0 18.89zm31.04 -138.56l0 18.89c0,6.96 -6.95,12.59 -15.52,12.6l0 75.58c8.57,0.01 15.52,5.64 15.52,12.6l0 18.89c0,6.95 -6.95,12.59 -15.52,12.59l0 12.6 131.94 0 0 -188.95 -131.94 0 0 12.6c8.57,0.01 15.52,5.64 15.52,12.6zm15.53 6.3c0.01,-10.43 10.42,-18.89 23.28,-18.9l38.81 0c12.86,0.01 23.27,8.47 23.28,18.9l0 25.19c-0.01,10.43 -10.42,18.88 -23.28,18.89l-38.81 0c-12.86,-0.01 -23.27,-8.46 -23.28,-18.89l0 -25.19zm-46.57 12.59l15.52 0 0 -18.89 -15.52 0 0 18.89zm77.96 -107.06l20.57 0c-3.74,-8.05 -5.57,-16.6 -5.39,-25.21 -0.18,-8.59 1.65,-17.13 5.39,-25.18l-20.92 0c-1.89,0.8 -7.76,9.57 -7.76,25.18 0,15.46 5.73,24.19 8.11,25.21zm46.22 25.19l46.57 0 0 -12.6 -46.57 0 0 12.6zm-7.4 -25.19l43.84 0c-3.73,-8.05 -5.56,-16.6 -5.39,-25.21 -0.17,-8.59 1.66,-17.13 5.39,-25.18l-44.2 0c-1.89,0.8 -7.76,9.57 -7.76,25.18 0,15.46 5.73,24.19 8.12,25.21zm178.16 107.06l15.52 0 0 -18.89 -15.52 0 0 18.89zm-116.07 -107.06l20.56 0c-3.73,-8.05 -5.56,-16.6 -5.39,-25.21 -0.17,-8.59 1.66,-17.13 5.39,-25.18l-20.91 0c-1.9,0.8 -7.77,9.57 -7.77,25.18 0,15.46 5.74,24.19 8.12,25.21zm38.44 -0.18c3.77,-3.89 6.05,-8.59 6.58,-13.51 0.09,-0.22 0.19,-0.45 0.31,-0.66 0.25,-0.79 0.72,-1.52 1.37,-2.14 0.6,-0.64 1.37,-1.16 2.23,-1.53 0.88,-0.41 1.85,-0.66 2.86,-0.74 0.28,-0.06 0.58,-0.11 0.87,-0.14l51.7 0c4.42,-1.4 8.38,-3.66 11.49,-6.58 2.21,-1.82 4.15,-3.84 5.79,-6.02l-68.98 0c-0.3,-0.04 -0.61,-0.08 -0.9,-0.15 -0.49,-0.03 -0.98,-0.12 -1.44,-0.24 -0.49,-0.12 -0.96,-0.29 -1.4,-0.5 -0.42,-0.18 -0.81,-0.4 -1.17,-0.64 -0.38,-0.26 -0.74,-0.55 -1.04,-0.87 -0.65,-0.63 -1.12,-1.36 -1.37,-2.15 -0.12,-0.22 -0.23,-0.43 -0.32,-0.65 -0.53,-4.93 -2.81,-9.62 -6.57,-13.52 -2.56,1.67 -7.75,10.31 -7.75,25.01 0,14.73 5.19,23.36 7.74,25.03zm-233.62 302.48l335.31 0c1.2,-4.57 3.65,-8.88 7.17,-12.59l-349.64 0c3.52,3.71 5.98,8.02 7.16,12.59zm311.25 -75.57l15.52 0 0 -18.89 -15.52 0 0 18.89z',
            'HOOK LIFT': 'M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm181.73 -343.53l0 -25.98c0,-17.67 -10.33,-27.99 -27.98,-27.99l-175.92 0c-23.74,0 -35.99,11.25 -35.99,27.99l0 33.73c0,17.5 11.75,26.23 35.99,26.23l175.92 0c17.65,0 27.98,-11.74 27.98,-33.98zm-383.82 -21.99l-8 0 0 15.99 127.94 0 0 -15.99 -15.99 0 0 -15.99 0 -7.99 15.99 0 0 -16 -127.94 0 0 16 16 0 0 23.98 -8 0zm247.89 -215.9l0 8 0 68.54 0 0 0 0.01 10.19 27.41 -17.15 0 -167.27 -233.05 -36.86 22.22 11.17 19.58 0 103.29 -11.79 0 7.92 55.97 3.48 0 0.39 0 0 16.92c-7.19,3.26 -15.19,10.29 -15.19,22.9 0,8.46 3.31,14.65 8.14,18.77l-43.37 37.36 12.25 0 38.41 -33.09c3.47,1.36 7.19,2.11 10.88,2.11 3.69,0 7.36,-0.98 10.67,-2.77l41.64 33.75 12.48 0 0.09 -0.11 -47.9 -38.82c3.24,-3.91 5.39,-9.15 5.39,-15.86 0,-4.42 -3.58,-8 -7.99,-8 -4.41,0 -7.99,3.58 -7.99,8 0,2.87 -0.78,4.67 -1.79,5.82l-2.19 -1.77c-1.5,-1.21 -3.67,-1.19 -5.13,0.09l-3 2.57c-2.59,-1.19 -4.59,-3.54 -4.59,-8.04 0,-7.24 7.16,-8.99 8.52,-9.25 3.85,-0.66 6.67,-3.99 6.67,-7.89l0 -22.69 5.61 0 3.26 0 11.2 -55.97 -8.07 0 -4 0 0 -58.04 95.95 184.67 0 1.27 0 2.4 0 5.64 31.99 0 0 15.98 111.94 0 8 0 0 -15.98 39.98 0 7.99 0 0 -151.94 -119.93 0zm-69.17 41.59l0.09 0.13 -32.63 21.23 16.78 -43.04 15.76 21.68zm-114.75 -25.59l-4 0 -4 0 0 -88.2 8 15.08 0 73.12zm6.95 -81.52l-13.79 -26.21 21.51 -14.14 -7.72 40.35zm18.72 -24.02l15.34 26.61 -24.76 15.95 9.42 -42.56zm2.46 57.85l23.31 -15.18 -9.12 41.54 -14.19 -26.36zm33.18 -1.63l17.19 25.09 -25.85 16.56 8.66 -41.65zm-2.66 60.76l28.9 -19.79 -16.46 44.06 -12.44 -24.27zm42.77 76.03l-13.93 -24.59 34.6 -24.7 -20.67 49.29zm10.86 17.48l23.65 -50.98 19.25 26.24 -42.9 24.74zm159.59 -40.98l-55.97 0 0 -47.98 55.97 0 0 47.98zm15.99 159.92l-175.91 0 0 -23.99 175.91 0 0 23.99zm-184.05 -420.13c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z',
            'PICKUP': 'M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm-103.57 -663.24c-16.03,-0.08 -34.37,-1.58 -47.54,4.99 -10.42,5.19 -20.37,16.76 -20.35,32.55 0.02,17.59 0,35.21 0,52.8 0,17.28 -0.63,35.82 0.06,52.94 -13.16,0.62 -25.07,-1.43 -34.28,5.95 -10.48,8.41 -7.86,19.83 -7.92,32.62 -0.13,26.98 -0.26,54.24 0.02,81.22l9.46 0.01c-2.69,-21.08 12.13,-39.27 33.15,-39.33 19.68,-0.06 37.35,18.06 33.3,39.29l115.07 0.04c-2.47,-8.66 -3.92,-13.75 -1.62,-23.5 0.88,-3.8 2.06,-6.71 3.73,-9.98 6.08,-11.95 18.7,-21.01 33.18,-22.52 5.48,-0.57 8.16,0.4 13.13,0.79l0.01 -10.5 17.82 -0.05 0.16 20.72c4.45,4.2 4.94,3.35 9.4,9.54 7.98,11.06 9.05,25.73 2.83,38.21 -1.19,2.4 -2.65,4.21 -3.68,6.26l162.78 0.04 -0.05 -3.32 -139.61 -11.86 -0.22 -236.09c-14.03,-0.74 -29.9,-0.04 -44.1,-0.06 -3.72,0 -6.78,-0.13 -8.7,2.26 -2.13,2.64 -1.07,6.08 0.9,7.74 3.23,2.73 15.81,0.53 20.94,1.47l0.01 133.53 -25.03 0.05c-1.09,-1.37 -33.13,-94.78 -34.82,-99.48 -3.53,-9.86 -13.96,-42.77 -18.08,-49.28 -4.01,-6.35 -10.34,-11.5 -19.16,-14.54 -10.97,-3.77 -20.87,-0.51 -31.56,-2.11 0.04,-3.77 0.52,-8.23 -1.16,-11.32 -1.35,-2.46 -4.4,-4.74 -8.12,-4.82 -4.04,-0.08 -7.05,2.4 -8.36,4.57 -2.37,3.97 -1.03,6.47 -1.59,11.17zm-72.08 228.62c-16.82,2.98 -27,18.7 -24.13,33.54 2.79,14.39 15.95,26.99 33.4,24.19 14.39,-2.31 27.13,-16.19 24.33,-33.64 -2.29,-14.24 -16.32,-27.15 -33.6,-24.09zm189.27 -17.05c-19.4,1.9 -35.94,19.2 -33.72,41.16 1.95,19.22 19.36,35.73 41.18,33.79 19.57,-1.74 36.11,-19.4 33.74,-41.45 -2.08,-19.24 -19.39,-35.64 -41.2,-33.5zm-166.45 -71.18l144.7 0.13c-0.18,-1.86 -5.07,-14.95 -6.03,-17.75l-19.06 -54.55c-2.31,-6.66 -10.57,-31.4 -13.04,-35.62 -8.82,-15.09 -19.17,-15.16 -34.05,-15.16 -9.09,0 -41.82,-0.53 -48.25,0.51 -13.25,2.16 -21.8,11.68 -24.07,25 -0.82,4.82 -0.27,63.13 -0.27,73.18 0,7.35 -0.57,17.23 0.07,24.26zm118.52 -238.81c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z',
            'STREET SWEEPER': 'M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm164.17 -564.97c-10.54,-2.46 -10.85,-12.91 0,-12.91 3.76,0 8.22,5.53 1.46,11.75 -0.18,0.16 -0.58,0.47 -0.73,0.59 -0.14,0.13 -0.5,0.37 -0.73,0.57zm-49.04 138.06c28.58,0 28.94,43.87 1.29,43.87 -20.79,0 -29.07,-23.01 -17,-36.36 2.79,-3.09 10.45,-7.51 15.71,-7.51zm-49.03 21.93c0,19.55 12.83,37.08 25.86,43.83 23.6,12.22 55.83,4.72 69.23,-24.87 1.76,-3.9 4.37,-15.1 4.11,-20.07 -1.51,-27.78 -22,-47.92 -52.74,-47.92 -25.04,0 -46.46,23.39 -46.46,49.03zm-99.35 -21.93c28.89,0 28.72,43.87 1.29,43.87 -25.13,0 -30.22,-32.84 -10.36,-41.34 1.08,-0.46 7.79,-2.53 9.07,-2.53zm-49.03 21.93c0,26.98 22.43,49.04 45.16,49.04 16.74,0 27.47,-1.26 40.99,-15.79 7.45,-8 13.63,-23.51 13.04,-34.36 -1.02,-18.84 -11.17,-34.29 -28,-42.98 -30.8,-15.89 -71.19,6.57 -71.19,44.09zm27.09 -67.09l0 11.61c10.88,-0.24 27.15,-8.06 49.51,3.4 6.41,3.28 10.57,6.93 15.35,11.73 16.44,16.49 18.98,36.4 13.85,58.41 6.31,0 13.96,1.27 17.71,0.95 5.18,-0.43 11.12,-0.95 18.42,-0.95 -3.24,-6.73 -2.58,-15.43 -2.58,-23.22 0,-23.04 20.35,-46.13 43.87,-51.61l0 -65.8 -77.92 27.88c-13.57,4.75 -25.15,9.15 -39.02,13.88 -12.87,4.37 -26.18,10.69 -39.19,13.72zm112.26 -140.64c4.43,0 10.14,-0.36 14.35,-0.16 7.17,0.34 5.69,0.9 6.94,7.25 1.53,7.78 3.87,16.2 4.51,23.88l-33.54 0c2.63,-5.49 6.04,-23.68 7.74,-30.97zm-58.07 30.97l-42.57 0c0.12,-5.31 4.09,-18.52 5.81,-25.16 0.92,-3.57 0.43,-5.81 4.51,-5.81l38.71 0c-0.22,9.84 -5.67,21.49 -6.46,30.97zm40.01 0l-28.39 0c0.81,-9.69 5.61,-20.85 6.45,-30.97l28.39 0c-0.83,9.93 -6.21,20.17 -6.45,30.97zm25.8 -65.81l5.16 0c1.86,7.95 3.31,16.56 5.16,24.51l-15.48 0c0.7,-8.35 4.97,-15.91 5.16,-24.51zm-16.77 24.51l-28.39 0c1.27,-2.63 2.4,-9.52 3.35,-13.43 3.26,-13.57 -2.32,-11.08 30.2,-11.08 -0.13,5.88 -3.75,18.46 -5.16,24.51zm-40 0l-41.29 0c1.29,-5.56 4.44,-20.32 6.45,-24.51l41.29 0c-3.04,6.34 -3.3,17.94 -6.45,24.51zm-60.65 -43.86c-5.45,23.4 11.11,14.07 2.04,45.9 -1.55,5.44 -2.55,9.7 -4.16,15.19l-23.68 89.87c7.94,-0.66 10.12,-4.5 18.06,-5.16 1.98,-8.5 4.41,-16.16 6.45,-24.52 1.86,-7.55 5.82,-18.18 6.46,-25.8l43.87 0c-1.7,7.29 -5.11,25.48 -7.75,30.96 13.54,-0.3 13.38,-4.69 17.02,-21.69 2.42,-11.34 -3.58,-9.27 29.44,-9.27 0,7.67 -2.43,9.71 -2.59,16.77 11.51,-5.52 13.85,-1.15 14.2,-16.77 23.34,0 31.86,2.35 52.9,-7.75l-13 -69.57c-1.23,-6.13 -2.18,-19.45 -8.93,-19.45 -16.93,0 -122.73,-1.66 -130.33,1.29zm-50.31 172.89l0 19.36c-7.95,0.17 -13.17,9.31 -23.77,17.52l-55.41 49.15c-5.25,3.3 -10.88,0.42 -20.18,0.42 -2.62,0 -14.02,9.43 -15.48,11.61l107.26 14c22.96,0.72 17.47,-27.47 20.97,-39.31 4.76,-16.08 14.01,-25.61 26.6,-34.04l0 -23.22 40.79 -14.69c14.33,-5.01 26.72,-9.83 40.99,-14.5l40.97 -14.52c6.65,-2.4 12.97,-4.47 19.93,-7.16 7.16,-2.77 13.75,-5.46 21.19,-7.2l0 -32.25c-6.48,0.54 -90.2,31.22 -103.39,35.96 -17.19,6.18 -33.93,12.6 -51.29,18.38l-44.7 15.95c-2.92,1.34 -4.48,0.78 -4.48,4.54zm215.48 -85.16l0 116.13 9.02 0c23.19,0 45.92,22.36 50.32,41.29 5.53,-0.12 21.81,-3.7 27.05,-5.21 4.64,-1.34 5.42,-3.31 8.77,-6.72 13.42,-13.69 12.71,-6.09 8.95,-34.13l-4.94 -44.08c-0.78,-6.78 -1.08,-7.42 -5.25,-11.53 -3.01,-2.95 -14.41,-13.48 -15.87,-16.39 -1.62,-3.23 -6.49,-51.45 -7.09,-58.71 -24.79,0 -15.69,0.35 -37.58,7.58l-33.38 11.77zm-146.87 -182.49c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z',
            "REFRIGERATOR": "M1012.86 1394.09c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm-117.39 -693.94l0 342.36c0,10.8 7.93,20.09 18.42,20.09l5.87 0c0,27.95 -3.52,23.43 39.33,23.43 10.83,0 8.37,-16.57 8.37,-23.43l92.08 0c0,27.73 -2.33,23.43 41.01,23.43 9.17,0 6.69,-18.05 6.69,-23.43l5.87 0c10.49,0 18.42,-9.29 18.42,-20.09l0 -340.69c0,-11.24 -6.33,-20.92 -16.74,-20.92l-202.58 0c-9.66,0 -16.74,9.29 -16.74,19.25zm27.62 141.18l194.77 0c9.06,0 9.06,-12.56 0,-12.56l-193.1 0c-12.1,0 -9.91,12.56 -1.67,12.56zm5.03 -102.68l0 25.96c0,9.04 12.56,9.04 12.56,0l0 -24.28c0,-12.1 -12.56,-9.9 -12.56,-1.68zm0 166.58l0 46.87c0,9.06 12.56,9.06 12.56,0l0 -46.87c0,-9.06 -12.56,-9.06 -12.56,0zm50.43 -272.8c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z",
            "location": "M197.849,0C122.131,0,60.531,61.609,60.531,137.329c0,72.887,124.591,243.177,129.896,250.388l4.951,6.738 c0.579,0.792,1.501,1.255,2.471,1.255c0.985,0,1.901-0.463,2.486-1.255l4.948-6.738c5.308-7.211,129.896-177.501,129.896-250.388 C335.179,61.609,273.569,0,197.849,0z M197.849,88.138c27.13,0,49.191,22.062,49.191,49.191c0,27.115-22.062,49.191-49.191,49.191 c-27.114,0-49.191-22.076-49.191-49.191C148.658,110.2,170.734,88.138,197.849,88.138z"
        };
        this.color = {
            'Idle': '#1f5baa',
            'Running': '#1eb15d',
            'Stop': '#FF0000',
            'Yet_to_transmit': '#7d410f',
            'No Transmission': '#000000',
            'Online': '#00E1BC',
            'Overspeed': '#f28918',
            'DoorOpen': '#FF851B',
            'HighTemp': '#FF0000',
            "PowerFail": '#412525',
            'Geofence': '#1f5baa',
            'Good': '#1eb15d'
        };
        this.markerInfo = {
            "show": false,
            "plateNo": '',
            "assetAddress": '',
            "status": '',
            "timeStamp": ''
        };
        this.filterValue = "All";
        this.app = { logo: 'logo.png' };
        this.vehicleCount = { "All": 0, "Running": 0, "Stop": 0, "Idle": 0, "NoTransmit": 0 };
        this.loginData = [];
        this.alertData = [];
        this.alertDataLength = [];
        this.selectedArrayVin = [];
        this.loadMoreData = function (event) {
            setTimeout(function () {
                //console.log("hit successfull!");
                _this.count += 10;
                //Hide Infinite List Loader on Complete
                event.target.complete();
                //Rerender Virtual Scroll List After Adding New Data
                //this.virtualScroll.checkEnd();
                var len = JSON.parse(localStorage.maxLength);
                // App logic to determine if all data is loaded
                // and disable the infinite scroll
                var data = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData;
                // if (len <= this.count && localStorage.statusChanger == "All") {
                //   event.target.disabled = true;
                // }
                if (localStorage.statusChanger != "All" && JSON.parse(localStorage.gridData).length > localStorage.maxLength) {
                    event.target.disabled = true;
                    setTimeout(function () {
                        event.target.disabled = false;
                    }, 2000);
                }
            }, 500);
        };
        this.updateCache = function () {
            if (!_this.liveDatas)
                _this.gridCardIssueRecursion();
            else
                _this.dashboardData = Object.values(_this.liveDatas);
        };
        this.loadMap = function () {
            setTimeout(function () {
                if (localStorage.map === "GoogleMap") {
                    _this.map = _this.mapService.loadMap(_this.mapElement.nativeElement, { lat: 17.786351, lng: 78.090820 }, true, _this.popUp);
                }
                else {
                    _this.map = _this.mapService.loadMap(_this.mapElement.nativeElement, { lat: 17.786351, lng: 78.090820 }, true, _this.popUp);
                    _this.mapService.setCenter(_this.map, { lat: 17.786351, lng: 78.090820 });
                }
                _this.mapService.setCenter(_this.map, { lat: 17.786351, lng: 78.090820 });
            });
        };
        this.getLiveTrackIcon = function (color, path, plateNo) {
            if (path == undefined || path == null) {
                path = _this.vehicleModel["CAR"];
            }
            // const svg = "<svg xmlns='http://www.w3.org/2000/svg' xml:space='preserve' width='8.4666mm' height='8.4666mm' version='1.1' viewBox='0 0 846.66 846.66' xmlns:xlink='http://www.w3.org/1999/xlink'> <path fill='"
            // +color
            // +"' d = '"
            // +path
            // +"'/></path></svg>";
            //////////////////////
            var svg = '<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve"  width="8.4666mm" height="8.4666mm" version="1.1" viewBox="0 0 846.66 846.66"  xmlns:xlink="http://www.w3.org/1999/xlink"> <path class="fil0" d="'
                + path
                + '" fill="'
                + color
                + '"/>  ' +
                '     <path fill="' + color + '"   d="M25.68 -0.01l1276.56 0c14.17,0.02 25.66,17.72 25.66,39.58l0 212.39c0,21.84 -11.49,39.54 -25.66,39.54l-1276.56 0.02c-14.17,-0.02 -25.66,-17.72 -25.66,-39.58l-0.01 -212.39c0.01,-21.84 11.5,-39.54 25.67,-39.54l0 -0.02z"/>  ' +
                '     <g transform="matrix(0.999986 0 0 0.999986 1.97989 -371.852)">  ' +
                '      <text style="font-weight:bold;font-size:176.39px;font-family:Arial" x="46.16" y="577.45"  fill="white">TEXT12345678</text>  ' +
                '     </g>  </svg>';
            var svgNew = '<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="12mm" height="12mm" version="1.1" viewBox="0 0 736.58 888.98" xmlns:xlink="http://www.w3.org/1999/xlink" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" >' +
                '<path fill="red"  d="M39.74 21.16l657.1 0c9.65,0.01 17.47,12.46 17.47,27.83l0 126.12c0,15.36 -7.82,27.81 -17.47,27.81l-657.1 0.01c-9.65,-0.01 -17.47,-12.46 -17.47,-27.83l-0.01 -126.12c0.01,-15.36 7.83,-27.81 17.48,-27.81l0 -0.01z"/>' +
                '<path fill="red" d="' + "M367.8 867.82c6.15,-4.18 17.64,-19.55 24.19,-26.79 3.49,-3.85 5.18,-5.77 8.39,-9.56 10.66,-12.57 20.92,-25.45 31.62,-38.43 21.35,-25.92 39.99,-51.48 60.71,-79.27 35.58,-47.69 82.08,-122.39 102.01,-177 15.4,-42.18 17.68,-73.63 7.29,-119.92 -22.43,-99.94 -115.44,-185.04 -234.1,-184.91 -44.74,0.05 -77.49,10.98 -111.5,27.52 -14.7,7.15 -31.34,19.22 -41.97,28.05 -37.13,30.84 -68.1,77.46 -79.93,129.09 -10.22,44.58 -8.08,78.82 7.08,119.66 25.95,69.9 85.42,158.5 131.33,216.69 5.19,6.58 10.1,12.92 15.28,19.72 24.3,31.88 53.26,64.85 79.6,95.15zm-151.52 -501.33l0 140.51c0,2.98 1.49,4.47 4.47,4.47l159.04 0c2.63,0 3.84,-3.51 3.84,-6.39l0 -136.03c0,-8.12 -5.52,-15.33 -13.42,-15.33l-139.88 0c-4.49,0 -7.3,1.38 -9.75,3.65 -2.08,1.92 -4.3,5.34 -4.3,9.12zm-11.5 178.11c0.52,2.89 1.87,4.56 5.11,4.56l50.46 0c2.79,0 1.31,-4.28 5.74,-12.79 1.12,-2.16 2.44,-3.92 3.83,-5.75 1.51,-1.99 3.16,-3.12 4.48,-5.09l-63.92 -0.06c-2.57,0.05 -2.05,-0.28 -3.67,0.8 -0.76,0.51 -2.03,1.91 -2.03,3.09l0 15.24zm246.54 24.36c-18.38,0 -19.27,-28.75 0.64,-28.75 17.99,0 18.39,28.75 -0.64,28.75zm-0.64 14.69c22.67,0 35.47,-21.9 27.67,-40.45 -7.32,-17.41 -31.2,-25.17 -47.94,-8.58 -4.84,4.79 -8.12,11.7 -8.02,20.19 0.19,15.67 12.79,28.84 28.29,28.84zm-150.73 -14.69c-19.31,0 -19.58,-28.75 0,-28.75 18.23,0 18.2,28.75 0,28.75zm-29.38 -17.89c0,6.49 -0.34,10.68 2.98,16.83 9.13,16.96 30.97,20.14 43.17,10.16 16.27,-13.29 15.96,-33.75 0.34,-46.65 -10.47,-8.65 -27.62,-7.81 -37.72,2.24 -4.15,4.14 -8.77,11.5 -8.77,17.42zm209.5 -101.56l-56.84 0c-2.34,0 -5.11,-1.76 -5.11,-3.83l0 -32.56c0,-3.81 2.58,-5.11 6.38,-5.11l31.29 0c6.65,0 10.56,9.11 16.24,16.97 2.48,3.43 13.15,17.82 13.15,20.07 0,2.5 -2.38,4.46 -5.11,4.46zm51.73 79.85l0 14.77c-0.64,6.6 -4.1,5.03 -19.8,5.03 -7.66,0 -15.33,0 -22.99,0 -0.76,-9.09 -5.83,-16.32 -11.65,-22.2 -13.85,-14.01 -38.08,-13.83 -52.07,-0.01 -6.68,6.6 -9.19,11.67 -11.64,22.21l-76.65 0c0,-11.1 -8.46,-18.14 -12.14,-23.63l71.54 0 0 -132.85c0,-5.77 4.45,-10.22 10.22,-10.22l42.78 0c11.34,0 20.03,4.86 26.3,10.11 1.28,1.07 2.89,3.32 4.1,4.85 1.51,1.93 2.34,3.15 3.72,5.22 6.62,10 25.54,36.9 28.7,43.46 1.86,3.86 3.61,8.86 3.61,14.27l0 65.16c5.76,0 15.97,-1.53 15.97,3.83zm-190.62 -252.35c-102.36,13.78 -189.37,107.21 -173.75,227.67 13.21,101.98 107.94,189.12 227.19,173.89 102.16,-13.05 190.55,-108.24 174.45,-228.04 -13.73,-102.15 -108.02,-189.65 -227.89,-173.52z" + '"/> <text style="font-weight:bold;font-size:105.83px;font-family:Arial" fill="#FEFEFE" x="66.89" y="150.15"  >' +
                "TNYHTRERTFHG" + '</text></svg>';
            var svgnew2 = ' <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="12mm" height="12mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 1065.5 1091.48"  ' +
                '    xmlns:xlink="http://www.w3.org/1999/xlink">  ' +
                '     <path style="width:500px; heigth:00px" fill="' + color + '"  d="M532.12 1091.49c7.93,-5.39 22.74,-25.21 31.19,-34.54 4.5,-4.96 6.68,-7.44 10.81,-12.32 13.75,-16.21 26.97,-32.81 40.77,-49.55 27.52,-33.41 51.55,-66.36 78.26,-102.19 45.87,-61.48 105.82,-157.78 131.51,-228.19 19.86,-54.37 22.8,-94.92 9.4,-154.59 -28.91,-128.85 -148.82,-238.55 -301.79,-238.39 -57.68,0.07 -99.9,14.16 -143.75,35.48 -18.95,9.22 -40.4,24.78 -54.11,36.16 -47.86,39.76 -87.79,99.86 -103.04,166.42 -13.17,57.47 -10.42,101.62 9.13,154.27 33.45,90.11 110.12,204.33 169.31,279.35 6.69,8.48 13.02,16.66 19.69,25.42 31.33,41.1 68.67,83.6 102.62,122.67zm-195.33 -646.31l0 181.15c0,3.84 1.92,5.76 5.76,5.76l205.03 0c3.39,0 4.95,-4.53 4.95,-8.24l0 -175.37c0,-10.47 -7.12,-19.76 -17.3,-19.76l-180.33 0c-5.79,0 -9.41,1.78 -12.57,4.71 -2.68,2.47 -5.54,6.88 -5.54,11.75zm-14.83 229.62c0.67,3.72 2.41,5.88 6.59,5.88l65.05 0c3.6,0 1.69,-5.52 7.4,-16.49 1.44,-2.79 3.15,-5.06 4.94,-7.41 1.94,-2.57 4.07,-4.03 5.77,-6.57l-82.4 -0.07c-3.31,0.06 -2.64,-0.36 -4.73,1.03 -0.98,0.66 -2.62,2.46 -2.62,3.98l0 19.65zm317.84 31.4c-23.7,0 -24.85,-37.06 0.82,-37.06 23.19,0 23.71,37.06 -0.82,37.06zm-0.83 18.94c29.23,0 45.73,-28.23 35.67,-52.15 -9.43,-22.44 -40.22,-32.45 -61.8,-11.06 -6.24,6.18 -10.47,15.09 -10.34,26.03 0.24,20.2 16.49,37.18 36.47,37.18zm-194.32 -18.94c-24.89,0 -25.24,-37.06 0,-37.06 23.5,0 23.47,37.06 0,37.06zm-37.87 -23.06c0,8.37 -0.44,13.77 3.84,21.7 11.77,21.86 39.92,25.96 55.65,13.09 20.98,-17.13 20.58,-43.51 0.44,-60.14 -13.5,-11.15 -35.61,-10.06 -48.63,2.89 -5.35,5.34 -11.3,14.83 -11.3,22.46zm270.08 -130.93l-73.28 0c-3.01,0 -6.59,-2.27 -6.59,-4.94l0 -41.97c0,-4.91 3.33,-6.59 8.23,-6.59l40.34 0c8.57,0 13.61,11.74 20.93,21.88 3.2,4.42 16.96,22.97 16.96,25.87 0,3.22 -3.07,5.75 -6.59,5.75zm66.69 102.94l0 19.04c-0.83,8.51 -5.29,6.49 -25.53,6.49 -9.87,0 -19.76,0 -29.64,0 -0.97,-11.72 -7.51,-21.04 -15.01,-28.62 -17.86,-18.06 -49.1,-17.83 -67.13,-0.02 -8.61,8.51 -11.85,15.05 -15.01,28.64l-98.81 0c0,-14.31 -10.91,-23.39 -15.65,-30.47l92.22 0 0 -171.26c0,-7.44 5.74,-13.18 13.18,-13.18l55.15 0c14.62,0 25.82,6.27 33.91,13.03 1.65,1.38 3.72,4.28 5.28,6.26 1.95,2.48 3.02,4.06 4.8,6.73 8.53,12.89 32.92,47.57 37,56.02 2.39,4.98 4.65,11.43 4.65,18.4l0 84c7.43,0 20.59,-1.97 20.59,4.94zm-245.74 -325.32c-131.97,17.76 -244.14,138.21 -224,293.5 17.03,131.47 139.15,243.81 292.89,224.18 131.7,-16.82 245.65,-139.54 224.9,-293.98 -17.7,-131.69 -139.26,-244.5 -293.79,-223.7z"/>  ' +
                '     <path fill="' + color + '"  d="M22.54 -0.01l1020.43 0c12.44,0.01 22.52,16.06 22.52,35.88l0 162.59c0,19.8 -10.08,35.85 -22.52,35.85l-1020.43 0.01c-12.44,-0.01 -22.52,-16.06 -22.52,-35.87l-0.01 -162.6c0.01,-19.8 10.09,-35.85 22.53,-35.85l0 -0.01z"/>  ' +
                '     <g transform="matrix(0.999986 0 0 0.999986 -476.263 -380.348)">  ' +
                '      <text style="font-weight:bold;font-size:160px;font-family:Arial; text-align:center;align-items: center;" x="532.75" y="545.74" fill="white">' + plateNo + '</text>  ' +
                '    </g>  ' +
                '   </svg>  ';
            var svg1 = '   <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="24.714mm" height="15.481mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"  ' +
                '   viewBox="0 0 2471.4 1548.1"  ' +
                '    xmlns:xlink="http://www.w3.org/1999/xlink">  ' +
                '     <path fill="' + color + '"  d="' + path + '"/>  ' +
                '     <path fill="' + color + '"  d="M47.78 0.01l2375.87 0c26.38,0.02 47.76,27.23 47.76,60.82l0 493.09c0,33.57 -21.38,60.78 -47.76,60.78l-2375.87 0.02c-26.39,-0.02 -47.76,-27.23 -47.76,-60.82l-0.03 -493.09c0.03,-33.57 21.4,-60.78 47.79,-60.78l0 -0.02z"/>  ' +
                '     <g transform="matrix(0.999986 0 0 0.999986 9.42214 -382.593)">  ' +
                '      <text style="font-weight:bold;font-size:300px;font-family:\'Arial\'" x="133.12" y="774.05" fill="white" >' + plateNo + '</text>  ' +
                '     </g>  ' +
                '   </svg>  ';
            var src = 'data:image/svg+xml;charset=utf-8,'
                + encodeURIComponent(svg1);
            return src;
        };
        if (localStorage.map == "GoogleMap") {
            this.mapService = new _services_google_map_service__WEBPACK_IMPORTED_MODULE_4__["GoogleMapService"]();
        }
        else {
            this.mapService = new _services_openlayer_map_service__WEBPACK_IMPORTED_MODULE_3__["OpenlayerMapService"]();
        }
    }
    MapviewTabPage.prototype.swipeNext = function () {
        this.slides.slideTo(9, 500);
    };
    MapviewTabPage.prototype.swipePrevious = function () {
        this.slides.slideTo(0, 500);
    };
    MapviewTabPage.prototype.todayAlerts = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var date, data, url;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        date = new Date();
                        data = {
                            "address": "false",
                            "vin": "All",
                            "userId": localStorage.userName,
                            "companyId": localStorage.corpId,
                            "fromDate": date.toJSON().split("T")[0],
                            "toDate": date.toJSON().split("T")[0],
                        };
                        url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + "/alert/mobileAlert";
                        return [4 /*yield*/, this.ajaxService.ajaxPostWithBody(url, data)
                                .subscribe(function (res) {
                                //console.log(res);
                                if (res != "" && res != []) {
                                    _this.alertDataLength = res.length;
                                }
                                else {
                                    _this.commonService.presentToast("No alerts for this vehicle");
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MapviewTabPage.prototype.changeGridView = function (data) {
        this.selectedArrayVin = [];
        if (data) {
            if (data.count > 0) {
                localStorage.removeItem('modalFilterData');
                if (data.type === "Notransmit") {
                    this.filterValue = "No Transmission";
                    localStorage.setItem('statusChanger', "No Transmission");
                }
                else {
                    this.filterValue = data.type;
                    localStorage.setItem('statusChanger', data.type);
                }
                this.loadFilterMarker(data);
            }
            else if (data.count == 0) {
                this.commonService.presentToast('You didn\'t have any vehicle to show');
            }
        }
    };
    MapviewTabPage.prototype.openFilter = function () {
        this.loadMarkers();
    };
    MapviewTabPage.prototype.loadFilterMarker = function (status) {
        var _this = this;
        var googleMapLatLng;
        var googleMapMarkerObj;
        this.mapService.clearClusteringMarkers(this.map, [1]);
        if (status.type == "All") {
            this.loadMarkers();
        }
        else {
            var filterGrid = this.updatedJsonData.liveDatas;
            Object.values(filterGrid).forEach(function (element) {
                if (element.status == status.type) {
                    if (element.status !== "Yet_to_transmit") {
                        var latLng = {
                            "lat": element.latitude,
                            "lng": element.longitude
                        };
                        var obj = {};
                        obj["img"] = _this.getLiveTrackIcon(_this.color[element.status], _this.vehicleModel[element.icon], element.plateNo);
                        obj["plateNo"] = element.plateNo;
                        obj["status"] = element.status;
                        obj["timeStamp"] = element.timeStamp;
                        obj["lat"] = element.latitude;
                        obj["lng"] = element.longitude;
                        obj['data'] = element;
                        if (localStorage.map == "GoogleMap") {
                            googleMapLatLng.push(latLng);
                            googleMapMarkerObj.push(obj);
                        }
                        else {
                            _this.mapService.addClusteringMarkers(_this.map, latLng, obj);
                        }
                    }
                }
            });
            if (localStorage.map == "GoogleMap") {
                this.mapService.addClusteringMarkers(this.map, googleMapLatLng, googleMapMarkerObj);
            }
        }
    };
    MapviewTabPage.prototype.showModal = function (data) {
        if (!this.selectedArrayVin.includes(data.vin))
            this.selectedArrayVin.push(data.vin);
        else {
            var index = this.selectedArrayVin.indexOf(data.vin);
            if (index !== -1) {
                this.selectedArrayVin.splice(index, 1);
            }
        }
        if (this.selectedArrayVin.length > 0) {
            this.mapService.clearClusteringMarkers(this.map, [1]);
            for (var i = 0; i < this.selectedArrayVin.length; i++) {
                var element = this.updatedJsonData.liveDatas[this.selectedArrayVin[i]];
                var latLng = {
                    "lat": element.latitude,
                    "lng": element.longitude
                };
                var obj = {};
                obj["img"] = this.getLiveTrackIcon(this.color[element.status], this.vehicleModel[element.icon], element.plateNo);
                obj["plateNo"] = element.plateNo;
                obj["status"] = element.status;
                obj["timeStamp"] = element.timeStamp;
                obj["lat"] = element.latitude;
                obj["lng"] = element.longitude;
                obj['data'] = element;
                this.mapService.addClusteringMarkers(this.map, latLng, obj);
            }
            this.mapService.fitBoundsForCluster(this.map);
        }
        else {
            this.loadMarkers();
        }
    };
    MapviewTabPage.prototype.filterRefreshMap = function () {
        this.loadMarkers();
    };
    MapviewTabPage.prototype.filterDesktopModal = function () {
        this.desktopFilterModal = !this.desktopFilterModal;
    };
    MapviewTabPage.prototype.wsResponse = function (res) {
        if (res != null) {
            var hasData = res.liveDatas;
            if (hasData) {
                if (res.statusCount == null) {
                    Object.values(res['liveDatas'])[0]["odometer"] = parseInt(Object.values(res['liveDatas'])[0]["odometer"]) / 1000;
                    this.updatedJsonData = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData;
                    if (this.updatedJsonData["liveDatas"][Object.keys(res['liveDatas'])[0]]) {
                        this.updatedJsonData["liveDatas"][Object.keys(res['liveDatas'])[0]] = Object.values(res['liveDatas'])[0];
                    }
                    else {
                        return;
                    }
                    res.statusCount = this.updatedJsonData['statusCount'];
                }
                else {
                    this.updatedJsonData = res;
                    for (var i = 0; i < Object.keys(this.updatedJsonData.liveDatas).length; i++) {
                        Object.values(this.updatedJsonData.liveDatas)[i]["odometer"] = parseInt(Object.values(this.updatedJsonData.liveDatas)[i]["odometer"]) / 1000;
                    }
                }
                var data = [];
                for (var i = 0; i < Object.keys(this.updatedJsonData.liveDatas).length; i++) {
                    var currentCount = Object.values(this.updatedJsonData.liveDatas)[i];
                    data.push(currentCount.status);
                }
                data.sort();
                var current = null;
                var cnt = 0;
                for (var i = 0; i < data.length; i++) {
                    if (data[i] != current) {
                        if (cnt > 0) {
                        }
                        current = data[i];
                        cnt = 1;
                    }
                    else {
                        cnt++;
                    }
                    res.statusCount[current] = cnt;
                }
                for (var i = 0; i < Object.keys(res.statusCount).length; i++) {
                    if (!data.includes(Object.keys(res.statusCount)[i]) && Object.keys(res.statusCount)[i] != 'Total') {
                        res.statusCount[Object.keys(res.statusCount)[i]] = 0;
                    }
                }
                if (cnt > 0) {
                    res.statusCount[current] = cnt;
                }
                res['statusCount']['Total'] = Object.keys(res.liveDatas).length;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].dashboardData = this.updatedJsonData;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData = this.updatedJsonData;
                if (this.liveDatas) {
                    Object.assign(this.liveDatas, res.liveDatas);
                }
                else {
                    this.liveDatas = res.liveDatas;
                    this.initFilterRow();
                }
                res['statusCount']['Total'] = Object.keys(this.updatedJsonData.liveDatas).length;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].dashboardData = this.updatedJsonData;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData = this.updatedJsonData;
                this.updateCache();
                var updatedData = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData;
                this.vehicleCount = updatedData.statusCount;
            }
            else {
            }
        }
    };
    MapviewTabPage.prototype.wsOnError = function () {
        var _this = this;
        setTimeout(function () {
            _this.websocketService.connectSocket(JSON.parse(localStorage.dashboardWebSocketData), "livetrack");
            _this.gridCardIssueRecursion();
        }, 60000);
    };
    ;
    MapviewTabPage.prototype.gridCardIssueRecursion = function () {
        var _this = this;
        var data = {
            "companyID": localStorage.corpId,
            "branchID": localStorage.corpId,
            "emailId": localStorage.userName,
            "Check": "false",
            "entryPoint": _environments_environment__WEBPACK_IMPORTED_MODULE_8__["app"].entryPoint,
            "dashboardMode": "mobile"
        };
        var url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + "/Dashboarddata/dashboard";
        this.ajaxService.ajaxPostWithBody(url, data)
            .subscribe(function (res) {
            if (res != undefined) {
                _this.updatedJsonData = res;
                var data_1 = [];
                for (var i = 0; i < Object.keys(_this.updatedJsonData.liveDatas).length; i++) {
                    var currentCount = Object.values(_this.updatedJsonData.liveDatas)[i];
                    data_1.push(currentCount.status);
                    // }
                    Object.values(_this.updatedJsonData.liveDatas)[i]["odometer"] = parseInt(Object.values(_this.updatedJsonData.liveDatas)[i]["odometer"]) / 1000;
                }
                data_1.sort();
                var current = null;
                var cnt = 0;
                for (var i = 0; i < data_1.length; i++) {
                    if (data_1[i] != current) {
                        if (cnt > 0) {
                        }
                        current = data_1[i];
                        cnt = 1;
                    }
                    else {
                        cnt++;
                    }
                    res.statusCount[current] = cnt;
                }
                for (var i = 0; i < Object.keys(res.statusCount).length; i++) {
                    if (!data_1.includes(Object.keys(res.statusCount)[i]) && Object.keys(res.statusCount)[i] != 'Total') {
                        res.statusCount[Object.keys(res.statusCount)[i]] = 0;
                    }
                }
                if (cnt > 0) {
                    res.statusCount[current] = cnt;
                }
                res['statusCount']['Total'] = Object.keys(res.liveDatas).length;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].dashboardData = _this.updatedJsonData;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData = _this.updatedJsonData;
                if (_this.liveDatas) {
                    Object.assign(_this.liveDatas, res.liveDatas);
                }
                else {
                    _this.liveDatas = res.liveDatas;
                    _this.initFilterRow();
                }
                res['statusCount']['Total'] = Object.keys(_this.updatedJsonData.liveDatas).length;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].dashboardData = _this.updatedJsonData;
                _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData = _this.updatedJsonData;
                _this.updateCache();
                var updatedData = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].upDatedJsonData;
                _this.vehicleCount = updatedData.statusCount;
            }
            else {
                _this.commonService.dismissLoader();
                var data = navigator.onLine;
                if (data == false) {
                    _this.commonService.networkChecker();
                }
                else if (data == true) {
                    _this.ajaxService.ajaxGetWithString(_environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + "/login/test")
                        .subscribe(function (res) {
                        if (res == '["Hi Web....!"]')
                            console.log("server run");
                        else {
                            _this.commonService.dismissLoader();
                            _this.commonService.presentAlert("Server maintanance error", "Sorry for the inconvenience please try after some time");
                        }
                    });
                }
            }
        });
    };
    MapviewTabPage.prototype.initFilterRow = function () {
        if (localStorage.dataFilter) {
            var filterData = localStorage.dataFilter != "" ? JSON.parse(localStorage.dataFilter) : {};
            if (filterData.dashboardFilter) {
                var result = filterData.dashboardFilter;
                var resultObj = [];
                var countRunning = 0;
                var countNodata = 0;
                this.dataFilter = resultObj;
            }
        }
    };
    MapviewTabPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.commonService.presentLoader();
        this.selectedArrayVin = [];
        this.search = true;
        if (localStorage.map != "GoogleMap") {
            this.fabButton = true;
        }
        this.status = this.activatedRoute.snapshot.paramMap.get('type');
        if (this.status != 'Vin') {
            this.status = localStorage.statusChanger;
        }
        this.updateCache();
        setTimeout(function () {
            if (_this.map.setTarget)
                _this.map.setTarget(_this.mapElement.nativeElement);
            _this.loadMarkers();
        }, 2000);
    };
    MapviewTabPage.prototype.searchVehicle = function (plateNo) {
        var _this = this;
        this.mapService.clearClusteringMarkers(this.map, [1]);
        var liveData = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].dashboardData.liveDatas;
        Object.values(liveData).forEach(function (element) {
            if (element.plateNo.replace(/ /g, '').toLowerCase().includes(plateNo.replace(/ /g, '').toLowerCase()) || plateNo == "") {
                var latLng = {
                    "lat": element.latitude,
                    "lng": element.longitude
                };
                var obj = {};
                obj["img"] = _this.getLiveTrackIcon(_this.color[element.status], _this.vehicleModel[element.icon], element.plateNo);
                obj["plateNo"] = element.plateNo;
                obj["status"] = element.status;
                obj["timeStamp"] = element.timeStamp;
                obj["lat"] = element.latitude;
                obj["lng"] = element.longitude;
                obj['data'] = element;
                _this.mapService.addClusteringMarkers(_this.map, latLng, obj);
            }
        });
        this.mapService.fitBoundsForCluster(this.map);
    };
    MapviewTabPage.prototype.loadMarkers = function () {
        var _this = this;
        this.mapService.clearClusteringMarkers(this.map, [1]);
        var liveData;
        if (this.updatedJsonData) {
            liveData = this.updatedJsonData.liveDatas;
        }
        else {
            liveData = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["storageVariable"].dashboardData.liveDatas;
        }
        var googleMapLatLng = [];
        var googleMapMarkerObj = [];
        if (this.status != 'Vin') {
            Object.values(liveData).forEach(function (element) {
                if (element.status == _this.status || _this.status == "All") {
                    if (element.status !== "Yet_to_transmit") {
                        var latLng = {
                            "lat": element.latitude,
                            "lng": element.longitude
                        };
                        var obj = {};
                        obj["img"] = _this.getLiveTrackIcon(_this.color[element.status], _this.vehicleModel[element.icon], element.plateNo);
                        obj["plateNo"] = element.plateNo;
                        obj["status"] = element.status;
                        obj["timeStamp"] = element.timeStamp;
                        obj["lat"] = element.latitude;
                        obj["lng"] = element.longitude;
                        obj['data'] = element;
                        if (localStorage.map == "GoogleMap") {
                            googleMapLatLng.push(latLng);
                            googleMapMarkerObj.push(obj);
                        }
                        else {
                            _this.mapService.addClusteringMarkers(_this.map, latLng, obj);
                        }
                    }
                }
            });
            if (localStorage.map == "GoogleMap") {
                this.mapService.addClusteringMarkers(this.map, googleMapLatLng, googleMapMarkerObj);
            }
        }
        else if (this.status == 'Vin') {
            var element = JSON.parse(localStorage.gridData);
            for (var i = 0; i < element.length; i++) {
                if (element[i].status !== "Yet_to_transmit") {
                    var latLng = {
                        "lat": element[i].latitude,
                        "lng": element[i].longitude
                    };
                    var obj = {};
                    obj["img"] = this.getLiveTrackIcon(this.color[element[i].status], this.vehicleModel[element[i].icon], element.plateNo);
                    obj["plateNo"] = element[i].plateNo;
                    obj["status"] = element[i].status;
                    obj["timeStamp"] = element[i].timeStamp;
                    obj["lat"] = element[i].latitude;
                    obj["lng"] = element[i].longitude;
                    obj['data'] = element;
                    if (localStorage.map == "GoogleMap") {
                        googleMapLatLng.push(latLng);
                        googleMapMarkerObj.push(obj);
                    }
                    else {
                        this.mapService.addClusteringMarkers(this.map, latLng, obj);
                    }
                }
            }
            if (localStorage.map == "GoogleMap") {
                this.mapService.addClusteringMarkers(this.map, googleMapLatLng, googleMapMarkerObj);
            }
        }
        this.mapService.fitBoundsForCluster(this.map);
        this.commonService.dismissLoader();
    };
    MapviewTabPage.prototype.closeInfo = function () {
        this.markerInfo.show = false;
    };
    // async ionViewDidEnter() {
    //   this.subscription = this.platform.backButton.subscribe(async () => {
    //     if (this.menuController.isOpen()) {
    //       this.menuController.close()
    //     }
    //   });
    // }
    // ionViewWillLeave(){
    //   this.subscription.unsubscribe(); 
    // }
    MapviewTabPage.prototype.itemHeightFn = function (item, index) {
        var size = (item.icon == "REFRIGERATOR" && item.isTempSensor == false) ? 90 : (item.icon == "REFRIGERATOR" && item.isTempSensor == true) ? 140 : (item.icon != "REFRIGERATOR" && item.isTempSensor == true) ? 220 : 180;
        if (item.icon == "REFRIGERATOR" && item.isTempSensor == false) {
            if (item.location) {
                if (item.location.length >= 50 && item.location.length <= 60) {
                    size = 110;
                }
                else if (item.location.length >= 60 && item.location.length <= 110) {
                    size = 130;
                }
                else if (item.location.length >= 110 && item.location.length <= 160) {
                    size = 150;
                }
                else if (item.location.length >= 160 && item.location.length <= 280) {
                    size = 160;
                }
                else {
                    size = 103;
                }
            }
            else {
                size = 105;
            }
        }
        else if (item.icon == "REFRIGERATOR" && item.isTempSensor == true) {
            if (item.location) {
                if (item.location.length <= 50) {
                    if (item.temprature2Val) {
                        size = 180;
                    }
                    else {
                        size = 140;
                    }
                }
                else if (item.location.length >= 50 && item.location.length <= 140) {
                    if (item.temprature2Val) {
                        size = 203;
                    }
                    else {
                        size = 173;
                    }
                }
                else if (item.location.length >= 140 && item.location.length <= 210) {
                    if (item.temprature2Val) {
                        size = 210;
                    }
                    else {
                        size = 170;
                    }
                }
                else if (item.location.length >= 210 && item.location.length <= 280) {
                    if (item.temprature2Val) {
                        size = 215;
                    }
                    else {
                        size = 175;
                    }
                }
                else {
                    if (item.temprature2Val) {
                        size = 200;
                    }
                    else {
                        size = 160;
                    }
                }
            }
            else {
                if (item.temprature2Val) {
                    size = 200;
                }
                else {
                    size = 160;
                }
            }
        }
        else if (item.icon != "REFRIGERATOR" && item.isTempSensor == true) {
            if (item.location) {
                if (item.location.length <= 70) {
                    size = 220;
                }
                else if (item.location.length >= 70 && item.location.length <= 140) {
                    size = 230;
                }
                else if (item.location.length >= 140 && item.location.length <= 210) {
                    size = 240;
                }
                else if (item.location.length >= 210 && item.location.length <= 280) {
                    size = 250;
                }
                else {
                    size = 220;
                }
            }
        }
        else {
            if (item.location) {
                if (item.location.length >= 50 && item.location.length <= 60) {
                    size = 165;
                }
                else if (item.location.length >= 60 && item.location.length <= 100) {
                    size = 170;
                }
                else if (item.location.length >= 100 && item.location.length <= 160) {
                    size = 180;
                }
                else if (item.location.length >= 160 && item.location.length <= 280) {
                    size = 192;
                }
                else {
                    size = 150;
                }
            }
            else {
                size = 163;
            }
        }
        return size;
    };
    MapviewTabPage.prototype.switchMap = function () {
        this.layerModal = !this.layerModal;
        if (this.layerModal) {
            this.map.getLayers().item(3).setVisible(this.layerModal);
            this.map.getLayers().item(0).setVisible(!this.layerModal);
        }
        else {
            this.map.getLayers().item(3).setVisible(this.layerModal);
            this.map.getLayers().item(0).setVisible(!this.layerModal);
        }
    };
    MapviewTabPage.prototype.showLocation = function (data) {
        var url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + "/login/company/latlngtoaddress/" + data.latitude + "/" + data.longitude + "/" + localStorage.corpId;
        this.ajaxService.ajaxGetObject(url)
            .subscribe(function (res) {
            if (JSON.stringify(res) == "{}") {
                data.location = data.latitude + ',' + data.longitude;
            }
            else {
                data.location = res;
            }
        });
    };
    MapviewTabPage.prototype.bellRouter = function () {
        this.router.navigateByUrl('/tabs/alerts/All');
    };
    MapviewTabPage.prototype.ngOnInit = function () {
        var _this = this;
        this.app["logo"] = localStorage.companyLogo;
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        if (/(iPhone|iPad|iPod)/i.test(navigator.userAgent)) {
            this.isIosPlatform = true;
        }
        else {
            this.isIosPlatform = false;
        }
        if (this.myPlatform == "desktop") {
            this.websocketService.setProductService(this);
            this.websocketService.reSendRequest(JSON.parse(localStorage.dashboardWebSocketData));
        }
        this.loginData = Object.keys(JSON.parse(localStorage.loginData)[0]);
        this.popUp = function (res) {
            _this.searchInput = res.plateNo;
            _this.clickMarker = res.data;
            _this.markerInfo.show = true;
            _this.markerInfo["status"] = res.status;
            _this.markerInfo["plateNo"] = res.plateNo;
            _this.markerInfo["timeStamp"] = res.timeStamp;
        };
        this.loadMap();
        this.todayAlerts();
        // this.loadMap();
    };
    MapviewTabPage.ctorParameters = function () { return [
        { type: _services_auth_map_service__WEBPACK_IMPORTED_MODULE_2__["AuthMapService"] },
        { type: _services_google_map_service__WEBPACK_IMPORTED_MODULE_4__["GoogleMapService"] },
        { type: _services_openlayer_map_service__WEBPACK_IMPORTED_MODULE_3__["OpenlayerMapService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["Platform"] },
        { type: _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
        { type: _services_websocket_service__WEBPACK_IMPORTED_MODULE_10__["WebsocketService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mySlider', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonSlides"])
    ], MapviewTabPage.prototype, "slides", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mapElement', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MapviewTabPage.prototype, "mapElement", void 0);
    MapviewTabPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mapview-tab',
            template: __webpack_require__(/*! raw-loader!./mapview-tab.page.html */ "./node_modules/raw-loader/index.js!./src/app/mapview-tab/mapview-tab.page.html"),
            styles: [__webpack_require__(/*! ./mapview-tab.page.scss */ "./src/app/mapview-tab/mapview-tab.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_map_service__WEBPACK_IMPORTED_MODULE_2__["AuthMapService"],
            _services_google_map_service__WEBPACK_IMPORTED_MODULE_4__["GoogleMapService"],
            _services_openlayer_map_service__WEBPACK_IMPORTED_MODULE_3__["OpenlayerMapService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["Platform"],
            _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
            _services_websocket_service__WEBPACK_IMPORTED_MODULE_10__["WebsocketService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], MapviewTabPage);
    return MapviewTabPage;
}());



/***/ }),

/***/ "./src/app/services/map-view-filter.pipe.ts":
/*!**************************************************!*\
  !*** ./src/app/services/map-view-filter.pipe.ts ***!
  \**************************************************/
/*! exports provided: MapViewFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapViewFilterPipe", function() { return MapViewFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MapViewFilterPipe = /** @class */ (function () {
    function MapViewFilterPipe() {
    }
    MapViewFilterPipe.prototype.transform = function (items, terms, count) {
        if (!items)
            return [];
        if (terms) {
            var data = void 0;
            if (terms !== "All" && terms !== "Vin") {
                data = items.filter(function (it) {
                    return it.status === terms;
                });
            }
            else if (terms === "All") {
                data = items;
            }
            localStorage.setItem('maxLength', data.length.toString());
            var data1 = [];
            for (var i = 0; i < count; i++) {
                if (data[i]) {
                    data1.push(data[i]);
                }
                else {
                    break;
                }
            }
            if (localStorage.gridDataLength !== data1.length.toString()) {
                localStorage.setItem('updateGridData', 'true');
            }
            localStorage.setItem('gridDataLength', data1.length.toString());
            localStorage.setItem('gridData', JSON.stringify(data1));
            return data1;
        }
    };
    MapViewFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'mapViewFilter'
        })
    ], MapViewFilterPipe);
    return MapViewFilterPipe;
}());



/***/ }),

/***/ "./src/app/services/map-view-search.pipe.ts":
/*!**************************************************!*\
  !*** ./src/app/services/map-view-search.pipe.ts ***!
  \**************************************************/
/*! exports provided: MapViewSearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapViewSearchPipe", function() { return MapViewSearchPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MapViewSearchPipe = /** @class */ (function () {
    function MapViewSearchPipe() {
    }
    MapViewSearchPipe.prototype.transform = function (items, terms) {
        if (!items)
            return [];
        if (!terms)
            return items;
        terms = terms.toLowerCase();
        return items.filter(function (it) {
            if (it.plateNo != null)
                return it.plateNo.replace(/ /g, '').toLowerCase().includes(terms.replace(/ /g, ''));
            else
                return false;
        });
    };
    MapViewSearchPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'mapViewSearch'
        })
    ], MapViewSearchPipe);
    return MapViewSearchPipe;
}());



/***/ })

}]);
//# sourceMappingURL=mapview-tab-mapview-tab-module-es5.js.map