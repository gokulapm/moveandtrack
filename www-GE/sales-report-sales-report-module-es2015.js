(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sales-report-sales-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/sales-report/sales-card/sales-card.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/sales-report/sales-card/sales-card.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-row>\n  <ion-col size=12>\n    <ion-row style=\"padding-left: 92%;\" >\n      <div class=\"pdf-wrapper\" (click)=\"createPdf()\"></div>\n  \n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngFor=\"let sales of sales\">\n        <ion-card class=\"status\"\n           class=\"card-wrapper validBorder\" >\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-row>\n                <ion-col size=6 style=\"align-self: center;\">\n                  <ion-row>\n                    <ion-col size=\"12\" class=\"align-dataCol\">\n                      <ion-row>\n                        <ion-col size=\"12\">\n                          {{sales.ImeiNo}}\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n\n                </ion-col>\n                <ion-col size=6>\n                  <ion-row>\n                    <ion-col size=\"12\" style=\"margin: 0 0px 0px 2px;left: 50%; font-size: 11px;\">\n                      Expiry on\n                    </ion-col>\n                    <ion-col size=\"12\" style=\"margin: 0 0px 0px 2px;margin-top:0px\">\n                      <ion-label class=\"status\"\n                        [ngClass]=\"{'valid' : sales.status == 'Expired', 'inValid' : sales.status == 'Not Expired'}\">\n                        {{sales.WarrantyExpiryDate}}</ion-label>\n\n                    </ion-col>\n                  </ion-row>\n\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=12>\n                  <ion-row>\n                    <ion-col size=\"12\" class=\"align-dataCol\">\n                      <ion-row>\n                        <ion-col size=\"12\" style=\"font-size:large;font-weight: bold;color: black;margin-top: -15px;\">\n                          {{sales.PlateNo}}\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=6 style=\"margin-top: -10px;\">\n                  <ion-row>\n                    <ion-col size=\"2\" class=\"icon_col\">\n                      <ion-icon name=\"business\" class=\"common-icon\"></ion-icon>\n                    </ion-col>\n                    <ion-col size=\"10\" class=\"align-dataCol\">\n                      <ion-row>\n                        <ion-col size=\"12\">\n                          {{sales.CompanyName}}\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n\n                </ion-col>\n                <ion-col size=6 style=\"margin-top: -10px;\">\n                  <ion-row>\n                    <ion-col size=\"2\" class=\"icon_col\">\n                      <ion-icon name=\"call\" class=\"common-icon\"></ion-icon>\n                    </ion-col>\n                    <ion-col size=\"10\" class=\"align-dataCol\">\n                      <ion-row>\n                        <ion-col size=\"12\">\n                          {{sales.ContactNo}}\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=12 style=\"margin-top: -5px;\">\n                  <ion-row>\n                    <ion-col size=\"1\" class=\"icon_col\">\n                      <ion-icon name=\"mail\" class=\"common-icon\"></ion-icon>\n                    </ion-col>\n                    <ion-col size=\"11\" class=\"align-dataCol\">\n                      <ion-row>\n                        <ion-col size=\"12\">\n                          {{sales.Fax}}\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                  </ion-row>\n\n                </ion-col>\n\n              </ion-row>\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-col>\n</ion-row>\n<ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"doInfinite($event)\">\n  <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n  </ion-infinite-scroll-content>\n</ion-infinite-scroll>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/sales-report/sales-report.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/sales-report/sales-report.page.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"dealerHeader\">\n    <ion-title>Sales report</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"padding25px\">\n    <ion-row>\n  \n      <ion-col size=\"6\">\n        <ion-row class=\"reportInfo paddingBottom10px\">\n          From Date\n        </ion-row>\n        <ion-row class=\"paddingBottom10px\">\n          <ion-datetime class=\"datetime\" [(ngModel)]=\"subscription.fromDate\" displayFormat=\"D MMM YYYY\"\n            placeholder=\"From date\"></ion-datetime>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-row class=\"reportInfo paddingBottom10px\">\n          To Date\n        </ion-row>\n        <ion-row class=\"paddingBottom10px\">\n          <ion-datetime class=\"datetime\"  [(ngModel)]=\"subscription.toDate\" displayFormat=\"D MMM YYYY\"\n            placeholder=\"To date\"></ion-datetime>\n        </ion-row>\n       \n      </ion-col>\n    </ion-row>\n    <ion-row >\n      <ion-button class=\"buttonWidth\" (click)='submit()'>Submit</ion-button>\n  </ion-row>\n  </div>\n  <ion-row *ngIf=\"reportData\" style=\"width: 100%; height: 100%;\">\n    <app-sales-card [reportData]=\"reportData\"></app-sales-card>\n  </ion-row>\n  \n  </ion-content>\n"

/***/ }),

/***/ "./src/app/delar-application/sales-report/sales-card/sales-card.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/delar-application/sales-report/sales-card/sales-card.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".valid {\n  color: red;\n}\n\n.inValid {\n  color: orange;\n}\n\n.inValidBorder {\n  border-left: 3px solid #7c68f8;\n}\n\n.validBorder {\n  border-left: 3px solid #7c68f8;\n}\n\n* {\n  margin: 0;\n  padding: 0.9px;\n}\n\n.card-wrapper {\n  font-size: 11px;\n  font-weight: initial;\n  margin: 5px 10px;\n  padding: 1px 0px 1px 4px;\n}\n\n.item-native {\n  padding: 0px;\n}\n\n.svg-size1 {\n  width: 25px !important;\n}\n\n.svg-size2, .svg-size1 {\n  width: 21px;\n  border: 1px solid #f1f1f1;\n  padding: 2px;\n}\n\n.companyName {\n  background-color: #09d8c5;\n  text-align: center;\n  padding: 5px;\n}\n\nion-toolbar {\n  background-color: #7c68f8;\n}\n\n.common-icon {\n  color: #7c68f8;\n  font-weight: bold;\n}\n\n.icon_col {\n  margin: 2px 0px;\n  display: grid;\n  place-content: center;\n}\n\n.status {\n  padding: 3px 0px 3px 0px;\n  font-size: 12px;\n  margin: 5px 0;\n  background-color: #e2dddd;\n  border-radius: 11px;\n  float: right;\n  width: 65%;\n  text-align: center;\n}\n\n.header-company {\n  font-weight: bold;\n  padding: 8px 0px 8px 3px;\n  font-size: 13px;\n  color: black;\n}\n\n.align-dataCol {\n  padding-left: 5px;\n  font-family: sans-serif;\n  color: #848484;\n  font-size: 12px;\n}\n\n.underline {\n  font-size: 11px;\n  color: #656565;\n  position: relative;\n  font-weight: bold;\n}\n\n.underline:before {\n  margin: auto;\n  position: absolute;\n  content: \"\";\n  height: 2px;\n  bottom: 13px;\n  width: 13px;\n  left: 2px;\n}\n\n.image-size {\n  height: 35px;\n  width: 35px;\n  cursor: pointer;\n  display: inline-block;\n  margin: 5px;\n}\n\n.download-wrapper {\n  padding: 6px;\n  background: #eaeaea;\n}\n\n.download-wrapper:hover {\n  background-color: #f6f6f6;\n}\n\n.subscription-title {\n  font-size: 20px;\n}\n\n.pdf-wrapper, .excel-wrapper {\n  background: url('pdf.svg') no-repeat;\n  width: 30px;\n  height: 30px;\n}\n\n.excel-wrapper {\n  background: url('excelNew.svg') no-repeat;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc2FsZXMtcmVwb3J0L3NhbGVzLWNhcmQvc2FsZXMtY2FyZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc2FsZXMtcmVwb3J0L3NhbGVzLWNhcmQvc2FsZXMtY2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7QUNDSjs7QURFQztFQUNHLGFBQUE7QUNDSjs7QURFQztFQUNHLDhCQUFBO0FDQ0o7O0FEQ0M7RUFDRyw4QkFBQTtBQ0VKOztBREFDO0VBQ0csU0FBQTtFQUNBLGNBQUE7QUNHSjs7QURBQztFQUNFLGVBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0Esd0JBQUE7QUNHSDs7QURDQztFQUNFLFlBQUE7QUNFSDs7QURBQztFQUNFLHNCQUFBO0FDR0g7O0FEREM7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDSUg7O0FERkM7RUFDRSx5QkFBQTtFQUVBLGtCQUFBO0VBQ0EsWUFBQTtBQ0lIOztBREZDO0VBQ0UseUJBQUE7QUNLSDs7QURIQztFQUVFLGNBQUE7RUFHQSxpQkFBQTtBQ0dIOztBRERDO0VBQ0UsZUFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtBQ0lIOztBRENDO0VBVUUsd0JBQUE7RUFDQyxlQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUVBLGtCQUFBO0FDUko7O0FEVUM7RUFFRSxpQkFBQTtFQUNBLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNSSDs7QURVQztFQUNFLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQyxjQUFBO0VBQ0EsZUFBQTtBQ1BKOztBRFVDO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDUEg7O0FEVUM7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBRUEsU0FBQTtBQ1JIOztBRFVDO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFFQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FDUkg7O0FEVUM7RUFDRSxZQUFBO0VBQ0EsbUJBQUE7QUNQSDs7QURTQztFQUNDLHlCQUFBO0FDTkY7O0FEUUM7RUFDRSxlQUFBO0FDTEg7O0FET0M7RUFDRSxvQ0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDSkg7O0FET0M7RUFDRSx5Q0FBQTtBQ0pIIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc2FsZXMtcmVwb3J0L3NhbGVzLWNhcmQvc2FsZXMtY2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi52YWxpZHtcbiAgICBjb2xvcjogcmVkO1xuICAgIFxuIH1cbiAuaW5WYWxpZHtcbiAgICBjb2xvcjogb3JhbmdlO1xuICAgXG4gfVxuIC5pblZhbGlkQm9yZGVye1xuICAgIGJvcmRlci1sZWZ0OjNweCBzb2xpZCAjN2M2OGY4O1xuIH1cbiAudmFsaWRCb3JkZXJ7XG4gICAgYm9yZGVyLWxlZnQ6M3B4IHNvbGlkIzdjNjhmODtcbiB9XG4gKnsgXG4gICAgbWFyZ2luOiAwO1xuICAgIHBhZGRpbmc6IDAuOXB4O1xuIH1cbiBcbiAuY2FyZC13cmFwcGVye1xuICAgZm9udC1zaXplOiAxMXB4O1xuICAgZm9udC13ZWlnaHQ6IGluaXRpYWw7XG4gICBtYXJnaW46NXB4IDEwcHg7XG4gICBwYWRkaW5nOiAxcHggMHB4IDFweCA0cHg7XG4gXG4gICAvLyBjb2xvcjogIzdjNjhmODtcbiB9XG4gLml0ZW0tbmF0aXZlIHtcbiAgIHBhZGRpbmc6MHB4O1xuIH1cbiAuc3ZnLXNpemUxe1xuICAgd2lkdGg6MjVweCAhaW1wb3J0YW50O1xuIH1cbiAuc3ZnLXNpemUyLC5zdmctc2l6ZTF7XG4gICB3aWR0aDogMjFweDtcbiAgIGJvcmRlcjogMXB4IHNvbGlkICNmMWYxZjE7XG4gICBwYWRkaW5nOiAycHg7XG4gfVxuIC5jb21wYW55TmFtZXtcbiAgIGJhY2tncm91bmQtY29sb3I6ICMwOWQ4YzU7XG4gICAvLyBjb2xvcjogd2hpdGU7XG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICBwYWRkaW5nOiA1cHg7XG4gfVxuIGlvbi10b29sYmFye1xuICAgYmFja2dyb3VuZC1jb2xvcjojN2M2OGY4OyBcbiB9XG4gLmNvbW1vbi1pY29ue1xuICAgLy8gZm9udC1zaXplOiAxNnB4O1xuICAgY29sb3I6ICM3YzY4Zjg7XG4gICAvLyBib3JkZXI6MXB4IHNvbGlkICNmMWYxZjE7XG4gICAvLyBwYWRkaW5nOiAycHg7XG4gICBmb250LXdlaWdodDogYm9sZDtcbiB9XG4gLmljb25fY29se1xuICAgbWFyZ2luOjJweCAwcHg7XG4gICBkaXNwbGF5OiBncmlkO1xuICAgcGxhY2UtY29udGVudDogY2VudGVyO1xuICAgXG4gfVxuIFxuIFxuIC5zdGF0dXN7XG4gICAvLyBwYWRkaW5nOiA0cHggMHB4IDRweCAzMHB4O1xuICAgLy8gZm9udC1zaXplOiAxMnB4O1xuICAgLy8gbWFyZ2luOiA1cHggMDtcbiAgIC8vIGJhY2tncm91bmQtY29sb3I6ICAjYjNiM2IzO1xuICAgLy8gYm9yZGVyLXJhZGl1czogMTFweDtcbiAgIFxuICAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAgLy8gd2lkdGg6IDk4JTtcbiAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICNiM2IzYjM7XG4gICBwYWRkaW5nOiAzcHggMHB4IDNweCAwcHg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIG1hcmdpbjogNXB4IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2UyZGRkZDtcbiAgICBib3JkZXItcmFkaXVzOiAxMXB4O1xuICAgIGZsb2F0OiByaWdodDtcbiAgICB3aWR0aDogNjUlO1xuICAgLy8gIGNvbG9yOiBibGFjaztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gfVxuIC5oZWFkZXItY29tcGFueXtcbiAgIC8vYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmNmY2ZjY7XG4gICBmb250LXdlaWdodDogYm9sZDtcbiAgIHBhZGRpbmc6IDhweCAwcHggOHB4IDNweDtcbiAgIGZvbnQtc2l6ZTogMTNweDtcbiAgIGNvbG9yOiBibGFjaztcbiB9XG4gLmFsaWduLWRhdGFDb2x7XG4gICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xuICAgIGNvbG9yOiAjODQ4NDg0O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiBcbiB9XG4gLnVuZGVybGluZSB7XG4gICBmb250LXNpemU6IDExcHg7XG4gICBjb2xvcjogIzY1NjU2NTtcbiAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuIH0gICBcbiBcbiAudW5kZXJsaW5lOmJlZm9yZSB7XG4gICBtYXJnaW46IGF1dG87XG4gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICBjb250ZW50OiBcIlwiO1xuICAgaGVpZ2h0OiAycHg7XG4gICBib3R0b206IDEzcHg7XG4gICB3aWR0aDogMTNweDtcbiAgIC8vIGJhY2tncm91bmQ6I2ZmZDMxYTtcbiAgIGxlZnQ6IDJweDtcbiB9XG4gLmltYWdlLXNpemUge1xuICAgaGVpZ2h0OiAzNXB4O1xuICAgd2lkdGg6IDM1cHg7XG4gICBcbiAgIGN1cnNvcjogcG9pbnRlcjtcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgIG1hcmdpbjogNXB4O1xuIH1cbiAuZG93bmxvYWQtd3JhcHBlciB7XG4gICBwYWRkaW5nOiA2cHg7XG4gICBiYWNrZ3JvdW5kOiAjZWFlYWVhO1xuIH1cbiAuZG93bmxvYWQtd3JhcHBlcjpob3ZlcntcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y2ZjZmNjtcbiB9XG4gLnN1YnNjcmlwdGlvbi10aXRsZXtcbiAgIGZvbnQtc2l6ZTogMjBweDtcbiB9XG4gLnBkZi13cmFwcGVyLC5leGNlbC13cmFwcGVye1xuICAgYmFja2dyb3VuZDp1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvcGRmLnN2Z1wiKW5vLXJlcGVhdDtcbiAgIHdpZHRoOjMwcHg7XG4gICBoZWlnaHQ6IDMwcHg7XG4gXG4gfVxuIC5leGNlbC13cmFwcGVye1xuICAgYmFja2dyb3VuZDp1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvZXhjZWxOZXcuc3ZnXCIpbm8tcmVwZWF0O1xuIH1cbiAiLCIudmFsaWQge1xuICBjb2xvcjogcmVkO1xufVxuXG4uaW5WYWxpZCB7XG4gIGNvbG9yOiBvcmFuZ2U7XG59XG5cbi5pblZhbGlkQm9yZGVyIHtcbiAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCAjN2M2OGY4O1xufVxuXG4udmFsaWRCb3JkZXIge1xuICBib3JkZXItbGVmdDogM3B4IHNvbGlkICM3YzY4Zjg7XG59XG5cbioge1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDAuOXB4O1xufVxuXG4uY2FyZC13cmFwcGVyIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBmb250LXdlaWdodDogaW5pdGlhbDtcbiAgbWFyZ2luOiA1cHggMTBweDtcbiAgcGFkZGluZzogMXB4IDBweCAxcHggNHB4O1xufVxuXG4uaXRlbS1uYXRpdmUge1xuICBwYWRkaW5nOiAwcHg7XG59XG5cbi5zdmctc2l6ZTEge1xuICB3aWR0aDogMjVweCAhaW1wb3J0YW50O1xufVxuXG4uc3ZnLXNpemUyLCAuc3ZnLXNpemUxIHtcbiAgd2lkdGg6IDIxcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmMWYxZjE7XG4gIHBhZGRpbmc6IDJweDtcbn1cblxuLmNvbXBhbnlOYW1lIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA5ZDhjNTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzdjNjhmODtcbn1cblxuLmNvbW1vbi1pY29uIHtcbiAgY29sb3I6ICM3YzY4Zjg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uaWNvbl9jb2wge1xuICBtYXJnaW46IDJweCAwcHg7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIHBsYWNlLWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLnN0YXR1cyB7XG4gIHBhZGRpbmc6IDNweCAwcHggM3B4IDBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW46IDVweCAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTJkZGRkO1xuICBib3JkZXItcmFkaXVzOiAxMXB4O1xuICBmbG9hdDogcmlnaHQ7XG4gIHdpZHRoOiA2NSU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmhlYWRlci1jb21wYW55IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmc6IDhweCAwcHggOHB4IDNweDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5hbGlnbi1kYXRhQ29sIHtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xuICBjb2xvcjogIzg0ODQ4NDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4udW5kZXJsaW5lIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBjb2xvcjogIzY1NjU2NTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnVuZGVybGluZTpiZWZvcmUge1xuICBtYXJnaW46IGF1dG87XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29udGVudDogXCJcIjtcbiAgaGVpZ2h0OiAycHg7XG4gIGJvdHRvbTogMTNweDtcbiAgd2lkdGg6IDEzcHg7XG4gIGxlZnQ6IDJweDtcbn1cblxuLmltYWdlLXNpemUge1xuICBoZWlnaHQ6IDM1cHg7XG4gIHdpZHRoOiAzNXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiA1cHg7XG59XG5cbi5kb3dubG9hZC13cmFwcGVyIHtcbiAgcGFkZGluZzogNnB4O1xuICBiYWNrZ3JvdW5kOiAjZWFlYWVhO1xufVxuXG4uZG93bmxvYWQtd3JhcHBlcjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XG59XG5cbi5zdWJzY3JpcHRpb24tdGl0bGUge1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5wZGYtd3JhcHBlciwgLmV4Y2VsLXdyYXBwZXIge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvcGRmLnN2Z1wiKSBuby1yZXBlYXQ7XG4gIHdpZHRoOiAzMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG59XG5cbi5leGNlbC13cmFwcGVyIHtcbiAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2V4Y2VsTmV3LnN2Z1wiKSBuby1yZXBlYXQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/delar-application/sales-report/sales-card/sales-card.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/delar-application/sales-report/sales-card/sales-card.component.ts ***!
  \***********************************************************************************/
/*! exports provided: SalesCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesCardComponent", function() { return SalesCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/export-excel.service */ "./src/app/services/export-excel.service.ts");
/* harmony import */ var _subscription_renewal_renewal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../subscription/renewal/renewal.page */ "./src/app/delar-application/subscription/renewal/renewal.page.ts");







let SalesCardComponent = class SalesCardComponent {
    constructor(ete, modalController, platform, commonService) {
        this.ete = ete;
        this.modalController = modalController;
        this.platform = platform;
        this.commonService = commonService;
        this.titles = 'jspdf-autotable-demo';
        this.title = 'angular-export-to-excel';
        this.head = ['SNo', 'Company Id', 'Company Name', 'Initial Transmission', 'Last Transmission', 'Vin', 'Plate No', 'Imei No', 'Email', 'Imei No', 'SimCard No', 'Status', 'Suffix', 'Warranty ExpiryDate'];
        this.sales = [];
        this.count = 12;
        this.currentPage = 1;
        this.exportTitle = [];
        this.pdfdatas = [];
    }
    updateSubscription(data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _subscription_renewal_renewal_page__WEBPACK_IMPORTED_MODULE_6__["RenewalPage"],
                cssClass: 'renewalPage',
                componentProps: {
                    value: data
                }
            });
            // modal.onDidDismiss().then(()=>{})
            return yield modal.present();
        });
    }
    exportToExcel() {
        let reportData = {
            title: 'Subscription',
            data: this.pdfdatas,
            headers: this.head
        };
        this.ete.exportExcel(reportData);
        console.log("Export Excel");
    }
    createPdf() {
        const doc = new jspdf__WEBPACK_IMPORTED_MODULE_3__["default"]('landscape', "px", 'a1');
        let imgdata = "data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAMoAAABKCAYAAAD61ctwAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAACHDwAAjA8AAP1SAACBQAAAfXkAAOmLAAA85QAAGcxzPIV3AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wUDDSMazUL+WgAAPB1JREFUeNrtnXeYZUXR/z/Vfc4Nk3dnc15gEzksQRZYliRJRUBERCSJiIrygqCCmEAxElRU9FXgVVAQEREQQQQl52Vh2cCyOaeZ2Zk7N5zT9fujz525c2dmdxF4efG33+c5zw3ndJ9O1VXVXVUN2/C2ouaqJ6n/5j+Hjjjn5umoMnnGZe90kbbh34B5pwvwn444naFYW3+AKZU+vcce5zZsa/F3J7Z129uIzPeeQV2cQvU4UywcVGxsmiryTpdqG/4dbCOUtxG2WMDm8zvWFHPTRuXW18ap1HGtw0bIhKO+8U4XbRveIIJ3ugD/qRhw0d0U0hnC1pZjgtrssIM7l+h94YhDOzoL4+JszcItpX9pwkSAQJCPA1MA1+eDggDtiP4D3JOIFHeZM59ZO0wEqEXkXGB4VXoD/APknl3mz2HWxEmgZMXwCYyOEYPDgBhFDAbDY5I3d2rGMeW5+e90074j2EYobxNsVKJu0YLBhcamo0vZmnDHzrW6JJUZ88DAEYeaqPjLEaf/nBU3fnKL2QAnAEcBUCG2eRFOu/8Tzhcx16nR776884ROzQOQBc4Eduwj7+HOxfe9tMNEJ4GCMkSEz2HYTgzgiYTkew2avVOCTe90s75j2CZ6vQ2oOfPbGBcjqge4MD3FgclEBXNoy7x0fcj71w4ZVeMydZvNo2KQ+pndVlxB+aLyGigBXzSGU40JehBVPxhnrNSJgAnAhIyQkEEmVCRUTAjl7xKCZEuY/4+n1W2E8jag0Q4nu3ZVqCIfiNPpBlsqiYLs1rrU7pxbvZem03tEmexm8zA2uSqJwfpPU/G9TDQmUMRqRgLOljAaZFLad8aSEGGgIyWgWcKuPMZLoHVSRSAm9Hmn04X/r+WPbYTyNsBEMS5MTXZh6gDAmmIBVLUmLpjDV700aADFY9deczx1X3mw/0wCf/VBEE6stkigG8Vqqet+93NTxOoUk6rKr0wgtpwPA02oI02omBRIqBNMqKYH4VUQC+m0//z/FNsI5S3GuJOvo3PIUNSYI10YjralIrZYBFXUCFM3zgv3bFt8mFw7b2RU07/4ZcqDNSiLXF0EsQqjH0V4r1g9SwKd3fVcoIilTgImSJDoL+KJKBn4zoSoJwCtlZDxJgSnsTEBO3iRSzEhTkJVE2jCXQCNtxHKNryFiCKa5rw8yAXhMUDKFAvYUgHBQQCN2iFHrHpm0sDC+umjCxuwP57dZzZ+4LseopXx3KAgoc6U0D1j0vyPsfojsbguggpUxOoQsZ57dBGR5wwbJdRNJgQJ1ZhAJ5iUYjPUSqjbVegla0xI0YTJu0NFg8gTzP+n2EYobyG2P/oKNAzRVHo/NXY3UypiiwVMsYixDgkVUsK+G16pP2D9y8fOP+KoTFzb2GdeCXeoVtjLHEZMl2LPIgm0VL5v/JWWAK+EJ1wiUf7XmVCXlZV1CXVCUSMxAc0m0BHlBQIT6FwJtdClrwSKCdKY0L3BFvnPwTZCeQthXUTj/NmBGvt+1DXZQgFTKGJLRYxxYkI/mJtdK0eteHRa08tzdhyZW9lnXl2Ku+1S1OkeyBWEE+owCQi7xKuE65TFJlNJLKF2SKCvl/UPEzI+XWNqkhWv5uSZooTMM4Fq14pXCJIqbuMo2/AWwQbkBw+fABxqE25iiwVMqdiRklJbedARwtQNL406btH9Ry779FT4fUevrEwlQVRyFqsigaZMRtMS6ASxfEhsWQkHCVAJWCOhIimQSmIJtCghc5PvmFCH21CbJdBxxussmFA7JNQFEiJlsc2EiqR9mncD2oEcEIHEQPwW5PmGFvyWDNkNUBFbkwJrNNaYYrGEiI7Z+Mw73T7vKCbPuJjOxoGk2ja+V+JonPUrXaBKIQjuHhmv30DIp00yoJtdizly5SPHDPvZizfsvPKpdS9XZ2j9oJTuDcXy7yGI/NQrPUxEGCc9Nx43AfNUEzGta7ULEFWxzBNDJEYDDAMRRptAd0Aw/hnWidXlYpDy7jwGpBRCWHinm3mrkQVi2A44Bhgbe9pZDrwGzFNYaaCUA+q2Ir+tIpSlo6aD04wEsi8iR2OYhJASJUdNajmOx5bWTv+HqZe1bgOMXv3wO91O//uwGbKrVwxw6fRxtlQ0fuNcEec6cmpuHiyb1ufC1MkS0CyJYr5Tx/zdLpp7w/5T2l7/80m/eJXcJ6Z0ZSddhCLdq1eeIGqAI8r/+WfKiQB4RZXZaCKKGeke7AYjVhdiaBPDQDHUIjpJAnbwy8cKhiVidb0IIjbZ9BTFZorE7yLRK+F9yxQeExgJfARPOAVgqcDjCn/IwCMxbIqB1Gby2yyhLBt+GNREUJIdSOklEuiJhHGThApGfWliQUvmXCmZp7RTrnCxeXDJyBluzPJ/vNNt9b+GyYdfiqiCMM2USlPLnAR1iHNPGLWPNZIrdIapf4jVE8siVQPttQdvfOr4X0848b6fzP5m6YyKPCXpGRHtmyAS+vHftfx/EeUmY2SDy3sxz3QRiYJBxOgKMazBMFAsgugeYstmKyCiCyTQDhERjCacqLw39O4QvSo4RKEAzzl4PoQbBE4FTgcmJNeJAn8Hvi/wWAlcf3NBvzrKosGHYpsjiGVfybjfmvrobNNcaLJDOjFDc9ihOczQHGZIJ2ZQPiVNxQOlJr7ZNsVnidpg2dhD3un2+l+DOFB1aeAUUVdrolKinxQjWyjc0ti2btOapsaiCfVWE5IvK8k2UIa59UdcsuAXu8xofapHnj1237t26LXvyz+3TizfwepvVLRy7yT5BBOoSKAbJGRJ99Iv+5lQx0n3nsk8E2rUpdsk/6fDFPIu0VEqkQayfkpf4OAbCu8D/huvytQC7wduM/A58WyhT/RJKIuGzyBscsQdsodk3Q3SWNrHDOrEDMlhhuTUDu7cZAZ1LrGDO1eZIbmSHZLDDO7EDCwMNTXR90xddFrr4kaWjp3xTrfT/w5EEJF9gPcCiComijDFwiwpFP769G0bZMZn/2CMdQ9LqM8lS7CeWEI3PBWUTk0PaJMV547ozjNwXfsopidB5MTyVzHcKZY7xXCLGP26iH5ARb8pho6xty9CUuqvoHLVCxFLpwn1tQpTlV0lYGhiqlIygc6ToNuMpUwseVvEvItEr2oEQOgJ5mWFzwDnAHOT28OAbwlcDtTF/aTvBVsDriTDTDb6rjSUdjUD8pjGIlJbWiWB+y1G7wZdBmREZS9qzDlSE73HpWLjrGukha83TtwwTwJ9dNnYGYxa/J8rhk0+9FJUXSBiTgMGVt4zcXz7+yetXnnn1Z37vqfm6eb14wbfO3jN2tvFsr8EKuXlXwk4gVL6V4R06fTG+k/p0kW6RLA1oOeJYymCUasuFiJRGPvbpV3vLi/lGukheoESS6hzu0Qto6mK763AQgm1UZIdfS+OgWbNu5KjVCMASpC3cGsMswW+DxwKZIALBAoKV0ZQrCSOXhxl6bgZlBZmxaTj86Q2OtQMyGMG5jFNhZlSW/qoNuUvtmPNI8GEQIMJwXIzxNwstcWTpKF4i2nOqxmQRxqKoyQTX6oFM4D0f/YmlYggYnbHs/RKLFLVO7+36EjGLF388Y3rzWf3HPdMNhVEf5ZQXzMVXEVCHWNC/Wi011pWf8Fzle5l4V6WwioBJVJEo363pDj6t0ujcb9Z2oNIfPryjny3GYrpNlGZ373XUrkpyRoJWGVCRJI9ny7OYo3fMP0PQIjX6AVmqndDuAPPbVLAfwl82ACVYlgvjmJCJTU+t7uk4zNNQ1FMYwHTUFwgmehTWrRPpLYbCtamiEpfAv5qs3pHlCuskFTxCxK4wSjvJTJo0RxKZI6XTPzfS8cewujFD73T7dMv5s6ZA4CKlPVlQ1llli7PD1duu8kTJwIw+bBLiYhMQPAxYGhVtne/WrxyzqPRnpPCvDu62L6+uXnJTQcMrf/1Ayujs/4slgsrdsIRy0mZ+c03kvLigAT+pWK6FPXuT93ygDWhJum7l4c9d1KRgMViaMHo0C5zfquIsDguaouESa1N9wKCZpV3gxtz5JutxsCuQKPCemClwhoDJfBOPhm69leWKpwvvs+Px68FfFnhWYFXy/n2IJQlY6czYt7DrJgy/WSpiUZKQxHTUCpINrqKtponaG5F6moB2UU0fRxIrTPy52CPhlI0c+EqMtFXjcrOWrAjTWcQukJwumtN3WnS8Ya3s3HmzpuHGEOpWDTWmLSIZIAsIhlVrRWReqAer7z1dWWAjPg2dEAHqq3AalVdLbBWYVUhn3/dGNMlwgpCQLAj8MGqIq13KrdeVjqBg3JLdgugLXBr1zSG8ZQnJ333ASN6u1g+LgGDEjMUJNDtRPiIM8HXNn5lOLnlyUDvYxl4a+Z1G/jnpNtTMSF/FQlYLYZVYhha5aA1L8xIhKhUE8W7gUgqEKvvzxMFDgGcwLPAnQp/i6Elwnd2AQg8IV0oMAg4CJgMfNrB5yOIEkPubphAWD5+xnCpj46WmghTW0Ky0WNkSn8g3EQ4bTfqN5XoqE+dCNSh7GdhZ+CFKBVh9l75tHts9M2mrvQlbS/BptSekjJ7Y7h/0aiDGLfsn2+4xs8//zyFfJ6m5ua06R7YAxEZjK/YUGCoOjc4CIJmoAE/K9QDDQnRhHi2mkraBzxBtANrgMXAAmAOsED9xtRqEdmEat6pFktRFK9ds0YzmQwAkw+5lNVPLWPovqNOAUaXy6uAFX3kSxNfkZlj95i86Dft9wRafMjkOqMwkyvtcPVs1nxm5IsS8pBYTkp22/1AhZPFRTdj5HVjKzhJ9fLw1nIU8MRRSSyCqNFWCVgoht0qvRkR5vbYvHwXIhnQhQgedvCkgfcA5+Mns+MF/gFcKfBoBBrgOYvAIuAS4FZgHPAhA/8DPFWRLzzJPogFFd1dQre9ZCMkGylh/Ac60i1SWyCorSVXwxgx5oOI3Ipzo1BOCAP7gpv+Hkr/eEoJ41skE31MaqJRkolrNBccltqp5X73THO/lXt19mwamptpa2lJGT/QmxEZAYwCxtTW1Y1Nvg/FE0cNfqAXgRZgLbAR2ARsSO7vlaQhaYsW/K7sfDxLna2wANWlQEtUKnWKiO60005b1yNGGLLvyAnAhyr/TmvsToln77C6OODmx2Xyd3bf85FfnH3QzzsKZ3lxbf1FIwgaKKjyO2P1/WLJVAzMSSKcZIy5ymQcLvKSX/d+SfLUVoxlSXUTWoWyDgI2pREwr4d4JXQm7fMfgQDIQ97APxSeFt9Pl+JXJneycImD30XgLF5ks/Ckg+8A1wBDgI/k4elSQlAAjBxbi9SVoD3YV1JxVtIxpOIVBO5hjJI68mBMEKDOHU0YNIsxv1SnexHHZ5VKpZ/YdGZlZBSpL8x1+eAxSccfllSMGH1P4cnBDRK4NoAXZs6kJZ9neFNTRmAwIuOBHdpbW3c0ItsDY/HLdY34AY+vMyvxg/xh/EBfoaodIlKnMFr8BtLYpIL1eG5xF/Ay8DKqcxNOsdFYWyoVCmw1UVRh8iFfxoQBWoo+DOxQ/l8RhpfaZZ3LNv0zt+PXnzIH/vbZUSMoJUQCMOgHK9hw2QiAR8TwHMK0quxPdXF8S9ggS0qtAqI9xK+uHfotoHvDEio3DkUUY0GFeeIzKue8EVjy9gzbdwaZ5DOCDgM3xvCSwA+Ag4GrDXQauLOIJ6xEpv4tnpiOA47KeKJZ1C16qRItzqTsoNLuBAqhA6OzJB0t0rzFGIOLokaTyZxCOv0U6dSzUiy1UipdCBwu1t4cB4JZ2VAijB6X0H2YIAZx22HikRtfe7gwd968MYjsVJPN7g3sBkzEE0UdPSfJTjwrfAF4EnhRVZcDRkRGA7sDHxWRnYGh4rnLEmAW8ACqsxQWqur6mtra4upVq9hn773fsg4QEbRYGoHI+5KyxgCC6uqg9vEV1H39Jb3gifCv11F68MI+MgBj2ADcQs/ADwIMR+S9YuQXEoAIUWLXFSFqgNLWyF7lpVwxlMQQJ+JVqWtXX5iP57J1eAFtCcoaBFRxIhTpFlUjTcK9vBsR4HWREJ53cKbAT/ABO74VwzwLr5BUNPZSybXAgcD2wP5UEooApJ3XAYyuFutEhOfjX07tNKfOBGMQ1QOwdhcJgs9Qiopak51Pu3sBkQ+7ttbba6Yf0Jm/+1Ekdi+rkaXU1w7W0oABhU99+LLU66834Qf2MHqb1Sh+deIl4J/AP1Gd7SAvIuME9hORTwFT8eKUw+sVTwGPKjwvqgtj1TYDOnny5Le35f1gygl6cSLYlAeuy5lwbhMdq1n/q76JBBj4zRWsv3w4KL8R4UnKOxzdXbH+x9ftxrlnv1QCrkK4ydvIAEKHqm7YouhlHcAmMXIRhsZEB9kAdKh0LY2ehpJK3rlaVdsScpijyunJGBOBBaVUSKr47l0eTuNn3jGw0PlVrpuA/QUuUDivvG+S1PAxgT8DZwAHx3BrV3svHX0wOAlMXTzSDO0M7ahNYprzG2Rjdn365B3QfD40zc03mdraHampeS/OrSafhyA4U3O572iu47iaK/742NxfvT7Stq8+RNpbP66S2RcX1Gpjk2imT5OzFjzX+KvC31X1VQERkZ2Aw4HD8JxnAF6UmgU8BDyk8IqqrhVV97YTxja8K7AcGAFEMEpgikC7wmsO1gloiGf/acDBAQK/x4v37wf+VdZVEqKYDtyNF/eP6uYoEoHIYHXmbGKpJTKqBfs7TUfriWLE2l0oFg/RMPyRrlixWkaPhjhCXPQIxUJ7SZovnPfLeR8SjY9Qk9ouPf/pNGuKuNYU0Z57Eu8+CZySlGUecC+qdyk8j2pBjJkgIp8EjgX2BJrwHHMW8FfgflRnIdIK3XsZ27ANZZR3zwU6xS/8fF5gkIU7FH4dwVrBb6b8ER49yesf3wVOysOjpYpVsGQB4BFgGrBdxfKwA5FBOHMuzgwiNkpsXsbZZ2rfdzTtt91xIsY6jLnHjhllNYrs6oaPDSzZgYcMzN8a2PblH7SFdcSZoZhCO+mn74W5OeLltbiGwcR7TM6BPgn8DrjXqS43UC8iByPyYfx690hfT1YBNwN3oPo4XizTSZMmvdN9sQ3/hzE8+YxhvcJvgAcEPglcJDA92VhcUABOAhRuEr9sfHgaRohnSjjAeObzR7x5y+QuQlG/aRlLbGItGrRohYINTKOl9fqfjzE1NcchksXIlfGGjRFByg4o3Dkm0oZJtmNNyrYuJb3uBTpGHwVRBEWDximUEGlp/aemU1dLR+dDiLSp6iAjcgbwMWAf/N6IAq8Dt6F6m8IroloqGcPO27jHNrwBlDfKSrDawRXW674/Erg6htNrYEOy3LcGuAG4VvxO/nLwm24JV3lY/GrrlAqOUgI0j4YFLVg0HyCZaKAYC525o1RkEsYYRI7GOcTmCbQVWyyinXm0o53M0kfoHHYgUsijHYrmAzQiDm77y0+Dex76U8cdPx4ocJaInI3f5yjboy4Dbkb1Jufca4i4Kdv0jm14kwiBIjgDf3JeUvmpgdMN/LDCjus+gc8BUwXuKy/1JRaKSwWeBrbvyVEk3oSGrVKoQXMBmgrHaLCuwbXLSca7/UAco5EQZwdiohySa4eCJxa78SXCsbOdbozytERZ7UwLpahTB6Q35O7+xbGSL1yIX24ra/Y5PHu7WlVfFHBTpkzZYgNswzZsLVJ4pdjBXYkVyUcd3GpgZeTvr3ZwDzAlBmMqgpkbn/QR4MPdyrwWUQptSLhYS+ldtD3EpeKJsq7uvRIX9lEK4Byl2nF0jD+ZUsN2mGIrtQv/RHr9Q2hnJ7Q6Mo/d8Uq8PJirreZEzRm01mrhWxdfKMXSvvhNxDLmAN9Sb7mZm7xN/9iGtwkBEPmlpBvEr6QeANzu6KKKvwFfEK8CbIJu8QtvI3ZihZl9GyJNBSg+oy6Pdli0PbW725D5otuYrnMbDCUdRNuUT5AfvA9xaiCl+u1pm3Qm+cY90I0O15LBPvfKIPvS/F1dewotFHHbD63XUSOOQLVMJDFwF+iJqzrt/2SN5ratYG3D2w0FDKwGbgP2NnhdRv31Cn5FrF6q0igsBJZ1cZTRS1ezdEw9oI9A5yYtpeppDwdpmB7kUFRSdM74GKWBk0ATWtMYl26iY/Kpy80zr1rWR8Nca2a4tqeHa4eg2km80wS0pqa8mVwEfmbQb5x0d9C2/QCGiQTyoV8tAO/5vUmETbefsT0n3/g6xVhrrFRwIb/RJ8VINx67U30cxQyvqK9EzrVsN6Ruw+wVmxAw2dAMNyLp8n0RSjUpswKIl20ssmR9kZ1GZocERurKzzjVXEtHtNoa9EtHjurR2P/92FoaM9YuainsUIh0b6eMNkI+MDK/KWuePWfakFWzlufYdVRtn531hxc2MKTOyrNLc6M7SzrVKdshpAJhVTows0Y3pV5e3lrs/NzBQxERvvqXZSCEzbXB8MBIxX7YZiEixIGRlS2dUXFDLkoNb0gN21J6IxTGDkivf2heW+G7HxyN9GEuPP6rL/H9D45kwdpCY23aNIt31hcRXDY0qyKn+bP3H9L/YFXla/cuZ3BdmG3Lx7s4ZVeFIcZILm15bXBd8Nxp+wxatWh9XscP8kHMz7rldQTsTsOyw9OBSZX7KbTkDhybXm0MesfLOSKnmaasHWalewNYUeOctr6yqXbdggkrmOD13r8B58feFD+XEMQGgVe122QK8NxGoFXg5Z7+KKUiqL5IoC+osQeRa8SRBgfR1GkUpszo3dTqFpRG7HiurtBx2pb5kWtLZ1xbAKUOtN4S77NX4iKnReCHqnrFNS/YjjH1uqeq/BJvQqFAoMq1wHVHXT+ftW1R2FhjL1fhRMpcUDGqLEP1E88tzYfql5obkpJY5/T6dTm9auH6AgL16UB+bkR2SXxJRGCjNZyp8EJLLmZ1W8lEIl+xRj6QvMOq6oP5op6bmHB04bzfL2b2mvyQjqL7bOz0VFVGa7LAYoS8EZn1+T8uvXrK0PQfrnpgRemLh4/o0UwX/HEJzy7tyLZ2uo/HTj/jYIL6XXEEnBHZ8Nq6wl/r0/bbH7l50ewb/rWK++d2gDC2KRvdYo0M2SyhdN8REdalrJzSmo/ntXbG41dvcr8NjAzaTHoVofOllflZtaH55a3Prv3Hjx9ZFX9m+rAeD9Wk4PjdBvLx3yz6r0xgzij3iwhRbcp8flMhvkc19uFbqnDebYuZ8bMFTGwKd1/RFl2syhHqPUIlacPOBeuKr8xaseyanYelb7/u4VXF8w8extw1RQSaS05uSlvZIRm/NhXI/RMHpz61bGMpGj+0jr/MXHd6NjQXS4VVnEJ7YLigEBcfWLRoUdkob7F4LlEH5JJ9EwVmSk8zKhQIIXIwswehdPztHko779xZ/5GPLbNPPoO6TdBeSzR2B/KHHY+mMuB6eCwuBT6dueP2B92qUo2Lmw/X1vgkjXLg2nC77YObOAGcU+BGVK8U6Fi4UXGQjVV3EG/AWC5YswhYA0HAjGLszpGYARX3S8D1QSp8bVlLaQ/QsVToPU4Z0FGC9e0lABNYGSEwpnsIMTYQ+eCTS3Mv7De6hrZ8zIrW0hAj3WbywJAo7mnYdNwvXmNNezSiGOt1TvWDqr08QzPA3sbIT5e3lAbsPy71s0/9fqH76YfHA3D8DfNZtr5gC8rnnfIVVarPfDDAIBFODYxsV5c2p/39tY4Fa3w9UrmiG2Okl2NYvxAhm7ISthcc7QWXKsQ6xgqDtyLpjtbIwUtbS5//3YMrf/flu5fxrfd1c9W6TMh+P5jTOKAmONyaHm1GJjTT7/j72nuOmNSbmx5z/Wss2lgkC9NXtBZ/qv4EsR5QyApMtYafLG8tNlx86KCfXfaXpXrnS20IBCtbS6NCK2PKAzhlZfCqtojfvdjKpsLGydnQXGAkHl85yAV+VtL4XwbhyFOOBE8URYWZdK+4lmePuSSOXWWk6fJynNVFKHPmziWwFhYu/FDpE2ceKytWYuYvQBszFE86Fjd8WDWRtAFfNtns/fnjT6Dz9NOagidmDkhf/XNkzjK0qY7ofUdDNgvOPQ58A5H2SRMnMuO6eSioNT1JWJMISIXIDUwHclHsehAJwB3q3C/b2yPiPjyMVVVb8o58MQZQI/RyyxN4//YDwp+uaiutbOt0OEquh5ihOCpm3n1/8CqrNpXSccxXFE7YguzTaITLVrQWXyrGPKqq7P69l1ncEgHsL8IFSgWRVFp3dWP/0PDZ7QeZ/2rpjF3sVDuKTuUNeE4JaMqinSWls6TaGTk1W59+qBW+MmPfQU8/trD99cobHQWHKhNKrjSxujyBYZ/3TR9Yf9MzG3sdy7W0tYDCdqGVH4rIlpY1G63w5Qv/tOrZfKTPbMrHIBC0C9ZI5fu0JRfpU4s7wuENqc9usq5b0fUOb09a0W+LSP7Jvzxe3eRz8BuKQJfSvqKaowAkMTxXV5iwCLFzO4vq5RjTEB82A5qaiA6cRrT/HtVEosD16tzv41wOoMFsbPuW23G7w4ufOoXwt0K86y5oUxNEUTvGfE9ElhdLnmA7Sy5xcEKrKSUMDJHTj2qJGV3xqvwbZ6P6TTGyaXlLib6goIGJieKuKIt9jesdgRkCtxRKjvaS0ao+1/JrB18ym+UtJZxyuAgf7eN9/j09/x4ucK5Bnx5z+UvFthK0/nMNIw4Z8CHpOas7vB+NUxgi3ftkCByzopVrIqeLIocGxr1RD0MNDFqKoRQrYV56pa9smD6ynmINh0WOGyr/bM3HFGPdOzDSy7lIYPKaTdFYpTtABkDDRc/x0twWGTay7lPWyJ5b2YajrOHjkwelnusoOhc5pRQrlcRuBHfOgcPiy+5bfdja9ugjVfVbGxi+CmbJxo0t8I8zu24krsKrKkWCtP/oc1A1ltsTPDdR1bSIXAhM1GwG2bCR4jln4SZPrCYS8Na914hIqVQqSZhKfQo4BafEe+6O22F7ggf+jqxZC5MnPUIUPaiq7LyjtyjvjBzqZ/xqxKViPDE08llwlWLhJlG+aTPB7M6Neda09xd9CTUiqLqkE/ocXSHwoZLTP6bRgjO94msozkezi7SE5jQdBubjVIiIScfON8LfgYxTjlUYVDH8DlWYBMxSBbtPXcPajtLUnunlbit8RZWSip7nlE/Tba40Cu+CsAjFIbTjJ7dEvyRLBWHhJ8XOZNwZgU2CZ7qqaPWEIcgyEe+kpUqdohPp1vWS6snuGgQEn3uW6NqpcP6zLN1QsEFgDuiHaAcDe4rIy5z/FFy3LwDtJTCNmQnrc9EJ1X1tRB4XeE5hV6d6ED09bg9d2VYarspyVciVeo5BQWL7+ecGGZGLROIBVfle2xTED3ZEQvt1B/RIl+lurzeEACivcMwAjkcVbW5G02lS1/+c4qc/iZs0sSuOLl7k+j4iqwGCVGoa8HkgxBgklyO4627sE08RXf5lBf6MSIdWEFtnklUfhJJx8NnI6YSqQfkrKcV/1Mgx52u7wgXPb6ZKW2UKfiBWdi+IfbKPSaA7PJAVQHYowbSe2eqrxHoa1019llOfMAxKfQKRa0DKvkJDEd6DMitZGGqIvWdmJV4AZiXy5o8RDgRJTu9VxRuFgtMleO+88opPDVauBdmtojwvE+v5eGIRoIiyhKAynGSP8v+JjuhCDIKYDClzMiJXQw/dqZm4ZLHiB1VgAIZFsFc/TWwQphGY/4HucC3OCsCBThlbVYa7KcWfpCG9hrZCM4G5AeT4igdGI0wCXY5A3NekZ82pDg6u6pt7idxPV8fiuHYqbxWChJtkROQsyrOKCPG09xDcfS/pb3yb0sknEk8/EG1oAJG/aBA8gHMgUidRdCEiw4gizNx5hLfdgX3kX0QnHIcOGbxB4/hZASpN4cuE0kfVZ+A9Fbsg8Jg4/T7WFBd/OfFIDKq4QFdGyWp51+Zqv/JKM3ACxE9jbe9uL/tF+QlkN7zXZOX9X5M2z3LhcyDGAXcgfBDpOuY6AIZ7hh35gohUs64zEFYg3EspXgxyPN6/vzy6/XkQV++RB14E4AsvANQh0l6VVztWnwM6+P4e3f9e/EJ/7RDTZItJPQvAvSAX42PzllEkdto1m/k234XKxRFPmElsOQD2BpoJzLqu95tYcMF+9AyN1Y7q9YRmDYUSZMP1qP4akaPpdkysxXuPPuQHS686TMZbmVdyoYU4/ToBG96SEPYVCBJusgfeetfDOdx22+F22wX7yL9I/einuIceJjpgWkQcPZW+7Ov1uM6g8y/3HKN1dUeYpcuwTz6NfeJJZPUatKmJ6KADwNp1Ese9DgDJ+XGo0nuI7lPVoJtEuVLFLJNCxWptuJnTKgRQy1bgWESu710K7WZKxoK6yUkouDLaQB8F4Ad7wSUvgrduPgMkS9eCi26i1AmZNMAmRNbjPebKGAv8GGEB1jyMcB8ij5OP1lMfwuU799FbSb17DZp+uGhQYXjeVzs554020CZUaqryXEgYOqIifHkmCf1Oo8eKnf4LGAdSVqS3xw/gR7vfb2owMqFX3iozAfjuHvDFmYA8CXwBTyDlEs70o6HSF7oLO1X96VC9hrrwOdpL8MPdt2YMbDUCEUFVj6EqyiHZDPH0A7FPPQOlEuaFmaRmzrLA5Tpq5Nmotqe/+s3RBEGNrN8A+XxXB7rdd8XtsD3EcQ7v794DnlCkL9/vagoIHdRbhY5KOa1fQkkCWWm/HMVVvGMCcGRv3lZBKLYguPSIqnxaUF0FwNdfAbQWZCjgEpddn4kSkg0MTh2vrmhhyoh/4cOuViIFTAGdAnIG8BK14c9x/I6vvZzja1XEEpb9eKvrpvTpHRya8lZr9Z3tMLwPjKDUopxMz7hk7WgyGVy5F3x1FkAtyP49m1vvAw5EugilAdgPeJQLni33Uy1UK/+6DNWWrp9X7QawDvhxrzpcPquiqj3qUT0IDNBIawz1b/3xxYGqDsSLPD3hHPEeu+PGjcXMm5/MPAhebGlGxCvrZfWxrBRnMkSHHAyZDMSxRbXXqC5QjmogW1IoMgrnKjxIEHbHBgs2RygCPV7ZY0DNArYDqccrwyeC5vokFAHIGIzUV93vBPEHhYiAchDIT6o6ziK8CJyKlVZ2HgXoLzByCEiFbNSzrqD7gOyKMBnhq1w5u5NLK1zqe3CUqjK7Ppoy6K8dOBo4oqusaNXB9HoPsT7WRWB+wI9H2bEiFEwbKk8iNFbpFtMQvY7mbBEV8JNBhh7QVrRvtaMXwn7r0Bc+zgBzO3759y2FUc8qextbqaLNA4kPnNZ/9DORnhq5c7hJE3C771ZeKWtEpKE6vbOCWsFtxaVWphGYD2ANXJXUPzD9XLbi01b87roeI7DPdP0OzT6Eds++8+jKx/S+3yPvGgIztuoaRWCGE5gkLZBOzcXIWQRyN4HpJDR+EPR+dwZrPoM1x2EMfOfV7oYLrb961ctCKrkq0WedDATGEph0cgVV91/EmMtJhx18NeFo1kJgpxLYIRV5LiEw8wnsSwSmVNGmuxHYkX28s/sKLaTM5kXoLdehr2t7AnMGWaRHu70FCMT7hQzo74F42v4Ed9+LrF275XCB1hLNOBhtbCgTyiC8LL6wx3Obb6DKEDrgZ6RzELkXb9S2BR1FNhekZAOqv0fkYDwHqKOvA5fK6Z1zWFN9blwtaLZHkK1+Z7okn4um+AEfmBdAT0WYgciHQKYjjKC3GJEFPozwR7yynfRWfzoK/YtePgFvAGMRplGM5/HD+Ykeg8HJgVXlnMW6tvU0189BZB3dDoYj8VFyFnbZ94l0Vr2jscLrFr4/D7wbRwM9l707UZfrR3ysaOAeFfwImFuwzHwjld4SDH4lo2/t1zncmFHE+0ztm7VXPzt6FPF79q3stFpgbwEemzev+9n+Z4QYa+4iMEur/t+bQE7EClzz2hZmFam4et0XArmXQOZuxewE2bQSyKqq/wcS2PEV3CAmMJ0EptBvPte8FpIOs6SDLJmwSE3qXow5E8sRWPkCgTxPYFxVuslYGeiXqMvt1m+9/GWr6K3/urURyEJ/mTVV7x6ANRdSE47Gip/JjRns+6BHHjlGNE0jZbcjMJsq/k8RmGnYoFzedgKzrirtaALT1EX4oYFQdiCQ2wnkfgL5K4E8gJUzPRftt68fJpAXe+VtzdmkxHLt/LeUULbf7BNhQDxjOtTXbTGcVHzA/ujQIdXPHe6groc2V654WHUF5mmsnE9g/lB1zxKYs0mZUaT7SNeVXipEE9vXfUNNehmBuavfPELpviwQmLmERiueqSE0x5A2Bitg5AkMxxHIz6qe6xYzQnMmofkTobmDwNyJcDMZO4J08CpZ+0OMnEAgT1aVJUtgsj30jLLo1V+5U1WzbtBfO8sfMHIgRg7CcCiB3Fn1zCQC857utHZHQjO+6plTsOZeAnMrodmu6t5+pGgkFUB7IUcg86rujyc0uxNauGZuYtxn9yYwBxOYvQjMVAKzB4FkN1OHJVhzIYH5KaGJq+5/CGv3JrDw49d5K2BgC8Z2TomnTCbecUqpz8056NZnDjqgLxa5j3g3y54d2NcsEZqHSNulBOY3BGZl1f3dsOYUbNj/rNpFLMnV130UAvMHAlmzeV3H+g3HwLxAINUz4ikYczxpE5Kyq8kGswntJM+xqstjIJDhBOYIAnMUgTmSwBxHYHYmMF7Hy9ilhLaKi0qJ0BR7iJk9uKbp+3f1hNR3O28iHSwnbZeRti8TmB9XcYWAQHar+L0/gamryidLYBoITL1/vse9SYm+AEPqITBPJJy3fL+WwH6G0AynLgMpGUZgTqvKp0BoXvX92aee8xyhzMSau/vgKkMJzDmkJNVr8ngThLL5Q1FVobYmcjvv+DCJ/X5vYnK4PXbHjRvXl7lLI3CqOhfOmTu3qsN7XVHS2TMJzF29xSZzBqFu77nG5gZ5fwMquQyzCMyDWxS9rICVeQTm6aqBOQRrfkZgbyVlr8eaPyeE0Me7BQIzh0AqB0qG0FxAykwntBMI7MkE5sCq9y/HSssbEr2CfjjKZtMYCM3rBLK26v8RmCKEJkNgpm2xrXpezQSyd1cbBuZfBPJ6VVmPIjB3YOQnBPY2AnNo1f3XsTLTc5s+6xATBpANVhGYmwmkSnSVD2DtNAILv170lhBKaotPOUe8z9R7UH2pz/uZjN9gTPeb1QfEmP1FhGcefzxZTib57ONK2xgjN2FkbdW9yYichu0axFWX6eOquh8YSAVFrPkdVnKbzSNQCG0OIzdjpbPnM9KMkRMw8imM7IUV0zufrnI/gzULe9w3cggidyHyd4zcgJURVekfIZVq77nobPwKVF/1KrdRJarL0ut31/8dWNNa9Uw9NmsQxmBll6p2asPKaqysSa61WClV3BesOQCDxQisd69jze+r8rcYeQ9GzsPIgVixVff/SC6/lKC/OiSX3564E2terko/ECOfRCTbnwr+Rglly8ZRqsbtOGU1cXx7X0Tkth+P23Vn+hXN/OrXharaWN/czJahoPoc3um/Gh/DuUlUBAF4QxDKPp7/wvtD948zxpOEjr8HHwRjSyj2ak8BpLgAH8KzusyN+CMjqnbFmQvciovhzHFVmW2pcr1evvkkvi3yJL7iFahH1CCyJz4MbhkF4CJ8oOtDkutw4F9V6fcChiICQwNAbwAe24o2BHgO+G8aarf8pADWLMUH164ey0f7FUbg2Pu38tV9w0BPT77NPFcjGzb+ieqlXhGvxDc1bUnZP1JEzmwvFKjrNq/quxdPHwMiJeBG/DEOlRiPD3e0ta6xfUBBaMGH1Nw8wZ0+BqAD5St4N9L+sBEftK/SDsvXT9OgXI8nlojNYwnwRaTm1X/DyHVrUd3uBbwZTiUGgtThg1VXbnWvxoe1ncPpo18h1lcQmYkPpl6JsYjsjABRJ4hdClwAbM6iFfwkcRHIwm4Liy0gVlC9ne7DS8uoB85DtZ4Td9y6vPpBQM+O7aNJk32Jzs4hUmpfrvAg8AmAsqVxvMfuPuidMZvbxwiBC+szmedPGBI/ctNKm8M79deTnL9DeZ8kyRt4CpFb8LNWedQYvE/J4CR9Y0X6VZXNhz/vI5t8777fvfVxD/AB/Np/2ZhxMb0IUEBYiHIWwgXAidDlrx/h49N+Dz8IJuE56GuUiUIcqGxA+S9EXgROS54rc5IIH4ztYdAfE+vTmA44Y1xf7ejwhx4NqKjX6/Q5qspGj/Iq3vel3E4rejxmTYTTp5MyxUkbrwcdnlgxzK5411NoRfqzxsCNS0F5HGEm3XsthvL5NDbtiyfyDOpOAfkcPnTuMLpjHmwA/g76PWT08+hSOGMs3LgEvK/IvOSz3E9LejTJiDELWbnsF/gzGSttmAbjt0C6vbf+DcjcefP+gWejfXSJQza1Iy0tmLnz7kx967s3EQT74QOGZXEOt/NORB94H9pQjzY2ogOa0MZGSPV71vLjAh+d+KvapUyKmugxu0kOXI4zxsEvFkHWQNHVejmzF3J404huKV7JEUiOuLxDpY30mA01hwlzROVQzSoYbaDCLRRvNdsGwNnje77x10sBAoTt8UZ5A/Cn9L5IzDIMJnmnASKUVkA5M7Ewv3EJqArIYLyn37hkoKwBXgNdiEiBdTm4sI/wTb9a6DtfTFWZtURcagNVzq4wsvj1YpL8y2UqoxORDpzzop1/roaeIqASu01YU0tPIb9AiU0I8ImkXr9aDBAiNPTsT82TSrfT2Qlnb9fdBhCCbId3CR4EtKPMBTcHbCezj4fvPZ3kvQj8ZmRjr3KUxcUzxvo6KGmkp99Qgg6gkzPG8u9C5s6b9xvo7b0n+TyyZBmyejWybj1m2fK59t77/kAUWVFOACYQWOIDD8CNGuWbJ51BBw5Ahw7GjR6FDuh3w/9WVf00sHFbPK9teDfAUK1zgI8JtHwFsno1ZvkKpKUVrB1ANptNoqnMRtXpgIG4QYO8Eh876OhAVq5EVq/FLF4C+UJ/7z1JRC4FsnPnzmUbtuH/Ogxelu5DoU84aLKBqGFYr01Njd63lNcxssaNGQXpdO90UvnZJyzwaRG5SCG9jVi24f86Arxi2E6lP4oIbtQIDIozBlm7DmnvyOjgwUNk0aLlqHZoXf3jOmLkUah6/cFayJZFryFeHOtFRD2QAb4oIpGq/nDO3LmFNyKGXXXVVeXyZ3lXn2O7DW8zVERyqhp/8Ytf/LczCdQHBFtNteNWOo0bPw4ZMhhpaUVaWkUb6hvMCy++RhyFFIvLtHngGB06eKqm01BXhzY2oE1N3mU4FWzN4m0NcJmIhKr6vTlz53ZuLbEkDmcTxR8f8dZ76mzDfwqKwA0isuDNZBLgPcsW0kdQMoxBGxrQ+noYOQImTRjqBjY9mT73/DVS3ER80LRQBzXvoUFg/Y6x7V4e3vodjhrgS8l58N+eO2/epklbH4v4deBqtnGUbegf5fNB3xRk/vz5ONXv4v2Vt4QCwkdQ7kx0l+3xR8ZtvxVpt4QScIOqXi4iGzZt2MDU/fZ7OxtwG7Zhq2Gc39ibxdaZhKRR3oeqVeeISqWFqL4524BuhMC5IvITVR3T0NzMSy+88KYz3YZteCsgc71D1d7A/WzG07ECi/AH1pc9sQ7D20HVb0XarcXDwBd2sPbZeVHElCq95bOf/SyjR4/GOVeP9+GvxAYRiVW1KyqjiHSq6hq8YoeqDkj+3wgQRRHW2sZy/UUkxm8CFlS1VkSypVJpXRAEKfxu8gagvVI5TBYXUsBAVV0LxEmEm1q8iNuaPJrFi5vtwBBVXSsi+URPKwfirrQu1aQsmSTPNgBVbUrut+AtwNOqul785mw6CIKWKIrA76w2AOuSujfTbTGuwHpjTPujjz7Kn//85+r61CXlXZu0S7ntOkUkr6qISJNq+XgDGvDWD3FS74bke05E2tUf/VFM+qMZv2lsgXQ2m13f2dlZbq+afD6/NpPJpPFuIOXNUhWR1apaSvqhvOnqVHW1iBSSMlfGUCuVy/RmlHmTnAGxGH883NZgLOXQRj4o3lP0tvN5szgYuO21OD4xiCKql4+z2a6N+mNF5F4R+Wty3SciHwQOTr7fLSJ/Ae4QkclJR1sR+bqIXKHqI2UGQVArItckefwFb9N1toggIocCX7bWWhE5U0SuFZHmyti73/zmN0mePUBE7hCRyf4UP0FEDhGRHwCNye+9ReS/8Ae9/sAY8zHjA3McJSLfFpEvJWW/P/m8U0T2FJFPiMhJ5XeKyOki8vEkzyNF5KLknXuKyI/jOC4bMk4SkctEJABGisgtSZv9JXnHFXEcB/vv3x1g5YorrsBai4h8UkR+TkUUSRH5jIicH8exSdrgVBF5PzBWRG4UkalJOS4QkbOSPD6alPNiETlGVQMR+bKIjBeRI0TkouQ+InKwiFyayWQQkUki8sekrHeLyG3AjiLSJCK/Supxt4jcb4w5I3nvMUk//jlJ91OqF6r+DQRJBMcNGHO1eFufLYlgBtUVkkoR5/PYINikzn0Xkee3Iu0bKhuqjbG1IX4G6YK1FvUi4+Oq+l8icgp+Rv01MFtEpgOvq9e9UiJyGTAeeBVP6Hsl/4/H22k14YOt/UBVF4nIUfjgauBnqCHGmKPwNm6XGWMWJ7M1CaFxySWX8J3vfOcIYISIHIa3QwPPPT4mIvOttd+J47gGGKqq60Tkb8DZzrmnkwCEfwSeVdUHROQC4ClV/TswJ5kAKtt3IN1LJg10B+lLA8cBq1T1YmNMmm7r39H4GftyVW0TkbHALsaYlKp2VSgMQ5xzTfiz1ieIyO7AP5PbQ4CPWmtn4qWQgQCdnZ2v1NTULADOc84hIkcAnwamAtOdc7eLyOFArYg8BYxKuMC+wJCKiacWGJr8rsGbqnyZJF62qr4mImVO9y1VXSgiJ+D99MFLBa/ibe+squZVtczN//3BmBwqGgG/3poEV155ZY/fl156KXhDyQffbGG2Ft/+9rcBGDt27OLzzjtvcdIZWRH5a/KIAOvFW7WGeMtevw0qciDevL4Oz7nm021F/bCIzMdHaSyLbg5vhPlF4BoRuc85x5e+9KWu8ogIV1111XD84sY1wEHAf9NtcPoKcFocx88l+am1VpxztyUz4C+Buap6B9CuqnPFHyn+vIg8UFH1qcApSV12pRxBsnfM7VeBo40xzyYDqyxyBklb/E18IIf1wDNAoY9o+Xskz94FvNda+884jsEP3IXAV1T1VRFxADU1NaqqPxKRm0TkOuBOEZmlqhngwyKyF36cjcNPVOvCMFyfTDg75HK5j+HFtGm+SQX1s2GAJ85FwBOJWNyQ9NdsEZmDn9Qqg6Bl8JPDPBF5WUS45JJL3tSYe8P7Dwlh/J/AGWecUf5a7XgR4/WoB/EycAOwLo5jY609GC9aNQCHq+qN1YMkjuPZ+MEM3YTymqo+oX1YRifp98MH+3sQOAE/kB9P3v9P/KD7KnALXqaWRN/4Gf4I54sTOX5zVT4kKQvACHz84mpY/HHRLwCXiMhP6M3pLd6n5Di8afqH8DGlufLKK7HWEsfxYXir4VnAhVEUDRIfbUXwk8AhInI5Xn/ZCGCMeUVV7wKOB25WVVT19YSYPpC0zY7AUcCshEgc3rr3YjzBNwOPJe2gST0vBJ7AE3WcpBmCd2vI4ye1m5O6OTyRfwn4H6qi6/+7MG8+i/+TMPjDYu7Cs+JvOOeetdZOwM/2XwLOB/YTkQlUDSRr7SHW2nMT3cHinZIWisi3gR76yVVXXUUcx4InzMPwg2i35HsZkapeix9Un0/yJBlIr+GtIxZtgUgE+ImqHqyqB+N9dfrtP+fcjXgHqEuAjPbMPFbVb+OJRSvzCYKAOI6bgUOBs4ArgD0SjlAux3rgcvyAPI7EBSJ5xSt4s6h1STk24vXfY/GLNKvwwffK3rIWuE9VD1HV6cDX6OlSMQd4n6peqKqFijK04f17BPh70jbl/P4CHKKqPzfGkHDCNz2g/hNhgMWJjvJXYFqizB6MJ6AfAdfiZ9Mj8CJBFjhNVT8FnI4Xc8B3xGr8PtNoEfmGqtYmq0KICNbaMXix6wfAL4Gf4wmy7GtjRKQFPwgsFVHj38gBQUCHiGwQkQ30F78gyXbBggXtqnoFfkCVTyUrAcNV9Rzgg/jVTkMyUZTrhI8B3Qb8ELgeeAA4xnQfkWHw3OZ7eF+eynGUrA95WGsdnig68UQ0K/n+WkX75mtra9cmq5BtVXkNUtWT8X4mn1LVUcn/uURU/SYwWVXLepgDJqrqacA5cRyfZoypr6jbv4X/FNOPdnp6DnYCbcaYgnPuRyJyg4i8D299cCNQXgfNA9NV9deJicO5iZjggCuSGaoTaBOROap6GfADEXm/tfbWCn1tR2C+c+5aY0wJ79T1C/wCQY7uzn8JuArvywKAqkbiA3h3lV9EFL/sWxm3uY2uA6C66lwekJ1AayLXF4DWsWPHGhFZhB9IH8APyKVAu4iUxRwBbnHOdYgI+Xyeuro64jjePWmjnyX5vwZ8Po7jAYm4mE/KfqeI7EfF6VV4HaZFvWxZ/m8m8LBzbp0xZhbwcLKYQdI+rRWzfifQ4pzDGFMAakTkCxV5v4LXwTYCzjl3jzHmKBE5J4qizwZB0I5fgLicbuv4h1S110lgbwT/D7UB1K61zu3cAAAAIXRFWHRDcmVhdGlvbiBUaW1lADIwMTk6MDI6MDYgMTA6NDA6MzZ92Op7AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTA1LTAzVDEzOjM1OjI2KzAzOjAwrx6ZEwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0wNS0wM1QxMzozNToyNiswMzowMN5DIa8AAAAASUVORK5CYII=";
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var currentToday = mm + '/' + dd + '/' + yyyy;
        doc.setFontSize(12);
        //let today = new Date().toTimeString;
        doc.text('Subscription List', 280, 20);
        doc.text(currentToday, 490, 20);
        doc.setFontSize(10);
        doc.addImage(imgdata, 'png', 30, 1, 55, 25);
        doc.setTextColor(100);
        //download Table------------->>>>>>>>>>
        doc.autoTable({
            head: [this.head],
            body: this.pdfdatas,
            theme: 'grid',
            didDrawCell: data => {
                console.log(this.exportTitle, "New Title");
            }
        });
        // Open PDF document in new tab
        // doc.output('dataurlnewwindow')
        //Download PDF document  
        doc.save('Sales Report.pdf');
    }
    doInfinite(event) {
        var data = [];
        console.log("event trigger");
        setTimeout(() => {
            console.log(this.sales);
            this.sales.push(...this.showList.slice(this.currentPage * this.count, (this.currentPage + 1) * this.count));
            this.currentPage++;
            event.target.complete();
            if (this.sales.length == this.showList.length) {
                event.target.disabled = true;
            }
            console.log("DISPLAY DATA----------------------\n");
        }, 500);
    }
    setDisplayData() {
        if (this.showList.length > this.count) {
            this.sales = this.showList.slice(0, this.count);
        }
        else {
            this.sales = this.showList;
        }
    }
    ionViewWillEnter() {
        this.showList = this.reportData;
        this.setDisplayData();
        var resData = this.showList;
        this.pdfdatas = [];
        for (var i = 0; i < resData.length; i++) {
            this.pdfdatas.push([i + 1, this.showList[i]["CompanyId"], this.showList[i]["CompanyName"], this.showList[i]["InitialTrans"], this.showList[i]["LastTrans"], this.showList[i]["Vin"], this.showList[i]["PlateNo"], this.showList[i]["ManufacturerName"], this.showList[i]["ModelName"], this.showList[i]["SimCardNo"], this.showList[i]["Suffix"], this.showList[i]["ContactNo"], this.showList[i]["Fax"], this.showList[i]["WarrantyExpiryDate"], this.showList[i]["ExpiredDate"]]);
        }
    }
    ngOnChanges() {
        // this.ionViewWillEnter()
        this.showList = this.reportData;
        this.setDisplayData();
        var resData = this.showList;
        this.pdfdatas = [];
        for (var i = 0; i < resData.length; i++) {
            this.pdfdatas.push([i + 1, this.showList[i]["CompanyId"], this.showList[i]["CompanyName"], this.showList[i]["InitialTrans"], this.showList[i]["LastTrans"], this.showList[i]["Vin"], this.showList[i]["PlateNo"], this.showList[i]["ManufacturerName"], this.showList[i]["ModelName"], this.showList[i]["SimCardNo"], this.showList[i]["Suffix"], this.showList[i]["ContactNo"], this.showList[i]["Fax"], this.showList[i]["WarrantyExpiryDate"], this.showList[i]["ExpiredDate"]]);
        }
    }
    ngOnInit() {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        this.showList = this.reportData;
        this.setDisplayData();
        var resData = this.showList;
        this.pdfdatas = [];
        for (var i = 0; i < resData.length; i++) {
            this.pdfdatas.push([i + 1, this.showList[i]["CompanyId"], this.showList[i]["CompanyName"], this.showList[i]["InitialTrans"], this.showList[i]["LastTrans"], this.showList[i]["Vin"], this.showList[i]["PlateNo"], this.showList[i]["ManufacturerName"], this.showList[i]["ModelName"], this.showList[i]["SimCardNo"], this.showList[i]["Suffix"], this.showList[i]["ContactNo"], this.showList[i]["Fax"], this.showList[i]["WarrantyExpiryDate"], this.showList[i]["ExpiredDate"]]);
        }
    }
};
SalesCardComponent.ctorParameters = () => [
    { type: src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_5__["ExportExcelService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], SalesCardComponent.prototype, "reportData", void 0);
SalesCardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sales-card',
        template: __webpack_require__(/*! raw-loader!./sales-card.component.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/sales-report/sales-card/sales-card.component.html"),
        styles: [__webpack_require__(/*! ./sales-card.component.scss */ "./src/app/delar-application/sales-report/sales-card/sales-card.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_5__["ExportExcelService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"]])
], SalesCardComponent);



/***/ }),

/***/ "./src/app/delar-application/sales-report/sales-report.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/delar-application/sales-report/sales-report.module.ts ***!
  \***********************************************************************/
/*! exports provided: SalesReportPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesReportPageModule", function() { return SalesReportPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sales_card_sales_card_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sales-card/sales-card.component */ "./src/app/delar-application/sales-report/sales-card/sales-card.component.ts");
/* harmony import */ var _sales_report_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./sales-report.page */ "./src/app/delar-application/sales-report/sales-report.page.ts");








const routes = [
    {
        path: '',
        component: _sales_report_page__WEBPACK_IMPORTED_MODULE_7__["SalesReportPage"]
    }
];
let SalesReportPageModule = class SalesReportPageModule {
};
SalesReportPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]],
        declarations: [_sales_report_page__WEBPACK_IMPORTED_MODULE_7__["SalesReportPage"], _sales_card_sales_card_component__WEBPACK_IMPORTED_MODULE_6__["SalesCardComponent"]]
    })
], SalesReportPageModule);



/***/ }),

/***/ "./src/app/delar-application/sales-report/sales-report.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/delar-application/sales-report/sales-report.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image-size {\n  height: 35px;\n  width: 35px;\n  cursor: pointer;\n  display: inline-block;\n  margin: 5px;\n}\n\n.download-wrapper {\n  padding: 6px;\n  background: #eaeaea;\n}\n\n.download-wrapper:hover {\n  background-color: #f6f6f6;\n}\n\n.subscription-title {\n  font-size: 20px;\n}\n\n.reportInfo {\n  color: #000000;\n  font-size: 14px;\n  font-family: sans-serif;\n}\n\n.paddingBottom10px {\n  padding-bottom: 4px;\n}\n\n.padding25px {\n  padding: 20px;\n}\n\n.buttonWidth {\n  width: 100%;\n  height: 30px;\n  --ion-color-primary: #7c68f8;\n}\n\n.datetime {\n  background-color: #e8e8e8;\n  color: #676464;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc2FsZXMtcmVwb3J0L3NhbGVzLXJlcG9ydC5wYWdlLnNjc3MiLCJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL3NhbGVzLXJlcG9ydC9zYWxlcy1yZXBvcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFFQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FDQUY7O0FERUE7RUFDRSxZQUFBO0VBQ0EsbUJBQUE7QUNDRjs7QURDQTtFQUNDLHlCQUFBO0FDRUQ7O0FEQUE7RUFDRSxlQUFBO0FDR0Y7O0FEREE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0FDSUY7O0FEQUE7RUFDRSxtQkFBQTtBQ0dGOztBREFBO0VBQ0UsYUFBQTtBQ0dGOztBREFBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtBQ0dGOztBREFBO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQ0dGIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc2FsZXMtcmVwb3J0L3NhbGVzLXJlcG9ydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW1hZ2Utc2l6ZSB7XHJcbiAgaGVpZ2h0OiAzNXB4O1xyXG4gIHdpZHRoOiAzNXB4O1xyXG4gIFxyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luOiA1cHg7XHJcbn1cclxuLmRvd25sb2FkLXdyYXBwZXIge1xyXG4gIHBhZGRpbmc6IDZweDtcclxuICBiYWNrZ3JvdW5kOiAjZWFlYWVhO1xyXG59XHJcbi5kb3dubG9hZC13cmFwcGVyOmhvdmVye1xyXG4gYmFja2dyb3VuZC1jb2xvcjogI2Y2ZjZmNjtcclxufVxyXG4uc3Vic2NyaXB0aW9uLXRpdGxle1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG4ucmVwb3J0SW5mbyB7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xyXG5cclxufVxyXG5cclxuLnBhZGRpbmdCb3R0b20xMHB4e1xyXG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XHJcbn1cclxuXHJcbi5wYWRkaW5nMjVweHtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG59XHJcblxyXG4uYnV0dG9uV2lkdGh7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIC0taW9uLWNvbG9yLXByaW1hcnk6ICM3YzY4Zjg7XHJcbn1cclxuXHJcbi5kYXRldGltZXtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gIGNvbG9yOiAjNjc2NDY0O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59IiwiLmltYWdlLXNpemUge1xuICBoZWlnaHQ6IDM1cHg7XG4gIHdpZHRoOiAzNXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiA1cHg7XG59XG5cbi5kb3dubG9hZC13cmFwcGVyIHtcbiAgcGFkZGluZzogNnB4O1xuICBiYWNrZ3JvdW5kOiAjZWFlYWVhO1xufVxuXG4uZG93bmxvYWQtd3JhcHBlcjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XG59XG5cbi5zdWJzY3JpcHRpb24tdGl0bGUge1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5yZXBvcnRJbmZvIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG59XG5cbi5wYWRkaW5nQm90dG9tMTBweCB7XG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XG59XG5cbi5wYWRkaW5nMjVweCB7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG5cbi5idXR0b25XaWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIC0taW9uLWNvbG9yLXByaW1hcnk6ICM3YzY4Zjg7XG59XG5cbi5kYXRldGltZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XG4gIGNvbG9yOiAjNjc2NDY0O1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/sales-report/sales-report.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/delar-application/sales-report/sales-report.page.ts ***!
  \*********************************************************************/
/*! exports provided: SalesReportPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesReportPage", function() { return SalesReportPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");







let SalesReportPage = class SalesReportPage {
    constructor(commonService, ajaxService, router, modalController) {
        this.commonService = commonService;
        this.ajaxService = ajaxService;
        this.router = router;
        this.modalController = modalController;
        this.today = new Date();
        this.subscription = {
            fromDate: "",
            toDate: ""
        };
        this.exportTitle = [];
        this.pdfdatas = [];
    }
    getExpriyDates() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["serverUrl"].web + '/login/getPreferences?key=subcriptionDropdown&companyId=""';
        this.ajaxService.ajaxGet(url).subscribe(res => {
            console.log(res);
        });
    }
    closeModal() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.modalController.dismiss();
            this.router.navigateByUrl('/dashboard');
        });
    }
    submit() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["serverUrl"].web + '/device/installationreport/';
        const jsonReq = {
            fromDate: this.subscription.fromDate.split("T")[0],
            toDate: this.subscription.toDate.split("T")[0]
        };
        this.ajaxService.ajaxPostWithBody(url, jsonReq).subscribe(res => {
            this.reportData = res;
            if (this.commonService.isLoading)
                this.commonService.dismissLoader();
        });
    }
    addDays(dateObj, numDays) {
        dateObj.setDate(dateObj.getDate() + numDays);
        console.log(dateObj);
        var month = (dateObj.getMonth() + 1) + '';
        const date = dateObj.getDate() + '';
        const year = dateObj.getFullYear() + '';
        var expiryDate = year + '-' + month + '-' + date;
        return expiryDate;
    }
    ngOnInit() {
        var today = new Date();
        var first_date = today.getFullYear() + "-";
        first_date += (today.getMonth() + 1 < 10 ? "0" + (today.getMonth() + 1).toString() : (today.getMonth() + 1).toString()) + "-";
        first_date += "01";
        console.log(first_date);
        var last_date = today.getFullYear() + "-";
        last_date += (today.getMonth() + 1 < 10 ? "0" + (today.getMonth() + 1).toString() : (today.getMonth() + 1).toString()) + "-";
        last_date += new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
        console.log("<br>" + last_date);
        this.subscription.fromDate = first_date;
        this.subscription.toDate = last_date;
        console.log(this.subscription.fromDate, this.subscription.toDate, 'sample');
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
    }
};
SalesReportPage.ctorParameters = () => [
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
SalesReportPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sales-report',
        template: __webpack_require__(/*! raw-loader!./sales-report.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/sales-report/sales-report.page.html"),
        styles: [__webpack_require__(/*! ./sales-report.page.scss */ "./src/app/delar-application/sales-report/sales-report.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], SalesReportPage);



/***/ })

}]);
//# sourceMappingURL=sales-report-sales-report-module-es2015.js.map