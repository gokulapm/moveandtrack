(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-dashboard-assert-status-list-assert-status-list-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"dealerHeader\">\n    <ion-row>\n      <ion-icon class=\"iconSize25px\" (click)=\"closePage()\" name=\"arrow-back\"></ion-icon>\n      <ion-title>Asset details</ion-title>\n    </ion-row>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-segment value=\"{{selectedTab}}\" scrollable (ionChange)=\"changeFilter($event)\">\n    <ion-segment-button *ngFor=\"let catagory of catagories\" value=\"{{catagory}}\">\n      <ion-label>{{catagory}}<ion-badge\n          [ngClass]='{\"notification\": selectedTab === catagory, \"unnotification\": selectedTab != catagory}'>{{countRowData[catagory]}}\n        </ion-badge>\n      </ion-label>\n    </ion-segment-button>\n  </ion-segment>\n  <ion-row>\n    <ion-col size=12 size-sm=\"12\" size-lg=\"4\" size-md=\"6\" *ngFor=\"let showList of displayData\">\n      <ion-card style=\"border-left: 4px solid #7c68f8; margin: 0px;\">\n        <ion-card-content style=\"padding: 7px;\">\n          <app-assert-list-card [cardDetail]=\"showList\"></app-assert-list-card>\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"doInfinite($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>"

/***/ }),

/***/ "./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.module.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: AssertStatusListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssertStatusListPageModule", function() { return AssertStatusListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dealer_component_dealer_component_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../dealer-component/dealer-component.module */ "./src/app/delar-application/dealer-component/dealer-component.module.ts");
/* harmony import */ var _assert_status_list_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./assert-status-list.page */ "./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.ts");








const routes = [
    {
        path: '',
        component: _assert_status_list_page__WEBPACK_IMPORTED_MODULE_7__["AssertStatusListPage"]
    }
];
let AssertStatusListPageModule = class AssertStatusListPageModule {
};
AssertStatusListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _dealer_component_dealer_component_module__WEBPACK_IMPORTED_MODULE_6__["DealerComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_assert_status_list_page__WEBPACK_IMPORTED_MODULE_7__["AssertStatusListPage"]]
    })
], AssertStatusListPageModule);



/***/ }),

/***/ "./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-segment-button {\n  --color-checked: #7d67f8;\n}\n\n.iconSize25px {\n  font-size: 25px;\n}\n\n.notification {\n  background-color: #7c68f8;\n  font-size: 10px;\n  font-family: sans-serif;\n  margin-bottom: -4px;\n  margin-left: 4px;\n  border-radius: 100%;\n  padding: 4px;\n}\n\n.unnotification {\n  background-color: #a8a8a8;\n  font-size: 10px;\n  font-family: sans-serif;\n  margin-bottom: -4px;\n  margin-left: 4px;\n  border-radius: 50%;\n  padding: 4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vbmV3LWRhc2hib2FyZC9hc3NlcnQtc3RhdHVzLWxpc3QvYXNzZXJ0LXN0YXR1cy1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vbmV3LWRhc2hib2FyZC9hc3NlcnQtc3RhdHVzLWxpc3QvYXNzZXJ0LXN0YXR1cy1saXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHdCQUFBO0FDQ0o7O0FERUU7RUFDRSxlQUFBO0FDQ0o7O0FEQ0E7RUFDRSx5QkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNFRjs7QURFQTtFQUNFLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vbmV3LWRhc2hib2FyZC9hc3NlcnQtc3RhdHVzLWxpc3QvYXNzZXJ0LXN0YXR1cy1saXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gICAgLS1jb2xvci1jaGVja2VkOiAjN2Q2N2Y4O1xuICB9XG5cbiAgLmljb25TaXplMjVweHtcbiAgICBmb250LXNpemU6IDI1cHg7XG59XG4ubm90aWZpY2F0aW9ue1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjN2M2OGY4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xuICBtYXJnaW4tYm90dG9tOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogNHB4O1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICBwYWRkaW5nOiA0cHg7XG5cbn1cblxuLnVubm90aWZpY2F0aW9ue1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYThhOGE4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xuICBtYXJnaW4tYm90dG9tOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogNHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHBhZGRpbmc6IDRweDtcblxufSIsImlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gIC0tY29sb3ItY2hlY2tlZDogIzdkNjdmODtcbn1cblxuLmljb25TaXplMjVweCB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLm5vdGlmaWNhdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM3YzY4Zjg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG4gIG1hcmdpbi1ib3R0b206IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiA0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIHBhZGRpbmc6IDRweDtcbn1cblxuLnVubm90aWZpY2F0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2E4YThhODtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgbWFyZ2luLWJvdHRvbTogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IDRweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiA0cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.ts ***!
  \***********************************************************************************************/
/*! exports provided: AssertStatusListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssertStatusListPage", function() { return AssertStatusListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





let AssertStatusListPage = class AssertStatusListPage {
    constructor(location, activatedRoute) {
        this.location = location;
        this.activatedRoute = activatedRoute;
        this.catagories = ["All", "Online", "Offline", "Expiry"];
        this.count = 15;
        this.currentPage = 1;
    }
    changeFilter(data) {
        this.selectedTab = data.detail.value;
        // if(this.selectedTab == "All")
        // this.selectedTab = "Total"
        this.dasboardDetail = JSON.parse(localStorage.dealerLoginData);
        this.showList = this.dasboardDetail.assets[this.selectedTab];
        // this.showList = [{"imeiNo": "876756454565768", "status":"Online", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm", "companySuffix":"76876"}, {"imeiNo": "876756454565768", "status":"Offiline", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm"}, {"imeiNo": "876756454565768", "status":"Online", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm"},{"imeiNo": "876756454565768", "status":"Online", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm"}]
        this.getAssertList();
    }
    closePage() {
        this.location.back();
    }
    setDisplayData() {
        if (this.showList.length > this.count) {
            this.displayData = this.showList.slice(0, this.count);
        }
        else {
            this.displayData = this.showList;
        }
    }
    getAssertList() {
        this.setDisplayData();
    }
    doInfinite(event) {
        console.log("event trigger");
        setTimeout(() => {
            console.log(this.showList);
            this.displayData.push(...this.showList.slice(this.currentPage * this.count, (this.currentPage + 1) * this.count));
            this.currentPage++;
            event.target.complete();
            if (this.displayData.length == this.showList.length) {
                event.target.disabled = true;
                setTimeout(() => {
                    event.target.disabled = false;
                }, 3000);
            }
        }, 500);
    }
    ngOnInit() {
        this.selectedTab = this.activatedRoute.snapshot.paramMap.get("type");
        // if(this.selectedTab == "All")
        // this.selectedTab = "Total"
        this.dasboardDetail = JSON.parse(localStorage.dealerLoginData);
        this.showList = this.dasboardDetail.assets[this.selectedTab];
        // this.showList = [{"imeiNo": "876756454565768", "status":"Online", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm", "companySuffix":"76876"}, {"imeiNo": "876756454565768", "status":"Offiline", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm"}, {"imeiNo": "876756454565768", "status":"Online", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm"},{"imeiNo": "876756454565768", "status":"Online", "warrantyExp":"27/02/45", "vin":"apm765", "timestamp":"27-09-2020 07:10:00 pm" ,"simNo": "9087657583787 / 4567655677655", "companyId": "apm"}]
        this.getAssertList();
        this.countRowData = {
            All: this.dasboardDetail.assets["All"].length,
            Expiry: this.dasboardDetail.assets["Expiry"].length,
            Stocks: this.dasboardDetail.assets["Stocks"].length,
            Online: this.dasboardDetail.assets["Online"].length,
            Offline: this.dasboardDetail.assets["Offline"].length,
        };
    }
};
AssertStatusListPage.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonInfiniteScroll"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonInfiniteScroll"])
], AssertStatusListPage.prototype, "infiniteScroll", void 0);
AssertStatusListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-assert-status-list',
        template: __webpack_require__(/*! raw-loader!./assert-status-list.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.html"),
        styles: [__webpack_require__(/*! ./assert-status-list.page.scss */ "./src/app/delar-application/new-dashboard/assert-status-list/assert-status-list.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
], AssertStatusListPage);



/***/ })

}]);
//# sourceMappingURL=new-dashboard-assert-status-list-assert-status-list-module-es2015.js.map