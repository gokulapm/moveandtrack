(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["diagnosis-diagnosis-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/diagnosis/diagnosis.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/diagnosis/diagnosis.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class='header'>\n  <ion-toolbar>\n      <ion-grid>\n          <ion-row></ion-row>\n          <ion-row>\n              <ion-col size='2'>\n                  <ion-menu-button></ion-menu-button>\n              </ion-col>\n              <ion-col size='8.5' style='align-self: center;'>\n                  <ion-row>\n                      <ion-label> Diagnosis </ion-label>\n                  </ion-row>\n              </ion-col>\n              <ion-col size='1.5' style=\"padding: 0px;\">\n                      <ion-img [src]=\"app.logo\"></ion-img>\n              </ion-col>\n          </ion-row>\n      </ion-grid>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<app-vts-diagnosis></app-vts-diagnosis>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-row>\n  <ion-col size=\"6\">\n    <ion-card>\n      <ion-card-header>\n        \n        <ion-row style=\"margin-left: 20%;\n        margin-top: 3%;\">\n    <ion-col  size=\"6\" >   <ion-searchbar showCancelButton searchIcon=\"search\" animated cancel-button-icon (ionChange)=\"hide($event)\" (ionCancel)=\"hideSearch()\" style=\"width: 125%;margin-left: -57px;\"    [(ngModel)]=\"searchTerm\" placeholder=\"enter your imei\" ></ion-searchbar></ion-col>\n    <!-- <ion-col  size=\"6\" style=\"border: 1px solid grey;\"><ion-input placeholder=\"Enter the ImeiNo\" (ionChange)=\"hide()\" [(ngModel)]=\"imeiNo\"></ion-input></ion-col> -->\n      <ion-col size=\"4\" ><ion-button (click)=\"submit()\" [disabled]='show'>Submit</ion-button></ion-col>\n      </ion-row>\n\n      </ion-card-header>\n      <ion-card-content>\n        \n        <ion-row *ngFor='let imei of imei' style=\"margin-left: 20%;\n        margin-top: 3%;\" >\n          <ion-col size=\"12\" size-lg=\"6\" class='input-label' (click)=\"submit(imei)\">\n              <ion-label >{{imei}}</ion-label>\n          </ion-col>\n          \n        </ion-row>\n      </ion-card-content> \n    </ion-card>\n  </ion-col>\n  <ion-col size=\"6\">\n    <ion-card>\n      <ion-card-header>\n      <ion-card>\n        <ion-row style=\"margin-left: 20%;\n        margin-top: 3%;\"><ion-col size=\"12\">{{data.rawData}}</ion-col>\n        \n      </ion-row>\n    </ion-card>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-row *ngFor='let details of data.packetItems'>\n          <ion-col size=\"6\" size-lg=\"6\" class='input-label'>\n              <ion-label>{{details.packetName}}</ion-label>\n          </ion-col>\n          <ion-col size=\"6\" size-lg=\"6\" size-sm=\"12\" size-md=\"6\" size-sm=\"8\">\n            <ion-item class=\"input-item\" >\n            <ion-label  *ngIf='details.packetValue != \"yes\" && details.packetValue != \"no\"'>{{details.packetValue}}\n            </ion-label>\n          \n      <ion-label *ngIf=\"details.packetValue == 'yes'\"> <ion-icon name=\"checkmark-circle\" style=\"color:green\" ></ion-icon></ion-label>\n           \n        \n       <ion-label  *ngIf=\"details.packetValue == 'no'\"><ion-icon name=\"close-circle\" style=\"color:red\" ></ion-icon></ion-label>\n      \n        </ion-item>\n        </ion-col>\n        </ion-row>\n      </ion-card-content> \n    </ion-card>\n\n\n\n  </ion-col>\n</ion-row>\n"

/***/ }),

/***/ "./src/app/diagnosis/diagnosis.module.ts":
/*!***********************************************!*\
  !*** ./src/app/diagnosis/diagnosis.module.ts ***!
  \***********************************************/
/*! exports provided: DiagnosisPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiagnosisPageModule", function() { return DiagnosisPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _vts_diagnosis_vts_diagnosis_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vts-diagnosis/vts-diagnosis.component */ "./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.ts");
/* harmony import */ var _diagnosis_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./diagnosis.page */ "./src/app/diagnosis/diagnosis.page.ts");








const routes = [
    {
        path: '',
        component: _diagnosis_page__WEBPACK_IMPORTED_MODULE_7__["DiagnosisPage"]
    }
];
let DiagnosisPageModule = class DiagnosisPageModule {
};
DiagnosisPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_diagnosis_page__WEBPACK_IMPORTED_MODULE_7__["DiagnosisPage"], _vts_diagnosis_vts_diagnosis_component__WEBPACK_IMPORTED_MODULE_6__["VtsDiagnosisComponent"]]
    })
], DiagnosisPageModule);



/***/ }),

/***/ "./src/app/diagnosis/diagnosis.page.scss":
/*!***********************************************!*\
  !*** ./src/app/diagnosis/diagnosis.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpYWdub3Npcy9kaWFnbm9zaXMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/diagnosis/diagnosis.page.ts":
/*!*********************************************!*\
  !*** ./src/app/diagnosis/diagnosis.page.ts ***!
  \*********************************************/
/*! exports provided: DiagnosisPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiagnosisPage", function() { return DiagnosisPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let DiagnosisPage = class DiagnosisPage {
    constructor(platform) {
        this.platform = platform;
        this.app = { logo: 'logo.png' };
    }
    ngOnInit() {
        this.app["logo"] = localStorage.companyLogo;
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
    }
};
DiagnosisPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }
];
DiagnosisPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-diagnosis',
        template: __webpack_require__(/*! raw-loader!./diagnosis.page.html */ "./node_modules/raw-loader/index.js!./src/app/diagnosis/diagnosis.page.html"),
        styles: [__webpack_require__(/*! ./diagnosis.page.scss */ "./src/app/diagnosis/diagnosis.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]])
], DiagnosisPage);



/***/ }),

/***/ "./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".input-item {\n  border: 1px;\n  border-radius: 5px;\n  height: 40px;\n  line-height: 19px;\n}\n\n.input-label {\n  text-align: center;\n  font-size: 20px;\n  padding-top: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGlhZ25vc2lzL3Z0cy1kaWFnbm9zaXMvdnRzLWRpYWdub3Npcy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZGlhZ25vc2lzL3Z0cy1kaWFnbm9zaXMvdnRzLWRpYWdub3Npcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQ0FKOztBREdBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNBQSIsImZpbGUiOiJzcmMvYXBwL2RpYWdub3Npcy92dHMtZGlhZ25vc2lzL3Z0cy1kaWFnbm9zaXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5wdXQtaXRlbXtcbiAgICAvLyBib3JkZXI6MXB4IHNvbGlkICNmM2UzZjY7XG4gICAgYm9yZGVyOjFweCAgO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gICAgXG59ICAgXG4uaW5wdXQtbGFiZWx7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5mb250LXNpemU6IDIwcHg7XG5wYWRkaW5nLXRvcDogMTRweDtcbn0iLCIuaW5wdXQtaXRlbSB7XG4gIGJvcmRlcjogMXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGhlaWdodDogNDBweDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG59XG5cbi5pbnB1dC1sYWJlbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nLXRvcDogMTRweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.ts ***!
  \********************************************************************/
/*! exports provided: VtsDiagnosisComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VtsDiagnosisComponent", function() { return VtsDiagnosisComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");





let VtsDiagnosisComponent = class VtsDiagnosisComponent {
    constructor(ajaxService, commonService) {
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.dat = "$,500,869247046192016,89910473121803851084,1,0,1,*";
        this.show = true;
        this.colors = ['sfas', 'asefas', 'sdf', 'safsafesa', 'asefaf'];
        // hide(){
        // }
        this.data = '';
        this.imei = ["866551036278671", "866551036278672", "866551036278673", "866551036278674"];
        // imei=[];
        this.isItemAvailable = false;
        this.items = [];
    }
    submit(imei) {
        this.imeiNo = imei;
        this.show = false;
        const body = {
            "imeiNo": this.imeiNo
        };
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/simcard/get/Diagnosis';
        this.ajaxService.ajaxPostWithBody(url, body)
            .subscribe(res => {
            this.data = JSON.parse(res);
            //  this.showDatas = res.split(",");
            //  this.showDatas =["$", "500", "869247046192016", "89910473121803851084", "1", "0", "1", "*"]
            console.log(res);
            // if(this.data.ra)
        });
    }
    initializeItems() {
        this.imei;
    }
    hide(ev) {
        if (ev.target.value.length == 15) {
            this.show = false;
        }
        else {
            this.show = true;
            this.data = "";
        }
        this.initializeItems();
        // set val to the value of the searchbar
        const val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.isItemAvailable = true;
            this.imei = this.imei.filter((item) => {
                return (item.indexOf(val) > -1);
            });
        }
        else {
            this.isItemAvailable = false;
            this.getImei();
        }
    }
    getImei() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/simcard/get/imei/list';
        this.ajaxService.ajaxGetWithString(url)
            .subscribe(res => {
            this.imei = JSON.parse(res);
            //  this.showDatas = res.split(",");
            //  this.showDatas =["$", "500", "869247046192016", "89910473121803851084", "1", "0", "1", "*"]
            console.log(res);
        });
    }
    ngOnInit() {
        this.getImei();
    }
};
VtsDiagnosisComponent.ctorParameters = () => [
    { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"] },
    { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"] }
];
VtsDiagnosisComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-vts-diagnosis',
        template: __webpack_require__(/*! raw-loader!./vts-diagnosis.component.html */ "./node_modules/raw-loader/index.js!./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.html"),
        styles: [__webpack_require__(/*! ./vts-diagnosis.component.scss */ "./src/app/diagnosis/vts-diagnosis/vts-diagnosis.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"],
        src_app_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]])
], VtsDiagnosisComponent);



/***/ })

}]);
//# sourceMappingURL=diagnosis-diagnosis-module-es2015.js.map