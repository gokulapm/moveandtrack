(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["skt-broad-broadcast-sms-broadcast-sms-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/skt/broad/broadcast-sms/broadcast-sms.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/skt/broad/broadcast-sms/broadcast-sms.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <div class=\"broadcast-wrapper\">\n     <ion-row  class=\"header-section\">\n      <ion-col size=\"2\" size-sm=\"2\" size-md=\"2\" size-lg=\"0\"> \n        <ion-menu-button></ion-menu-button>\n      </ion-col>  \n      <ion-col size=\"10\" size-sm=\"10\" size-md=\"10\" size-lg=\"12\" id=\"btn-wrapper\">\n        Broadcast Sms Report\n      </ion-col>\n    </ion-row>    \n  <ion-row id=\"form-container\">\n  <ion-col >\n    <ion-button class=\"btnsize\"   (click)=\"broadCast()\">Add New Broadcast</ion-button>\n    </ion-col>\n  </ion-row>\n  </div> \n  <div id=\"export-wrapper\">\n    <!-- <ion-img src=\"assets/student_Details/print.svg\"  class=\"toolbar-row\" (click)=\"btnOnClick()\"></ion-img> -->\n    <ion-img src=\"assets/student_Details/pdf.svg\" class=\"toolbar-row\" (click)=\"createPdf()\"></ion-img>\n    <ion-img src=\"assets/student_Details/excel.svg\" class=\"toolbar-row\"  (click)=\"exportToExcel()\"> </ion-img>\n       <!-- <ion-img src=\"assets/student_Details/refresher.svg\" class=\"toolbar-row\" (click)=\"refresh()\"></ion-img> -->\n    </div> \n   </ion-toolbar>\n</ion-header>\n<ion-content>\n <div class=\"grid_Container\">\n  <jqxGrid\n  (onRowselect)=\"myGridOnRowSelect($event)\"\n  [pageable]=\"true\"\n  [selectionmode]=\"'singlerow'\"\n  [showfilterrow]=\"true\"\n  [filterable]=\"true\"\n  [sortable]=\"true\"\n  [width]=\"'100%'\"\n  [source]=\"dataAdapter\" \n  [columns]=\"columns\" \n  [autoheight]=\"true\"\n  [theme]=\"'material'\"\n  [enabletooltips]=\"true\" \n  #myGrid \n  style=\"font-size:16px;text-align: center !important;\">\n  </jqxGrid>\n</div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/skt/broad/broadcast-sms/broadcast-sms.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/skt/broad/broadcast-sms/broadcast-sms.module.ts ***!
  \*****************************************************************/
/*! exports provided: BroadcastSmsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastSmsPageModule", function() { return BroadcastSmsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _broadcast_sms_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./broadcast-sms.page */ "./src/app/skt/broad/broadcast-sms/broadcast-sms.page.ts");
/* harmony import */ var _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../sktcomponents.module */ "./src/app/skt/sktcomponents.module.ts");
/* harmony import */ var src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared-mod/shared-mod.module */ "./src/app/shared-mod/shared-mod.module.ts");









const routes = [
    {
        path: '',
        component: _broadcast_sms_page__WEBPACK_IMPORTED_MODULE_6__["BroadcastSmsPage"]
    }
];
let BroadcastSmsPageModule = class BroadcastSmsPageModule {
};
BroadcastSmsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__["SktComponentsModule"],
            src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__["SharedModModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_broadcast_sms_page__WEBPACK_IMPORTED_MODULE_6__["BroadcastSmsPage"]]
    })
], BroadcastSmsPageModule);



/***/ }),

/***/ "./src/app/skt/broad/broadcast-sms/broadcast-sms.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/skt/broad/broadcast-sms/broadcast-sms.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".broadcast-wrapper, .grid_Container {\n  border: 1px solid #c7c3c3;\n  margin: 5px 10px 0;\n}\n\n.grid_Container {\n  margin: 0px 10px 0px;\n}\n\n.header-section {\n  background-color: #e8e8e8;\n  border: 1px solid #cac2c2;\n}\n\n#export-wrapper {\n  text-align: right;\n  background-color: #e8e8e8;\n  height: 42px;\n  border-top: 1px solid gainsboro;\n  margin: 0px 10px 0px;\n  border-left: 2px solid #c7c7c7;\n}\n\n#btn-wrapper {\n  font-weight: 700;\n  text-align: center;\n  background-color: #e8e8e8;\n  height: 40px;\n  padding: 0px;\n  font-size: 20px;\n  border-bottom: 1px solid #cac2c2;\n}\n\n.toolbar-row {\n  height: 40px;\n  width: 40px;\n  display: inline-block;\n  margin: 0px;\n  border: 1px solid #b9b7b7;\n  padding: 5px;\n}\n\n.toolbar-row:hover {\n  background-color: whitesmoke;\n}\n\n.mytable:hover {\n  background-color: blue;\n}\n\n.align-self {\n  align-self: center;\n}\n\n.datebox {\n  background: #f3f3f3;\n  color: #383838;\n  max-width: -webkit-fill-available;\n  height: 34px;\n  border: 1px solid lightgray;\n  outline: none;\n  text-align: center;\n  width: 100%;\n}\n\n.btnsize {\n  font-size: 13px;\n  --background:#388fff;\n  height: 32px;\n}\n\n#form-container {\n  font-size: 14px;\n  padding: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvc2t0L2Jyb2FkL2Jyb2FkY2FzdC1zbXMvYnJvYWRjYXN0LXNtcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3NrdC9icm9hZC9icm9hZGNhc3Qtc21zL2Jyb2FkY2FzdC1zbXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENDO0VBQ0csb0JBQUE7QUNFSjs7QURBQztFQUNDLHlCQUFBO0VBQ0EseUJBQUE7QUNHRjs7QUREQztFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsK0JBQUE7RUFDQSxvQkFBQTtFQUNBLDhCQUFBO0FDSUg7O0FERkM7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQ0FBQTtBQ0tIOztBREhDO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUNNSDs7QURKQTtFQUNJLDRCQUFBO0FDT0o7O0FESkM7RUFDRSxzQkFBQTtBQ09IOztBREpDO0VBQ0Msa0JBQUE7QUNPRjs7QURMQTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlDQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ1FGOztBRE5BO0VBQ0UsZUFBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtBQ1NGOztBRE5BO0VBQ0UsZUFBQTtFQUNBLGFBQUE7QUNTRiIsImZpbGUiOiJzcmMvYXBwL3NrdC9icm9hZC9icm9hZGNhc3Qtc21zL2Jyb2FkY2FzdC1zbXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJyb2FkY2FzdC13cmFwcGVyLC5ncmlkX0NvbnRhaW5lcntcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigxOTksIDE5NSwgMTk1KTtcclxuICAgIG1hcmdpbjogNXB4IDEwcHggMDtcclxuIH1cclxuIC5ncmlkX0NvbnRhaW5lcntcclxuICAgIG1hcmdpbjogMHB4IDEwcHggMHB4OyAgXHJcbiB9XHJcbiAuaGVhZGVyLXNlY3Rpb257XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjY2FjMmMyO1xyXG59XHJcbiAjZXhwb3J0LXdyYXBwZXJ7XHJcbiAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gICBoZWlnaHQ6IDQycHg7XHJcbiAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBnYWluc2Jvcm87XHJcbiAgIG1hcmdpbjogMHB4IDEwcHggMHB4O1xyXG4gICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNjN2M3Yzc7XHJcbiB9XHJcbiAjYnRuLXdyYXBwZXJ7XHJcbiAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gICBwYWRkaW5nOiAwcHg7XHJcbiAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgYm9yZGVyLWJvdHRvbToxcHggc29saWQgI2NhYzJjMjtcclxuICB9XHJcbiAudG9vbGJhci1yb3d7XHJcbiAgIGhlaWdodDogNDBweDtcclxuICAgd2lkdGg6IDQwcHg7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgbWFyZ2luOjBweDtcclxuICAgYm9yZGVyOiAxcHggc29saWQgI2I5YjdiNztcclxuICAgcGFkZGluZzogNXB4O1xyXG59XHJcbi50b29sYmFyLXJvdzpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlc21va2U7XHJcbn1cclxuXHJcbiAubXl0YWJsZTpob3ZlciB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XHJcbiB9XHJcblxyXG4gLmFsaWduLXNlbGZ7XHJcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xyXG59XHJcbi5kYXRlYm94e1xyXG4gIGJhY2tncm91bmQ6ICNmM2YzZjM7XHJcbiAgY29sb3I6ICMzODM4Mzg7XHJcbiAgbWF4LXdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xyXG4gIGhlaWdodDogMzRweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmJ0bnNpemV7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIC0tYmFja2dyb3VuZDojMzg4ZmZmO1xyXG4gIGhlaWdodDogMzJweDtcclxuICBcclxufVxyXG4jZm9ybS1jb250YWluZXJ7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbn0iLCIuYnJvYWRjYXN0LXdyYXBwZXIsIC5ncmlkX0NvbnRhaW5lciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjN2MzYzM7XG4gIG1hcmdpbjogNXB4IDEwcHggMDtcbn1cblxuLmdyaWRfQ29udGFpbmVyIHtcbiAgbWFyZ2luOiAwcHggMTBweCAwcHg7XG59XG5cbi5oZWFkZXItc2VjdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjYWMyYzI7XG59XG5cbiNleHBvcnQtd3JhcHBlciB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xuICBoZWlnaHQ6IDQycHg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBnYWluc2Jvcm87XG4gIG1hcmdpbjogMHB4IDEwcHggMHB4O1xuICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNjN2M3Yzc7XG59XG5cbiNidG4td3JhcHBlciB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjYWMyYzI7XG59XG5cbi50b29sYmFyLXJvdyB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNiOWI3Yjc7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLnRvb2xiYXItcm93OmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcbn1cblxuLm15dGFibGU6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xufVxuXG4uYWxpZ24tc2VsZiB7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLmRhdGVib3gge1xuICBiYWNrZ3JvdW5kOiAjZjNmM2YzO1xuICBjb2xvcjogIzM4MzgzODtcbiAgbWF4LXdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xuICBoZWlnaHQ6IDM0cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgb3V0bGluZTogbm9uZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmJ0bnNpemUge1xuICBmb250LXNpemU6IDEzcHg7XG4gIC0tYmFja2dyb3VuZDojMzg4ZmZmO1xuICBoZWlnaHQ6IDMycHg7XG59XG5cbiNmb3JtLWNvbnRhaW5lciB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgcGFkZGluZzogMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/skt/broad/broadcast-sms/broadcast-sms.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/skt/broad/broadcast-sms/broadcast-sms.page.ts ***!
  \***************************************************************/
/*! exports provided: BroadcastSmsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastSmsPage", function() { return BroadcastSmsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jqwidgets-ng/jqxgrid */ "./node_modules/jqwidgets-ng/fesm2015/jqwidgets-ng-jqxgrid.js");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/export-excel.service */ "./src/app/services/export-excel.service.ts");
/* harmony import */ var src_app_services_skt_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/skt.service */ "./src/app/services/skt.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _broadcast_additional_broadcast_additional_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../broadcast-additional/broadcast-additional.component */ "./src/app/skt/broad/broadcast-additional/broadcast-additional.component.ts");











src_app_services_skt_service__WEBPACK_IMPORTED_MODULE_7__["SktService"];
let BroadcastSmsPage = class BroadcastSmsPage {
    constructor(modalController, formBuilder, ajaxService, ete, commonService, platform) {
        this.modalController = modalController;
        this.formBuilder = formBuilder;
        this.ajaxService = ajaxService;
        this.ete = ete;
        this.commonService = commonService;
        this.platform = platform;
        this.pdfdata = [];
        this.head = ['Category', 'From Mobile', 'To Mobile', 'Message', 'Event Time', 'Server Time'];
    }
    btnOnClick() {
        let gridContent = this.myGrid.exportdata('html');
        let newWindow = window.open('', '', 'width=800, height=500'), document = newWindow.document.open(), pageContent = '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '<meta charset="utf-8" />\n' +
            '<title>Broad cast sms report</title>\n' +
            '</head>\n' +
            '<body>\n' + gridContent + '\n</body>\n</html>';
        document.write(pageContent);
        document.close();
        newWindow.print();
    }
    ;
    createPdf() {
        this.commonService.createPdf(this.head, this.pdfdata, "Broadcast Sms Report", this.myPlatform, "Broadcast Sms Report");
    }
    exportToExcel() {
        let reportData = {
            title: 'Broadcast Sms Report',
            data: this.pdfdata,
            headers: this.head
        };
        this.ete.exportExcel(reportData);
    }
    broadCast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _broadcast_additional_broadcast_additional_component__WEBPACK_IMPORTED_MODULE_10__["BroadcastAdditionalComponent"],
                cssClass: 'broadcast-css'
            });
            modal.onDidDismiss().then(() => {
                this.myGrid.clearselection();
                this.getDatas();
            });
            return yield modal.present();
        });
    }
    myGridOnRowSelect(event) {
        this.selectedRow = event.args.row;
    }
    ngAfterViewInit() {
        this.getDatas();
    }
    getDatas() {
        this.companyDetail = {
            branchID: localStorage.getItem('corpId'),
            companyID: localStorage.getItem('corpId'),
            userId: localStorage.getItem('userName')
        };
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + `/report/getbrodcastSms?companyId=${this.companyDetail.companyID}&branchId=${this.companyDetail.branchID}`;
        this.ajaxService.ajaxGet(url).subscribe(res => {
            this.broadOdj = res;
            for (var i = 0; i < this.broadOdj.length; i++) {
                this.pdfdata.push([this.broadOdj[i].category, this.broadOdj[i].fromMobile, this.broadOdj[i].toMobile, this.broadOdj[i].msg, this.broadOdj[i].eventTimeStamp, this.broadOdj[i].serverTimeStamp]);
            }
            this.renderer = (row, column, value) => {
                if (value == "" || null || undefined || value == ",") {
                    return "---";
                }
                else {
                    return '<span  style="line-height:32px;font-size:11px;color:darkblue;margin:auto;padding:0px 5px">' + value + '</span>';
                }
            };
            this.source = { localdata: this.broadOdj };
            this.dataAdapter = new jqx.dataAdapter(this.source);
            this.columns = [
                { text: 'Category', datafield: 'category', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                { text: 'From Mobile', datafield: 'fromMobile', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                { text: 'To Mobile', datafield: 'toMobile', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                { text: 'Message', datafield: 'msg', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                { text: 'Event Time', datafield: 'eventTimeStamp', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                { text: 'Server Time', datafield: 'serverTimeStamp', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
            ];
            this.myGrid.updatebounddata();
            this.myGrid.unselectrow;
        });
    }
    refresh() {
        this.getDatas();
        this.commonService.presentToast("Data refreshed successfully.");
    }
    ngOnInit() {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        this.getDatas();
    }
};
BroadcastSmsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormBuilder"] },
    { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"] },
    { type: src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_6__["ExportExcelService"] },
    { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myGrid', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_3__["jqxGridComponent"])
], BroadcastSmsPage.prototype, "myGrid", void 0);
BroadcastSmsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-broadcast-sms',
        template: __webpack_require__(/*! raw-loader!./broadcast-sms.page.html */ "./node_modules/raw-loader/index.js!./src/app/skt/broad/broadcast-sms/broadcast-sms.page.html"),
        styles: [__webpack_require__(/*! ./broadcast-sms.page.scss */ "./src/app/skt/broad/broadcast-sms/broadcast-sms.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormBuilder"],
        src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"],
        src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_6__["ExportExcelService"],
        src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]])
], BroadcastSmsPage);



/***/ })

}]);
//# sourceMappingURL=skt-broad-broadcast-sms-broadcast-sms-module-es2015.js.map