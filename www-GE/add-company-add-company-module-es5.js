(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-company-add-company-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/dashboard/add-company/add-company.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/dashboard/add-company/add-company.page.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n\n    <ion-toolbar  [ngClass]=\"{'dealerHeader' : appName != 'Armoron' , 'header-background-color': appName == 'Armoron'}\"  mode=\"md\" >\n\n    <ion-buttons *ngIf=\"appName != 'Armoron'\" slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-icon (click)=\"backbutton()\" style=\"padding: 10px 10px 0px 10px;\" *ngIf=\"appName == 'Armoron'\" name=\"arrow-back\"></ion-icon>\n    <ion-title>Add company</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-row *ngIf=\"appName != 'Armoron'\">\n    <ion-col size=\"12\" class=\"form-field\">\n      <form class='formPadding' [formGroup]=\"addCompany\">\n        <ion-row>\n          <ion-col size=\"12\" size-sm=\"12\" size-lg=\"6\" size-md=\"12\">\n            <ion-input class =\"heading\" [ngClass]=\"{'armoron' : appName == 'Armoron', 'heading' : appName != 'Armoron'}\" \n            type=\"text\" formControlName=\"companyName\" placeholder=\"Company name\"></ion-input>\n          </ion-col>\n          <ion-col size=\"12\" size-sm=\"12\" size-lg=\"6\" size-md=\"12\">\n            <ion-input class =\"heading\" [ngClass]=\"{'armoron' : appName == 'Armoron', 'heading' : appName != 'Armoron'}\" \n            type=\"text\" formControlName=\"email\" placeholder=\"Email id\"></ion-input>\n          </ion-col>\n          <!------->\n          <ion-col size=\"12\" size-sm=\"12\" size-lg=\"6\" size-md=\"12\">\n            <ion-input class =\"heading\" [ngClass]=\"{'armoron' : appName == 'Armoron', 'heading' : appName != 'Armoron'}\" \n            type=\"text\" formControlName=\"loginId\" placeholder=\"Login id\"></ion-input>\n          </ion-col>\n          <!------->\n          <ion-col size=\"12\" size-sm=\"12\" size-lg=\"6\" size-md=\"12\">\n            <ion-input class =\"heading\" [ngClass]=\"{'armoron' : appName == 'Armoron', 'heading' : appName != 'Armoron'}\" \n            type=\"text\" formControlName=\"password\" placeholder=\"Password\"></ion-input>\n          </ion-col>\n          <!------->\n          <ion-col size=\"12\" size-sm=\"12\" size-lg=\"6\" size-md=\"12\">\n            <ion-input class =\"heading\" [ngClass]=\"{'armoron' : appName == 'Armoron', 'heading' : appName != 'Armoron'}\" \n            type=\"text\" formControlName=\"address\" placeholder=\"Address\"></ion-input>\n          </ion-col>\n          <!------->\n          <ion-col size=\"12\" size-sm=\"12\" size-lg=\"6\" size-md=\"12\">\n            <ion-input class =\"heading\" [ngClass]=\"{'armoron' : appName == 'Armoron', 'heading' : appName != 'Armoron'}\" \n            type=\"number\" formControlName=\"contactNo\" placeholder=\"Contact number\"></ion-input>\n          </ion-col>\n          <!------->\n          <ion-col size=\"12\" size-sm=\"12\" size-lg=\"6\" size-md=\"12\">\n            <ion-select class =\"heading\" [ngClass]=\"{'armoron' : appName == 'Armoron', 'heading' : appName != 'Armoron'}\" \n            formControlName=\"country\" placeHolder=\"Country\">\n              <ion-select-option *ngFor=\"let country of country\" [value]=\"country\" >\n                {{country}}\n              </ion-select-option>\n            </ion-select>\n          </ion-col>\n        </ion-row>\n      </form>\n\n      <ion-col size=\"12\">\n        <ion-row>\n          <ion-col size=\"12\" style=\"text-align: center;\">\n            <ion-button (click)=\"onSubmit()\" id=\"submitbtn\">\n              Add\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-col>\n  </ion-row>\n\n\n  <ion-row *ngIf=\"appName == 'Armoron'\">\n    <form style=\"width:100%;\" [formGroup]=\"addCompany\">\n\n      <!-- <ion-list> -->\n        <!-- <ion-item>\n          <ion-label position=\"fixed\">First_name:</ion-label>\n          <ion-input formControlName=\"firstName\" type=\"text\" placeholder=\"First name\">\n          </ion-input>\n        </ion-item> -->\n        <!-- <ion-item>\n          <ion-label position=\"fixed\">Last_name:</ion-label>\n          <ion-input formControlName=\"lastname\" type=\"text\" placeholder=\"Last_name\">\n          </ion-input>\n        </ion-item> -->\n        <!-- <ion-item>\n          <ion-label position=\"fixed\">DOB:</ion-label>\n          <ion-input formControlName=\"date\" type=\"date\">\n          </ion-input>\n        </ion-item> -->\n        <!-- <ion-item>\n          <ion-label position=\"fixed\">Mobile:</ion-label>\n        \n          </ion-input>\n        </ion-item> -->\n        <!-- <ion-item>\n          <ion-label position=\"fixed\">Mobile:</ion-label>\n          <ion-input type=\"text\" formControlName=\"email\" placeholder=\"Email id\">\n          </ion-input>\n        </ion-item> -->\n        <!-- <ion-item>\n          <ion-label>Gender</ion-label>\n          <ion-select formControlName=\"gender\">\n            <ion-select-option value=\"male\">Male</ion-select-option>\n            <ion-select-option value=\"female\">Female</ion-select-option>\n          </ion-select>\n        </ion-item> -->\n        <!-- <ion-item>\n          <ion-label>Address</ion-label>\n          <ion-textarea formControlName=\"address\" type=\"text\" placeholder=\"Address\"></ion-textarea>\n        </ion-item> -->\n        <!-- <ion-item>\n          <ion-label>Country</ion-label>\n          <ion-select formControlName=\"country\" placeHolder=\"Country\">\n            <ion-select-option *ngFor=\"let country of country\" [value]=\"country\">\n              {{country}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item> -->\n      <!-- </ion-list> -->\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"floating\">First name</ion-label>\n            <ion-input type=\"text\" formControlName=\"firstName\"></ion-input>\n          </ion-item>\n          \n        </ion-card>\n\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"floating\">Last name</ion-label>\n            <ion-input type=\"text\" formControlName=\"lastname\"></ion-input>\n          </ion-item>\n        </ion-card>\n\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"stacked\">Date of birth</ion-label>\n            <ion-input type=\"date\" formControlName=\"date\"></ion-input>\n          </ion-item>\n        </ion-card>\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"floating\">Mobile</ion-label>\n            <ion-input formControlName=\"contactNo\" type=\"text\" readonly></ion-input>\n          </ion-item>\n        </ion-card>\n\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"floating\">Email id</ion-label>\n            <ion-input type=\"text\" formControlName=\"email\" placeholder=\"Email id\"></ion-input>\n          </ion-item>\n        </ion-card>\n\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"floating\">Gender</ion-label>\n            <ion-select formControlName=\"gender\">\n              <ion-select-option value=\"male\">Male</ion-select-option>\n              <ion-select-option value=\"female\">Female</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-card>\n\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"floating\">Address</ion-label>\n              <ion-textarea formControlName=\"address\" type=\"text\" placeholder=\"Address\"></ion-textarea>\n          </ion-item>\n        </ion-card>\n        <ion-card>\n          <ion-item>\n            <ion-label style=\"color: #b1b1b1;\" position=\"floating\">Country</ion-label>\n            <ion-select formControlName=\"country\" placeHolder=\"Country\">\n              <ion-select-option *ngFor=\"let country of country\" [value]=\"country\">\n                {{country}}\n              </ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-card>\n       \n       \n  \n\n      <ion-row class=\"signin\">\n        <ion-col>\n          <ion-button (click)=\"onSubmit()\" [ngClass]=\"{'appBackground' : appName != 'Armoron' , 'header-background-color': appName == 'Armoron'}\" shape=\"full\" expand=\"block\">Submit</ion-button>\n        </ion-col>\n      </ion-row>\n\n    </form>\n  </ion-row>\n</ion-content>"

/***/ }),

/***/ "./src/app/delar-application/dashboard/add-company/add-company-routing.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/delar-application/dashboard/add-company/add-company-routing.module.ts ***!
  \***************************************************************************************/
/*! exports provided: AddCompanyPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCompanyPageRoutingModule", function() { return AddCompanyPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_company_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-company.page */ "./src/app/delar-application/dashboard/add-company/add-company.page.ts");




var routes = [
    {
        path: '',
        component: _add_company_page__WEBPACK_IMPORTED_MODULE_3__["AddCompanyPage"]
    }
];
var AddCompanyPageRoutingModule = /** @class */ (function () {
    function AddCompanyPageRoutingModule() {
    }
    AddCompanyPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], AddCompanyPageRoutingModule);
    return AddCompanyPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/delar-application/dashboard/add-company/add-company.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/delar-application/dashboard/add-company/add-company.module.ts ***!
  \*******************************************************************************/
/*! exports provided: AddCompanyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCompanyPageModule", function() { return AddCompanyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_company_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-company-routing.module */ "./src/app/delar-application/dashboard/add-company/add-company-routing.module.ts");
/* harmony import */ var _add_company_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-company.page */ "./src/app/delar-application/dashboard/add-company/add-company.page.ts");







var AddCompanyPageModule = /** @class */ (function () {
    function AddCompanyPageModule() {
    }
    AddCompanyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _add_company_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddCompanyPageRoutingModule"]
            ],
            declarations: [_add_company_page__WEBPACK_IMPORTED_MODULE_6__["AddCompanyPage"]]
        })
    ], AddCompanyPageModule);
    return AddCompanyPageModule;
}());



/***/ }),

/***/ "./src/app/delar-application/dashboard/add-company/add-company.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/delar-application/dashboard/add-company/add-company.page.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media only screen and (min-width: 10px) and (max-width: 767px) {\n  .form-field {\n    zoom: 80%;\n    margin: 5% auto 3px;\n  }\n\n  #submitbtn {\n    width: 80%;\n  }\n}\n@media only screen and (min-width: 768px) {\n  .form-field {\n    margin: 3% auto 5px;\n    padding: 0px 20px;\n  }\n\n  #submitbtn {\n    width: 38%;\n  }\n}\n#submitbtn {\n  --background:#6252ee !important;\n}\n.heading {\n  border: 1px solid #e5e5e5;\n  height: 42px;\n  line-height: 18px;\n  --padding-start: 15px;\n  background: #e8e8e8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vZGFzaGJvYXJkL2FkZC1jb21wYW55L2FkZC1jb21wYW55LnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vZGFzaGJvYXJkL2FkZC1jb21wYW55L2FkZC1jb21wYW55LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFTQTtFQUNJO0lBQ0MsU0FBQTtJQUNBLG1CQUFBO0VDUkg7O0VEVUU7SUFDQyxVQUFBO0VDUEg7QUFDRjtBRFNBO0VBQ0k7SUFDSSxtQkFBQTtJQUNBLGlCQUFBO0VDUE47O0VEVUU7SUFDQyxVQUFBO0VDUEg7QUFDRjtBRFNBO0VBQ0ksK0JBQUE7QUNQSjtBRFVBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FDUEoiLCJmaWxlIjoic3JjL2FwcC9kZWxhci1hcHBsaWNhdGlvbi9kYXNoYm9hcmQvYWRkLWNvbXBhbnkvYWRkLWNvbXBhbnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWlucHV0LGlvbi1zZWxlY3R7XHJcbi8vICAgICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4vLyAgICAgaGVpZ2h0OiA0MnB4O1xyXG4vLyAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbi8vICAgICAtLXBhZGRpbmctc3RhcnQ6IDE1cHg7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiAjZThlOGU4O1xyXG4vLyB9XHJcblxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZChtaW4td2lkdGg6MTBweClhbmQobWF4LXdpZHRoOjc2N3B4KXtcclxuICAgIC5mb3JtLWZpZWxke1xyXG4gICAgIHpvb206ODAlO1xyXG4gICAgIG1hcmdpbjogNSUgYXV0byAzcHg7XHJcbiAgICB9XHJcbiAgICAjc3VibWl0YnRue1xyXG4gICAgIHdpZHRoOjgwJTtcclxuICAgIH1cclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NzY4cHgpe1xyXG4gICAgLmZvcm0tZmllbGR7XHJcbiAgICAgICAgbWFyZ2luOiAzJSBhdXRvIDVweDtcclxuICAgICAgICBwYWRkaW5nOiAwcHggMjBweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgI3N1Ym1pdGJ0bntcclxuICAgICB3aWR0aDozOCU7XHJcbiAgICB9XHJcbn1cclxuI3N1Ym1pdGJ0bntcclxuICAgIC0tYmFja2dyb3VuZDojNjI1MmVlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5oZWFkaW5ne1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2U1ZTVlNTtcclxuICAgIGhlaWdodDogNDJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2U4ZThlODtcclxufVxyXG4iLCJAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEwcHgpIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuZm9ybS1maWVsZCB7XG4gICAgem9vbTogODAlO1xuICAgIG1hcmdpbjogNSUgYXV0byAzcHg7XG4gIH1cblxuICAjc3VibWl0YnRuIHtcbiAgICB3aWR0aDogODAlO1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5mb3JtLWZpZWxkIHtcbiAgICBtYXJnaW46IDMlIGF1dG8gNXB4O1xuICAgIHBhZGRpbmc6IDBweCAyMHB4O1xuICB9XG5cbiAgI3N1Ym1pdGJ0biB7XG4gICAgd2lkdGg6IDM4JTtcbiAgfVxufVxuI3N1Ym1pdGJ0biB7XG4gIC0tYmFja2dyb3VuZDojNjI1MmVlICFpbXBvcnRhbnQ7XG59XG5cbi5oZWFkaW5nIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2U1ZTVlNTtcbiAgaGVpZ2h0OiA0MnB4O1xuICBsaW5lLWhlaWdodDogMThweDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxNXB4O1xuICBiYWNrZ3JvdW5kOiAjZThlOGU4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/delar-application/dashboard/add-company/add-company.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/delar-application/dashboard/add-company/add-company.page.ts ***!
  \*****************************************************************************/
/*! exports provided: AddCompanyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCompanyPage", function() { return AddCompanyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");








var AddCompanyPage = /** @class */ (function () {
    function AddCompanyPage(formBuilder, router, ajaxService, commonService, location) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.location = location;
        this.region = { "India": "Asia/Kolkata", "Saudi Arabia": "Asia/Riyadh" };
        this.modelTypes = { "APMKT01 Basic": "KT-Mini", "APMKT01 Advance": "KT-Mini", "APMKT02 Basic": "TK003", "APMKT02 Advance": "TK003", "APMKT03 Basic": "KT-Mini", "APMKT03 Advance": "KT-Mini", "APMKT04": "wetrack", "APMKT05": "GT06N", "APMKT06": "GT300", "APMKT07": "JV200", "APMKT08": "KT-Mini", "APMKT09": "KT-Mini", "APMKT10": "GT800", "APMKT11": "GT06D", "AIS1401A": "AIS1401A" };
        this.model = [];
        this.provider = [
            { sim: 'Idea' },
            { sim: 'Airtel' },
            { sim: 'Vodafone' },
            { sim: 'Bsnl' }
        ];
        this.manufacture = [{ name: 'APMKT', type: 'Concox' }, { name: 'APMKT-AIS140', type: 'APMKT' }];
    }
    AddCompanyPage.prototype.getBack = function () {
        this.router.navigateByUrl('/dashboard');
    };
    AddCompanyPage.prototype.backbutton = function () {
        this.location.back();
    };
    AddCompanyPage.prototype.onSubmit = function () {
        var _this = this;
        if (this.addCompany.valid) {
            var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/global/validate/contact?contactNo=' + this.addCompany.value.contactNo;
            this.ajaxService.ajaxGet(url)
                .subscribe(function (res) {
                if (res.message == 'Not Available') {
                    _this.addTheCompany();
                }
                else {
                    _this.commonService.presentToast("Phone number already exit");
                }
            });
        }
        else {
            this.commonService.presentToast("Fill required field..");
        }
    };
    AddCompanyPage.prototype.addTheCompany = function () {
        var _this = this;
        var adminData;
        var addCompany;
        if (src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["app"].appName != "Armoron") {
            adminData = {
                loginId: this.addCompany.value.loginId,
                loginCompany: localStorage.getItem('companyId')
            };
            var countryCode = this.countryCode[this.addCompany.value.country];
            console.log(countryCode);
            var region = this.region[this.addCompany.value.country];
            addCompany = {
                "password": "12345",
                "companyName": this.addCompany.value.companyName,
                "address": this.addCompany.value.address,
                "city": this.addCompany.value.address,
                "region": region,
                "countryCode": countryCode,
                "companyAdminID": '',
                "firstName": this.addCompany.value.companyName,
                "contactNo": this.addCompany.value.contactNo,
                "branchID": this.addCompany.value.loginId,
                "emailId": this.addCompany.value.email,
                "emailID": this.addCompany.value.email,
                "companyID": this.addCompany.value.loginId,
            };
        }
        else {
            adminData = {
                loginId: this.addCompany.value.contactNo,
                loginCompany: "apm"
            };
            var countryCode = this.countryCode[this.addCompany.value.country];
            console.log(countryCode);
            var region = this.region[this.addCompany.value.country];
            addCompany = {
                "password": this.addCompany.value.contactNo,
                "companyName": this.addCompany.value.contactNo,
                "address": this.addCompany.value.address,
                "city": this.addCompany.value.address,
                "region": region,
                "countryCode": countryCode,
                "companyAdminID": '',
                "firstName": this.addCompany.value.firstName,
                "contactNo": this.addCompany.value.contactNo,
                "branchID": this.addCompany.value.contactNo,
                "emailId": this.addCompany.value.email,
                "emailID": this.addCompany.value.email,
                "companyID": this.addCompany.value.contactNo,
            };
        }
        var dataJson = JSON.stringify(addCompany);
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/user/provider/company?providerName=' + adminData.loginCompany + '&companyid=' + adminData.loginId;
        this.ajaxService.ajaxPostWithBody(url, dataJson)
            .subscribe(function (res) {
            _this.newFleetCreation(res);
        });
    };
    AddCompanyPage.prototype.newFleetCreation = function (res) {
        var _this = this;
        if (res) {
            if (res.Message === 'Company added successfully.') {
                var datetoday = new Date();
                var expDate = datetoday.getFullYear() + 1 + '-' + (datetoday.getMonth() + 1) + '-' + (datetoday.getDate() - 1);
                var countryCode = this.countryCode[this.addCompany.value.country];
                var fleetData = void 0;
                if (src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["app"].appName != "Armoron") {
                    fleetData = {
                        "userName": this.addCompany.value.loginId,
                        "password": this.addCompany.value.password,
                        "firstName": this.addCompany.value.companyName,
                        "categoryrole": "FleetManager",
                        "userCity": "city",
                        "contactNo": this.addCompany.value.contactNo,
                        "useraddress1": this.addCompany.value.address,
                        "useraddress2": " ",
                        "userCountry": countryCode,
                        "emailId": this.addCompany.value.email,
                        "emailID": this.addCompany.value.email,
                        "userExpiryDate": expDate,
                        "companyId": this.addCompany.value.loginId,
                        "branchID": this.addCompany.value.loginId,
                        "applicationType": "false"
                    };
                }
                else {
                    fleetData = {
                        "userName": this.addCompany.value.contactNo,
                        "password": this.addCompany.value.contactNo,
                        "firstName": this.addCompany.value.firstName,
                        "categoryrole": "FleetManager",
                        "userCity": "city",
                        "contactNo": this.addCompany.value.contactNo,
                        "useraddress1": this.addCompany.value.address,
                        "useraddress2": " ",
                        "userCountry": countryCode,
                        "emailId": this.addCompany.value.email,
                        "emailID": this.addCompany.value.email,
                        "userExpiryDate": expDate,
                        "companyId": this.addCompany.value.contactNo,
                        "branchID": this.addCompany.value.contactNo,
                        "applicationType": "false"
                    };
                }
                localStorage.setItem('fleetData', JSON.stringify(fleetData));
                var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/global/newFleetCreation';
                this.ajaxService.ajaxPostMethod(url, fleetData)
                    .subscribe(function (res) {
                    _this.newCompanyCreation(res);
                });
                console.log('this is call back end');
            }
            else if (res["Error Message"] === 'Company already exist.') {
                this.commonService.presentToast("Company already exist.");
            }
            else {
                this.commonService.presentToast("sorry, not able process your request.");
            }
        }
    };
    AddCompanyPage.prototype.newCompanyCreation = function (data) {
        var _this = this;
        var fleetData = JSON.parse(localStorage.getItem('fleetData'));
        var userDetail;
        if (src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["app"].appName != "Armoron") {
            userDetail = {
                "companyname": fleetData['firstName'],
                "companyId": fleetData['userName'],
                "userId": fleetData['userName'] + '-ca',
                "fmid": fleetData['userName'],
                "email": fleetData['emailId'],
                "contact": fleetData['contactNo'],
                "password": fleetData['password'],
                "suffix": localStorage.getItem('companySuffix')
            };
        }
        else {
            userDetail = {
                "companyname": fleetData['firstName'],
                "companyId": fleetData['userName'],
                "userId": fleetData['userName'],
                "fmid": fleetData['userName'],
                "email": fleetData['emailId'],
                "contact": fleetData['contactNo'],
                "password": fleetData['password'],
                "suffix": "21"
            };
        }
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/global/companyCreation';
        this.ajaxService.ajaxPostMethod(url, userDetail)
            .subscribe(function (res) {
            _this.alterNewCompanySuccess(res);
        });
    };
    AddCompanyPage.prototype.alterNewCompanySuccess = function (res) {
        if (res.result == "updated successfully") {
            // const loginData = {
            //   userId: localStorage.getItem('userId'),
            //   password: localStorage.getItem('password')
            // };
            // const url = serverUrl.web + '/api/vts/superadmin/auth/' + JSON.stringify(loginData);
            // this.ajaxService.ajaxGetWithString(url)
            // .subscribe(res => {
            // localStorage.removeItem("dashboardData");
            // localStorage.setItem('dashboardData', JSON.stringify(res.CompanyDetials));
            this.location.back();
            this.commonService.presentToast('Successfully Presisted');
            this.addCompany.reset();
            //   });
            // }else {
            //   this.commonService.presentToast('Presisted Failed...!');
        }
    };
    AddCompanyPage.prototype.getCountries = function () {
        var _this = this;
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/login/getPreferences?key=countries&companyId=' + this.companyDetail.companyID;
        this.ajaxService.ajaxGetPerference(url)
            .subscribe(function (res) {
            _this.country = res;
            console.log(res);
        });
    };
    AddCompanyPage.prototype.getCountryCode = function () {
        var _this = this;
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/login/getPreferences?key=countrycode&companyId=' + this.companyDetail.companyID;
        this.ajaxService.ajaxGetPerference(url)
            .subscribe(function (res) {
            _this.countryCode = res;
            console.log(res);
        });
    };
    AddCompanyPage.prototype.ngOnInit = function () {
        this.companyDetail = {
            companyID: localStorage.getItem('companyId'),
            userId: localStorage.getItem('userId')
        };
        this.selectedData = "company";
        this.header = "Company";
        this.appName = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["app"].appName;
        this.getCountries();
        this.getCountryCode();
        if (this.appName != "Armoron") {
            this.addCompany = this.formBuilder.group({
                companyName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email],
                loginId: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                contactNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                region: [''],
                country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
        }
        else {
            this.addCompany = this.formBuilder.group({
                firstName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                lastname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email],
                date: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                region: [''],
                country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                contactNo: [localStorage.getItem('PhoneNumber'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                gender: ['male', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
        }
    };
    AddCompanyPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"] },
        { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"] }
    ]; };
    AddCompanyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-company',
            template: __webpack_require__(/*! raw-loader!./add-company.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/dashboard/add-company/add-company.page.html"),
            styles: [__webpack_require__(/*! ./add-company.page.scss */ "./src/app/delar-application/dashboard/add-company/add-company.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"]])
    ], AddCompanyPage);
    return AddCompanyPage;
}());



/***/ })

}]);
//# sourceMappingURL=add-company-add-company-module-es5.js.map