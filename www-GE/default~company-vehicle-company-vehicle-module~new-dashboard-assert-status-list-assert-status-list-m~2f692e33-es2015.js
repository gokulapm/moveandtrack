(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~company-vehicle-company-vehicle-module~new-dashboard-assert-status-list-assert-status-list-m~2f692e33"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-row style=\"padding-bottom: 6px;\">\n  <ion-col size=12>\n    <ion-row>\n      <ion-col size=8 class=\"otherText\">\n        {{cardDetail.companyId}}\n      </ion-col>\n      <ion-col size=4 class=\"otherText statusStyle\">\n        {{cardDetail.warrantyExpiryDate}}\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=12 class=\"mainText\">\n        {{cardDetail.imei}}\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=4 class=\"otherText\">\n        {{cardDetail.simcardNo}}\n      </ion-col>\n      <ion-col size=4 class=\"otherText textCenter\">\n        {{cardDetail.plateNo}}\n      </ion-col>\n\n      <ion-col size=4 class=\"otherText\">\n        {{cardDetail.currentAgency}}\n      </ion-col>\n    </ion-row>\n  </ion-col>\n</ion-row>\n<ion-row class=\"timestampRow\">\n  <ion-col size=12 class=\"otherText timestampCol\">\n    {{cardDetail.serverTimeStamp}}\n  </ion-col>\n</ion-row> -->\n<ion-row style=\"padding-bottom: 6px;\" (click)=\"deleteVehicle()\">\n  <ion-col size=12>\n    <ion-row>\n      <ion-col size=8 class=\"otherText\">\n        {{cardDetail.plateNo}}\n      </ion-col>\n      <ion-col size=4 class=\"otherText statusStyle\">\n        {{cardDetail.warrantyExpiryDate}}\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=12 class=\"mainText\">\n        {{cardDetail.imei}}\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=12 class=\"otherText\">\n        <ion-icon class=\"iconColor\" name=\"call\"></ion-icon> {{cardDetail.simcardNo}}\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=12 class=\"otherText\">\n        <ion-icon class=\"iconColor\" name=\"business\"></ion-icon> {{cardDetail.companyId}} ({{cardDetail.currentAgency}})\n      </ion-col>\n    </ion-row>\n  </ion-col>\n</ion-row>\n<ion-row class=\"timestampRow\">\n  <ion-col size=12 class=\"otherText timestampCol\">\n    {{cardDetail.serverTimeStamp}}\n  </ion-col>\n</ion-row>\n"

/***/ }),

/***/ "./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".padding0px {\n  padding: 0px;\n}\n\n.otherText {\n  font-family: sans-serif;\n  font-size: 11px;\n  text-transform: uppercase;\n  padding: 0px;\n  align-self: center;\n}\n\n.mainText {\n  font-family: sans-serif;\n  font-size: x-large;\n  font-weight: bold;\n  color: #3c3c3c;\n  padding: 0px;\n}\n\n.statusStyle {\n  padding: 3px;\n  background: #dadada;\n  color: #3c3c3c;\n  text-align: center;\n  border-radius: 29px;\n}\n\n.timestampRow {\n  text-align: center;\n  border-top: 1px solid #dadada;\n}\n\n.timestampCol {\n  padding: 6px;\n}\n\n.textCenter {\n  text-align: center;\n}\n\n.iconColor {\n  color: #7c68f8;\n  padding-right: 7px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vZGVhbGVyLWNvbXBvbmVudC9hc3NlcnQtbGlzdC1jYXJkL2Fzc2VydC1saXN0LWNhcmQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL2RlYWxlci1jb21wb25lbnQvYXNzZXJ0LWxpc3QtY2FyZC9hc3NlcnQtbGlzdC1jYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0EsWUFBQTtBQ0NBOztBREVBO0VBQ0ksdUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSw2QkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURFQTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vZGVhbGVyLWNvbXBvbmVudC9hc3NlcnQtbGlzdC1jYXJkL2Fzc2VydC1saXN0LWNhcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFkZGluZzBweHtcbnBhZGRpbmc6IDBweDtcbn1cblxuLm90aGVyVGV4dHtcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xufVxuXG4ubWFpblRleHR7XG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiAjM2MzYzNjO1xuICAgIHBhZGRpbmc6IDBweDtcbn1cblxuLnN0YXR1c1N0eWxle1xuICAgIHBhZGRpbmc6IDNweDtcbiAgICBiYWNrZ3JvdW5kOiAjZGFkYWRhO1xuICAgIGNvbG9yOiAjM2MzYzNjO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBib3JkZXItcmFkaXVzOiAyOXB4O1xufVxuXG4udGltZXN0YW1wUm93e1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2RhZGFkYTtcbn1cblxuLnRpbWVzdGFtcENvbHtcbiAgICBwYWRkaW5nOiA2cHg7XG59XG5cbi50ZXh0Q2VudGVye1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmljb25Db2xvciB7XG4gICAgY29sb3I6ICM3YzY4Zjg7XG4gICAgcGFkZGluZy1yaWdodDogN3B4O1xuICB9IiwiLnBhZGRpbmcwcHgge1xuICBwYWRkaW5nOiAwcHg7XG59XG5cbi5vdGhlclRleHQge1xuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAxMXB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwYWRkaW5nOiAwcHg7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLm1haW5UZXh0IHtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjM2MzYzNjO1xuICBwYWRkaW5nOiAwcHg7XG59XG5cbi5zdGF0dXNTdHlsZSB7XG4gIHBhZGRpbmc6IDNweDtcbiAgYmFja2dyb3VuZDogI2RhZGFkYTtcbiAgY29sb3I6ICMzYzNjM2M7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogMjlweDtcbn1cblxuLnRpbWVzdGFtcFJvdyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkYWRhZGE7XG59XG5cbi50aW1lc3RhbXBDb2wge1xuICBwYWRkaW5nOiA2cHg7XG59XG5cbi50ZXh0Q2VudGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaWNvbkNvbG9yIHtcbiAgY29sb3I6ICM3YzY4Zjg7XG4gIHBhZGRpbmctcmlnaHQ6IDdweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: AssertListCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssertListCardComponent", function() { return AssertListCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let AssertListCardComponent = class AssertListCardComponent {
    constructor(alertController, ajaxService, modalController, commonService, router) {
        this.alertController = alertController;
        this.ajaxService = ajaxService;
        this.modalController = modalController;
        this.commonService = commonService;
        this.router = router;
    }
    deleteVehicle() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Are you sure?',
                inputs: [{
                        name: 'Password',
                        type: 'text',
                        placeholder: 'Enter dealer password'
                    }, {
                        name: 'userPassword',
                        placeholder: 'Enter user pasword',
                        type: 'text',
                    }],
                message: 'You want to Delete ' + this.cardDetail.imei,
                backdropDismiss: false,
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: data => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: data => {
                            this.commonService.presentLoader();
                            if (data.Password == localStorage.password || (data.Password).toLowerCase() == (localStorage.password).toLowerCase()) {
                                const url1 = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/global/validate/pass?companyId=' + this.cardDetail.companyId + '&pass=' + data.userPassword;
                                this.ajaxService.ajaxGetWithString(url1).subscribe(res => {
                                    this.commonService.dismissLoader();
                                    console.log(res);
                                    // JSON.parse(res).message
                                    if (JSON.parse(res).message == "Available") {
                                        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/global/delete/imei?imeiNo=' + this.cardDetail.imei;
                                        this.ajaxService.ajaxDeleteWithString(url).subscribe(res => {
                                            console.log(res);
                                            if (res.message == "success") {
                                                this.commonService.presentToast("Successfully deleted");
                                                this.commonService.dismissLoader();
                                                this.router.navigateByUrl('/dashboard');
                                            }
                                            else {
                                                this.commonService.presentToast("contact support team");
                                                this.commonService.dismissLoader();
                                            }
                                        });
                                    }
                                    else {
                                        this.commonService.dismissLoader();
                                        this.commonService.presentToast("Please check user password");
                                    }
                                });
                            }
                            else {
                                this.commonService.dismissLoader();
                                this.commonService.presentToast("Please check dealer password");
                            }
                            //  data.Password
                            console.log(this.cardDetail);
                        }
                    }]
            });
            yield alert.present();
        });
    }
    ngOnInit() { }
};
AssertListCardComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_3__["AjaxService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], AssertListCardComponent.prototype, "cardDetail", void 0);
AssertListCardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-assert-list-card',
        template: __webpack_require__(/*! raw-loader!./assert-list-card.component.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.html"),
        styles: [__webpack_require__(/*! ./assert-list-card.component.scss */ "./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_3__["AjaxService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        src_app_services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
], AssertListCardComponent);



/***/ }),

/***/ "./src/app/delar-application/dealer-component/dealer-component.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/delar-application/dealer-component/dealer-component.module.ts ***!
  \*******************************************************************************/
/*! exports provided: DealerComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealerComponentsModule", function() { return DealerComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _assert_list_card_assert_list_card_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./assert-list-card/assert-list-card.component */ "./src/app/delar-application/dealer-component/assert-list-card/assert-list-card.component.ts");






const routes = [];
let DealerComponentsModule = class DealerComponentsModule {
};
DealerComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_assert_list_card_assert_list_card_component__WEBPACK_IMPORTED_MODULE_5__["AssertListCardComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"].forRoot(),
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        entryComponents: [],
        exports: [_assert_list_card_assert_list_card_component__WEBPACK_IMPORTED_MODULE_5__["AssertListCardComponent"]],
    })
], DealerComponentsModule);



/***/ })

}]);
//# sourceMappingURL=default~company-vehicle-company-vehicle-module~new-dashboard-assert-status-list-assert-status-list-m~2f692e33-es2015.js.map