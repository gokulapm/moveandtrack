(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ticket-ticket-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/ticket/ticket.page.html":
/*!*******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/ticket/ticket.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar mode=\"md\" class=\"appHeader\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-row *ngIf=\"myPlatform != 'desktop'\" >\n              <ion-col size=\"2\">\n                <!-- <ion-icon name=\"arrow-back\"  (click)=\"getBack()\"></ion-icon> -->\n                <ion-menu-button></ion-menu-button>\n              </ion-col>\n             <ion-col size=\"8\">\n              <ion-title>Tickets </ion-title>\n             </ion-col>\n             <ion-col size=\"2\">\n               <!-- <ion-icon name=\"add-circle\" class=\"add-icon\" (click)=\"openModel()\"></ion-icon> -->\n             </ion-col>\n          </ion-row>\n         \n         \n          <ion-row *ngIf=\"myPlatform == 'desktop'\">\n            <ion-col size='2'>\n              <ion-menu-button></ion-menu-button>\n            </ion-col>\n            <ion-col size=\"9\">\n              <ion-label> TICKETS </ion-label>\n            </ion-col>\n          \n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content>\n<app-ticket-card ></app-ticket-card>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/ticket/ticket.module.ts":
/*!*****************************************!*\
  !*** ./src/app/ticket/ticket.module.ts ***!
  \*****************************************/
/*! exports provided: TicketPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketPageModule", function() { return TicketPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ticket_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ticket.page */ "./src/app/ticket/ticket.page.ts");








const routes = [
    {
        path: '',
        component: _ticket_page__WEBPACK_IMPORTED_MODULE_7__["TicketPage"]
    }
];
let TicketPageModule = class TicketPageModule {
};
TicketPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        // entryComponents:[ConversationTabPage],
        declarations: [_ticket_page__WEBPACK_IMPORTED_MODULE_7__["TicketPage"]]
    })
], TicketPageModule);



/***/ }),

/***/ "./src/app/ticket/ticket.page.scss":
/*!*****************************************!*\
  !*** ./src/app/ticket/ticket.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".add-icon {\n  color: white;\n  background: gray;\n  padding: 5px;\n  font-size: 16px;\n  border-radius: 15px;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvdGlja2V0L3RpY2tldC5wYWdlLnNjc3MiLCJzcmMvYXBwL3RpY2tldC90aWNrZXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFNBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3RpY2tldC90aWNrZXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFkZC1pY29ue1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZDogZ3JheTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICBtYXJnaW46IDA7XHJcbn0iLCIuYWRkLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQ6IGdyYXk7XG4gIHBhZGRpbmc6IDVweDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBtYXJnaW46IDA7XG59Il19 */"

/***/ }),

/***/ "./src/app/ticket/ticket.page.ts":
/*!***************************************!*\
  !*** ./src/app/ticket/ticket.page.ts ***!
  \***************************************/
/*! exports provided: TicketPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketPage", function() { return TicketPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");




let TicketPage = class TicketPage {
    constructor(platform, modalController, location) {
        this.platform = platform;
        this.modalController = modalController;
        this.location = location;
    }
    getBack() {
        this.location.back();
    }
    ngOnInit() {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        this.sub = localStorage.getItem('subject');
    }
};
TicketPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] }
];
TicketPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ticket',
        template: __webpack_require__(/*! raw-loader!./ticket.page.html */ "./node_modules/raw-loader/index.js!./src/app/ticket/ticket.page.html"),
        styles: [__webpack_require__(/*! ./ticket.page.scss */ "./src/app/ticket/ticket.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"]])
], TicketPage);



/***/ })

}]);
//# sourceMappingURL=ticket-ticket-module-es2015.js.map