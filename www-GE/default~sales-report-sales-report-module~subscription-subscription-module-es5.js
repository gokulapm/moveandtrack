(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~sales-report-sales-report-module~subscription-subscription-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/subscription/renewal/renewal.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/subscription/renewal/renewal.page.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-content >\n  <ion-row>\n    <ion-col size=\"2\">\n      <ion-icon name=\"arrow-back\" (click)=\"getBack()\"></ion-icon>\n    </ion-col>\n    <ion-col size=\"8\" style=\"font-size: 20px;background-color:#7c68f8;color:white\"> Subscription renewal</ion-col>\n  </ion-row>\n  <ion-grid>\n    <form class='formPadding' [formGroup]=\"subscription\">\n<ion-row>\n  <ion-col  class=\"wrapper-container\">\n   <ion-item>\n      <ion-icon name=\"phone-portrait\" class=\"renewal-icon\" slot=\"start\" ></ion-icon>\n      <ion-input formControlName=\"imeiNo\" type=\"number\" placeholder=\"IMEI No\" readonly></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon name=\"time\" class=\"renewal-icon\" slot=\"start\"></ion-icon>\n      <ion-input  id=\"today\" formControlName=\"todayDate\"   [type]=\"typeSetter\" (click)=\"focusType()\" ></ion-input>\n    </ion-item>\n    <ion-row>\n      <ion-col size=\"6\"  style=\"text-align: center;margin: auto;\">\n        <ion-button shape=\"round\" (click)=\"sendBtn()\">Submit</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-col>\n</ion-row>\n</form>\n\n\n  <!-- <ion-row>\n    <ion-col  class=\"wrapper-container\">\n     <ion-item>\n        <ion-icon name=\"phone-portrait\" class=\"renewal-icon\" slot=\"start\" ></ion-icon>\n        <ion-input  type=\"number\" placeholder=\"IMEI No\" ></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-icon name=\"time\" class=\"renewal-icon\" slot=\"start\"></ion-icon>\n        <ion-input  id=\"today\" type=\"date\"></ion-input>\n      </ion-item>\n      <ion-row>\n        <ion-col size=\"6\"  style=\"text-align: center;margin: auto;\">\n          <ion-button shape=\"round\" (click)=\"sendBtn()\">Submit</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n  </ion-row> -->\n\n</ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/delar-application/subscription/renewal/renewal.page.scss":
/*!**************************************************************************!*\
  !*** ./src/app/delar-application/subscription/renewal/renewal.page.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".wrapper-container {\n  border: 1px solid #f6f6f6;\n}\n\nion-item {\n  border: 1px solid #eaf5f4;\n  margin: 5px 0px;\n  border-radius: 5px;\n}\n\n.btn {\n  --background:#7c68f8;\n  width: 50%;\n}\n\nion-input {\n  --placeholder-color:darkgray;\n  color: gray;\n}\n\n.renewal-icon {\n  color: #7c68f8;\n  border-right: 1px solid #f6f6f6;\n  padding-right: 5px;\n}\n\n@media only screen and (min-width: 320px) and (max-width: 766px) {\n  .wrapper-container {\n    padding: 1em 5px;\n  }\n}\n\n@media only screen and (min-width: 767px) and (max-width: 1010px) {\n  .wrapper-container {\n    padding: 1em;\n  }\n}\n\n@media only screen and (min-width: 1011px) {\n  .wrapper-container {\n    padding: 2em 1em;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc3Vic2NyaXB0aW9uL3JlbmV3YWwvcmVuZXdhbC5wYWdlLnNjc3MiLCJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL3N1YnNjcmlwdGlvbi9yZW5ld2FsL3JlbmV3YWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7QUNDSjs7QURFQTtFQUNJLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FEQ0E7RUFDQyxvQkFBQTtFQUNDLFVBQUE7QUNFRjs7QURBQTtFQUNJLDRCQUFBO0VBQ0EsV0FBQTtBQ0dKOztBRERBO0VBQ0ksY0FBQTtFQUNBLCtCQUFBO0VBQ0Esa0JBQUE7QUNJSjs7QUREQTtFQUNJO0lBQ0MsZ0JBQUE7RUNJSDtBQUNGOztBRERBO0VBQ0k7SUFDQyxZQUFBO0VDR0g7QUFDRjs7QURBQTtFQUNJO0lBQ0MsZ0JBQUE7RUNFSDtBQUNGIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc3Vic2NyaXB0aW9uL3JlbmV3YWwvcmVuZXdhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcHBlci1jb250YWluZXJ7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZjZmNmY2O1xyXG4gICAgLy8gcGFkZGluZzozZW0gNWVtO1xyXG59XHJcbmlvbi1pdGVte1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2VhZjVmNDtcclxuICAgIG1hcmdpbjogNXB4IDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG4uYnRue1xyXG4gLS1iYWNrZ3JvdW5kOiM3YzY4Zjg7XHJcbiAgd2lkdGg6NTAlO1xyXG4gIH1cclxuaW9uLWlucHV0e1xyXG4gICAgLS1wbGFjZWhvbGRlci1jb2xvcjpkYXJrZ3JheTtcclxuICAgIGNvbG9yOmdyYXk7XHJcbn1cclxuLnJlbmV3YWwtaWNvbntcclxuICAgIGNvbG9yOiM3YzY4Zjg7XHJcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZjZmNmY2O1xyXG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kKG1pbi13aWR0aDozMjBweClhbmQobWF4LXdpZHRoOjc2NnB4KXtcclxuICAgIC53cmFwcGVyLWNvbnRhaW5lcntcclxuICAgICBwYWRkaW5nOjFlbSA1cHg7XHJcbiAgICB9IFxyXG5cclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kKG1pbi13aWR0aDo3NjdweClhbmQobWF4LXdpZHRoOjEwMTBweCl7XHJcbiAgICAud3JhcHBlci1jb250YWluZXJ7XHJcbiAgICAgcGFkZGluZzoxZW0gO1xyXG4gICAgfSBcclxuXHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZChtaW4td2lkdGg6MTAxMXB4KXtcclxuICAgIC53cmFwcGVyLWNvbnRhaW5lcntcclxuICAgICBwYWRkaW5nOjJlbSAxZW07XHJcbiAgICB9IFxyXG5cclxufSIsIi53cmFwcGVyLWNvbnRhaW5lciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmNmY2ZjY7XG59XG5cbmlvbi1pdGVtIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2VhZjVmNDtcbiAgbWFyZ2luOiA1cHggMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5idG4ge1xuICAtLWJhY2tncm91bmQ6IzdjNjhmODtcbiAgd2lkdGg6IDUwJTtcbn1cblxuaW9uLWlucHV0IHtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjpkYXJrZ3JheTtcbiAgY29sb3I6IGdyYXk7XG59XG5cbi5yZW5ld2FsLWljb24ge1xuICBjb2xvcjogIzdjNjhmODtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2Y2ZjZmNjtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDMyMHB4KSBhbmQgKG1heC13aWR0aDogNzY2cHgpIHtcbiAgLndyYXBwZXItY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAxZW0gNXB4O1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2N3B4KSBhbmQgKG1heC13aWR0aDogMTAxMHB4KSB7XG4gIC53cmFwcGVyLWNvbnRhaW5lciB7XG4gICAgcGFkZGluZzogMWVtO1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEwMTFweCkge1xuICAud3JhcHBlci1jb250YWluZXIge1xuICAgIHBhZGRpbmc6IDJlbSAxZW07XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/subscription/renewal/renewal.page.ts":
/*!************************************************************************!*\
  !*** ./src/app/delar-application/subscription/renewal/renewal.page.ts ***!
  \************************************************************************/
/*! exports provided: RenewalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenewalPage", function() { return RenewalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");







var RenewalPage = /** @class */ (function () {
    function RenewalPage(modalController, alertController, formBuilder, commonService, ajaxService) {
        this.modalController = modalController;
        this.alertController = alertController;
        this.formBuilder = formBuilder;
        this.commonService = commonService;
        this.ajaxService = ajaxService;
        this.typeSetter = "text";
    }
    RenewalPage.prototype.getBack = function () {
        this.modalController.dismiss();
    };
    RenewalPage.prototype.focusType = function () {
        this.typeSetter = "Date";
    };
    RenewalPage.prototype.sendBtn = function () {
        var _this = this;
        console.log(this.subscription);
        if (this.subscription.status == "VALID") {
            var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["serverUrl"].web + '/global/updateExpirydate';
            var data = {
                imeiNo: this.subscription.value.imeiNo,
                warrantyExpiryDate: this.subscription.value.todayDate
            };
            this.ajaxService.ajaxPostMethod(url, data).subscribe(function (res) {
                console.log(res);
                var respData = JSON.parse(res);
                if (respData.message == "Success") {
                    _this.modalController.dismiss();
                    _this.commonService.presentToast('Successfully Updated');
                }
                else {
                    _this.commonService.presentToast('Please tryagain');
                }
            });
        }
        else {
            this.commonService.presentToast('Please complete the form');
            console.log('Please complete the form');
        }
    };
    RenewalPage.prototype.subscriptionAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.modalController.dismiss();
                        return [4 /*yield*/, this.alertController.create({
                                header: 'Are you sure?',
                                backdropDismiss: false,
                                message: "You want to renew your subscription!",
                                buttons: [{
                                        text: 'No',
                                        role: 'cancel',
                                        handler: function (data) {
                                            localStorage.setItem("exitPopup", "false");
                                        },
                                    },
                                    {
                                        text: 'Yes',
                                    }]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RenewalPage.prototype.ngOnInit = function () {
        this.field = document.querySelector('#today');
        this.date = new Date();
        // Set the date
        this.field.value = (this.date.getFullYear() + 1).toString() + '-' + (this.date.getMonth() + 1).toString().padStart(2, 0) +
            '-' + this.date.getDate().toString().padStart(2, 0);
        this.subscription = this.formBuilder.group({
            imeiNo: [this.value, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            todayDate: [this.field.value, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    RenewalPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], RenewalPage.prototype, "value", void 0);
    RenewalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-renewal',
            template: __webpack_require__(/*! raw-loader!./renewal.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/subscription/renewal/renewal.page.html"),
            styles: [__webpack_require__(/*! ./renewal.page.scss */ "./src/app/delar-application/subscription/renewal/renewal.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
            _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"]])
    ], RenewalPage);
    return RenewalPage;
}());



/***/ })

}]);
//# sourceMappingURL=default~sales-report-sales-report-module~subscription-subscription-module-es5.js.map