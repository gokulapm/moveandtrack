(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["stock-uploader-stock-uploader-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/stock-uploader/stock-uploader.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/stock-uploader/stock-uploader.page.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n<ion-toolbar mode=\"md\"  class=\"dealerHeader\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n     <ion-title >Stock uploader</ion-title>\n      </ion-toolbar>\n</ion-header>\n<ion-content>\n   <ion-row style=\"padding: 0px;\">\n      <ion-col size=\"12\" class=\"ion-text-center\">    <input type=\"file\" (change)=\"onFileChange($event)\" /></ion-col>\n          </ion-row>\n   \n    <!-- <div>\n      <a id=\"download\" (click)=\"sendToServer()\"> Submit </a>\n    </div> -->\n    <!-- <ion-row>\n      <ion-col size=\"12\"><ion-input type=\"file\" [(ngModel)]=\"file\" (ionChange)=\"onFileChange($event)\"></ion-input></ion-col>\n     \n    </ion-row> -->\n    <ion-row style=\"padding: 0px;\">\n<ion-col size=\"12\" class=\"ion-text-center\"><ion-button (click)=\"sendToServer()\" id=\"submitbtn\">Submit </ion-button></ion-col>\n    </ion-row>\n    <jqxGrid *ngIf='show'\n    #myGrid\n    [theme]=\"'material'\"\n    [width]=\"'99%'\"\n    [autoheight]=\"true\"\n    [source]=\"dataAdapter\"\n    [columns]=\"columns\"\n    [sortable]=\"true\"\n    [filterable]=\"true\"\n    [columns]=\"columns\"\n    [columnsresize]=\"true\"\n    [enabletooltips]=\"true\"\n    [pageable]=\"true\"\n   style=\"font-size: 10px !important\"\n  >\n  </jqxGrid>\n  </ion-content>\n\n  "

/***/ }),

/***/ "./src/app/delar-application/stock-uploader/stock-uploader-routing.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/delar-application/stock-uploader/stock-uploader-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: StockUploaderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockUploaderPageRoutingModule", function() { return StockUploaderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _stock_uploader_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./stock-uploader.page */ "./src/app/delar-application/stock-uploader/stock-uploader.page.ts");




const routes = [
    {
        path: '',
        component: _stock_uploader_page__WEBPACK_IMPORTED_MODULE_3__["StockUploaderPage"]
    }
];
let StockUploaderPageRoutingModule = class StockUploaderPageRoutingModule {
};
StockUploaderPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], StockUploaderPageRoutingModule);



/***/ }),

/***/ "./src/app/delar-application/stock-uploader/stock-uploader.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/delar-application/stock-uploader/stock-uploader.module.ts ***!
  \***************************************************************************/
/*! exports provided: StockUploaderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockUploaderPageModule", function() { return StockUploaderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _stock_uploader_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./stock-uploader-routing.module */ "./src/app/delar-application/stock-uploader/stock-uploader-routing.module.ts");
/* harmony import */ var _shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared-mod/shared-mod.module */ "./src/app/shared-mod/shared-mod.module.ts");
/* harmony import */ var _stock_uploader_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./stock-uploader.page */ "./src/app/delar-application/stock-uploader/stock-uploader.page.ts");







// import { jqxGridModule } from 'jqwidgets-ng/jqxgrid';

let StockUploaderPageModule = class StockUploaderPageModule {
};
StockUploaderPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_6__["SharedModModule"],
            // jqxGridModule,
            _stock_uploader_routing_module__WEBPACK_IMPORTED_MODULE_5__["StockUploaderPageRoutingModule"]
        ],
        declarations: [_stock_uploader_page__WEBPACK_IMPORTED_MODULE_7__["StockUploaderPage"]]
    })
], StockUploaderPageModule);



/***/ }),

/***/ "./src/app/delar-application/stock-uploader/stock-uploader.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/delar-application/stock-uploader/stock-uploader.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "input {\n  margin-top: 11px;\n  margin-left: 18%;\n}\n\n#myGrid {\n  border: 1px solid;\n}\n\n#submitbtn {\n  --background:#6252ee !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc3RvY2stdXBsb2FkZXIvc3RvY2stdXBsb2FkZXIucGFnZS5zY3NzIiwic3JjL2FwcC9kZWxhci1hcHBsaWNhdGlvbi9zdG9jay11cGxvYWRlci9zdG9jay11cGxvYWRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0E7RUFDUSxpQkFBQTtBQ0VSOztBRENJO0VBQ0ksK0JBQUE7QUNFUiIsImZpbGUiOiJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL3N0b2NrLXVwbG9hZGVyL3N0b2NrLXVwbG9hZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlucHV0e1xyXG4gICAgbWFyZ2luLXRvcDoxMXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE4JTtcclxufVxyXG4jbXlHcmlkIHtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgI3N1Ym1pdGJ0bntcclxuICAgICAgICAtLWJhY2tncm91bmQ6IzYyNTJlZSAhaW1wb3J0YW50O1xyXG4gICAgfSIsImlucHV0IHtcbiAgbWFyZ2luLXRvcDogMTFweDtcbiAgbWFyZ2luLWxlZnQ6IDE4JTtcbn1cblxuI215R3JpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xufVxuXG4jc3VibWl0YnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiM2MjUyZWUgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/stock-uploader/stock-uploader.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/delar-application/stock-uploader/stock-uploader.page.ts ***!
  \*************************************************************************/
/*! exports provided: StockUploaderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockUploaderPage", function() { return StockUploaderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! xlsx */ "./node_modules/xlsx/xlsx.js");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! jqwidgets-ng/jqxgrid */ "./node_modules/jqwidgets-ng/fesm2015/jqwidgets-ng-jqxgrid.js");








let StockUploaderPage = class StockUploaderPage {
    constructor(ajaxService, router, commonService) {
        this.ajaxService = ajaxService;
        this.router = router;
        this.commonService = commonService;
        this.name = false;
        this.willDownload = false;
        this.output = '';
        this.show = false;
        this.imeiIssues = [];
        this.excellKeyValid = false;
    }
    onFileChange(ev) {
        this.show = false;
        var fileName = ev.srcElement.files[0];
        this.name = fileName.name.includes(".xlsx");
        if (this.name == true) {
            this.show = false;
            this.dataString = [];
            let workBook = null;
            let jsonData = null;
            const reader = new FileReader();
            const file = ev.srcElement.files[0];
            reader.onload = (event) => {
                const data = reader.result;
                workBook = xlsx__WEBPACK_IMPORTED_MODULE_6__["read"](data, { type: 'binary' });
                jsonData = workBook.SheetNames.reduce((initial, name) => {
                    const sheet = workBook.Sheets[name];
                    initial[name] = xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].sheet_to_json(sheet);
                    return initial;
                }, {});
                let json = [];
                for (let i = 0; i < jsonData["Sheet1"].length; i++) {
                    jsonData["Sheet1"][i]["IMEI_NO"] = jsonData["Sheet1"][i]["IMEI_NO"].toString();
                    jsonData["Sheet1"][i]["SIM_NO"] = jsonData["Sheet1"][i]["SIM_NO"].toString();
                    jsonData["Sheet1"][i]["SIM_NO2"] = jsonData["Sheet1"][i]["SIM_NO2"] + '';
                    jsonData["Sheet1"][i]["PROVIDER"] = jsonData["Sheet1"][i]["PROVIDER"].toString();
                    jsonData["Sheet1"][i]["PROVIDER2"] = jsonData["Sheet1"][i]["PROVIDER2"] + '';
                    jsonData["Sheet1"][i]["MANUFACTURE"] = jsonData["Sheet1"][i]["MANUFACTURE"].toString();
                    jsonData["Sheet1"][i]["DEALER_ID"] = jsonData["Sheet1"][i]["DEALER_ID"] + '';
                    json.push(jsonData["Sheet1"][i]);
                }
                this.dataString = json;
                this.output = this.dataString.slice(0, 300).concat("...");
                // this.sendToServer(dataString);
            };
            reader.readAsBinaryString(file);
        }
        else {
            this.commonService.presentToast("please insert only excel file (.xlsx)");
        }
    }
    sendToServer() {
        // checking excell keys is valid or not
        if (this.dataString.length == 0) {
            this.commonService.presentToast("check your excell file,don't enter blank spaces");
        }
        else {
            var excellKeys = Object.keys(this.dataString[0]);
            for (var i = 0; i < excellKeys.length; i++) {
                if ((excellKeys[i] == "IMEI_NO") || (excellKeys[i] == "MODEL")) {
                    console.log("present");
                    this.excellKeyValid = true;
                }
            }
            if (this.name == true && this.excellKeyValid == true) {
                this.imeiIssues = [];
                this.willDownload = true;
                // let url ="http://localhost:8090/Web/api/vts/comp/inventory";
                const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/global/comp/inventory';
                // const data = [];
                // data.push(this.dataString["Sheet1"]);
                this.ajaxService.ajaxPostWithBody(url, this.dataString)
                    .subscribe(res => {
                    console.log(res);
                    (res == '[]') ? this.commonService.presentToast("please insert a value excell file only") :
                        this.commonService.presentToast('uploaded');
                    var resDatas = res;
                    for (var i = 0; i < resDatas.length; i++) {
                        var data = '';
                        data += Object.values(resDatas[i]);
                        if (data != "Device Persisted") {
                            this.imeiIssues.push({ imei: Object.keys(resDatas[i]), issue: Object.values(resDatas[i]) });
                        }
                    }
                    if (this.imeiIssues.length > 0) {
                        this.show = true;
                        this.source.localdata = this.imeiIssues;
                        // this.source.localdata = [{imei:"121212121212121",issue:"PROVIDER is Missing"}];
                        this.myGrid.updatebounddata();
                    }
                });
            }
            else {
                this.commonService.presentToast("please insert valid excel file (.xlsx)");
            }
        }
    }
    ngAfterViewInit() {
        // this.myGrid.showloadelement();
        // this.getDatas();
    }
    ngOnInit() {
        this.renderer = (row, column, value) => {
            if (value == "" || null || undefined) {
                return "----No Data----";
            }
            else {
                return '<span  style="line-height:32px;font-size:11px;color:darkblue;margin:auto;padding:0px 5px">' + value + '</span>';
            }
        };
        this.source = { localdata: this.tableData };
        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.columns = [
            { text: 'IMEI', datafield: 'imei', cellsrenderer: this.renderer },
            // {text :'Operator',datafield:'operatorName',cellsrenderer:this.renderer},
            { text: 'ISSUE', datafield: 'issue', cellsrenderer: this.renderer },
        ];
    }
};
StockUploaderPage.ctorParameters = () => [
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myGrid', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_7__["jqxGridComponent"])
], StockUploaderPage.prototype, "myGrid", void 0);
StockUploaderPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-stock-uploader',
        template: __webpack_require__(/*! raw-loader!./stock-uploader.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/stock-uploader/stock-uploader.page.html"),
        styles: [__webpack_require__(/*! ./stock-uploader.page.scss */ "./src/app/delar-application/stock-uploader/stock-uploader.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"]])
], StockUploaderPage);



/***/ })

}]);
//# sourceMappingURL=stock-uploader-stock-uploader-module-es2015.js.map