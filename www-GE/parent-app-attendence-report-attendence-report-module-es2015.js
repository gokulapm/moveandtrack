(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["parent-app-attendence-report-attendence-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/parent-app/attendence-report/attendence-report.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/parent-app/attendence-report/attendence-report.page.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class='header'>\r\n  <ion-toolbar class=\"header-style\">\r\n    <ion-row style=\"align-items: center;\">\r\n      <ion-icon (click)=\"locationBack()\" class=\"icon-size\" name=\"arrow-back\"></ion-icon>\r\n      <ion-label> Attendence Report </ion-label>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-row style=\"margin-top: 8px;\">\r\n    <ion-col size=\"7.8\" style=\"margin: auto;\">\r\n      <ion-input type=\"month\" class=\"date-input\" [(ngModel)]=\"currentDate\"></ion-input>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <form [formGroup]='dateForm'>\r\n    <ion-row>\r\n      <ion-col style=\"text-align: center;margin: 0;\r\n      padding: 0;\">\r\n        <ion-button (click)=\"submit()\" class=\"btn-submit\" shape=\"round\" size=\"small\">SUBMIT</ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row style=\"margin-top: 10px;display:none\">\r\n      <ion-col size=\"6\">\r\n        <label class=\"top-label\">Present From</label>\r\n        <ion-input class=\"date-input\" name='presentFrom' formControlName='presentFrom'></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <label class=\"top-label\">Absent From</label>\r\n        <ion-input class=\"date-input\" name='dateFrom' formControlName='dateFrom'></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <label class=\"top-label\">Holiday From</label>\r\n        <ion-input class=\"date-input\" name='holidayFrom' formControlName='holidayFrom'></ion-input>\r\n      </ion-col>\r\n    </ion-row>\r\n </form>\r\n\r\n  <div class='flex-container flex-center'>\r\n    <div id='calendar-container' class='flex-container flex-column'>\r\n      <div style=\"height: 10px;\"></div>\r\n      <div class=' flex-container flex-center-horz '>\r\n        <div class='fa fa-chevron-left ' (click)='previousMonth()'></div>\r\n         <div class='width-5'></div>\r\n          <div *ngIf=\"!next\"> {{currentDate | date:'longDate'}}</div>\r\n           <div *ngIf=\"next\">{{date.format('YYYY ')}}{{date.format('MMMM ')}} </div>\r\n          <div class='width-5'></div>\r\n        <div class='fa fa-chevron-right ' (click)='nextMonth()'></div>\r\n      </div>\r\n      <div class='height-20px '></div>\r\n      <div class='flex-container '>\r\n        <div class='calendar-day flex-container flex-center ' *ngFor=\"let items of days\">{{items}}</div>\r\n      </div>\r\n      <div class='flex-container flex-wrap '>\r\n        <div *ngFor=\"let day of daysArr\">\r\n          <div class='calendar-days flex-container flex-center '\r\n            [ngClass]=\"{'inactive ': !day,  'present':isPresent(day),'absent': isAbsent(day) ,'holiday':isHoliday(day)}\">\r\n            {{day?.date()}}\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/parent-app/attendence-report/attendence-report.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/parent-app/attendence-report/attendence-report.module.ts ***!
  \**************************************************************************/
/*! exports provided: AttendenceReportPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttendenceReportPageModule", function() { return AttendenceReportPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _attendence_report_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./attendence-report.page */ "./src/app/parent-app/attendence-report/attendence-report.page.ts");
/* harmony import */ var src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared-mod/shared-mod.module */ "./src/app/shared-mod/shared-mod.module.ts");








const routes = [
    {
        path: '',
        component: _attendence_report_page__WEBPACK_IMPORTED_MODULE_6__["AttendenceReportPage"]
    }
];
let AttendenceReportPageModule = class AttendenceReportPageModule {
};
AttendenceReportPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_7__["SharedModModule"]
        ],
        declarations: [_attendence_report_page__WEBPACK_IMPORTED_MODULE_6__["AttendenceReportPage"]]
    })
], AttendenceReportPageModule);



/***/ }),

/***/ "./src/app/parent-app/attendence-report/attendence-report.page.scss":
/*!**************************************************************************!*\
  !*** ./src/app/parent-app/attendence-report/attendence-report.page.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-style {\n  --background: #1aba7e;\n  color: white;\n}\n\n.icon-size {\n  font-size: 18px;\n  padding: 9px;\n}\n\n.date-input {\n  border: 1px solid #f6f6f6;\n  background: #f6f6f6;\n  --padding-top: 2px;\n  --padding-bottom: 2px;\n}\n\na.active {\n  background-color: gray;\n}\n\n#calendar-container {\n  width: 271px;\n}\n\n.calendar-days, .calendar-day {\n  width: 38px;\n  height: 38px;\n  background: #e1fbe1;\n  border: 1px solid #b3b3b3;\n}\n\n.calendar-day {\n  background: gray;\n  color: white;\n  font-size: 14px;\n}\n\n.inactive {\n  opacity: 0.5;\n  background: #cacaca;\n}\n\n.present {\n  background: #1aba7e;\n}\n\n.absent {\n  background: red;\n}\n\n.holiday {\n  background: white;\n}\n\n.top-label {\n  color: #1aba7e;\n  font-size: 12px;\n  padding-left: 10px;\n}\n\n.btn-submit {\n  padding: 0px 10px;\n  --background: #1aba7e;\n  --background-focused: #1aba7e;\n  --background-hover: #1aba7e;\n  --background-activated: #1aba7e;\n}\n\n.fill-height-or-more {\n  min-height: 100%;\n}\n\n.flex-footer-pos {\n  -webkit-box-flex: 1;\n          flex: 1 0 auto;\n}\n\n.flex-container {\n  display: -webkit-box;\n  display: flex;\n  font-size: 14px;\n}\n\n.flex-1 {\n  -webkit-box-flex: 1;\n          flex: 1;\n}\n\n.flex-left {\n  -webkit-align-self: flex-start;\n  align-self: flex-start;\n}\n\n.flex-column {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n}\n\n.flex-center {\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.flex-center-horz {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.flex-center-vert {\n  -webkit-box-align: center;\n          align-items: center;\n}\n\n.flex-left-row {\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n}\n\n.flex-right-row {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-right-column {\n  -webkit-box-align: end;\n          align-items: flex-end;\n}\n\n.flex-left-column {\n  -webkit-box-align: start;\n          align-items: flex-start;\n}\n\n.flex-wrap {\n  flex-wrap: wrap;\n}\n\n.flex-space-between-row {\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-justify-content: space-between;\n}\n\n.flex-space-between-column {\n  -webkit-box-align: space-between;\n          align-items: space-between;\n  -webkit-align-itemst: space-between;\n}\n\n.height-20px {\n  height: 20px;\n}\n\n.width-5 {\n  width: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvcGFyZW50LWFwcC9hdHRlbmRlbmNlLXJlcG9ydC9hdHRlbmRlbmNlLXJlcG9ydC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhcmVudC1hcHAvYXR0ZW5kZW5jZS1yZXBvcnQvYXR0ZW5kZW5jZS1yZXBvcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FDQ0o7O0FEQ0E7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQ0VKOztBREVBO0VBQ0UseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUNDRjs7QURJQTtFQUNJLHNCQUFBO0FDREo7O0FESUE7RUFDSSxZQUFBO0FDREo7O0FESUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUNESjs7QURHQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNBRjs7QURHQTtFQUNDLFlBQUE7RUFDQSxtQkFBQTtBQ0FEOztBREVBO0VBQ0UsbUJBQUE7QUNDRjs7QURDQTtFQUNJLGVBQUE7QUNFSjs7QURBQTtFQUNFLGlCQUFBO0FDR0Y7O0FEREE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDSUY7O0FEREE7RUFDRSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsNkJBQUE7RUFDQSwyQkFBQTtFQUNBLCtCQUFBO0FDSUY7O0FEQ0E7RUFDRSxnQkFBQTtBQ0VGOztBRENBO0VBQ0UsbUJBQUE7VUFBQSxjQUFBO0FDRUY7O0FEQ0E7RUFFRSxvQkFBQTtFQUFBLGFBQUE7RUFDQSxlQUFBO0FDRUY7O0FEQ0E7RUFDRSxtQkFBQTtVQUFBLE9BQUE7QUNFRjs7QURDQTtFQUNFLDhCQUFBO0VBQ0Esc0JBQUE7QUNFRjs7QURDQTtFQUVFLDRCQUFBO0VBQUEsNkJBQUE7VUFBQSxzQkFBQTtBQ0VGOztBRENBO0VBRUUseUJBQUE7VUFBQSxtQkFBQTtFQUVBLHdCQUFBO1VBQUEsdUJBQUE7QUNFRjs7QURDQTtFQUVFLHdCQUFBO1VBQUEsdUJBQUE7QUNFRjs7QURDQTtFQUVFLHlCQUFBO1VBQUEsbUJBQUE7QUNFRjs7QURDQTtFQUVFLHVCQUFBO1VBQUEsMkJBQUE7QUNFRjs7QURDQTtFQUVFLHFCQUFBO1VBQUEseUJBQUE7QUNFRjs7QURDQTtFQUVFLHNCQUFBO1VBQUEscUJBQUE7QUNFRjs7QURDQTtFQUVFLHdCQUFBO1VBQUEsdUJBQUE7QUNFRjs7QURDQTtFQUNFLGVBQUE7QUNFRjs7QURDQTtFQUNFLHlCQUFBO1VBQUEsOEJBQUE7RUFDQSxzQ0FBQTtBQ0VGOztBRENBO0VBQ0UsZ0NBQUE7VUFBQSwwQkFBQTtFQUNBLG1DQUFBO0FDRUY7O0FEQUE7RUFDRSxZQUFBO0FDR0Y7O0FEREE7RUFDRSxTQUFBO0FDSUYiLCJmaWxlIjoic3JjL2FwcC9wYXJlbnQtYXBwL2F0dGVuZGVuY2UtcmVwb3J0L2F0dGVuZGVuY2UtcmVwb3J0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItc3R5bGV7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMxYWJhN2U7XHJcbiAgICBjb2xvcjogd2hpdGU7ICAgXHJcbn1cclxuLmljb24tc2l6ZSB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBwYWRkaW5nOiA5cHg7XHJcbn1cclxuXHJcblxyXG4uZGF0ZS1pbnB1dHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZjZmNmY2O1xyXG4gIGJhY2tncm91bmQ6ICNmNmY2ZjY7XHJcbiAgLS1wYWRkaW5nLXRvcDogMnB4O1xyXG4gIC0tcGFkZGluZy1ib3R0b206IDJweDtcclxufVxyXG5cclxuXHJcblxyXG5hLmFjdGl2ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmF5O1xyXG59XHJcblxyXG4jY2FsZW5kYXItY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiAyNzFweDtcclxufVxyXG5cclxuLmNhbGVuZGFyLWRheXMsLmNhbGVuZGFyLWRheSB7XHJcbiAgICB3aWR0aDogMzhweDtcclxuICAgIGhlaWdodDogMzhweDtcclxuICAgIGJhY2tncm91bmQ6ICNlMWZiZTE7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYjNiM2IzO1xyXG59XHJcbi5jYWxlbmRhci1kYXl7XHJcbiAgYmFja2dyb3VuZDpncmF5O1xyXG4gIGNvbG9yOndoaXRlO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuLmluYWN0aXZlIHtcclxuIG9wYWNpdHk6IDAuNTtcclxuIGJhY2tncm91bmQ6ICNjYWNhY2E7XHJcbn1cclxuLnByZXNlbnR7XHJcbiAgYmFja2dyb3VuZDogIzFhYmE3ZTtcclxufVxyXG4uYWJzZW50e1xyXG4gICAgYmFja2dyb3VuZDpyZWQ7XHJcbn1cclxuLmhvbGlkYXl7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbn1cclxuLnRvcC1sYWJlbHtcclxuICBjb2xvcjogIzFhYmE3ZTtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG59XHJcblxyXG4uYnRuLXN1Ym1pdHtcclxuICBwYWRkaW5nOiAwcHggMTBweDtcclxuICAtLWJhY2tncm91bmQ6ICMxYWJhN2U7XHJcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMxYWJhN2U7XHJcbiAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjMWFiYTdlO1xyXG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMxYWJhN2U7IFxyXG4gIFxyXG59XHJcblxyXG5cclxuLmZpbGwtaGVpZ2h0LW9yLW1vcmUge1xyXG4gIG1pbi1oZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5mbGV4LWZvb3Rlci1wb3Mge1xyXG4gIGZsZXg6IDEgMCBhdXRvO1xyXG59XHJcblxyXG4uZmxleC1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuLmZsZXgtMSB7XHJcbiAgZmxleDogMTtcclxufVxyXG5cclxuLmZsZXgtbGVmdCB7XHJcbiAgLXdlYmtpdC1hbGlnbi1zZWxmOiBmbGV4LXN0YXJ0O1xyXG4gIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XHJcbn1cclxuXHJcbi5mbGV4LWNvbHVtbiB7XHJcbiAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbn1cclxuXHJcbi5mbGV4LWNlbnRlciB7XHJcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmZsZXgtY2VudGVyLWhvcnoge1xyXG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5mbGV4LWNlbnRlci12ZXJ0IHtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmZsZXgtbGVmdC1yb3cge1xyXG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxufVxyXG5cclxuLmZsZXgtcmlnaHQtcm93IHtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxufVxyXG5cclxuLmZsZXgtcmlnaHQtY29sdW1uIHtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbn1cclxuXHJcbi5mbGV4LWxlZnQtY29sdW1uIHtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG59XHJcblxyXG4uZmxleC13cmFwIHtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbi5mbGV4LXNwYWNlLWJldHdlZW4tcm93IHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuXHJcbi5mbGV4LXNwYWNlLWJldHdlZW4tY29sdW1uIHtcclxuICBhbGlnbi1pdGVtczogc3BhY2UtYmV0d2VlbjtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zdDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4uaGVpZ2h0LTIwcHgge1xyXG4gIGhlaWdodDogMjBweDtcclxufVxyXG4ud2lkdGgtNXtcclxuICB3aWR0aDogNSU7XHJcbn1cclxuXHJcbiIsIi5oZWFkZXItc3R5bGUge1xuICAtLWJhY2tncm91bmQ6ICMxYWJhN2U7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmljb24tc2l6ZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgcGFkZGluZzogOXB4O1xufVxuXG4uZGF0ZS1pbnB1dCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmNmY2ZjY7XG4gIGJhY2tncm91bmQ6ICNmNmY2ZjY7XG4gIC0tcGFkZGluZy10b3A6IDJweDtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMnB4O1xufVxuXG5hLmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyYXk7XG59XG5cbiNjYWxlbmRhci1jb250YWluZXIge1xuICB3aWR0aDogMjcxcHg7XG59XG5cbi5jYWxlbmRhci1kYXlzLCAuY2FsZW5kYXItZGF5IHtcbiAgd2lkdGg6IDM4cHg7XG4gIGhlaWdodDogMzhweDtcbiAgYmFja2dyb3VuZDogI2UxZmJlMTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2IzYjNiMztcbn1cblxuLmNhbGVuZGFyLWRheSB7XG4gIGJhY2tncm91bmQ6IGdyYXk7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uaW5hY3RpdmUge1xuICBvcGFjaXR5OiAwLjU7XG4gIGJhY2tncm91bmQ6ICNjYWNhY2E7XG59XG5cbi5wcmVzZW50IHtcbiAgYmFja2dyb3VuZDogIzFhYmE3ZTtcbn1cblxuLmFic2VudCB7XG4gIGJhY2tncm91bmQ6IHJlZDtcbn1cblxuLmhvbGlkYXkge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuLnRvcC1sYWJlbCB7XG4gIGNvbG9yOiAjMWFiYTdlO1xuICBmb250LXNpemU6IDEycHg7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbn1cblxuLmJ0bi1zdWJtaXQge1xuICBwYWRkaW5nOiAwcHggMTBweDtcbiAgLS1iYWNrZ3JvdW5kOiAjMWFiYTdlO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogIzFhYmE3ZTtcbiAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjMWFiYTdlO1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjMWFiYTdlO1xufVxuXG4uZmlsbC1oZWlnaHQtb3ItbW9yZSB7XG4gIG1pbi1oZWlnaHQ6IDEwMCU7XG59XG5cbi5mbGV4LWZvb3Rlci1wb3Mge1xuICBmbGV4OiAxIDAgYXV0bztcbn1cblxuLmZsZXgtY29udGFpbmVyIHtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5mbGV4LTEge1xuICBmbGV4OiAxO1xufVxuXG4uZmxleC1sZWZ0IHtcbiAgLXdlYmtpdC1hbGlnbi1zZWxmOiBmbGV4LXN0YXJ0O1xuICBhbGlnbi1zZWxmOiBmbGV4LXN0YXJ0O1xufVxuXG4uZmxleC1jb2x1bW4ge1xuICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5mbGV4LWNlbnRlciB7XG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5mbGV4LWNlbnRlci1ob3J6IHtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5mbGV4LWNlbnRlci12ZXJ0IHtcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uZmxleC1sZWZ0LXJvdyB7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG59XG5cbi5mbGV4LXJpZ2h0LXJvdyB7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtcmlnaHQtY29sdW1uIHtcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtbGVmdC1jb2x1bW4ge1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cblxuLmZsZXgtd3JhcCB7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmZsZXgtc3BhY2UtYmV0d2Vlbi1yb3cge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4uZmxleC1zcGFjZS1iZXR3ZWVuLWNvbHVtbiB7XG4gIGFsaWduLWl0ZW1zOiBzcGFjZS1iZXR3ZWVuO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zdDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLmhlaWdodC0yMHB4IHtcbiAgaGVpZ2h0OiAyMHB4O1xufVxuXG4ud2lkdGgtNSB7XG4gIHdpZHRoOiA1JTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/parent-app/attendence-report/attendence-report.page.ts":
/*!************************************************************************!*\
  !*** ./src/app/parent-app/attendence-report/attendence-report.page.ts ***!
  \************************************************************************/
/*! exports provided: AttendenceReportPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttendenceReportPage", function() { return AttendenceReportPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");








let AttendenceReportPage = class AttendenceReportPage {
    constructor(location, formBuilder, ajaxService, commonService) {
        this.location = location;
        this.formBuilder = formBuilder;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.date = moment__WEBPACK_IMPORTED_MODULE_4__();
        this.currentDate = `${this.date.format('MMMM ')}${this.date.format('YYYY ')}`;
        this.days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        this.next = false;
        this.present = [];
    }
    initDateRange() {
        var today = new Date();
        var month = String(today.getMonth() + 1).padStart(2, '0');
        var year = today.getFullYear();
        today = year + '-' + month;
        this.currentDate = today;
        this.dateForm = this.formBuilder.group({
            dateFrom: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            presentFrom: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            holidayFrom: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
        });
    }
    ngOnInit() {
        this.initDateRange();
        this.daysArr = this.createCalendar(this.date);
        this.submit();
    }
    submit() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + `/parentapp/attendance?stin=12345&period=${this.currentDate}`;
        this.ajaxService.ajaxGet(url).subscribe(res => {
            var data = res;
            this.next = false;
            this.date = moment__WEBPACK_IMPORTED_MODULE_4__(this.currentDate, 'YYYY MM');
            if (this.currentDate !== "") {
                if (this.currentDate == data.month) {
                    //form data
                    this.dateForm.patchValue({
                        dateFrom: data.absent,
                        presentFrom: data.present,
                        holidayFrom: data.holiday,
                    });
                    this.daysArr = this.createCalendar(moment__WEBPACK_IMPORTED_MODULE_4__(this.currentDate, 'YYYY MM'));
                }
            }
            else {
                this.commonService.presentToast("Please try again later.");
            }
        });
    }
    locationBack() {
        this.location.back();
    }
    createCalendar(month) {
        let firstDay = moment__WEBPACK_IMPORTED_MODULE_4__(month).startOf('M');
        let days = Array.apply(null, { length: month.daysInMonth() })
            .map(Number.call, Number)
            .map(n => {
            return moment__WEBPACK_IMPORTED_MODULE_4__(firstDay).add(n, 'd');
        });
        for (let n = 0; n < firstDay.weekday(); n++) {
            days.unshift(null);
        }
        return days;
    }
    nextMonth() {
        this.date.add(1, 'M');
        this.daysArr = this.createCalendar(this.date);
        this.next = true;
    }
    previousMonth() {
        this.date.subtract(1, 'M');
        this.daysArr = this.createCalendar(this.date);
        this.next = true;
    }
    isAbsent(day) {
        if (!day) {
            return false;
        }
        const absentArr = this.dateForm.value.dateFrom;
        const dayString = day ? day.format('YYYY-MM-DD') : '';
        if (this.dateForm.valid) {
            return absentArr.includes(dayString);
        }
        return false;
    }
    isPresent(day) {
        if (!day) {
            return false;
        }
        const presentArr = this.dateForm.value.presentFrom;
        const dayString = day ? day.format('YYYY-MM-DD') : '';
        if (this.dateForm.valid) {
            return presentArr.includes(dayString);
        }
        return false;
    }
    isHoliday(day) {
        if (!day) {
            return false;
        }
        const holidayArr = this.dateForm.value.holidayFrom;
        const dayString = day ? day.format('YYYY-MM-DD') : '';
        if (this.dateForm.valid) {
            return holidayArr.includes(dayString);
        }
        return false;
    }
};
AttendenceReportPage.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
    { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] }
];
AttendenceReportPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-attendence-report',
        template: __webpack_require__(/*! raw-loader!./attendence-report.page.html */ "./node_modules/raw-loader/index.js!./src/app/parent-app/attendence-report/attendence-report.page.html"),
        styles: [__webpack_require__(/*! ./attendence-report.page.scss */ "./src/app/parent-app/attendence-report/attendence-report.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"],
        src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
], AttendenceReportPage);



/***/ })

}]);
//# sourceMappingURL=parent-app-attendence-report-attendence-report-module-es2015.js.map