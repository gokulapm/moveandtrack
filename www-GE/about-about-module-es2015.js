(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["about-about-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/parent-app/about/about.page.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/parent-app/about/about.page.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-header class='header'>\n  <ion-toolbar class=\"header-style\">\n    <ion-row style=\"align-items: center;\">\n      <ion-icon (click)=\"locationBack()\" class=\"icon-size\" name=\"arrow-back\"></ion-icon>\n      <ion-label> About  </ion-label>\n   </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <!-- <ion-row class=\"iconPlace\">\n    <ion-icon class=\"iconStyle\" style=\"font-size: 120px;\" name= \"contact\"> </ion-icon>\n    <ion-img style=\"width:70%;\" src=\"assets/vtstrackhisIcon/seatBelt.svg\"></ion-img>\n  </ion-row> -->\n  <ion-row>\n    <ion-col size=\"8\" offset=\"2\" class=\"ion-align-items-center ion-justify-content-center\">\n        <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n            <ion-img style=\"width:70%;\n            border: 2px solid lightgrey;\n            border-radius: 78px;\" src=\"assets/vtstrackhisIcon/seatBelt.svg\"></ion-img>\n        </ion-row>\n    </ion-col>\n</ion-row>\n<ion-row><ion-col size=12 class=\"ion-text-center\">{{data.schoolName}}\n</ion-col></ion-row>\n<!-- <ion-row><ion-col size=8 offset=\"2\" class=\"ion-text-center\" off-set=4><ion-card>\n<ion-row><ion-col size=12 class=\"ion-text-center\">Description</ion-col></ion-row>\n<ion-row>NO 1 school in chennai</ion-row>\n</ion-card></ion-col></ion-row> -->\n  <!-- description\n  Mail Id\n  Address\n  Phone Number -->\n  <ion-row class=\"rowPadding\">\n    <ion-col size=\"2\" style=\"text-align: right;\">\n      <ion-icon class=\"detailIcon\" name=\"phone-portrait\"></ion-icon> \n    </ion-col>\n    <ion-col size=\"10\">\n    <label>{{data.contactNo}}</label>\n    </ion-col>\n  </ion-row>\n  <ion-row class=\"rowPadding\">\n    <ion-col size=\"2\" style=\"text-align: right;\">\n      <ion-icon class=\"detailIcon\" name=\"mail\"></ion-icon> \n    </ion-col>\n    <ion-col size=\"10\">\n    <label>{{data.mailId}}</label>\n    </ion-col>\n  </ion-row>\n  <ion-row class=\"rowPadding\">\n    <ion-col size=\"2\" style=\"text-align: right;align-self: center;\">\n      <ion-icon  name=\"compass\"></ion-icon> \n    </ion-col>  \n    <ion-col size=\"10\">\n    <label>{{data.address}}</label>\n    </ion-col>\n  </ion-row>\n  <ion-row class=\"rowPadding\">\n    <ion-col size=\"2\" style=\"text-align: right;align-self: center;\">\n      <ion-icon  name=\"business\"></ion-icon> \n    </ion-col>\n    <ion-col size=\"10\">\n    <label>{{data.describe}}</label>\n    </ion-col>\n  </ion-row>\n  <!-- <ion-row><ion-col size=12 class=\"ion-text-center\" ><ion-card>\n    <ion-row><ion-col size=12 class=\"ion-text-center\">Description</ion-col></ion-row>\n    <ion-row><ion-col size=12 class=\"ion-text-left\">No:3 Ramanujam garden street, perambur, chennai-600012</ion-col></ion-row>\n    </ion-card></ion-col></ion-row> -->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/parent-app/about/about.module.ts":
/*!**************************************************!*\
  !*** ./src/app/parent-app/about/about.module.ts ***!
  \**************************************************/
/*! exports provided: AboutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPageModule", function() { return AboutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _about_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./about.page */ "./src/app/parent-app/about/about.page.ts");







const routes = [
    {
        path: '',
        component: _about_page__WEBPACK_IMPORTED_MODULE_6__["AboutPage"]
    }
];
let AboutPageModule = class AboutPageModule {
};
AboutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_about_page__WEBPACK_IMPORTED_MODULE_6__["AboutPage"]]
    })
], AboutPageModule);



/***/ }),

/***/ "./src/app/parent-app/about/about.page.scss":
/*!**************************************************!*\
  !*** ./src/app/parent-app/about/about.page.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-style {\n  --background: #1aba7e;\n  color: white;\n}\n\n.rowPadding {\n  padding: 8px;\n  border-bottom: 1px solid lightgrey;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvcGFyZW50LWFwcC9hYm91dC9hYm91dC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhcmVudC1hcHAvYWJvdXQvYWJvdXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0VBQ0Esa0NBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BhcmVudC1hcHAvYWJvdXQvYWJvdXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlci1zdHlsZSB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMxYWJhN2U7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5yb3dQYWRkaW5ne1xyXG4gICAgcGFkZGluZzogOHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcclxuICB9XHJcbiIsIi5oZWFkZXItc3R5bGUge1xuICAtLWJhY2tncm91bmQ6ICMxYWJhN2U7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnJvd1BhZGRpbmcge1xuICBwYWRkaW5nOiA4cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyZXk7XG59Il19 */"

/***/ }),

/***/ "./src/app/parent-app/about/about.page.ts":
/*!************************************************!*\
  !*** ./src/app/parent-app/about/about.page.ts ***!
  \************************************************/
/*! exports provided: AboutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPage", function() { return AboutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");





let AboutPage = class AboutPage {
    constructor(ajaxservice, location) {
        this.ajaxservice = ajaxservice;
        this.location = location;
        this.data = {
            schoolName: "",
            mailId: "",
            contactNo: "",
            address: "",
            describe: ""
        };
    }
    getAboutDatas() {
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/parentapp/about';
        this.ajaxservice.ajaxGetObject(url).subscribe(res => {
            this.data = JSON.parse(res);
        });
    }
    locationBack() {
        this.location.back();
    }
    ngOnInit() {
        this.getAboutDatas();
    }
};
AboutPage.ctorParameters = () => [
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] }
];
AboutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about',
        template: __webpack_require__(/*! raw-loader!./about.page.html */ "./node_modules/raw-loader/index.js!./src/app/parent-app/about/about.page.html"),
        styles: [__webpack_require__(/*! ./about.page.scss */ "./src/app/parent-app/about/about.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"]])
], AboutPage);



/***/ })

}]);
//# sourceMappingURL=about-about-module-es2015.js.map