(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["skt-tag-tag-table-tag-table-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/skt/tag/tag-table/tag-table.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/skt/tag/tag-table/tag-table.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <div class=\"tag-wrapper\">\n     <ion-row  class=\"header-section\">\n      <ion-col size=\"2\" size-sm=\"2\" size-md=\"2\" size-lg=\"0\"> \n        <ion-menu-button></ion-menu-button>\n      </ion-col>  \n      <ion-col size=\"10\" size-sm=\"10\" size-md=\"10\" size-lg=\"12\" id=\"btn-wrapper\">\n        Tag Details\n      </ion-col>\n    </ion-row>  \n   <ion-row>\n     <ion-col>\n       <ion-button color='primary' class=\"tag-btn\" (click)='openparentDetailModel()'>Add</ion-button>\n       <ion-button color='primary' class=\"tag-btn\" (click)='parentEditModel()'>Edit</ion-button>\n       <ion-button color='primary' class=\"tag-btn\"(click)='deletebtn()' >Delete</ion-button>\n     </ion-col>\n    </ion-row> \n  </div> \n  <div id=\"export-wrapper\">\n      <!-- <ion-img src=\"assets/student_Details/print.svg\"  class=\"toolbar-row\" (click)=\"btnOnClick()\"></ion-img> -->\n      <ion-img src=\"assets/student_Details/pdf.svg\" class=\"toolbar-row\" (click)=\"createPdf()\"></ion-img>\n      <ion-img src=\"assets/student_Details/excel.svg\" class=\"toolbar-row\"  (click)=\"exportToExcel()\"> </ion-img>\n        <!-- <ion-img src=\"assets/student_Details/refresher.svg\" class=\"toolbar-row\" (click)=\"refresher()\"></ion-img> -->\n    </div> \n </ion-toolbar>\n</ion-header>\n<ion-content>\n <div class=\"grid_Container\">\n  <jqxGrid\n  (onRowselect)=\"myGridOnRowSelect($event)\"\n  [pageable]=\"true\"\n  [selectionmode]=\"'singlerow'\"\n  [showfilterrow]=\"true\"\n  [filterable]=\"true\"\n  [sortable]=\"true\"\n  [width]=\"'100%'\"\n  [source]=\"dataAdapter\" \n  [columns]=\"columns\" \n  [autoheight]=\"true\"\n  #myGrid \n  [theme]=\"'material'\"\n  style=\"font-size:16px;text-align: center !important;\">\n  </jqxGrid>\n</div>\n</ion-content>\n\n"

/***/ }),

/***/ "./src/app/skt/tag/tag-table/tag-table.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/skt/tag/tag-table/tag-table.module.ts ***!
  \*******************************************************/
/*! exports provided: TagTablePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagTablePageModule", function() { return TagTablePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tag_table_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tag-table.page */ "./src/app/skt/tag/tag-table/tag-table.page.ts");
/* harmony import */ var _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../sktcomponents.module */ "./src/app/skt/sktcomponents.module.ts");
/* harmony import */ var src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared-mod/shared-mod.module */ "./src/app/shared-mod/shared-mod.module.ts");









var routes = [
    {
        path: '',
        component: _tag_table_page__WEBPACK_IMPORTED_MODULE_6__["TagTablePage"]
    }
];
var TagTablePageModule = /** @class */ (function () {
    function TagTablePageModule() {
    }
    TagTablePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__["SktComponentsModule"],
                src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__["SharedModModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_tag_table_page__WEBPACK_IMPORTED_MODULE_6__["TagTablePage"]]
        })
    ], TagTablePageModule);
    return TagTablePageModule;
}());



/***/ }),

/***/ "./src/app/skt/tag/tag-table/tag-table.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/skt/tag/tag-table/tag-table.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tag-wrapper, .grid_Container {\n  border: 1px solid #c7c3c3;\n  margin: 5px 10px 0px;\n}\n\n.grid_Container {\n  margin: 0px 10px 0px;\n}\n\n.header-section {\n  background-color: #e8e8e8;\n  border: 1px solid #cac2c2;\n}\n\n#export-wrapper {\n  text-align: right;\n  background-color: #e8e8e8;\n  height: 42px;\n  border-top: 1px solid gainsboro;\n  margin: 0px 10px 0px;\n  border-left: 2px solid #c7c7c7;\n}\n\n#btn-wrapper {\n  text-align: center;\n  background-color: #e8e8e8;\n  height: 40px;\n  padding: 0px;\n  font-size: 20px;\n  border-bottom: 1px solid #cac2c2;\n  font-weight: 700;\n}\n\n.toolbar-row {\n  height: 40px;\n  width: 40px;\n  display: inline-block;\n  margin: 0px;\n  border: 1px solid #b9b7b7;\n  padding: 5px;\n}\n\n.toolbar-row:hover {\n  background-color: whitesmoke;\n}\n\n.tag-btn {\n  width: 80px;\n  height: 30px;\n  margin: 5px 10px;\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvc2t0L3RhZy90YWctdGFibGUvdGFnLXRhYmxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2t0L3RhZy90YWctdGFibGUvdGFnLXRhYmxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0Esb0JBQUE7QUNDSjs7QURDQztFQUNHLG9CQUFBO0FDRUo7O0FEQUM7RUFDRSx5QkFBQTtFQUNBLHlCQUFBO0FDR0g7O0FEREM7RUFDRSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0VBQ0Esb0JBQUE7RUFDQSw4QkFBQTtBQ0lIOztBREZDO0VBQ0Usa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7QUNLSDs7QURIQztFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDTUg7O0FESkE7RUFDSSw0QkFBQTtBQ09KOztBRExDO0VBQ0csV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNRSiIsImZpbGUiOiJzcmMvYXBwL3NrdC90YWcvdGFnLXRhYmxlL3RhZy10YWJsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGFnLXdyYXBwZXIsLmdyaWRfQ29udGFpbmVye1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDE5OSwgMTk1LCAxOTUpO1xyXG4gICAgbWFyZ2luOiA1cHggMTBweCAwcHg7XHJcbiB9XHJcbiAuZ3JpZF9Db250YWluZXJ7XHJcbiAgICBtYXJnaW46IDBweCAxMHB4IDBweDsgIFxyXG4gfVxyXG4gLmhlYWRlci1zZWN0aW9ue1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gICBib3JkZXI6IDFweCBzb2xpZCAjY2FjMmMyO1xyXG4gfVxyXG4gI2V4cG9ydC13cmFwcGVye1xyXG4gICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcclxuICAgaGVpZ2h0OiA0MnB4O1xyXG4gICBib3JkZXItdG9wOiAxcHggc29saWQgZ2FpbnNib3JvO1xyXG4gICBtYXJnaW46IDBweCAxMHB4IDBweDtcclxuICAgYm9yZGVyLWxlZnQ6IDJweCBzb2xpZCAjYzdjN2M3O1xyXG4gfVxyXG4gI2J0bi13cmFwcGVye1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XHJcbiAgIGhlaWdodDogNDBweDtcclxuICAgcGFkZGluZzogMHB4O1xyXG4gICBmb250LXNpemU6IDIwcHg7XHJcbiAgIGJvcmRlci1ib3R0b206MXB4IHNvbGlkICNjYWMyYzI7XHJcbiAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiB9XHJcbiAudG9vbGJhci1yb3d7XHJcbiAgIGhlaWdodDogNDBweDtcclxuICAgd2lkdGg6IDQwcHg7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgbWFyZ2luOjBweDtcclxuICAgYm9yZGVyOiAxcHggc29saWQgI2I5YjdiNztcclxuICAgcGFkZGluZzogNXB4O1xyXG59XHJcbi50b29sYmFyLXJvdzpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlc21va2U7XHJcbn1cclxuIC50YWctYnRue1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBtYXJnaW46IDVweCAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gfVxyXG4gIiwiLnRhZy13cmFwcGVyLCAuZ3JpZF9Db250YWluZXIge1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzdjM2MzO1xuICBtYXJnaW46IDVweCAxMHB4IDBweDtcbn1cblxuLmdyaWRfQ29udGFpbmVyIHtcbiAgbWFyZ2luOiAwcHggMTBweCAwcHg7XG59XG5cbi5oZWFkZXItc2VjdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjYWMyYzI7XG59XG5cbiNleHBvcnQtd3JhcHBlciB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xuICBoZWlnaHQ6IDQycHg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBnYWluc2Jvcm87XG4gIG1hcmdpbjogMHB4IDEwcHggMHB4O1xuICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNjN2M3Yzc7XG59XG5cbiNidG4td3JhcHBlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjYWMyYzI7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi50b29sYmFyLXJvdyB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNiOWI3Yjc7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLnRvb2xiYXItcm93OmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcbn1cblxuLnRhZy1idG4ge1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBtYXJnaW46IDVweCAxMHB4O1xuICBmb250LXNpemU6IDEycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/skt/tag/tag-table/tag-table.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/skt/tag/tag-table/tag-table.page.ts ***!
  \*****************************************************/
/*! exports provided: TagTablePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagTablePage", function() { return TagTablePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jqwidgets-ng/jqxgrid */ "./node_modules/jqwidgets-ng/fesm5/jqwidgets-ng-jqxgrid.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tag_additional_tag_additional_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../tag-additional/tag-additional.component */ "./src/app/skt/tag/tag-additional/tag-additional.component.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/export-excel.service */ "./src/app/services/export-excel.service.ts");










var TagTablePage = /** @class */ (function () {
    function TagTablePage(modalController, alertController, ajaxService, platform, commonService, ete) {
        this.modalController = modalController;
        this.alertController = alertController;
        this.ajaxService = ajaxService;
        this.platform = platform;
        this.commonService = commonService;
        this.ete = ete;
        this.head = ['Tag Id', 'tagType', 'Class', 'section'];
        this.pdfdatas = [];
        this.exportTitle = "Tag-details report";
    }
    TagTablePage.prototype.refresher = function () {
        this.commonService.presentToast("please wait");
        this.refreshData = "refresh";
        this.getDatas();
    };
    TagTablePage.prototype.deletebtn = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert_1;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.selectedRow) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.alertController.create({
                                header: 'Delete ',
                                backdropDismiss: false,
                                message: "Are you sure you want to delete?",
                                buttons: [{
                                        text: 'Cancel',
                                        role: 'cancel',
                                        handler: function (data) {
                                        }
                                    },
                                    {
                                        text: 'Ok',
                                        handler: function (data) {
                                            var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/tag/deletetag?tagId=' + _this.selectedRow.tagId;
                                            _this.ajaxService.ajaxDeleteWithString(url).subscribe(function (res) {
                                                if (res.statusText == "OK") {
                                                    _this.commonService.presentToast('Deleted Successfully');
                                                    _this.myGrid.clearselection();
                                                    _this.getDatas();
                                                }
                                            });
                                        }
                                    }]
                            })];
                    case 1:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        this.commonService.presentToast('Please select a row to delete');
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    TagTablePage.prototype.btnOnClick = function () {
        var gridContent = this.myGrid.exportdata('html');
        var newWindow = window.open('', '', 'width=800, height=500'), document = newWindow.document.open(), pageContent = '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '<meta charset="utf-8" />\n' +
            '<title>Parent Details</title>\n' +
            '</head>\n' +
            '<body>\n' + gridContent + '\n</body>\n</html>';
        document.write(pageContent);
        document.close();
        newWindow.print();
    };
    ;
    TagTablePage.prototype.openparentDetailModel = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _tag_additional_tag_additional_component__WEBPACK_IMPORTED_MODULE_4__["TagAdditionalComponent"],
                            cssClass: 'my-custome-css'
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function () {
                            if (_this.myPlatform == "desktop") {
                                _this.myGrid.clearselection();
                            }
                            _this.getDatas();
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TagTablePage.prototype.parentEditModel = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.selectedRow) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.modalController.create({
                                component: _tag_additional_tag_additional_component__WEBPACK_IMPORTED_MODULE_4__["TagAdditionalComponent"],
                                cssClass: 'my-custome-css',
                                componentProps: {
                                    value: this.selectedRow
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function () {
                            if (_this.myPlatform == "desktop") {
                                _this.myGrid.clearselection();
                            }
                            _this.getDatas();
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        this.commonService.presentToast('Please select a row to edit');
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    TagTablePage.prototype.myGridOnRowSelect = function (event) {
        this.selectedRow = event.args.row;
    };
    TagTablePage.prototype.createPdf = function () {
        this.commonService.createPdf(this.head, this.pdfdatas, this.exportTitle, this.myPlatform, 'Tag-details report');
    };
    TagTablePage.prototype.exportToExcel = function () {
        var reportData = {
            title: 'Tag-details report',
            data: this.pdfdatas,
            headers: this.head
        };
        this.ete.exportExcel(reportData);
    };
    TagTablePage.prototype.getDatas = function () {
        var _this = this;
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/tag/getTag?schoolId=' + localStorage.getItem('corpId') + '&branchId=' + localStorage.getItem('corpId');
        this.ajaxService.ajaxGet(url).subscribe(function (res) {
            var detail = res;
            _this.pdfdatas = [];
            for (var i = 0; i < detail.length; i++) {
                _this.pdfdatas.push([detail[i].tagId, detail[i].tagType, detail[i].classId, detail[i].sectionId]);
            }
            _this.renderer = function (row, column, value) {
                if (value == "" || null || undefined) {
                    return "----";
                }
                else {
                    return '<span  style="line-height:32px;font-size:11px;color:darkblue;margin:auto"  >' + value + '</span>';
                }
            };
            _this.source =
                {
                    localdata: res,
                };
            _this.dataAdapter = new jqx.dataAdapter(_this.source);
            _this.columns = [
                { text: 'Tag Id', datafield: 'tagId', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                { text: 'Tag Type', datafield: 'tagType', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                // { text: 'Associated Student', datafield: 'associateStu',cellsrenderer:this.renderer,cellsalign: 'center', align: 'center'},
                { text: 'Class', datafield: 'classId', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                { text: 'Section', datafield: 'sectionId', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' }
            ];
        });
        this.myGrid.updatebounddata();
        this.myGrid.unselectrow;
        // if(this.refreshData == "refresh"){
        //   this.commonService.presentToast("refreshed Successfully");
        //   this.refreshData=''
        // }
    };
    TagTablePage.prototype.ngAfterViewInit = function () {
        if (this.myPlatform == 'desktop') {
            this.myGrid.showloadelement();
        }
        this.getDatas();
    };
    TagTablePage.prototype.ngOnInit = function () {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
    };
    TagTablePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
        { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
        { type: src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myGrid', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_2__["jqxGridComponent"])
    ], TagTablePage.prototype, "myGrid", void 0);
    TagTablePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tag-table',
            template: __webpack_require__(/*! raw-loader!./tag-table.page.html */ "./node_modules/raw-loader/index.js!./src/app/skt/tag/tag-table/tag-table.page.html"),
            styles: [__webpack_require__(/*! ./tag-table.page.scss */ "./src/app/skt/tag/tag-table/tag-table.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
            src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"]])
    ], TagTablePage);
    return TagTablePage;
}());



/***/ })

}]);
//# sourceMappingURL=skt-tag-tag-table-tag-table-module-es5.js.map