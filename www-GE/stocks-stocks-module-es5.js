(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["stocks-stocks-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/stocks/companylist/companylist.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/stocks/companylist/companylist.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-header >\n  <ion-toolbar mode=\"md\"    class=\"dealerHeader\" >\n    <ion-buttons slot=\"start\" (click)='closeModal()' >\n      <ion-icon name=\"arrow-back\"></ion-icon>\n    </ion-buttons>\n     <ion-title style=\"bottom: 16px;\n     position: fixed;\">Select company</ion-title>\n  \n   \n      </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-searchbar [(ngModel)]=\"terms\" placeholder=\"Search\">\n  </ion-searchbar>\n  \n  <ion-virtual-scroll [items]=\"companyList  | searchDelar:terms\"  approxItemHeight=\"320px\">\n    <ion-list  *virtualItem=\"let companyList;let i = index\"  [id]='companyList' style= 'padding:0px;' >\n     <ion-item (click)=\"updateAnswer($event,i, companyList.id, companyList)\">\n          <ion-grid style='padding:0px;margin: 0px;'>\n            <ion-row style='padding:0px;margin: 0px;'>\n              <ion-col size='8'  >{{companyList.name}}</ion-col>\n              <ion-col size='3'></ion-col>\n           </ion-row>\n          </ion-grid>\n               \n       \n        </ion-item>\n    </ion-list>\n \n  </ion-virtual-scroll>\n  <!-- <ion-row class=\"ion-text-center\"> \n  <ion-col size=\"12\" style=\"position: fixed;\n  bottom: 0px;\"><ion-button (click)=\"assign()\" >Submit</ion-button></ion-col>\n </ion-row> -->\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/stocks/stocks.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/stocks/stocks.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-header>\n  <ion-toolbar mode=\"md\" class=\"dealerHeader\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n     <ion-title style=\"bottom: 16px;\n     position: fixed;\">Stocks</ion-title>\n  <!-- <ion-button  shape=\"round\" class=\"dealerHeader\" style=\"margin-left: 50%;\" (click)=\"openModel()\">\n       Assign  \n    \n      </ion-button> -->\n      </ion-toolbar>\n      <ion-row class=\"upperRow\">\n        <ion-col (click)=\"typeSelector('Assign')\" [ngClass]=\"{'selectedUpperCol': type =='Assign', 'upperCol': type !='Assign'}\" size=3.5>\n          <ion-label>Assign</ion-label>\n        </ion-col>\n        <ion-col (click)=\"typeSelector('Re-assign')\" [ngClass]=\"{'selectedUpperCol': type !='Assign', 'upperCol': type =='Assign'}\" size=3.5>\n          <ion-label>Re-assign</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-searchbar [(ngModel)]=\"terms\" placeholder=\"Search\">\n      </ion-searchbar>\n</ion-header>\n<ion-content>\n  \n  <!-- <ion-virtual-scroll [items]=\"showList  | searchDelar:terms\"  approxItemHeight=\"320px\">\n    <ion-list  *virtualItem=\"let showList;let i = index\"  [id]='showList.imei' style= 'padding:0px;' >\n     <ion-item>\n          <ion-grid style='padding:0px;margin: 0px;'>\n            <ion-row style='padding:0px;margin: 0px;'>\n            \n              <ion-col size='8'  >{{showList.imei}}</ion-col>\n              <ion-col size='3'></ion-col>\n            \n           </ion-row>\n          </ion-grid>\n          <ion-checkbox class=\"checkbox\" #checkboxes (ionChange)=\"updateAnswer($event,i, showList.imei, showList)\" slot=\"end\"></ion-checkbox>\n    </ion-item>\n    </ion-list>\n  </ion-virtual-scroll> -->\n  <ion-list style=\"padding: 0px;\" *ngFor=\"let showList of showList | searchDelar:terms\">\n    <ion-item style=\"padding: 0px;\">\n      <ion-grid style='padding:0px;margin: 0px;'>\n        <ion-row style='padding:0px;margin: 0px;'>\n          <ion-col style=\"padding: 0px;\" size='12'>{{showList.imei}}</ion-col>\n        </ion-row>\n      </ion-grid>\n      <ion-checkbox class=\"checkbox\" #checkboxes (ionChange)=\"updateAnswer($event,i, showList.imei, showList)\"\n        slot=\"end\"></ion-checkbox>\n    </ion-item>\n  </ion-list>\n  <!-- <ion-row class=\"ion-text-center\"> \n    \n  <ion-col size=\"12\" style=\"position: fixed;\n   bottom: 0px;\"><ion-button (click)=\"assign()\" >Submit</ion-button></ion-col>\n  </ion-row> -->\n  <ion-fab *ngIf=\"selectedArray.length != 0\" vertical=\"bottom\" horizontal=\"center\" slot=\"fixed\">\n    <ion-fab-button style=\"--background: #0000000;\n    --box-shadow: #0000000; width: 100px; height: 100px;\">\n      <ion-icon (click)=\"openModel()\" style=\"font-size: 100px;     color: #7c68f8;\" [name]=\"icon\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/delar-application/stocks/companylist/companylist.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/delar-application/stocks/companylist/companylist.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".checkbox {\n  --background-checked: #7c68f8;\n  --border-color-checked: #7d7d7d;\n  --checkmark-color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc3RvY2tzL2NvbXBhbnlsaXN0L2NvbXBhbnlsaXN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9kZWxhci1hcHBsaWNhdGlvbi9zdG9ja3MvY29tcGFueWxpc3QvY29tcGFueWxpc3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw2QkFBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL3N0b2Nrcy9jb21wYW55bGlzdC9jb21wYW55bGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jaGVja2JveHtcbiAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogIzdjNjhmODtcbiAgICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiAjN2Q3ZDdkO1xuICAgIC0tY2hlY2ttYXJrLWNvbG9yOiB3aGl0ZTtcbn0iLCIuY2hlY2tib3gge1xuICAtLWJhY2tncm91bmQtY2hlY2tlZDogIzdjNjhmODtcbiAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogIzdkN2Q3ZDtcbiAgLS1jaGVja21hcmstY29sb3I6IHdoaXRlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/delar-application/stocks/companylist/companylist.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/delar-application/stocks/companylist/companylist.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CompanylistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanylistComponent", function() { return CompanylistComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");







var CompanylistComponent = /** @class */ (function () {
    function CompanylistComponent(modalController, ajaxService, router, commonService, alertController) {
        this.modalController = modalController;
        this.ajaxService = ajaxService;
        this.router = router;
        this.commonService = commonService;
        this.alertController = alertController;
        this.selectedArray = [];
        this.answer = [];
        this.assignedImei = [];
        this.showImeiList = [];
        this.companyID = '';
        this.show = false;
    }
    CompanylistComponent.prototype.closeModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.modalController.dismiss();
                return [2 /*return*/];
            });
        });
    };
    CompanylistComponent.prototype.removeCheckedFromArray = function (checkbox) {
        return this.selectedArray.findIndex(function (category) {
            return category === checkbox;
        });
    };
    CompanylistComponent.prototype.updateAnswer = function (event, index, name, company) {
        this.companyID = name;
        this.assign(company.name);
    };
    CompanylistComponent.prototype.assign = function (name) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var text, alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        text = "Are you sure? You want to add this shock to this company " + name;
                        return [4 /*yield*/, this.alertController.create({
                                header: 'Assign Stock',
                                backdropDismiss: false,
                                message: text,
                                buttons: [{
                                        text: 'Cancel',
                                        role: 'cancel',
                                        handler: function (data) {
                                        }
                                    },
                                    {
                                        text: 'Confirm',
                                        handler: function (confirm) {
                                            var currentSuffix = localStorage.getItem('companySuffix');
                                            var data = {
                                                "fromDelar": currentSuffix, "toDelar": _this.companyID.toString() + '',
                                                "imeiNo": _this.value2
                                            };
                                            var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["serverUrl"].web + '/simcard/update/asset/manager';
                                            _this.ajaxService.ajaxPostMethod(url, data)
                                                .subscribe(function (res) {
                                                var responseData = res;
                                                if (responseData.message == "updated") {
                                                    _this.commonService.presentToast('Updated Successfully');
                                                    _this.modalController.dismiss();
                                                }
                                            });
                                        }
                                    }]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CompanylistComponent.prototype.ngOnInit = function () {
        this.companyList = this.value;
    };
    CompanylistComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_3__["AjaxService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CompanylistComponent.prototype, "value", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CompanylistComponent.prototype, "value2", void 0);
    CompanylistComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-companylist',
            template: __webpack_require__(/*! raw-loader!./companylist.component.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/stocks/companylist/companylist.component.html"),
            styles: [__webpack_require__(/*! ./companylist.component.scss */ "./src/app/delar-application/stocks/companylist/companylist.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _services_ajax_service__WEBPACK_IMPORTED_MODULE_3__["AjaxService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], CompanylistComponent);
    return CompanylistComponent;
}());



/***/ }),

/***/ "./src/app/delar-application/stocks/stocks-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/delar-application/stocks/stocks-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: StocksPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StocksPageRoutingModule", function() { return StocksPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _stocks_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./stocks.page */ "./src/app/delar-application/stocks/stocks.page.ts");




var routes = [
    {
        path: '',
        component: _stocks_page__WEBPACK_IMPORTED_MODULE_3__["StocksPage"]
    }
];
var StocksPageRoutingModule = /** @class */ (function () {
    function StocksPageRoutingModule() {
    }
    StocksPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], StocksPageRoutingModule);
    return StocksPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/delar-application/stocks/stocks.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/delar-application/stocks/stocks.module.ts ***!
  \***********************************************************/
/*! exports provided: StocksPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StocksPageModule", function() { return StocksPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _companylist_companylist_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./companylist/companylist.component */ "./src/app/delar-application/stocks/companylist/companylist.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _stocks_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./stocks-routing.module */ "./src/app/delar-application/stocks/stocks-routing.module.ts");
/* harmony import */ var _services_search_delar_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/search-delar.pipe */ "./src/app/services/search-delar.pipe.ts");
/* harmony import */ var _stocks_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./stocks.page */ "./src/app/delar-application/stocks/stocks.page.ts");









var StocksPageModule = /** @class */ (function () {
    function StocksPageModule() {
    }
    StocksPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _stocks_routing_module__WEBPACK_IMPORTED_MODULE_6__["StocksPageRoutingModule"]
            ],
            declarations: [
                _stocks_page__WEBPACK_IMPORTED_MODULE_8__["StocksPage"],
                _services_search_delar_pipe__WEBPACK_IMPORTED_MODULE_7__["SearchDelarPipe"],
                _companylist_companylist_component__WEBPACK_IMPORTED_MODULE_4__["CompanylistComponent"]
            ],
            entryComponents: [_companylist_companylist_component__WEBPACK_IMPORTED_MODULE_4__["CompanylistComponent"]]
        })
    ], StocksPageModule);
    return StocksPageModule;
}());



/***/ }),

/***/ "./src/app/delar-application/stocks/stocks.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/delar-application/stocks/stocks.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".upperCol {\n  text-align: center;\n  border-radius: 8px;\n  margin: 2px;\n  color: black;\n  font-family: sans-serif;\n  font-size: 13px;\n  text-transform: uppercase;\n}\n\n.upperRow {\n  background: #eeeeee;\n  padding: 4px;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.selectedUpperCol {\n  background: #ffffff;\n  text-align: center;\n  border-radius: 8px;\n  margin: 2px;\n  color: black;\n  font-family: sans-serif;\n  font-size: 13px;\n  text-transform: uppercase;\n  box-shadow: 0.2px 0.2px #888888;\n}\n\n.checkbox {\n  --background-checked: #7c68f8;\n  --border-color-checked: #7d7d7d;\n  --checkmark-color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vc3RvY2tzL3N0b2Nrcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL3N0b2Nrcy9zdG9ja3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7QUNDSjs7QURDQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSwrQkFBQTtBQ0VKOztBRENBO0VBQ0ksNkJBQUE7RUFDQSwrQkFBQTtFQUNBLHdCQUFBO0FDRUoiLCJmaWxlIjoic3JjL2FwcC9kZWxhci1hcHBsaWNhdGlvbi9zdG9ja3Mvc3RvY2tzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51cHBlckNvbHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIG1hcmdpbjogMnB4O1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLnVwcGVyUm93e1xuICAgIGJhY2tncm91bmQ6ICNlZWVlZWU7XG4gICAgcGFkZGluZzogNHB4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnNlbGVjdGVkVXBwZXJDb2x7XG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIG1hcmdpbjogMnB4O1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBib3gtc2hhZG93OiAwLjJweCAwLjJweCM4ODg4ODg7XG59XG5cbi5jaGVja2JveHtcbiAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogIzdjNjhmODtcbiAgICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiAjN2Q3ZDdkO1xuICAgIC0tY2hlY2ttYXJrLWNvbG9yOiB3aGl0ZTtcbn0iLCIudXBwZXJDb2wge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgbWFyZ2luOiAycHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLnVwcGVyUm93IHtcbiAgYmFja2dyb3VuZDogI2VlZWVlZTtcbiAgcGFkZGluZzogNHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLnNlbGVjdGVkVXBwZXJDb2wge1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgbWFyZ2luOiAycHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgYm94LXNoYWRvdzogMC4ycHggMC4ycHggIzg4ODg4ODtcbn1cblxuLmNoZWNrYm94IHtcbiAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICM3YzY4Zjg7XG4gIC0tYm9yZGVyLWNvbG9yLWNoZWNrZWQ6ICM3ZDdkN2Q7XG4gIC0tY2hlY2ttYXJrLWNvbG9yOiB3aGl0ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/stocks/stocks.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/delar-application/stocks/stocks.page.ts ***!
  \*********************************************************/
/*! exports provided: StocksPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StocksPage", function() { return StocksPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _companylist_companylist_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./companylist/companylist.component */ "./src/app/delar-application/stocks/companylist/companylist.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






// import { ANY_STATE } from '@angular/animation/src/dsl/animation_transition_expr';


var StocksPage = /** @class */ (function () {
    function StocksPage(modalController, ajaxService, router, commonService, alertController) {
        this.modalController = modalController;
        this.ajaxService = ajaxService;
        this.router = router;
        this.commonService = commonService;
        this.alertController = alertController;
        this.answer = [];
        this.assignedImei = [];
        this.showImeiList = [];
        this.companyID = '';
        this.selectedArray = [];
        this.type = "Assign";
        this.icon = "cloud-upload";
    }
    StocksPage.prototype.updateAnswer = function (event, index, imei, imeiDetail) {
        if (event.currentTarget.checked == true) {
            if (this.type == "Assign")
                this.selectedArray.push(imei);
            else
                this.selectedArray.push(imeiDetail);
        }
        else {
            var index_1;
            if (this.type == "Assign")
                index_1 = this.removeCheckedFromArray(imei);
            else
                index_1 = this.removeCheckedFromArray(imeiDetail.imei);
            this.selectedArray.splice(index_1, 1);
        }
        console.log(this.selectedArray);
    };
    StocksPage.prototype.openModel = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal, text, alert_1;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.type == "Assign")) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.modalController.create({
                                component: _companylist_companylist_component__WEBPACK_IMPORTED_MODULE_6__["CompanylistComponent"],
                                componentProps: {
                                    value: this.companyList,
                                    value2: this.selectedArray
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function () {
                            _this.selectedArray = [];
                            _this.typeSelector(_this.type);
                            _this.uncheckAll();
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        text = "Are you sure? You want to re-assign this shock to your company ";
                        return [4 /*yield*/, this.alertController.create({
                                header: 'Assign Stock',
                                backdropDismiss: false,
                                message: text,
                                buttons: [{
                                        text: 'Cancel',
                                        role: 'cancel',
                                        handler: function (data) {
                                        }
                                    },
                                    {
                                        text: 'Confirm',
                                        handler: function (confirm) {
                                            for (var i = 0; i < _this.selectedArray.length; i++) {
                                                var currentSuffix = localStorage.getItem('companySuffix');
                                                var data = {
                                                    "fromDelar": _this.selectedArray[i].currentAgency,
                                                    "toDelar": _this.selectedArray[i].previousAgency,
                                                    "imeiNo": [_this.selectedArray[i].imei]
                                                };
                                                var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/simcard/update/asset/manager';
                                                _this.ajaxService.ajaxPostMethod(url, data)
                                                    .subscribe(function (res) {
                                                    var responseData = res;
                                                    if (responseData.message == "updated") {
                                                        _this.typeSelector(_this.type);
                                                        _this.selectedArray = [];
                                                        _this.uncheckAll();
                                                        _this.commonService.presentToast('Updated Successfully');
                                                    }
                                                    else if (responseData.message == "Error") {
                                                        _this.commonService.presentToast("Reassign error");
                                                    }
                                                });
                                            }
                                        }
                                    }]
                            })];
                    case 4:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    StocksPage.prototype.removeCheckedFromArray = function (checkbox) {
        return this.selectedArray.findIndex(function (category) {
            return category === checkbox;
        });
    };
    //Empties array with checkedboxes
    StocksPage.prototype.emptyCheckedArray = function () {
        this.selectedArray = [];
    };
    StocksPage.prototype.getCheckedBoxes = function () {
        console.log(this.selectedArray);
    };
    StocksPage.prototype.assign = function () {
        var _this = this;
        if (this.companyID != "" && this.selectedArray.length > 0) {
            var currentSuffix = localStorage.getItem('companySuffix');
            var data = {
                "formDelar": currentSuffix, "toDelar": this.companyID + '',
                "imeiNo": this.selectedArray
            };
            var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/simcard/update/asset/manager';
            this.ajaxService.ajaxPostMethod(url, data)
                .subscribe(function (res) {
                var responseData = JSON.parse(res);
                if (responseData.message == "updated") {
                    _this.router.navigateByUrl('/dashboard');
                    _this.commonService.presentToast('Updated Successfully');
                }
            });
        }
        else {
            this.commonService.presentToast('Please Select the Company');
        }
    };
    StocksPage.prototype.getImeiList = function () {
        // const companySuffix = {suffix:''};
        // companySuffix.suffix = localStorage.getItem('companySuffix');
        // const url = serverUrl.web + '/api/vts/superadmin/device/' + JSON.stringify(companySuffix);
        // this.ajaxService.ajaxGet(url)
        // .subscribe(res => {
        //   this.showList = res;
        //   for(var i=0;i<this.showList.length;i++){
        //     this.showImeiList.push(this.showList[i].imei)
        //   }
        // });
        this.showList = JSON.parse(localStorage.dealerLoginData).assets["Stocks"];
    };
    StocksPage.prototype.getDelar = function () {
        var _this = this;
        var suffix = localStorage.getItem('companySuffix');
        var companyUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/global/getdealerlist?suffix=' + suffix;
        this.ajaxService.ajaxGet(companyUrl)
            .subscribe(function (res) {
            _this.companyList = res;
        });
    };
    StocksPage.prototype.ionViewWillEnter = function () {
        this.showList = [];
        this.companyList;
        this.companyID = '';
        this.selectedArray = [];
        this.getImeiList();
        this.getDelar();
        this.typeSelector(this.type);
    };
    StocksPage.prototype.typeSelector = function (data) {
        var _this = this;
        this.uncheckAll();
        this.type = data;
        this.icon = "cloud-upload";
        var companySuffix = { suffix: '', mode: "stock" };
        if (this.type != "Assign") {
            companySuffix.mode = "revoke";
            this.icon = "cloud-download";
        }
        companySuffix.suffix = localStorage.getItem('companySuffix');
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/global/getImeiNoDetails';
        this.ajaxService.ajaxPostWithBody(url, companySuffix)
            .subscribe(function (res) {
            _this.showList = res;
        });
    };
    StocksPage.prototype.uncheckAll = function () {
        for (var i = 0; i < this.checkboxes.length; i++) {
            this.checkboxes["_results"][i].checked = false;
        }
    };
    StocksPage.prototype.ngOnInit = function () {
    };
    StocksPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"])("checkboxes"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], StocksPage.prototype, "checkboxes", void 0);
    StocksPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-stocks',
            template: __webpack_require__(/*! raw-loader!./stocks.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/stocks/stocks.page.html"),
            styles: [__webpack_require__(/*! ./stocks.page.scss */ "./src/app/delar-application/stocks/stocks.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"],
            _services_ajax_service__WEBPACK_IMPORTED_MODULE_2__["AjaxService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]])
    ], StocksPage);
    return StocksPage;
}());



/***/ }),

/***/ "./src/app/services/search-delar.pipe.ts":
/*!***********************************************!*\
  !*** ./src/app/services/search-delar.pipe.ts ***!
  \***********************************************/
/*! exports provided: SearchDelarPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchDelarPipe", function() { return SearchDelarPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SearchDelarPipe = /** @class */ (function () {
    function SearchDelarPipe() {
    }
    SearchDelarPipe.prototype.transform = function (items, terms) {
        if (!items)
            return [];
        if (!terms)
            return items;
        terms = terms.toLowerCase();
        return items.filter(function (it) {
            if (it.imei != null) {
                return it.imei.replace(/ /g, '').toLowerCase().includes(terms.replace(/ /g, ''));
            }
            else if (it.name) {
                return it.name.toLowerCase().includes(terms);
            }
            else
                return false;
        });
    };
    SearchDelarPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'searchDelar'
        })
    ], SearchDelarPipe);
    return SearchDelarPipe;
}());



/***/ })

}]);
//# sourceMappingURL=stocks-stocks-module-es5.js.map