(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["skt-parent-parent-table-parent-table-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/skt/parent/parent-table/parent-table.page.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/skt/parent/parent-table/parent-table.page.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <div class=\"parent-wrapper\">\n     <ion-row  class=\"header-section\">\n      <ion-col size=\"2\" size-sm=\"2\" size-md=\"2\" size-lg=\"0\"> \n        <ion-menu-button></ion-menu-button>\n      </ion-col>  \n      <ion-col size=\"10\" size-sm=\"10\" size-md=\"10\" size-lg=\"12\" id=\"btn-wrapper\">\n        Parent Details\n      </ion-col>\n    </ion-row>\n   <ion-row>\n     <ion-col>\n       <ion-button color='primary' class=\"parent-btn\" (click)='openparentDetailModel()'>Add</ion-button>\n       <ion-button color='primary' class=\"parent-btn\" (click)='parentEditModel()'>Edit</ion-button>\n       <ion-button color='primary' class=\"parent-btn\"(click)='deletebtn()' >Delete</ion-button>\n     </ion-col>\n    </ion-row> \n  </div> \n  <div id=\"export-wrapper\" >\n      <!-- <ion-img src=\"assets/student_Details/print.svg\"  class=\"toolbar-row\" (click)=\"btnOnClick()\"></ion-img> -->\n      <ion-img src=\"assets/student_Details/pdf.svg\" class=\"toolbar-row\" (click)=\"createPdf()\"></ion-img>\n      <ion-img src=\"assets/student_Details/excel.svg\" class=\"toolbar-row\"  (click)=\"exportToExcel()\"> </ion-img>\n        <!-- <ion-img src=\"assets/student_Details/refresher.svg\" class=\"toolbar-row\"></ion-img> -->\n    </div> \n</ion-toolbar>\n</ion-header>\n<ion-content>\n <div class=\"grid_Container\">\n  <jqxGrid\n  (onRowselect)=\"myGridOnRowSelect($event)\"\n  [pageable]=\"true\"\n  [selectionmode]=\"'singlerow'\"\n  [showfilterrow]=\"true\"\n  [filterable]=\"true\"\n  [sortable]=\"true\"\n  [width]=\"'100%'\"\n  [source]=\"dataAdapter\" \n  [columns]=\"columns\" \n  [autoheight]=\"true\"\n  [theme]=\"'material'\"\n  #myGrid \n  style=\"font-size:16px;text-align: center !important;\">\n  </jqxGrid>\n</div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/skt/parent/parent-table/parent-table.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/skt/parent/parent-table/parent-table.module.ts ***!
  \****************************************************************/
/*! exports provided: ParentTablePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParentTablePageModule", function() { return ParentTablePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _parent_table_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./parent-table.page */ "./src/app/skt/parent/parent-table/parent-table.page.ts");
/* harmony import */ var _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../sktcomponents.module */ "./src/app/skt/sktcomponents.module.ts");
/* harmony import */ var src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared-mod/shared-mod.module */ "./src/app/shared-mod/shared-mod.module.ts");









var routes = [
    {
        path: '',
        component: _parent_table_page__WEBPACK_IMPORTED_MODULE_6__["ParentTablePage"]
    }
];
var ParentTablePageModule = /** @class */ (function () {
    function ParentTablePageModule() {
    }
    ParentTablePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__["SktComponentsModule"],
                src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__["SharedModModule"]
            ],
            declarations: [_parent_table_page__WEBPACK_IMPORTED_MODULE_6__["ParentTablePage"]]
        })
    ], ParentTablePageModule);
    return ParentTablePageModule;
}());



/***/ }),

/***/ "./src/app/skt/parent/parent-table/parent-table.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/skt/parent/parent-table/parent-table.page.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".parent-wrapper, .grid_Container {\n  border: 1px solid #c7c3c3;\n  margin: 5px 10px 0;\n}\n\n.grid_Container {\n  margin: 0px 10px 0px;\n}\n\n.header-section {\n  background-color: #e8e8e8;\n  border: 1px solid #cac2c2;\n}\n\n#export-wrapper {\n  text-align: right;\n  background-color: #e8e8e8;\n  height: 42px;\n  border-top: 1px solid gainsboro;\n  margin: 0px 10px 0px;\n  border-left: 2px solid #c7c7c7;\n}\n\n#btn-wrapper {\n  text-align: center;\n  background-color: #e8e8e8;\n  height: 40px;\n  padding: 0px;\n  font-size: 20px;\n  border-bottom: 1px solid #cac2c2;\n  font-weight: 700;\n}\n\n.toolbar-row {\n  height: 40px;\n  width: 40px;\n  display: inline-block;\n  margin: 0px;\n  border: 1px solid #b9b7b7;\n  padding: 5px;\n}\n\n.toolbar-row:hover {\n  background-color: whitesmoke;\n}\n\n.parent-btn {\n  width: 80px;\n  height: 30px;\n  margin: 5px 10px;\n  font-size: 12px;\n}\n\n.mytable:hover {\n  background-color: blue;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvc2t0L3BhcmVudC9wYXJlbnQtdGFibGUvcGFyZW50LXRhYmxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2t0L3BhcmVudC9wYXJlbnQtdGFibGUvcGFyZW50LXRhYmxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDQztFQUNHLG9CQUFBO0FDRUo7O0FEQUM7RUFDQyx5QkFBQTtFQUNBLHlCQUFBO0FDR0Y7O0FEREM7RUFDRSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0VBQ0Esb0JBQUE7RUFDQSw4QkFBQTtBQ0lIOztBREZDO0VBQ0Usa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7QUNLSDs7QURIQztFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDTUg7O0FESkE7RUFDSSw0QkFBQTtBQ09KOztBRExDO0VBQ0csV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNRSjs7QUROQztFQUNFLHNCQUFBO0FDU0giLCJmaWxlIjoic3JjL2FwcC9za3QvcGFyZW50L3BhcmVudC10YWJsZS9wYXJlbnQtdGFibGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBhcmVudC13cmFwcGVyLC5ncmlkX0NvbnRhaW5lcntcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigxOTksIDE5NSwgMTk1KTtcclxuICAgIG1hcmdpbjogNXB4IDEwcHggMDtcclxuIH1cclxuIC5ncmlkX0NvbnRhaW5lcntcclxuICAgIG1hcmdpbjogMHB4IDEwcHggMHB4OyAgXHJcbiB9XHJcbiAuaGVhZGVyLXNlY3Rpb257XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjY2FjMmMyO1xyXG59XHJcbiAjZXhwb3J0LXdyYXBwZXJ7XHJcbiAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gICBoZWlnaHQ6IDQycHg7XHJcbiAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBnYWluc2Jvcm87XHJcbiAgIG1hcmdpbjogMHB4IDEwcHggMHB4O1xyXG4gICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNjN2M3Yzc7XHJcbiB9XHJcbiAjYnRuLXdyYXBwZXJ7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gICBwYWRkaW5nOiAwcHg7XHJcbiAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgYm9yZGVyLWJvdHRvbToxcHggc29saWQgI2NhYzJjMjtcclxuICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuIH1cclxuIC50b29sYmFyLXJvd3tcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gICB3aWR0aDogNDBweDtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBtYXJnaW46MHB4O1xyXG4gICBib3JkZXI6IDFweCBzb2xpZCAjYjliN2I3O1xyXG4gICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuLnRvb2xiYXItcm93OmhvdmVye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcclxufVxyXG4gLnBhcmVudC1idG57XHJcbiAgICB3aWR0aDogODBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIG1hcmdpbjogNXB4IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiB9XHJcbiAubXl0YWJsZTpob3ZlciB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XHJcbiB9IiwiLnBhcmVudC13cmFwcGVyLCAuZ3JpZF9Db250YWluZXIge1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzdjM2MzO1xuICBtYXJnaW46IDVweCAxMHB4IDA7XG59XG5cbi5ncmlkX0NvbnRhaW5lciB7XG4gIG1hcmdpbjogMHB4IDEwcHggMHB4O1xufVxuXG4uaGVhZGVyLXNlY3Rpb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2FjMmMyO1xufVxuXG4jZXhwb3J0LXdyYXBwZXIge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcbiAgaGVpZ2h0OiA0MnB4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgZ2FpbnNib3JvO1xuICBtYXJnaW46IDBweCAxMHB4IDBweDtcbiAgYm9yZGVyLWxlZnQ6IDJweCBzb2xpZCAjYzdjN2M3O1xufVxuXG4jYnRuLXdyYXBwZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XG4gIGhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMHB4O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2FjMmMyO1xuICBmb250LXdlaWdodDogNzAwO1xufVxuXG4udG9vbGJhci1yb3cge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiA0MHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYjliN2I3O1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbi50b29sYmFyLXJvdzpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlc21va2U7XG59XG5cbi5wYXJlbnQtYnRuIHtcbiAgd2lkdGg6IDgwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgbWFyZ2luOiA1cHggMTBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ubXl0YWJsZTpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XG59Il19 */"

/***/ }),

/***/ "./src/app/skt/parent/parent-table/parent-table.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/skt/parent/parent-table/parent-table.page.ts ***!
  \**************************************************************/
/*! exports provided: ParentTablePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParentTablePage", function() { return ParentTablePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jqwidgets-ng/jqxgrid */ "./node_modules/jqwidgets-ng/fesm5/jqwidgets-ng-jqxgrid.js");
/* harmony import */ var _parent_additional_parent_additional_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../parent-additional/parent-additional.component */ "./src/app/skt/parent/parent-additional/parent-additional.component.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/export-excel.service */ "./src/app/services/export-excel.service.ts");










var ParentTablePage = /** @class */ (function () {
    function ParentTablePage(modalController, ajaxService, alertController, platform, commonService, ete) {
        this.modalController = modalController;
        this.ajaxService = ajaxService;
        this.alertController = alertController;
        this.platform = platform;
        this.commonService = commonService;
        this.ete = ete;
        this.head = ['Roll', 'Parent Name', 'Address of Student', 'Email Address', 'Contact No'];
        this.pdfdatas = [];
        this.exportTitle = "Parent-details report";
    }
    ParentTablePage.prototype.deletebtn = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert_1;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.selectedRow) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.alertController.create({
                                header: 'Delete ',
                                backdropDismiss: false,
                                message: "Are you sure you want to delete?",
                                buttons: [{
                                        text: 'Cancel',
                                        role: 'cancel',
                                        handler: function (data) {
                                        }
                                    },
                                    {
                                        text: 'Ok',
                                        handler: function (data) {
                                            var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/parent/deleteParent?companyId=' + localStorage.getItem('corpId') + '&branchId=' + localStorage.getItem('corpId') + '&emailAddress=' + _this.selectedRow.emailAddress;
                                            _this.ajaxService.ajaxDeleteWithString(url).subscribe(function (res) {
                                                if (res.message == "success") {
                                                    _this.commonService.presentToast("Deleted successfully");
                                                    _this.myGrid.clearselection();
                                                    _this.getData();
                                                }
                                            });
                                        }
                                    }]
                            })];
                    case 1:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        this.commonService.presentToast('Please select a row to delete');
                        return [2 /*return*/, ""];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ParentTablePage.prototype.btnOnClick = function () {
        var gridContent = this.myGrid.exportdata('html');
        var newWindow = window.open('', '', 'width=800, height=500'), document = newWindow.document.open(), pageContent = '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '<meta charset="utf-8" />\n' +
            '<title>Parent Details</title>\n' +
            '</head>\n' +
            '<body>\n' + gridContent + '\n</body>\n</html>';
        document.write(pageContent);
        document.close();
        newWindow.print();
    };
    ;
    ParentTablePage.prototype.openparentDetailModel = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _parent_additional_parent_additional_component__WEBPACK_IMPORTED_MODULE_4__["ParentAdditionalComponent"],
                            cssClass: 'my-parent-css'
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function () {
                            if (_this.myPlatform == "desktop") {
                                _this.myGrid.clearselection();
                            }
                            _this.getData();
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ParentTablePage.prototype.parentEditModel = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.selectedRow) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.modalController.create({
                                component: _parent_additional_parent_additional_component__WEBPACK_IMPORTED_MODULE_4__["ParentAdditionalComponent"],
                                cssClass: 'my-parent-css',
                                componentProps: {
                                    value: this.selectedRow
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function () {
                            if (_this.myPlatform == "desktop") {
                                _this.myGrid.clearselection();
                            }
                            _this.getData();
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        this.commonService.presentToast('Please select a row to edit');
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ParentTablePage.prototype.myGridOnRowSelect = function (event) {
        this.selectedRow = event.args.row;
    };
    ParentTablePage.prototype.createPdf = function () {
        this.commonService.createPdf(this.head, this.pdfdatas, this.exportTitle, this.myPlatform, 'Parent-details report');
    };
    ParentTablePage.prototype.exportToExcel = function () {
        var reportData = {
            title: 'Parent-details report',
            data: this.pdfdatas,
            headers: this.head
        };
        this.ete.exportExcel(reportData);
    };
    ParentTablePage.prototype.getData = function () {
        var _this = this;
        var data = { "emailId": "", "value": "search", "companyId": localStorage.getItem('corpId') };
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/parent/parentdetails';
        this.ajaxService.ajaxPostWithBody(url, data).subscribe(function (res) {
            var detail = res;
            _this.pdfdatas = [];
            for (var i = 0; i < detail.length; i++) {
                _this.pdfdatas.push([detail[i].role, detail[i].parentFsName, detail[i].address, detail[i].fax, detail[i].contactNo]);
            }
            _this.renderer = function (row, column, value) {
                if (value == "" || null || undefined) {
                    return "----";
                }
                else {
                    return '<span  style="line-height:32px;font-size:11px;color:darkblue;margin:auto"  >' + value + '</span>';
                }
            };
            _this.source =
                {
                    localdata: res,
                };
            _this.dataAdapter = new jqx.dataAdapter(_this.source);
            _this.columns =
                [
                    //  { text: 'User Id',  datafield: 'emailAddress',cellsrenderer:this.renderer , cellsalign: 'center', align: 'center'},
                    { text: 'Roll', datafield: 'role', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Parent Name', datafield: 'parentFsName', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Address of Student', datafield: 'address', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Email Address', datafield: 'fax', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Contact No', datafield: 'contactNo', cellsrenderer: _this.renderer, cellsalign: 'center', align: 'center' },
                ];
            _this.myGrid.updatebounddata();
            _this.myGrid.unselectrow;
        });
    };
    ParentTablePage.prototype.ngAfterViewInit = function () {
        this.getData();
        this.myGrid.showloadelement();
    };
    ParentTablePage.prototype.ngOnInit = function () {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
    };
    ParentTablePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
        { type: src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myGrid', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_3__["jqxGridComponent"])
    ], ParentTablePage.prototype, "myGrid", void 0);
    ParentTablePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-parent-table',
            template: __webpack_require__(/*! raw-loader!./parent-table.page.html */ "./node_modules/raw-loader/index.js!./src/app/skt/parent/parent-table/parent-table.page.html"),
            styles: [__webpack_require__(/*! ./parent-table.page.scss */ "./src/app/skt/parent/parent-table/parent-table.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
            src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"]])
    ], ParentTablePage);
    return ParentTablePage;
}());



/***/ })

}]);
//# sourceMappingURL=skt-parent-parent-table-parent-table-module-es5.js.map