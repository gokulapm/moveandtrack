(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-delar-add-delar-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/add-delar/add-delar.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/add-delar/add-delar.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"dealerHeader\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n     <ion-title style=\"margin-left: 1%;\">Dealer creation</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n  <ion-row class=\"form-field\">\n   <ion-col>\n      <form class='formPadding' [formGroup]=\"details\">\n      <ion-row>\n        <ion-col>\n         <div class=\"header-label\">Login details</div>\n          <ion-row>\n        <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n         <ion-input formControlName=\"dealerName\" placeholder=\"Dealer name\" class=\"form-control\" ></ion-input>\n         </ion-col>\n       <!------->\n       <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n        <ion-input formControlName=\"delaarID\" placeholder=\"Login id\" \n        ></ion-input>\n       </ion-col>\n        <!------->\n      <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n        <ion-input formControlName=\"password\" placeholder=\"Password\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <!------->\n     <div class=\"header-label\">Address</div>\n      <ion-row>\n        <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n          <ion-input formControlName=\"address\" placeholder=\"Address\" ></ion-input>\n        </ion-col>\n           <!--******************Mobile view***************----->\n          <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform !== \"desktop\"'>\n           <ion-select class=\"input select-btn\"  placeholder=\" Country\" formControlName=\"country\" (ionChange)= \"getState('state')\" [disabled]=\"imeiTrue\" >\n              <ion-select-option  *ngFor=\"let countries of country\" [value]=\"countries\">{{countries}}</ion-select-option>\n              </ion-select>\n           </ion-col>\n            <!-------> \n            <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform !== \"desktop\"'>\n             <ion-select class=\"input select-btn\" placeholder=\"State\" formControlName=\"state\" (ionChange)= \"getState('city')\" >\n                <ion-select-option  *ngFor=\"let states of state\" [value]=\"states\">{{states}}</ion-select-option>\n                </ion-select>\n            </ion-col>\n           <!------->\n           <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform !== \"desktop\"'>\n            <ion-select class=\"input select-btn\" placeholder=\"District\" formControlName=\"city\" (ionChange)= \"getState('rto')\" >\n                <ion-select-option  *ngFor=\"let city of cities\" >{{city}}</ion-select-option>\n                </ion-select>\n           </ion-col>\n           <!------->\n           <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform !== \"desktop\"'>\n             <ion-select class=\"input select-btn\" placeholder=\"Rto area\" formControlName=\"rtoArea\" >\n                <ion-select-option  *ngFor=\"let rto of rtos\" >{{rto}}</ion-select-option>\n                </ion-select>\n            </ion-col>\n          <!--******************Mobile view***************----->\n            <!--*******************Webview***************----->\n          <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform === \"desktop\"'>\n            <ion-select class=\"input select-btn\"  placeholder=\" Country\" formControlName=\"country\" (ionChange)= \"getState('state')\" [disabled]=\"imeiTrue\" interface=\"popover\">\n               <ion-select-option  *ngFor=\"let countries of country\" [value]=\"countries\">{{countries}}</ion-select-option>\n               </ion-select>\n            </ion-col>\n             <!-------> \n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform === \"desktop\"'>\n              <ion-select class=\"input select-btn\" placeholder=\"State\" formControlName=\"state\" (ionChange)= \"getState('city')\" interface=\"popover\">\n                 <ion-select-option  *ngFor=\"let states of state\" [value]=\"states\">{{states}}</ion-select-option>\n                 </ion-select>\n             </ion-col>\n            <!------->\n            <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform === \"desktop\"'>\n             <ion-select class=\"input select-btn\" placeholder=\"District\" formControlName=\"city\" (ionChange)= \"getState('rto')\" interface=\"popover\">\n                 <ion-select-option  *ngFor=\"let city of cities\" >{{city}}</ion-select-option>\n                 </ion-select>\n            </ion-col>\n            <!------->\n            <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\" *ngIf='myPlatform === \"desktop\"'>\n              <ion-select class=\"input select-btn\" placeholder=\"Rto area\" formControlName=\"rtoArea\" interface=\"popover\">\n                 <ion-select-option  *ngFor=\"let rto of rtos\" >{{rto}}</ion-select-option>\n                 </ion-select>\n             </ion-col>\n           <!--*******************Webview***************----->\n          <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n           <ion-input formControlName=\"dtcZone\" placeholder=\"Dtc zone\">\n            </ion-input>\n           </ion-col>\n             <!------->\n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\"> \n            <ion-input formControlName=\"taluk\" placeholder=\"Taluk\"></ion-input>\n           </ion-col>\n             <!------->\n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n             <ion-input formControlName=\"pin\" placeholder=\" Pin number\" type=\"number\" ></ion-input>\n            </ion-col>\n           <!------->\n           <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n            <ion-input formControlName=\"gstIn\" placeholder=\" Gstin\"></ion-input>\n           </ion-col>\n        </ion-row>\n             <!------->\n            <div class=\"header-label\">Contact details</div>\n           <ion-row>\n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n            <ion-input formControlName=\"mobile\" placeholder=\"Mobile number\" type=\"number\"></ion-input>\n           </ion-col> \n             <!------->\n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n            <ion-input formControlName=\"panNo\" placeholder=\"Pan number\"></ion-input>\n            </ion-col>\n             <!-------> \n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n            <ion-input formControlName=\"e_Mail\" placeholder=\"Email id\"></ion-input>\n            </ion-col>\n             <!-------> \n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n            <ion-input formControlName=\"adhaarNo\" placeholder=\"Aadhaar number\" type=\"number\"></ion-input>\n           </ion-col>\n             <!-------> \n             <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"4\">\n              <ion-input formControlName=\"whatApp\" placeholder=\"Whatsapp\" type=\"text\" class=\"form-control\" >\n           </ion-input>\n           </ion-col>\n           </ion-row>\n         </ion-col>\n          </ion-row>\n       </form>\n    </ion-col>\n  </ion-row>\n\n<!---submitbutton-->\n<ion-row>\n  <ion-col style=\"text-align: center\">\n    <ion-button (click)=\"onSubmit()\" id=\"submitbtn\">Submit</ion-button>\n  </ion-col>\n</ion-row>\n</ion-grid>\n</ion-content>\n\n\n"

/***/ }),

/***/ "./src/app/delar-application/add-delar/add-delar-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/delar-application/add-delar/add-delar-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: AddDelarPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDelarPageRoutingModule", function() { return AddDelarPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_delar_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-delar.page */ "./src/app/delar-application/add-delar/add-delar.page.ts");




const routes = [
    {
        path: '',
        component: _add_delar_page__WEBPACK_IMPORTED_MODULE_3__["AddDelarPage"]
    }
];
let AddDelarPageRoutingModule = class AddDelarPageRoutingModule {
};
AddDelarPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddDelarPageRoutingModule);



/***/ }),

/***/ "./src/app/delar-application/add-delar/add-delar.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/delar-application/add-delar/add-delar.module.ts ***!
  \*****************************************************************/
/*! exports provided: AddDelarPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDelarPageModule", function() { return AddDelarPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_delar_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-delar-routing.module */ "./src/app/delar-application/add-delar/add-delar-routing.module.ts");
/* harmony import */ var _add_delar_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-delar.page */ "./src/app/delar-application/add-delar/add-delar.page.ts");







let AddDelarPageModule = class AddDelarPageModule {
};
AddDelarPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _add_delar_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddDelarPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_add_delar_page__WEBPACK_IMPORTED_MODULE_6__["AddDelarPage"]]
    })
], AddDelarPageModule);



/***/ }),

/***/ "./src/app/delar-application/add-delar/add-delar.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/delar-application/add-delar/add-delar.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-label {\n  color: #6252ee;\n  font-weight: 500;\n  font-size: 20px;\n  padding: 8px 0px;\n}\n\nion-input, ion-select {\n  border: 1px solid #e5e5e5;\n  height: 42px;\n  line-height: 18px;\n  --padding-start: 15px;\n  background: #e8e8e8;\n}\n\n@media only screen and (min-width: 768px) {\n  #submitbtn {\n    width: 20%;\n  }\n}\n\n@media only screen and (max-width: 767px) {\n  .form-field {\n    zoom: 80%;\n  }\n\n  #submitbtn {\n    width: 80%;\n  }\n}\n\n#submitbtn {\n  --background: #6252ee;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vYWRkLWRlbGFyL2FkZC1kZWxhci5wYWdlLnNjc3MiLCJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL2FkZC1kZWxhci9hZGQtZGVsYXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0E7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURFQTtFQUNNO0lBQ0UsVUFBQTtFQ0NOO0FBQ0Y7O0FEQ0E7RUFDSTtJQUNBLFNBQUE7RUNDRjs7RURDRTtJQUNDLFVBQUE7RUNFSDtBQUNGOztBREFBO0VBQ0kscUJBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL2FkZC1kZWxhci9hZGQtZGVsYXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlci1sYWJlbHtcclxuICAgIGNvbG9yOiAjNjI1MmVlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIHBhZGRpbmc6IDhweCAwcHg7XHJcbn1cclxuaW9uLWlucHV0LGlvbi1zZWxlY3R7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xyXG4gICAgaGVpZ2h0OiA0MnB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZThlOGU4O1xyXG59XHJcblxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjc2OHB4KXtcclxuICAgICAgI3N1Ym1pdGJ0bntcclxuICAgICAgICB3aWR0aDoyMCU7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjc2N3B4KXtcclxuICAgIC5mb3JtLWZpZWxke1xyXG4gICAgem9vbTo4MCU7XHJcbiAgICB9XHJcbiAgICAjc3VibWl0YnRue1xyXG4gICAgIHdpZHRoOjgwJTtcclxuICAgIH1cclxufVxyXG4jc3VibWl0YnRue1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNjI1MmVlO1xyXG59XHJcbiIsIi5oZWFkZXItbGFiZWwge1xuICBjb2xvcjogIzYyNTJlZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiA4cHggMHB4O1xufVxuXG5pb24taW5wdXQsIGlvbi1zZWxlY3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTVlNWU1O1xuICBoZWlnaHQ6IDQycHg7XG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAtLXBhZGRpbmctc3RhcnQ6IDE1cHg7XG4gIGJhY2tncm91bmQ6ICNlOGU4ZTg7XG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgI3N1Ym1pdGJ0biB7XG4gICAgd2lkdGg6IDIwJTtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuZm9ybS1maWVsZCB7XG4gICAgem9vbTogODAlO1xuICB9XG5cbiAgI3N1Ym1pdGJ0biB7XG4gICAgd2lkdGg6IDgwJTtcbiAgfVxufVxuI3N1Ym1pdGJ0biB7XG4gIC0tYmFja2dyb3VuZDogIzYyNTJlZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/add-delar/add-delar.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/delar-application/add-delar/add-delar.page.ts ***!
  \***************************************************************/
/*! exports provided: AddDelarPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDelarPage", function() { return AddDelarPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _services_countries_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/countries.service */ "./src/app/services/countries.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");









let AddDelarPage = class AddDelarPage {
    constructor(formBuilder, ajaxService, countries, router, commonService, platform) {
        this.formBuilder = formBuilder;
        this.ajaxService = ajaxService;
        this.countries = countries;
        this.router = router;
        this.commonService = commonService;
        this.platform = platform;
        this.delarDetails = {};
        this.submitted = false;
        this.mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
        this.mobnumPattern2 = "^((\\+91-?)|0)?[0-9]{12}$";
        this.mobnumPattern3 = "^((\\+91-?)|0)?[0-9]{15}$";
        this.Pattern4 = "^((\\+91-?)|0)?[0-9]{6}$";
        this.expression = "^[A-Za-z][A-Za-z0-9]*$";
        this.region = { "India": "Asia/Kolkata", "Saudi Arabia": "Asia/Riyadh" };
        this.state = [];
        this.cities = [];
        this.rtos = [];
    }
    getCountries() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["serverUrl"].web + '/login/getPreferences?key=countries&companyId=' + this.companyDetail.companyID;
        this.ajaxService.ajaxGetPerference(url)
            .subscribe(res => {
            this.country = res;
            console.log(res);
        });
    }
    getCountryCode() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["serverUrl"].web + '/login/getPreferences?key=countrycode&companyId=' + this.companyDetail.companyID;
        this.ajaxService.ajaxGetPerference(url)
            .subscribe(res => {
            this.countryCode = res;
            console.log(res);
        });
    }
    getState(data) {
        if (data == 'state') {
            this.state = this.countries.region[this.details.value.country];
        }
        else if (data == 'city') {
            this.cities = this.countries.states[this.details.value.state];
            const url1 = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["serverUrl"].web + '/global/getrtozones';
            this.ajaxService.ajaxGet(url1).subscribe(res => {
                console.log(res);
                //  rto=res;
                this.rtos = res[this.details.value.state];
            });
        }
    }
    onSubmit() {
        if (this.details.value.whatApp != null && this.details.value.whatApp.length >= 10) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the WhatApp No");
            this.valid = false;
        }
        if ((this.details.value.e_Mail != null && this.details.value.e_Mail.length > 2) && (this.details.value.adhaarNo != null && JSON.stringify(this.details.value.adhaarNo).length > 2)) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the E-mail Or AdhaarNo");
            this.valid = false;
        }
        if (this.details.value.mobile != null && (JSON.stringify(this.details.value.mobile).length >= 10)) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the Mobile");
            this.valid = false;
        }
        if (this.details.value.pin != null && (JSON.stringify(this.details.value.pin)).length === 6) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the Pincode , Is should be 6 Digits");
            this.valid = false;
        }
        // if((this.details.value.district != null   && this.details.value.district.length > 2) && (this.details.value.taluk != null  &&  this.details.value.taluk.length > 2)){
        //   this.valid=true;
        // }else{
        // this.commonService.presentToast("Check the  Taluk");
        // this.valid=false;
        // }
        if (this.details.value.taluk != null && this.details.value.taluk.length > 2) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the  Taluk");
            this.valid = false;
        }
        if ((this.details.value.city != null && this.details.value.city.length > 2) && (this.details.value.country != null && this.details.value.country.length > 2)) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the District Or Country");
            this.valid = false;
        }
        if ((this.details.value.rtoArea != null && this.details.value.rtoArea.length > 2) && (this.details.value.dtcZone != null && this.details.value.dtcZone.length > 2)) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the RTO Area Or Address");
            this.valid = false;
        }
        if ((this.details.value.password != null && this.details.value.password.length >= 6) && (this.details.value.address != null && this.details.value.address.length > 2)) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the Password Or Address, Password should be Minimum 6 Digits");
            this.valid = false;
        }
        if (this.details.value.delaarID != null) {
            var my_string = this.details.value.delaarID;
            var spaceCount = (my_string.split(" ").length - 1);
            console.log(spaceCount);
            if (this.details.value.delaarID != null && this.details.value.delaarID.length < 16 && spaceCount == 0) {
                this.valid = true;
            }
            else {
                this.commonService.presentToast("Check the Dealer ID, Avoid spaces , 15 Characters only allowed");
                this.valid = false;
            }
        }
        else {
            this.commonService.presentToast("Check the Dealer ID");
        }
        if (this.details.value.dealerName != null && this.details.value.dealerName.length > 2) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the DealerName Or Dealer ID");
            this.valid = false;
        }
        if (this.details.value.state != null && this.details.value.state.length > 2) {
            this.valid = true;
        }
        else {
            this.commonService.presentToast("Check the state ");
            this.valid = false;
        }
        if (this.details.status != "INVALID" && this.details.status != "PENDING") {
            this.submitted = true;
            const region = this.region[this.details.value.country];
            const countryCode = this.countryCode[this.details.value.country];
            this.delarDetails = {
                companyId: this.details.value.delaarID,
                // DealerName: this.details.value.dealerName,
                password: this.details.value.password,
                delaarId: this.details.value.dealerName,
                Address: this.details.value.address,
                rtoArea: this.details.value.rtoArea,
                dtcZone: this.details.value.dtcZone,
                taluk: this.details.value.taluk,
                district: this.details.value.city,
                pincode: this.details.value.pin + '',
                city: this.details.value.city,
                gstin: this.details.value.gstIn,
                mobileNo: this.details.value.mobile + '',
                pancardNo: this.details.value.panNo,
                email: this.details.value.e_Mail,
                adharNo: this.details.value.adhaarNo + '',
                whatsApp: this.details.value.whatApp,
                countryCode: countryCode,
                region: region,
                createBy: localStorage.getItem('companySuffix')
            };
            console.log(this.delarDetails);
            const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["serverUrl"].web + '/global/dealer';
            this.ajaxService.ajaxPostWithBody(url, JSON.stringify(this.delarDetails))
                .subscribe(res => {
                const respData = res;
                if (respData.Message == "Dealer already exist.") {
                    this.commonService.presentToast("Dealer already exist.");
                }
                else if (respData.Message == "Dealer Added Successfully.") {
                    this.commonService.presentToast("Dealer Added Successfully.");
                    this.router.navigateByUrl('/dashboard');
                }
                else {
                    this.commonService.presentToast(respData.Message);
                }
                console.log(res);
            });
        }
    }
    //  data = this.details.value.dealerName
    // "asdad"
    // 4add-delar.page.ts:201 VALID
    // /[^a-zA-Z0-9\-\/]/.test( data)
    // false
    // data = this.details.value.dealerName
    // "asdad @@ "
    // /[^a-zA-Z0-9\-\/]/.test( data)
    // true
    ionViewWillEnter() {
        this.companyDetail = {
            companyID: localStorage.getItem('companyId'),
            userId: localStorage.getItem('userId')
        };
        this.details.reset();
        this.getCountryCode();
        this.getCountries();
        this.valid = false;
    }
    ngOnInit() {
        // const yourhandle= require('countrycitystatejson');
        // console.log(yourhandle);
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        this.details = this.formBuilder.group({
            dealerName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15)]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]],
            city: ['',],
            country: ['',],
            address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            delaarID: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            rtoArea: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            dtcZone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            // district:['',Validators.required],
            taluk: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            pin: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.Pattern4)]],
            gstIn: [''],
            // gstIn:['',[Validators.required,Validators.minLength(15),Validators.maxLength(15)]],
            mobile: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.mobnumPattern), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10)]],
            panNo: [''],
            e_Mail: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]],
            adhaarNo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.mobnumPattern2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(12)]],
            whatApp: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10)]],
            state: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
};
AddDelarPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"] },
    { type: _services_countries_service__WEBPACK_IMPORTED_MODULE_6__["CountriesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Platform"] }
];
AddDelarPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-delar',
        template: __webpack_require__(/*! raw-loader!./add-delar.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/add-delar/add-delar.page.html"),
        styles: [__webpack_require__(/*! ./add-delar.page.scss */ "./src/app/delar-application/add-delar/add-delar.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"],
        _services_countries_service__WEBPACK_IMPORTED_MODULE_6__["CountriesService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Platform"]])
], AddDelarPage);



/***/ })

}]);
//# sourceMappingURL=add-delar-add-delar-module-es2015.js.map