(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["expense-maintenance-expense-maintenance-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/expense-maintenance/expense-maintenance.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/expense-maintenance/expense-maintenance.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar mode=\"md\" [ngClass]=\"{'appHeader' : appName != 'Armoron' , 'header-background-color': appName == 'Armoron'}\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-row *ngIf=\"myPlatform != 'desktop'\">\r\n              <ion-col size=\"2\">\r\n                  <ion-menu-button></ion-menu-button>\r\n              </ion-col>\r\n             <ion-col size=\"8\">\r\n              <ion-label>Expense Maintenance</ion-label>\r\n             </ion-col>  \r\n          </ion-row>\r\n         \r\n          <ion-row *ngIf=\"myPlatform == 'desktop'\">\r\n            <ion-col size='2'>\r\n              <ion-menu-button></ion-menu-button>\r\n            </ion-col>\r\n            <ion-col size=\"9\">\r\n              <ion-label>Expense Maintenance</ion-label>\r\n            </ion-col>\r\n           </ion-row>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-toolbar>  \r\n</ion-header>\r\n<ion-content>\r\n  <app-expense-table></app-expense-table>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/expense-maintenance/expense-maintenance.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/expense-maintenance/expense-maintenance.module.ts ***!
  \*******************************************************************/
/*! exports provided: ExpenseMaintenancePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseMaintenancePageModule", function() { return ExpenseMaintenancePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _expense_maintenance_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./expense-maintenance.page */ "./src/app/expense-maintenance/expense-maintenance.page.ts");








var routes = [
    {
        path: '',
        component: _expense_maintenance_page__WEBPACK_IMPORTED_MODULE_7__["ExpenseMaintenancePage"]
    }
];
var ExpenseMaintenancePageModule = /** @class */ (function () {
    function ExpenseMaintenancePageModule() {
    }
    ExpenseMaintenancePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_expense_maintenance_page__WEBPACK_IMPORTED_MODULE_7__["ExpenseMaintenancePage"]]
        })
    ], ExpenseMaintenancePageModule);
    return ExpenseMaintenancePageModule;
}());



/***/ }),

/***/ "./src/app/expense-maintenance/expense-maintenance.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/expense-maintenance/expense-maintenance.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4cGVuc2UtbWFpbnRlbmFuY2UvZXhwZW5zZS1tYWludGVuYW5jZS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/expense-maintenance/expense-maintenance.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/expense-maintenance/expense-maintenance.page.ts ***!
  \*****************************************************************/
/*! exports provided: ExpenseMaintenancePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseMaintenancePage", function() { return ExpenseMaintenancePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




var ExpenseMaintenancePage = /** @class */ (function () {
    function ExpenseMaintenancePage(platform) {
        this.platform = platform;
        this.isDeleteShow = false;
    }
    ExpenseMaintenancePage.prototype.ngOnInit = function () {
        this.myPlatform = this.platform.platforms()[0];
        this.appName = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].appName;
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        var localMainMenu = JSON.parse(localStorage.mainMenu);
        this.isDeleteShow = localMainMenu.includes("Delete");
    };
    ExpenseMaintenancePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }
    ]; };
    ExpenseMaintenancePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-expense-maintenance',
            template: __webpack_require__(/*! raw-loader!./expense-maintenance.page.html */ "./node_modules/raw-loader/index.js!./src/app/expense-maintenance/expense-maintenance.page.html"),
            styles: [__webpack_require__(/*! ./expense-maintenance.page.scss */ "./src/app/expense-maintenance/expense-maintenance.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]])
    ], ExpenseMaintenancePage);
    return ExpenseMaintenancePage;
}());



/***/ })

}]);
//# sourceMappingURL=expense-maintenance-expense-maintenance-module-es5.js.map