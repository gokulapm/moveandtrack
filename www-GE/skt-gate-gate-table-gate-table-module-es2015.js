(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["skt-gate-gate-table-gate-table-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/skt/gate/gate-table/gate-table.page.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/skt/gate/gate-table/gate-table.page.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <div class=\"gate-wrapper\">\n     <!-- <div id=\"btn-wrapper\" >Gate Details</div> -->\n     <ion-row  class=\"header-section\">\n      <ion-col size=\"2\" size-sm=\"2\" size-md=\"2\" size-lg=\"0\"> \n        <ion-menu-button></ion-menu-button>\n      </ion-col>  \n      <ion-col size=\"10\" size-sm=\"10\" size-md=\"10\" size-lg=\"12\" id=\"btn-wrapper\">\n        Gate Details\n      </ion-col>\n    </ion-row>  \n   <ion-row>\n     <ion-col>\n       <ion-button color='primary' class=\"parent-btn\" (click)='openparentDetailModel()'>Add</ion-button>\n       <ion-button color='primary' class=\"parent-btn\" (click)='parentEditModel()'>Edit</ion-button>\n       <ion-button color='primary' class=\"parent-btn\" (click)='deletebtn()' >Delete</ion-button>\n     </ion-col>\n    </ion-row> \n  </div> \n  <div id=\"export-wrapper\" >\n      <!-- <ion-img src=\"assets/student_Details/print.svg\"  class=\"toolbar-row\" (click)=\"btnOnClick()\"></ion-img> -->\n      <ion-img src=\"assets/student_Details/pdf.svg\" class=\"toolbar-row\" (click)=\"createPdf()\"></ion-img>\n      <ion-img src=\"assets/student_Details/excel.svg\" class=\"toolbar-row\"  (click)=\"exportToExcel()\"> </ion-img>\n        <!-- <ion-img src=\"assets/student_Details/refresher.svg\" class=\"toolbar-row\"></ion-img> -->\n    </div> \n</ion-toolbar>\n</ion-header>\n<ion-content>\n <div class=\"grid_Container\">\n  <jqxGrid\n  (onRowselect)=\"myGridOnRowSelect($event)\"\n  [pageable]=\"true\"\n  [selectionmode]=\"'singlerow'\"\n  [showfilterrow]=\"true\"\n  [filterable]=\"true\"\n  [sortable]=\"true\"\n  [width]=\"'100%'\"\n  [source]=\"dataAdapter\" \n  [columns]=\"columns\" \n  [autoheight]=\"true\"\n  [theme]=\"'material'\"\n  #myGrid \n  style=\"font-size:16px;text-align: center !important;\">\n  </jqxGrid>\n</div>\n</ion-content>\n\n"

/***/ }),

/***/ "./src/app/skt/gate/gate-table/gate-table.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/skt/gate/gate-table/gate-table.module.ts ***!
  \**********************************************************/
/*! exports provided: GateTablePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GateTablePageModule", function() { return GateTablePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _gate_table_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gate-table.page */ "./src/app/skt/gate/gate-table/gate-table.page.ts");
/* harmony import */ var src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared-mod/shared-mod.module */ "./src/app/shared-mod/shared-mod.module.ts");
/* harmony import */ var _sktcomponents_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../sktcomponents.module */ "./src/app/skt/sktcomponents.module.ts");









const routes = [
    {
        path: '',
        component: _gate_table_page__WEBPACK_IMPORTED_MODULE_6__["GateTablePage"]
    }
];
let GateTablePageModule = class GateTablePageModule {
};
GateTablePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_7__["SharedModModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _sktcomponents_module__WEBPACK_IMPORTED_MODULE_8__["SktComponentsModule"]
        ],
        declarations: [_gate_table_page__WEBPACK_IMPORTED_MODULE_6__["GateTablePage"]]
    })
], GateTablePageModule);



/***/ }),

/***/ "./src/app/skt/gate/gate-table/gate-table.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/skt/gate/gate-table/gate-table.page.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".gate-wrapper, .grid_Container {\n  border: 1px solid #c7c3c3;\n  margin: 5px 10px 0;\n}\n\n.grid_Container {\n  margin: 0px 10px 0px;\n}\n\n.header-section {\n  background-color: #e8e8e8;\n  border: 1px solid #cac2c2;\n}\n\n#export-wrapper {\n  text-align: right;\n  background-color: #e8e8e8;\n  height: 42px;\n  border-top: 1px solid gainsboro;\n  margin: 0px 10px 0px;\n  border-left: 2px solid #c7c7c7;\n}\n\n#btn-wrapper {\n  text-align: center;\n  background-color: #e8e8e8;\n  height: 40px;\n  padding: 0px;\n  font-size: 20px;\n  border-bottom: 1px solid #cac2c2;\n  font-weight: 700;\n}\n\n.toolbar-row {\n  height: 40px;\n  width: 40px;\n  display: inline-block;\n  margin: 0px;\n  border: 1px solid #b9b7b7;\n  padding: 5px;\n}\n\n.toolbar-row:hover {\n  background-color: whitesmoke;\n}\n\n.parent-btn {\n  width: 80px;\n  height: 30px;\n  margin: 5px 10px;\n  font-size: 12px;\n}\n\n.mytable:hover {\n  background-color: blue;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvc2t0L2dhdGUvZ2F0ZS10YWJsZS9nYXRlLXRhYmxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2t0L2dhdGUvZ2F0ZS10YWJsZS9nYXRlLXRhYmxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDQztFQUNHLG9CQUFBO0FDRUo7O0FEQUM7RUFDRSx5QkFBQTtFQUNBLHlCQUFBO0FDR0g7O0FEREM7RUFDRyxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0VBQ0Esb0JBQUE7RUFDQSw4QkFBQTtBQ0lKOztBREZFO0VBQ0Usa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7QUNLSjs7QURIRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDTUo7O0FESkM7RUFDSSw0QkFBQTtBQ09MOztBRExDO0VBQ0csV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNRSjs7QUROQztFQUNFLHNCQUFBO0FDU0giLCJmaWxlIjoic3JjL2FwcC9za3QvZ2F0ZS9nYXRlLXRhYmxlL2dhdGUtdGFibGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmdhdGUtd3JhcHBlciwuZ3JpZF9Db250YWluZXJ7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMTk5LCAxOTUsIDE5NSk7XHJcbiAgICBtYXJnaW46IDVweCAxMHB4IDA7XHJcbiB9XHJcbiAuZ3JpZF9Db250YWluZXJ7XHJcbiAgICBtYXJnaW46IDBweCAxMHB4IDBweDsgIFxyXG4gfVxyXG4gLmhlYWRlci1zZWN0aW9ue1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gICBib3JkZXI6IDFweCBzb2xpZCAjY2FjMmMyO1xyXG4gfVxyXG4gI2V4cG9ydC13cmFwcGVye1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gICAgaGVpZ2h0OiA0MnB4O1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdhaW5zYm9ybztcclxuICAgIG1hcmdpbjogMHB4IDEwcHggMHB4O1xyXG4gICAgYm9yZGVyLWxlZnQ6IDJweCBzb2xpZCAjYzdjN2M3O1xyXG4gIH1cclxuICAjYnRuLXdyYXBwZXJ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbToxcHggc29saWQgI2NhYzJjMjtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgfVxyXG4gIC50b29sYmFyLXJvd3tcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOjBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNiOWI3Yjc7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiB9XHJcbiAudG9vbGJhci1yb3c6aG92ZXJ7XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcclxuIH1cclxuIC5wYXJlbnQtYnRue1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBtYXJnaW46IDVweCAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gfVxyXG4gLm15dGFibGU6aG92ZXIge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xyXG4gfVxyXG4gIiwiLmdhdGUtd3JhcHBlciwgLmdyaWRfQ29udGFpbmVyIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2M3YzNjMztcbiAgbWFyZ2luOiA1cHggMTBweCAwO1xufVxuXG4uZ3JpZF9Db250YWluZXIge1xuICBtYXJnaW46IDBweCAxMHB4IDBweDtcbn1cblxuLmhlYWRlci1zZWN0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NhYzJjMjtcbn1cblxuI2V4cG9ydC13cmFwcGVyIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XG4gIGhlaWdodDogNDJweDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdhaW5zYm9ybztcbiAgbWFyZ2luOiAwcHggMTBweCAwcHg7XG4gIGJvcmRlci1sZWZ0OiAycHggc29saWQgI2M3YzdjNztcbn1cblxuI2J0bi13cmFwcGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDBweDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NhYzJjMjtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLnRvb2xiYXItcm93IHtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogNDBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW46IDBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2I5YjdiNztcbiAgcGFkZGluZzogNXB4O1xufVxuXG4udG9vbGJhci1yb3c6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZXNtb2tlO1xufVxuXG4ucGFyZW50LWJ0biB7XG4gIHdpZHRoOiA4MHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIG1hcmdpbjogNXB4IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm15dGFibGU6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/skt/gate/gate-table/gate-table.page.ts":
/*!********************************************************!*\
  !*** ./src/app/skt/gate/gate-table/gate-table.page.ts ***!
  \********************************************************/
/*! exports provided: GateTablePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GateTablePage", function() { return GateTablePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jqwidgets-ng/jqxgrid */ "./node_modules/jqwidgets-ng/fesm2015/jqwidgets-ng-jqxgrid.js");
/* harmony import */ var _gate_additional_gate_additional_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../gate-additional/gate-additional.component */ "./src/app/skt/gate/gate-additional/gate-additional.component.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/export-excel.service */ "./src/app/services/export-excel.service.ts");










let GateTablePage = class GateTablePage {
    constructor(alertController, commonService, modalController, ajaxService, platform, ete) {
        this.alertController = alertController;
        this.commonService = commonService;
        this.modalController = modalController;
        this.ajaxService = ajaxService;
        this.platform = platform;
        this.ete = ete;
        this.head = ['Gate Id', 'Gate Name', 'Location', 'Description', 'Device IMEI'];
        this.pdfdatas = [];
        this.exportTitle = "Gate-details report";
    }
    deletebtn() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.selectedRow) {
                const alert = yield this.alertController.create({
                    header: 'Delete ',
                    backdropDismiss: false,
                    message: "Are you sure you want to delete?",
                    buttons: [{
                            text: 'Cancel',
                            role: 'cancel',
                            handler: data => {
                            }
                        },
                        {
                            text: 'Ok',
                            handler: data => {
                                let selectedrowindex = this.myGrid.getselectedrowindex();
                                let gateId = this.myGrid.getrows()[selectedrowindex]["gateid"];
                                const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/gate/deleteGate?gateid=' + gateId;
                                this.ajaxService.ajaxDeleteWithString(url).subscribe(res => {
                                    if (res.statusText == "OK") {
                                        this.commonService.presentToast('Deleted Successfully');
                                        this.myGrid.clearselection();
                                        this.getDatas();
                                    }
                                });
                            }
                        }]
                });
                yield alert.present();
            }
            else {
                this.commonService.presentToast('Please select a row to delete');
                return "";
            }
        });
    }
    btnOnClick() {
        let gridContent = this.myGrid.exportdata('html');
        let newWindow = window.open('', '', 'width=800, height=500'), document = newWindow.document.open(), pageContent = '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '<meta charset="utf-8" />\n' +
            '<title>Parent Details</title>\n' +
            '</head>\n' +
            '<body>\n' + gridContent + '\n</body>\n</html>';
        document.write(pageContent);
        document.close();
        newWindow.print();
    }
    ;
    openparentDetailModel() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _gate_additional_gate_additional_component__WEBPACK_IMPORTED_MODULE_4__["GateAdditionalComponent"],
                cssClass: 'gate_Details'
            });
            modal.onDidDismiss().then(() => {
                if (this.myPlatform == "desktop") {
                    this.myGrid.clearselection();
                }
                this.getDatas();
            });
            return yield modal.present();
        });
    }
    parentEditModel() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.selectedRow) {
                const modal = yield this.modalController.create({
                    component: _gate_additional_gate_additional_component__WEBPACK_IMPORTED_MODULE_4__["GateAdditionalComponent"],
                    cssClass: 'gate_Details',
                    componentProps: {
                        value: this.selectedRow
                    }
                });
                modal.onDidDismiss().then(() => {
                    if (this.myPlatform == "desktop") {
                        this.myGrid.clearselection();
                    }
                    this.getDatas();
                });
                return yield modal.present();
            }
            else {
                this.commonService.presentToast('Please select a row to edit');
            }
        });
    }
    myGridOnRowSelect(event) {
        this.selectedRow = event.args.row;
    }
    ngAfterViewInit() {
        if (this.myPlatform == 'desktop') {
            this.myGrid.showloadelement();
        }
        this.getDatas();
    }
    createPdf() {
        this.commonService.createPdf(this.head, this.pdfdatas, this.exportTitle, this.myPlatform, 'Gate-details report');
    }
    exportToExcel() {
        let reportData = {
            title: 'Gate-details report',
            data: this.pdfdatas,
            headers: this.head
        };
        this.ete.exportExcel(reportData);
    }
    getDatas() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/gate/getGate';
        this.ajaxService.ajaxGet(url).subscribe(res => {
            var detail = res;
            this.pdfdatas = [];
            for (var i = 0; i < detail.length; i++) {
                this.pdfdatas.push([detail[i].gateid, detail[i].gateName, detail[i].location, detail[i].description, detail[i].deviceimei]);
            }
            this.renderer = (row, column, value) => {
                if (value == "" || null || undefined) {
                    return "----";
                }
                else {
                    return '<span  style="line-height:32px;font-size:11px;color:darkblue;margin:auto"  >' + value + '</span>';
                }
            };
            this.source =
                {
                    localdata: res,
                };
            this.dataAdapter = new jqx.dataAdapter(this.source);
            this.columns =
                [
                    { text: 'Gate Id', datafield: 'gateid', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Gate Name', datafield: 'gateName', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Location', datafield: 'location', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Description', datafield: 'description', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Device IMEI', datafield: 'deviceimei', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                ];
        });
        this.myGrid.updatebounddata();
        this.myGrid.unselectrow;
    }
    ngOnInit() {
        // console.log(this.obj);
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        // this.getDatas();
    }
};
GateTablePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myGrid', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_3__["jqxGridComponent"])
], GateTablePage.prototype, "myGrid", void 0);
GateTablePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-gate-table',
        template: __webpack_require__(/*! raw-loader!./gate-table.page.html */ "./node_modules/raw-loader/index.js!./src/app/skt/gate/gate-table/gate-table.page.html"),
        styles: [__webpack_require__(/*! ./gate-table.page.scss */ "./src/app/skt/gate/gate-table/gate-table.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"]])
], GateTablePage);



/***/ })

}]);
//# sourceMappingURL=skt-gate-gate-table-gate-table-module-es2015.js.map