(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["parent-app-parent-tab-parent-tab-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/parent-app/parent-tab/parent-tab.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/parent-app/parent-tab/parent-tab.page.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs>\n  <ion-tab-bar slot=\"bottom\">\n    <ion-tab-button tab=\"routmap\">\n      <ion-icon name=\"analytics\"></ion-icon>\n      <ion-label>Rout map</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"studentlivetrack\">\n      <ion-icon name=\"person\"></ion-icon>\n      <ion-label>Live track</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"studentoverview\">\n      <ion-icon name=\"settings\"></ion-icon>\n      <ion-label>Overview</ion-label>\n    </ion-tab-button>\n\n  </ion-tab-bar>\n\n</ion-tabs>\n"

/***/ }),

/***/ "./src/app/parent-app/parent-tab/parent-tab.module.ts":
/*!************************************************************!*\
  !*** ./src/app/parent-app/parent-tab/parent-tab.module.ts ***!
  \************************************************************/
/*! exports provided: ParentTabPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParentTabPageModule", function() { return ParentTabPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _parent_tab_router_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./parent-tab.router.module */ "./src/app/parent-app/parent-tab/parent-tab.router.module.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _parent_tab_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./parent-tab.page */ "./src/app/parent-app/parent-tab/parent-tab.page.ts");








var routes = [
    {
        path: '',
        component: _parent_tab_page__WEBPACK_IMPORTED_MODULE_7__["ParentTabPage"]
    }
];
var ParentTabPageModule = /** @class */ (function () {
    function ParentTabPageModule() {
    }
    ParentTabPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
                _parent_tab_router_module__WEBPACK_IMPORTED_MODULE_5__["ParentTabPageRoutingModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_parent_tab_page__WEBPACK_IMPORTED_MODULE_7__["ParentTabPage"]]
        })
    ], ParentTabPageModule);
    return ParentTabPageModule;
}());



/***/ }),

/***/ "./src/app/parent-app/parent-tab/parent-tab.page.scss":
/*!************************************************************!*\
  !*** ./src/app/parent-app/parent-tab/parent-tab.page.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tab-selected {\n  color: #1aba7e;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvcGFyZW50LWFwcC9wYXJlbnQtdGFiL3BhcmVudC10YWIucGFnZS5zY3NzIiwic3JjL2FwcC9wYXJlbnQtYXBwL3BhcmVudC10YWIvcGFyZW50LXRhYi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxjQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9wYXJlbnQtYXBwL3BhcmVudC10YWIvcGFyZW50LXRhYi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi50YWItc2VsZWN0ZWR7XG4gICAgY29sb3I6ICMxYWJhN2U7XG59IiwiLnRhYi1zZWxlY3RlZCB7XG4gIGNvbG9yOiAjMWFiYTdlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/parent-app/parent-tab/parent-tab.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/parent-app/parent-tab/parent-tab.page.ts ***!
  \**********************************************************/
/*! exports provided: ParentTabPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParentTabPage", function() { return ParentTabPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");





var ParentTabPage = /** @class */ (function () {
    function ParentTabPage(activatedRoute, platform) {
        this.activatedRoute = activatedRoute;
        this.platform = platform;
    }
    ParentTabPage.prototype.tabChanged = function () {
        this.selectedTab = this.myTabs.getSelected();
    };
    ParentTabPage.prototype.ngOnInit = function () {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        this.appName = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["app"].appName;
    };
    ParentTabPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myTabs', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ParentTabPage.prototype, "myTabs", void 0);
    ParentTabPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-parent-tab',
            template: __webpack_require__(/*! raw-loader!./parent-tab.page.html */ "./node_modules/raw-loader/index.js!./src/app/parent-app/parent-tab/parent-tab.page.html"),
            styles: [__webpack_require__(/*! ./parent-tab.page.scss */ "./src/app/parent-app/parent-tab/parent-tab.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])
    ], ParentTabPage);
    return ParentTabPage;
}());



/***/ }),

/***/ "./src/app/parent-app/parent-tab/parent-tab.router.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/parent-app/parent-tab/parent-tab.router.module.ts ***!
  \*******************************************************************/
/*! exports provided: ParentTabPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParentTabPageRoutingModule", function() { return ParentTabPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _parent_tab_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./parent-tab.page */ "./src/app/parent-app/parent-tab/parent-tab.page.ts");




var routes = [
    {
        path: '',
        component: _parent_tab_page__WEBPACK_IMPORTED_MODULE_3__["ParentTabPage"],
        children: [
            {
                path: 'routmap',
                children: [
                    {
                        path: '',
                        loadChildren: function () { return __webpack_require__.e(/*! import() | rout-map-rout-map-module */ "rout-map-rout-map-module").then(__webpack_require__.bind(null, /*! ../rout-map/rout-map.module */ "./src/app/parent-app/rout-map/rout-map.module.ts")).then(function (m) { return m.RoutMapPageModule; }); }
                    }
                ]
            },
            {
                path: 'studentlivetrack',
                children: [
                    {
                        path: '',
                        loadChildren: function () { return Promise.all(/*! import() | student-livetrack-student-livetrack-module */[__webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~diagnosis-user-diagnosis-user-modu~62fede08"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~diagnosis-user-diagnosis-user-modu~29e9431a"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~diagnosis-user-diagnosis-user-modu~d5fc68e6"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~diagnosis-user-diagnosis-user-modu~b65ad506"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~dashboard-dashboard-module~diagnos~331e5709"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~diagnosis-user-diagnosis-user-modu~a8bb431b"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~device-commands-device-commands-mo~14083cef"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~device-activation-device-activatio~f604aa4d"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~diagnosis-user-diagnosis-user-modu~23aeb344"), __webpack_require__.e("student-livetrack-student-livetrack-module")]).then(__webpack_require__.bind(null, /*! ../student-livetrack/student-livetrack.module */ "./src/app/parent-app/student-livetrack/student-livetrack.module.ts")).then(function (m) { return m.StudentLivetrackPageModule; }); }
                    }
                ]
            },
            {
                path: 'studentoverview',
                children: [
                    {
                        path: '',
                        loadChildren: function () { return Promise.all(/*! import() | student-overview-student-overview-module */[__webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~diagnosis-user-diagnosis-user-modu~29e9431a"), __webpack_require__.e("default~alerts-tab-alerts-tab-module~asset-info-asset-info-module~dashboard-dashboard-module~diagnos~331e5709"), __webpack_require__.e("default~dashboard-dashboard-module~student-overview-student-overview-module"), __webpack_require__.e("student-overview-student-overview-module")]).then(__webpack_require__.bind(null, /*! ../student-overview/student-overview.module */ "./src/app/parent-app/student-overview/student-overview.module.ts")).then(function (m) { return m.StudentOverviewPageModule; }); }
                    }
                ]
            },
            {
                path: 'about',
                children: [
                    {
                        path: '',
                        loadChildren: function () { return __webpack_require__.e(/*! import() | about-about-module */ "about-about-module").then(__webpack_require__.bind(null, /*! ../about/about.module */ "./src/app/parent-app/about/about.module.ts")).then(function (m) {
                            m.AboutPageModule;
                        }); }
                    }
                ]
            },
            {
                path: 'parent-tab',
                redirectTo: '/parent-tab/routmap',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: 'parent-tab',
        redirectTo: '/parent-tab/routmap',
        pathMatch: 'full'
    }
];
var ParentTabPageRoutingModule = /** @class */ (function () {
    function ParentTabPageRoutingModule() {
    }
    ParentTabPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ParentTabPageRoutingModule);
    return ParentTabPageRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=parent-app-parent-tab-parent-tab-module-es5.js.map