(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["parent-app-student-dashboard-student-dashboard-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/parent-app/student-dashboard/student-dashboard.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/parent-app/student-dashboard/student-dashboard.page.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class='header'>\n  <ion-toolbar class=\"header-style\">\n    <ion-row style=\"align-items: center;\">\n      <ion-menu-button></ion-menu-button>\n      <ion-label> Dashboard  </ion-label>\n      <!-- <ion-button routerLink=\"/about\">About</ion-button> -->\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card (click)=\"studentDetail(data.SelectedVin)\" *ngFor=\"let data of studentsData\">\n    <ion-card-content style=\"padding: 0px;\">\n      <ion-row>\n        <ion-col size=2>\n          <ion-icon class=\"student-icon\" name=\"contact\"></ion-icon>\n        </ion-col>\n        <ion-col>\n          <ion-row class=\"name-style\">\n            {{data.studentName}}\n          </ion-row>\n          <ion-row class=\"class-sec\">\n           {{ \"Std:\"+data.std+\" || Sec:\"+data.sec+\"\"}}\n          </ion-row>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-card style=\"width: 100%; margin: 0px;\">\n          <ion-card-content>\n            <!-- <ion-row>\n              <div class=\"border-line\"></div>\n              <div size=2 class=\"circle\" [ngClass]=\"{'current': data.routesDatas[0].name }\"></div>\n              <div style=\"width: 48%;\" class=\"border-line \"></div>\n              <div style=\"left:47%\" size=2 class=\"circle\"></div>\n              <div style=\"left:91%\" size=2 class=\"circle\"></div>\n            </ion-row> -->\n            <!-- <ion-row class=\"padding-top-10 padding-bottom-10\">\n              <ion-col class=\"padding0px\">\n                {{data.routesDatas[0].name}}\n              </ion-col>\n              <ion-col class=\"padding0px text-center\">\n                {{data.routesDatas[0].name}}\n              </ion-col>\n              <ion-col class=\"padding0px text-right\">\n              {{data.routesDatas[0].name}}\n              </ion-col>\n            </ion-row> -->\n            <ion-row >\n              <!-- <ion-col class=\"padding0px\" size=12 class=\"ion-text-center\">\n                <ion-icon class=\"iconCall\" name=\"call\"></ion-icon>\n              </ion-col> -->\n              <ion-col class=\"padding0px\"  size=3.5 class=\"ion-text-right\">\n                <ion-icon   name=\"md-locate\" style=\"font-size: 24px;color: #1aba7e;\"></ion-icon>\n                </ion-col>\n                <ion-col class=\"padding0px\" size=8.5   class=\"ion-text-left\" style=\"font-size: 17px;\">\n                  {{data.routesDatas[0].name}}\n                </ion-col>\n              \n             \n            </ion-row>\n            <ion-row class=\"border\">\n              <ion-col size=3>\n                <ion-row class=\"rowCenter\">\n                  <ion-icon class=\"iconCall\" name=\"call\"></ion-icon>\n                </ion-row>\n                <ion-row class=\"rowCenter fontFamily\">\n                  {{data.driverNo}}\n                </ion-row>\n              </ion-col>\n              <ion-col size=6>\n                <ion-row class=\"rowCenter\">\n                  <svg version=\"1.0\" xmlns=\"http://www.w3.org/2000/svg\" width=\"50px\" height=\"31px\"\n                    viewBox=\"0 0 820.000000 742.000000\" preserveAspectRatio=\"xMidYMid meet\">\n                    <g transform=\"translate(0.000000,742.000000) scale(0.100000,-0.100000)\" fill=\"#1aba7e\"\n                      stroke=\"none\">\n                      <path d=\"M3055 7003 c-134 -16 -336 -78 -460 -140 -243 -122 -414 -287 -526\n                 -508 -118 -231 -133 -311 -149 -762 -5 -155 -14 -292 -19 -306 -5 -14 -21 -37\n                 -36 -51 -63 -63 -91 -192 -82 -386 12 -267 83 -448 272 -699 104 -137 116\n                 -157 160 -271 84 -212 171 -353 309 -498 352 -369 798 -469 1213 -272 291 138\n                 523 402 663 755 47 118 78 174 135 245 117 146 222 338 273 500 25 79 26 97\n                 27 275 0 221 -11 273 -72 345 -36 41 -38 49 -45 129 -3 48 -7 134 -7 192 -2\n                 220 -38 431 -96 557 -14 31 -50 89 -79 127 -66 86 -79 112 -100 194 -78 304\n                 -430 522 -925 571 -97 9 -389 12 -456 3z\" />\n                      <path d=\"M6165 3654 c-260 -28 -548 -133 -774 -282 -394 -259 -655 -656 -753\n                 -1143 -33 -165 -33 -463 0 -627 144 -715 662 -1237 1382 -1393 92 -20 133 -23\n                 340 -23 206 0 248 3 339 22 450 98 821 339 1086 707 123 170 245 452 290 670\n                 35 170 29 561 -11 720 -67 267 -181 500 -344 704 -254 318 -606 534 -1015 623\n                 -69 15 -139 20 -295 23 -113 1 -223 1 -245 -1z m-126 -414 c56 -16 108 -58\n                 127 -103 16 -37 18 -87 22 -453 l4 -412 -45 -30 c-105 -73 -177 -207 -177\n                 -332 0 -26 -16 -38 -281 -190 -388 -224 -428 -242 -504 -238 -99 6 -161 64\n                 -192 179 -24 90 -24 386 0 516 33 177 103 348 213 516 80 123 242 288 359 365\n                 139 93 352 188 425 191 8 1 30 -3 49 -9z m781 -12 c67 -20 215 -96 305 -156\n                 274 -181 453 -421 566 -757 l34 -100 0 -290 0 -290 -28 -48 c-57 -96 -158\n                 -129 -267 -88 -46 17 -263 138 -622 346 l-68 39 0 56 c-1 103 -66 224 -158\n                 292 l-53 40 3 421 c3 401 4 424 23 456 51 87 146 115 265 79z m-413 -1149 c34\n                 -13 80 -55 99 -91 18 -35 18 -101 0 -137 -21 -39 -79 -88 -114 -96 -76 -16\n                 -169 43 -193 123 -36 121 92 245 208 201z m-150 -524 l112 -29 78 23 c164 50\n                 148 49 206 16 162 -90 476 -277 561 -334 83 -55 103 -73 119 -110 49 -111 -21\n                 -237 -198 -352 -376 -247 -841 -302 -1254 -149 -226 83 -445 232 -492 335 -25\n                 54 -24 135 1 185 23 44 66 76 270 197 223 132 457 254 475 249 6 -1 60 -16\n                 122 -31z\" />\n                      <path d=\"M2175 3430 c-11 -5 -136 -57 -278 -115 -678 -277 -929 -404 -1120\n               -563 -260 -216 -484 -604 -592 -1026 -51 -201 -78 -374 -79 -513 -1 -111 1\n                 -124 20 -143 47 -48 410 -173 679 -234 387 -87 928 -162 1447 -198 244 -17\n                 1857 -17 2108 0 193 13 462 40 469 47 2 3 -23 44 -57 93 -171 252 -295 549\n                 -343 822 -25 144 -28 445 -5 623 41 319 166 601 404 910 46 59 72 101 66 107\n                 -11 11 -473 200 -489 200 -6 0 -27 -19 -45 -42 -203 -256 -430 -422 -713 -521\n                 l-92 -32 -245 0 -245 0 -92 32 c-237 83 -488 253 -631 426 -122 150 -120 148\n                 -167 127z\" />\n                    </g>\n                  </svg>\n\n                </ion-row>\n                <ion-row class=\"rowCenter fontFamily\">\n                  {{data.driverName}}\n                </ion-row>\n              </ion-col>\n              <ion-col size=3>\n                <ion-row class=\"rowCenter\">\n                  <svg width='32px' heigth='31px' version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\"\n                    xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" style=\"fill:#1aba7e\"\n                    viewBox=\"0 0 1000 1000\" enable-background=\"new 0 0 1000 1000\" xml:space=\"preserve\">\n                    <g>\n                      <path\n                        d=\"M642.1,588.3c-4.6-57.7,189.9-56.2,189.9-56.2s-151-33-221.8,25.8c-89.6,74.4,45.6,121.6,76,151.9c30.4,30.4,33.4,83.6-100.3,86.6c-133.7,3-227.9-6.1-227.9-6.1s-26.5,110.5-50.1,168.7c0,0,256.8,7.6,360.1-16.7C771.2,918,880.6,836,833.5,741.8C786.4,647.6,646.6,646.1,642.1,588.3z\" />\n                      <path\n                        d=\"M265.3,158.3C124.3,158.3,10,272.6,10,413.6c0,141,203.6,252.2,255.3,533.4c72.9-276.6,255.3-392.4,255.3-533.4S406.2,158.3,265.3,158.3z M265.3,548.8c-74.7,0-135.2-60.5-135.2-135.2s60.5-135.2,135.2-135.2c74.7,0,135.2,60.5,135.2,135.2S339.9,548.8,265.3,548.8z\" />\n                      <path\n                        d=\"M990,191c0-83.5-67.7-151.2-151.2-151.2c-83.5,0-151.2,67.7-151.2,151.2s120.6,149.4,151.2,315.9C882,343.1,990,274.5,990,191z M758.7,191c0-44.2,35.9-80.1,80.1-80.1c44.2,0,80.1,35.9,80.1,80.1s-35.9,80.1-80.1,80.1C794.6,271.1,758.7,235.2,758.7,191z\" />\n                    </g>\n                  </svg>\n                </ion-row>\n                <ion-row class=\"rowCenter fontFamily\">\n                  {{data.routeName}}\n                </ion-row>\n              </ion-col>\n              <!-- <ion-col size=12 class=\"text-center trip-detail\">\n                <ion-label> TRIP DETAIL</ion-label>\n              </ion-col>\n              <ion-col size=12>\n                <ion-row>\n                  <ion-col>Route name</ion-col>\n                  <ion-col> AH1</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>Driver name</ion-col>\n                  <ion-col> Mohamed Imran</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>Driver contact</ion-col>\n                  <ion-col> 9087657583</ion-col>\n                </ion-row>\n              </ion-col> -->\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>"

/***/ }),

/***/ "./src/app/parent-app/student-dashboard/student-dashboard.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/parent-app/student-dashboard/student-dashboard.module.ts ***!
  \**************************************************************************/
/*! exports provided: StudentDashboardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentDashboardPageModule", function() { return StudentDashboardPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _student_dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./student-dashboard.page */ "./src/app/parent-app/student-dashboard/student-dashboard.page.ts");







const routes = [
    {
        path: '',
        component: _student_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["StudentDashboardPage"]
    }
];
let StudentDashboardPageModule = class StudentDashboardPageModule {
};
StudentDashboardPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_student_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["StudentDashboardPage"]]
    })
], StudentDashboardPageModule);



/***/ }),

/***/ "./src/app/parent-app/student-dashboard/student-dashboard.page.scss":
/*!**************************************************************************!*\
  !*** ./src/app/parent-app/student-dashboard/student-dashboard.page.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".padding0px {\n  padding: 0px;\n}\n\n.text-right {\n  text-align: right;\n}\n\n.text-center {\n  text-align: center;\n}\n\n.header-style {\n  --background: #1aba7e;\n  color: white;\n}\n\n.padding-top-10 {\n  padding-top: 10px;\n}\n\n.padding-bottom-10 {\n  padding-bottom: 10px;\n}\n\n.trip-detail {\n  font-weight: bold;\n  color: black;\n}\n\n.border {\n  border-top: 2px solid #ececec;\n  padding-top: 9px;\n}\n\n.name-style {\n  font-size: 17px;\n  font-weight: bold;\n  color: black;\n  text-transform: uppercase;\n}\n\n.student-icon {\n  font-size: 46px;\n  color: #1aba7e;\n}\n\n.class-sec {\n  font-size: 11px;\n  text-transform: uppercase;\n}\n\n.circle {\n  padding: 6px;\n  position: absolute;\n  display: inline-block;\n  content: \"\";\n  top: 13px;\n  left: 10px;\n  border-radius: 50%;\n  background-color: #ffffff;\n  border: 3px solid #ececec;\n}\n\n.border-line {\n  border-bottom: 3px solid;\n  width: 50%;\n  padding: 4px;\n}\n\n.fixline {\n  height: 70px;\n  font-size: smaller;\n  color: white;\n  padding: 12px;\n  text-align: center;\n}\n\n.rowCenter {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.countSize {\n  font-size: large;\n  font-weight: bolder;\n  font-family: sans-serif;\n}\n\n.fontFamily {\n  font-family: sans-serif;\n}\n\n.iconCall {\n  font-size: 31px;\n  color: #1aba7e;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvcGFyZW50LWFwcC9zdHVkZW50LWRhc2hib2FyZC9zdHVkZW50LWRhc2hib2FyZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhcmVudC1hcHAvc3R1ZGVudC1kYXNoYm9hcmQvc3R1ZGVudC1kYXNoYm9hcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxxQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxvQkFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSw2QkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURFQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDQ0o7O0FERUE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0FDQ0o7O0FEQ0E7RUFDSSx3QkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FDRUo7O0FEQ0E7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FDRUo7O0FEQ0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDRUo7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNFSjs7QURFQTtFQUNJLHVCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ0VKIiwiZmlsZSI6InNyYy9hcHAvcGFyZW50LWFwcC9zdHVkZW50LWRhc2hib2FyZC9zdHVkZW50LWRhc2hib2FyZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFkZGluZzBweCB7XG4gICAgcGFkZGluZzogMHB4O1xufVxuXG4udGV4dC1yaWdodCB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi50ZXh0LWNlbnRlciB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaGVhZGVyLXN0eWxlIHtcbiAgICAtLWJhY2tncm91bmQ6ICMxYWJhN2U7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4ucGFkZGluZy10b3AtMTAge1xuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuXG4ucGFkZGluZy1ib3R0b20tMTAge1xuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4udHJpcC1kZXRhaWwge1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiBibGFjaztcbn1cblxuLmJvcmRlciB7XG4gICAgYm9yZGVyLXRvcDogMnB4IHNvbGlkICNlY2VjZWM7XG4gICAgcGFkZGluZy10b3A6IDlweDtcbn1cblxuLm5hbWUtc3R5bGUge1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLnN0dWRlbnQtaWNvbiB7XG4gICAgZm9udC1zaXplOiA0NnB4O1xuICAgIGNvbG9yOiAjMWFiYTdlO1xufVxuXG4uY2xhc3Mtc2VjIHtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmNpcmNsZSB7XG4gICAgcGFkZGluZzogNnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgY29udGVudDogXCJcIjtcbiAgICB0b3A6IDEzcHg7XG4gICAgbGVmdDogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBib3JkZXI6IDNweCBzb2xpZCAjZWNlY2VjO1xufVxuLmJvcmRlci1saW5lIHtcbiAgICBib3JkZXItYm90dG9tOiAzcHggc29saWQ7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBwYWRkaW5nOiA0cHg7XG59XG5cbi5maXhsaW5lIHtcbiAgICBoZWlnaHQ6IDcwcHg7XG4gICAgZm9udC1zaXplOiBzbWFsbGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiAxMnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnJvd0NlbnRlciB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5jb3VudFNpemUge1xuICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbn1cblxuXG4uZm9udEZhbWlseXtcbiAgICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbn1cbi5pY29uQ2FsbHtcbiAgICBmb250LXNpemU6IDMxcHg7XG4gICAgY29sb3I6ICMxYWJhN2U7XG59IiwiLnBhZGRpbmcwcHgge1xuICBwYWRkaW5nOiAwcHg7XG59XG5cbi50ZXh0LXJpZ2h0IHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi50ZXh0LWNlbnRlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmhlYWRlci1zdHlsZSB7XG4gIC0tYmFja2dyb3VuZDogIzFhYmE3ZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ucGFkZGluZy10b3AtMTAge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cblxuLnBhZGRpbmctYm90dG9tLTEwIHtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG5cbi50cmlwLWRldGFpbCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5ib3JkZXIge1xuICBib3JkZXItdG9wOiAycHggc29saWQgI2VjZWNlYztcbiAgcGFkZGluZy10b3A6IDlweDtcbn1cblxuLm5hbWUtc3R5bGUge1xuICBmb250LXNpemU6IDE3cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5zdHVkZW50LWljb24ge1xuICBmb250LXNpemU6IDQ2cHg7XG4gIGNvbG9yOiAjMWFiYTdlO1xufVxuXG4uY2xhc3Mtc2VjIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uY2lyY2xlIHtcbiAgcGFkZGluZzogNnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgY29udGVudDogXCJcIjtcbiAgdG9wOiAxM3B4O1xuICBsZWZ0OiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlcjogM3B4IHNvbGlkICNlY2VjZWM7XG59XG5cbi5ib3JkZXItbGluZSB7XG4gIGJvcmRlci1ib3R0b206IDNweCBzb2xpZDtcbiAgd2lkdGg6IDUwJTtcbiAgcGFkZGluZzogNHB4O1xufVxuXG4uZml4bGluZSB7XG4gIGhlaWdodDogNzBweDtcbiAgZm9udC1zaXplOiBzbWFsbGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDEycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnJvd0NlbnRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uY291bnRTaXplIHtcbiAgZm9udC1zaXplOiBsYXJnZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG59XG5cbi5mb250RmFtaWx5IHtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG59XG5cbi5pY29uQ2FsbCB7XG4gIGZvbnQtc2l6ZTogMzFweDtcbiAgY29sb3I6ICMxYWJhN2U7XG59Il19 */"

/***/ }),

/***/ "./src/app/parent-app/student-dashboard/student-dashboard.page.ts":
/*!************************************************************************!*\
  !*** ./src/app/parent-app/student-dashboard/student-dashboard.page.ts ***!
  \************************************************************************/
/*! exports provided: StudentDashboardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentDashboardPage", function() { return StudentDashboardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");






let StudentDashboardPage = class StudentDashboardPage {
    constructor(menuController, router, ajaxService) {
        this.menuController = menuController;
        this.router = router;
        this.ajaxService = ajaxService;
        //   studentsData={
        //     studName:' ',
        //     driverNo:'',
        //     driverName:'',
        //     routeName:'',
        //     std:"",
        //     sec:"",
        //     routes:[{name:"",value:""},{name:"",value:""},
        //     { name:" ",value:""}],
        // }
        this.studentsData = [{ "sec": "B", "std": "STD XII", "routesDatas": [{ "name": "Stared at Home", "value": "current" }],
                "studentName": "GOKUL", "driverName": "demo", "driverNo": "12345678910", "routeName": "GT45" }];
    }
    studentDetail(data) {
        localStorage.setItem('selectedVin', JSON.stringify(data));
        this.router.navigateByUrl('/parent-tab/routmap');
    }
    getStudentDetails() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/parentapp/dashboard?parentId=' + localStorage.getItem('userName');
        this.ajaxService.ajaxGet(url).subscribe(res => {
            console.log(res);
            this.studentsData = res;
            // localStorage.setItem('selectedVin',JSON.stringify({vin:''}))
            localStorage.setItem('appSettings', JSON.stringify("asas"));
            localStorage.setItem('staticIOData', JSON.stringify({ paramVin: '' }));
        });
    }
    ngOnInit() {
        this.getStudentDetails();
        this.menuController.enable(true);
    }
};
StudentDashboardPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"] }
];
StudentDashboardPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-student-dashboard',
        template: __webpack_require__(/*! raw-loader!./student-dashboard.page.html */ "./node_modules/raw-loader/index.js!./src/app/parent-app/student-dashboard/student-dashboard.page.html"),
        styles: [__webpack_require__(/*! ./student-dashboard.page.scss */ "./src/app/parent-app/student-dashboard/student-dashboard.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"]])
], StudentDashboardPage);



/***/ })

}]);
//# sourceMappingURL=parent-app-student-dashboard-student-dashboard-module-es2015.js.map