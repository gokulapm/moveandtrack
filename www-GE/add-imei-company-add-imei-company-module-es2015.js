(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-imei-company-add-imei-company-module"],{

/***/ "./src/app/delar-application/dashboard/add-imei-company/add-imei-company.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/delar-application/dashboard/add-imei-company/add-imei-company.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: AddImeiCompanyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddImeiCompanyPageModule", function() { return AddImeiCompanyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_imei_company_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-imei-company.page */ "./src/app/delar-application/dashboard/add-imei-company/add-imei-company.page.ts");






let AddImeiCompanyPageModule = class AddImeiCompanyPageModule {
};
AddImeiCompanyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
        ],
        declarations: [_add_imei_company_page__WEBPACK_IMPORTED_MODULE_5__["AddImeiCompanyPage"]]
    })
], AddImeiCompanyPageModule);



/***/ })

}]);
//# sourceMappingURL=add-imei-company-add-imei-company-module-es2015.js.map