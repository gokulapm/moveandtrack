(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["device-activation-device-activation-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/device-activation/device-activation.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/device-activation/device-activation.page.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"dealerHeader\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n     <ion-title style=\"margin-left: 1%;\">Device activation</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-row>\n    <ion-col size=\"12\"    class=\"wrapper-container\">\n      <form [formGroup]=\"device\">\n          <ion-progress-bar  [value]=\"count\"></ion-progress-bar>\n           <ion-row>\n            <ion-col  size-sm=\"8\" size-md=\"8\" size-lg=\"6\" class=\"head\" >\n              <label class=\"heading \">General details</label>\n            </ion-col>\n          </ion-row>\n         <div>         \n        <ion-row>\n         <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\" class=\"form_col\">\n          <ion-input type=\"text\" (ionChange)=\"progressbar()\" placeholder=\"Name-in-rc-book\" formControlName=\"customerName\" ></ion-input>\n         </ion-col>\n       <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\"  class=\"form_col\">\n         <ion-input type=\"text\" placeholder=\"Address-in-rc_book\"  (ionChange)=\"progressbar()\" formControlName=\"address\"></ion-input>\n         </ion-col>\n       <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\"  class=\"form_col\">\n          <ion-input type=\"text\" (ionChange)=\"progressbar()\" placeholder=\"Mobile number\"  formControlName=\"mobile\"></ion-input>\n        </ion-col>\n       <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\"  class=\"form_col\">\n          <ion-input type=\"text\" (ionChange)=\"progressbar()\" placeholder=\"Email id\"  formControlName=\"mail\">\n            </ion-input>\n           </ion-col>\n        </ion-row>\n       </div>\n       <ion-row>\n        <ion-col  size-sm=\"8\" size-md=\"8\" size-lg=\"6\" class=\"head\">\n         <label class=\"heading\"  (click)=\"assestDetails()\">Assets details</label>\n          </ion-col>\n       </ion-row>\n     <div>\n      <ion-row>\n        <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\">\n          <ion-input type=\"text\" placeholder=\"Imei number\"  (ionChange)=\"progressbar()\" formControlName=\"imeiNO\"></ion-input>\n           </ion-col>\n           <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\">\n          <ion-input type=\"text\" placeholder=\"Engine number\" (ionChange)=\"progressbar()\" formControlName=\"EngineNo\"></ion-input>\n        </ion-col>\n      <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\">\n    <ion-input type=\"text\" placeholder=\"Chassis number\"  (ionChange)=\"progressbar()\" formControlName=\"ChassisNo\"></ion-input>\n    </ion-col>\n    <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\">\n     <ion-input type=\"text\" placeholder=\"Vehicle number\" (ionChange)=\"progressbar()\"  formControlName=\"vehNo\"></ion-input>\n      </ion-col>\n     </ion-row>\n    </div>\n     <ion-row>\n      <ion-col size-sm=\"8\" size-md=\"8\" size-lg=\"6\"  class=\"head\" >\n       <label class=\"heading\">Documentation</label>\n        </ion-col>\n       </ion-row>\n      <div >\n     <ion-row>\n        <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\">\n          <ion-item>\n          <ion-label class=\"lab\">Select</ion-label>\n            <ion-select  ok-text=\"okay\" cancel-text=\"cancel\" formControlName=\"proof\"  >\n             <ion-select-option *ngFor=\"let list of data\">{{list}}</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      <ion-col size=\"12\" size-sm=\"12\" size-md=\"6\" size-lg=\"6\">\n        <ion-input type=\"text\"  (ionChange)=\"progressbar()\" placeholder=\"Enter your proof number\"  formControlName=\"proofNumber\"></ion-input>\n         </ion-col>\n      </ion-row>\n    </div>\n      <ion-row>\n       <ion-col size-sm=\"8\" size-md=\"8\" size-lg=\"6\" class=\"head\" >\n        <label class=\"heading\" >Add document</label>\n       </ion-col>\n      </ion-row>\n      <ion-row style=\"margin:6px;\">\n        <input  type=\"file\" class=\"documents\" ng2FileSelect [uploader]=\"uploader\" multiple>\n        <div class=\"down\">\n      <div *ngFor=\"let item of uploader.queue; let id = index\" style=\"margin: 10px;\">\n        <ion-icon name=\"trash\" class=\"deleteIcon\" (click)=\"delete(id)\" (click)=\"deletes()\" ></ion-icon>\n          {{ item?.file?.name }}\n           </div>\n             <ion-row col-3 *ngFor=\"let photo of photos; let id = index\" class=\"first\"  >\n               <ion-icon name=\"trash\" class=\"deleteIcon\" (click)=\"deletePhoto(id)\" (click)=\"deletes()\" ></ion-icon>\n              <img [src]=\"photo\" *ngIf=\"photo\">\n             </ion-row>\n            </div>\n          </ion-row>\n         </form>\n    <ion-row style=\"padding: 0px;\">\n  <ion-col size=\"12\" class=\"ion-text-center\"> <ion-button id=\"btn\"  (click)=\"submit()\"   [disabled]=\"!device.valid\" autocapitalize=\"characters\" \n   >Submit</ion-button></ion-col></ion-row>\n    </ion-col>\n     </ion-row>\n  </ion-content>"

/***/ }),

/***/ "./src/app/delar-application/device-activation/device-activation-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/delar-application/device-activation/device-activation-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: DeviceActivationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceActivationPageRoutingModule", function() { return DeviceActivationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _device_activation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./device-activation.page */ "./src/app/delar-application/device-activation/device-activation.page.ts");




var routes = [
    {
        path: '',
        component: _device_activation_page__WEBPACK_IMPORTED_MODULE_3__["DeviceActivationPage"]
    }
];
var DeviceActivationPageRoutingModule = /** @class */ (function () {
    function DeviceActivationPageRoutingModule() {
    }
    DeviceActivationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], DeviceActivationPageRoutingModule);
    return DeviceActivationPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/delar-application/device-activation/device-activation.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/delar-application/device-activation/device-activation.module.ts ***!
  \*********************************************************************************/
/*! exports provided: DeviceActivationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceActivationPageModule", function() { return DeviceActivationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _device_activation_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./device-activation-routing.module */ "./src/app/delar-application/device-activation/device-activation-routing.module.ts");
/* harmony import */ var _device_activation_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./device-activation.page */ "./src/app/delar-application/device-activation/device-activation.page.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm5/ng2-file-upload.js");
// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';

// import { IonicModule } from '@ionic/angular';
// import { DeviceActivationPageRoutingModule } from './device-activation-routing.module';
// import { DeviceActivationPage } from './device-activation.page';
// @NgModule({
//   imports: [
//     CommonModule,
//     FormsModule,
//     IonicModule,
//     DeviceActivationPageRoutingModule
//   ],
//   declarations: [DeviceActivationPage]
// })
// export class DeviceActivationPageModule {}







// import {ProgressBarModule} from "angular-progress-bar";


var DeviceActivationPageModule = /** @class */ (function () {
    function DeviceActivationPageModule() {
    }
    DeviceActivationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _device_activation_routing_module__WEBPACK_IMPORTED_MODULE_6__["DeviceActivationPageRoutingModule"],
                // ProgressBarModule,
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            ],
            declarations: [_device_activation_page__WEBPACK_IMPORTED_MODULE_7__["DeviceActivationPage"]]
        })
    ], DeviceActivationPageModule);
    return DeviceActivationPageModule;
}());



/***/ }),

/***/ "./src/app/delar-application/device-activation/device-activation.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/delar-application/device-activation/device-activation.page.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-input {\n  background: #e8e8e8;\n  text-align: initial;\n}\n\nion-icon {\n  color: #fafcff;\n  -webkit-clip-path: polygon(0 0, 100% 0%, 100% 100%, 0% 100%);\n          clip-path: polygon(0 0, 100% 0%, 100% 100%, 0% 100%);\n  padding: 1px 3px;\n  background-color: #6252ec;\n  vertical-align: bottom;\n}\n\n.sub {\n  margin-left: 27%;\n}\n\n.pro {\n  text-align: center;\n  margin-left: 50px;\n  font-size: 15px;\n  background-color: #3d6cec;\n  border-radius: 4px;\n}\n\n.lab {\n  font-family: sans-serif;\n}\n\n.cam {\n  border: 1px solid #3d6cec;\n  margin-left: 38%;\n}\n\n.text {\n  text-align: center;\n}\n\n.icon {\n  margin-left: 38%;\n}\n\n.drop-zone {\n  background-color: #f6f6f6;\n  border: dotted 3px #dedddd;\n  height: 30vh;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  margin: 20px 0;\n}\n\n.file-input-container input[type=file] {\n  display: none;\n}\n\n.first {\n  margin-left: 38%;\n  padding: 5px;\n}\n\n.nv-file-over {\n  border: dotted 3px red;\n}\n\n.heading {\n  text-align: center;\n  border-radius: 4px;\n  color: #6252ee;\n  font-weight: 500;\n  font-size: 20px;\n}\n\nion-progress-bar {\n  border-radius: 50px;\n  height: 7px;\n}\n\n.file {\n  margin-left: 8%;\n  border: 1px solid #3d6cec;\n}\n\n.down {\n  text-align: center;\n}\n\n.arrow {\n  position: absolute;\n  right: 0;\n}\n\n.wrapper-container {\n  text-align: center;\n}\n\nion-item {\n  --padding-end:0;\n  border: 1px solid #e5e5e5;\n  height: 42px;\n  line-height: 18px;\n  background: #e8e8e8;\n  --background: #e8e8e8;\n  color: #7a7b7b;\n  -webkit-box-align: baseline;\n          align-items: baseline;\n}\n\n.form_col {\n  line-height: 18px;\n  font-size: 16px;\n}\n\n@media only screen and (min-width: 320px) and (max-width: 767px) {\n  .wrapper-container {\n    zoom: 80%;\n  }\n\n  #btn {\n    width: 80%;\n  }\n}\n\n@media only screen and (min-width: 768px) {\n  .wrapper-container {\n    zoom: 90%;\n    padding: 20px;\n  }\n\n  #btn {\n    width: 20%;\n  }\n}\n\n#btn {\n  --background:#6252ee;\n}\n\n.head {\n  text-align: initial;\n}\n\nion-input {\n  --padding-start:17px;\n  height: 42px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vZGV2aWNlLWFjdGl2YXRpb24vZGV2aWNlLWFjdGl2YXRpb24ucGFnZS5zY3NzIiwic3JjL2FwcC9kZWxhci1hcHBsaWNhdGlvbi9kZXZpY2UtYWN0aXZhdGlvbi9kZXZpY2UtYWN0aXZhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0FDQUo7O0FERUU7RUFDRSxjQUFBO0VBQ0EsNERBQUE7VUFBQSxvREFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBRENFO0VBQ0UsZ0JBQUE7QUNFSjs7QURBRTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0dKOztBRERFO0VBRUEsdUJBQUE7QUNHRjs7QURERTtFQUNFLHlCQUFBO0VBQ0EsZ0JBQUE7QUNJSjs7QURGRTtFQUNBLGtCQUFBO0FDS0Y7O0FESEU7RUFDQSxnQkFBQTtBQ01GOztBREhFO0VBQ0UseUJBQUE7RUFDQSwwQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLGNBQUE7QUNNSjs7QURISTtFQUVJLGFBQUE7QUNLUjs7QURPRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQ0pKOztBRE1FO0VBQ0Usc0JBQUE7QUNISjs7QURNRTtFQUNDLGtCQUFBO0VBQ0Msa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDSEo7O0FET0U7RUFDRSxtQkFBQTtFQUNBLFdBQUE7QUNKSjs7QURPRTtFQUNFLGVBQUE7RUFDQSx5QkFBQTtBQ0pKOztBRE1FO0VBQ0Usa0JBQUE7QUNISjs7QURLRTtFQUVFLGtCQUFBO0VBQ0EsUUFBQTtBQ0hKOztBREtFO0VBQ0csa0JBQUE7QUNGTDs7QURNRTtFQUNFLGVBQUE7RUFDQyx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUVBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7VUFBQSxxQkFBQTtBQ0pMOztBRE1FO0VBQ0csaUJBQUE7RUFDQSxlQUFBO0FDSEw7O0FEUUU7RUFDRTtJQUNJLFNBQUE7RUNMTjs7RURPQztJQUNDLFVBQUE7RUNKRjtBQUNGOztBRE1FO0VBQ0U7SUFDSSxTQUFBO0lBQ0EsYUFBQTtFQ0pOOztFRE1FO0lBQ0ksVUFBQTtFQ0hOO0FBQ0Y7O0FES0U7RUFDRSxvQkFBQTtBQ0hKOztBRE1FO0VBQ0MsbUJBQUE7QUNISDs7QURLRTtFQUNDLG9CQUFBO0VBQ0EsWUFBQTtBQ0ZIIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vZGV2aWNlLWFjdGl2YXRpb24vZGV2aWNlLWFjdGl2YXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmlvbi1pbnB1dHtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyMzIsIDIzMiwgMjMyKTtcclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbiAgfVxyXG4gIGlvbi1pY29ue1xyXG4gICAgY29sb3I6ICNmYWZjZmY7XHJcbiAgICBjbGlwLXBhdGg6IHBvbHlnb24oMCAwLCAxMDAlIDAlLCAxMDAlIDEwMCUsIDAlIDEwMCUpO1xyXG4gICAgcGFkZGluZzogMXB4IDNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM2MjUyZWM7IFxyXG4gICAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcclxuICB9XHJcbiAgLnN1YntcclxuICAgIG1hcmdpbi1sZWZ0OiAyNyU7XHJcbiAgfVxyXG4gIC5wcm97XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6cmdiKDYxLCAxMDgsIDIzNik7IFxyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIH1cclxuICAubGFie1xyXG4gIC8vICBtYXJnaW4tbGVmdDoyMHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xyXG4gIH1cclxuICAuY2Fte1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDYxLCAxMDgsIDIzNik7XHJcbiAgICBtYXJnaW4tbGVmdDogMzglO1xyXG4gIH1cclxuICAudGV4dHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5pY29ue1xyXG4gIG1hcmdpbi1sZWZ0OiAzOCU7XHJcbiAgfVxyXG4gIC8vIG11bHRpcGFydCBmaWxlXHJcbiAgLmRyb3Atem9uZSB7IFxyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y2ZjZmNjtcclxuICAgIGJvcmRlcjogZG90dGVkIDNweCAjZGVkZGRkOyBcclxuICAgIGhlaWdodDogMzB2aDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDIwcHggMDtcclxuICB9XHJcbiAgLmZpbGUtaW5wdXQtY29udGFpbmVyIHtcclxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcclxuICAgICBcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLy8gIGxhYmVsIHtcclxuICAgIC8vICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgIC8vICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAvLyAgICAgYmFja2dyb3VuZC1jb2xvcjpyZ2IoNjEsIDEwOCwgMjM2KTsgXHJcbiAgICAvLyAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgLy8gICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgLy8gICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIC8vICB9XHJcbiAgfVxyXG4gIC5maXJzdHtcclxuICAgIG1hcmdpbi1sZWZ0OiAzOCU7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgfVxyXG4gIC5udi1maWxlLW92ZXIgeyBcclxuICAgIGJvcmRlcjogZG90dGVkIDNweCByZWQ7IFxyXG4gIH1cclxuICBcclxuICAuaGVhZGluZ3tcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czo0cHg7XHJcbiAgICBjb2xvcjojNjI1MmVlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgXHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1wcm9ncmVzcy1iYXJ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA3cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5maWxle1xyXG4gICAgbWFyZ2luLWxlZnQ6IDglO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDYxLCAxMDgsIDIzNik7XHJcbiAgfVxyXG4gIC5kb3due1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuYXJyb3d7XHJcbiAgIFxyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgfVxyXG4gIC53cmFwcGVyLWNvbnRhaW5lcntcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIFxyXG4gICAgLy9tYXJnaW46IDMwcHggYXV0bztcclxuICB9XHJcbiAgaW9uLWl0ZW17XHJcbiAgICAtLXBhZGRpbmctZW5kOjA7XHJcbiAgICAgYm9yZGVyOiAxcHggc29saWQgI2U1ZTVlNTtcclxuICAgICBoZWlnaHQ6IDQycHg7XHJcbiAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAgXHJcbiAgICAgYmFja2dyb3VuZDogI2U4ZThlODtcclxuICAgICAtLWJhY2tncm91bmQ6ICNlOGU4ZTg7XHJcbiAgICAgY29sb3I6ICM3YTdiN2I7XHJcbiAgICAgYWxpZ24taXRlbXM6IGJhc2VsaW5lO1xyXG4gIH1cclxuICAuZm9ybV9jb2x7XHJcbiAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAgZm9udC1zaXplOiAxNnB4OyAgXHJcbiAgXHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQobWluLXdpZHRoOjMyMHB4KWFuZChtYXgtd2lkdGg6NzY3cHgpe1xyXG4gICAgLndyYXBwZXItY29udGFpbmVye1xyXG4gICAgICAgIHpvb206ODAlO1xyXG4gICAgfVxyXG4gICAjYnRue1xyXG4gICAgd2lkdGg6ODAlO1xyXG4gICB9XHJcbiAgICB9XHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZChtaW4td2lkdGg6NzY4cHgpe1xyXG4gICAgLndyYXBwZXItY29udGFpbmVye1xyXG4gICAgICAgIHpvb206OTAlO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICB9XHJcbiAgICAjYnRue1xyXG4gICAgICAgIHdpZHRoOjIwJTtcclxuICAgICAgIH1cclxuICAgIH1cclxuICAjYnRue1xyXG4gICAgLS1iYWNrZ3JvdW5kOiM2MjUyZWU7XHJcbiAgXHJcbiAgfVxyXG4gIC5oZWFke1xyXG4gICB0ZXh0LWFsaWduOiBpbml0aWFsO1xyXG4gIH1cclxuICBpb24taW5wdXR7XHJcbiAgIC0tcGFkZGluZy1zdGFydDoxN3B4O1xyXG4gICBoZWlnaHQ6IDQycHg7XHJcbiAgfVxyXG5cclxuIiwiaW9uLWlucHV0IHtcbiAgYmFja2dyb3VuZDogI2U4ZThlODtcbiAgdGV4dC1hbGlnbjogaW5pdGlhbDtcbn1cblxuaW9uLWljb24ge1xuICBjb2xvcjogI2ZhZmNmZjtcbiAgY2xpcC1wYXRoOiBwb2x5Z29uKDAgMCwgMTAwJSAwJSwgMTAwJSAxMDAlLCAwJSAxMDAlKTtcbiAgcGFkZGluZzogMXB4IDNweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzYyNTJlYztcbiAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcbn1cblxuLnN1YiB7XG4gIG1hcmdpbi1sZWZ0OiAyNyU7XG59XG5cbi5wcm8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICBmb250LXNpemU6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzZDZjZWM7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmxhYiB7XG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xufVxuXG4uY2FtIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzNkNmNlYztcbiAgbWFyZ2luLWxlZnQ6IDM4JTtcbn1cblxuLnRleHQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5pY29uIHtcbiAgbWFyZ2luLWxlZnQ6IDM4JTtcbn1cblxuLmRyb3Atem9uZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XG4gIGJvcmRlcjogZG90dGVkIDNweCAjZGVkZGRkO1xuICBoZWlnaHQ6IDMwdmg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMDtcbn1cblxuLmZpbGUtaW5wdXQtY29udGFpbmVyIGlucHV0W3R5cGU9ZmlsZV0ge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uZmlyc3Qge1xuICBtYXJnaW4tbGVmdDogMzglO1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbi5udi1maWxlLW92ZXIge1xuICBib3JkZXI6IGRvdHRlZCAzcHggcmVkO1xufVxuXG4uaGVhZGluZyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBjb2xvcjogIzYyNTJlZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG5pb24tcHJvZ3Jlc3MtYmFyIHtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgaGVpZ2h0OiA3cHg7XG59XG5cbi5maWxlIHtcbiAgbWFyZ2luLWxlZnQ6IDglO1xuICBib3JkZXI6IDFweCBzb2xpZCAjM2Q2Y2VjO1xufVxuXG4uZG93biB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmFycm93IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMDtcbn1cblxuLndyYXBwZXItY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5pb24taXRlbSB7XG4gIC0tcGFkZGluZy1lbmQ6MDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2U1ZTVlNTtcbiAgaGVpZ2h0OiA0MnB4O1xuICBsaW5lLWhlaWdodDogMThweDtcbiAgYmFja2dyb3VuZDogI2U4ZThlODtcbiAgLS1iYWNrZ3JvdW5kOiAjZThlOGU4O1xuICBjb2xvcjogIzdhN2I3YjtcbiAgYWxpZ24taXRlbXM6IGJhc2VsaW5lO1xufVxuXG4uZm9ybV9jb2wge1xuICBsaW5lLWhlaWdodDogMThweDtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDMyMHB4KSBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgLndyYXBwZXItY29udGFpbmVyIHtcbiAgICB6b29tOiA4MCU7XG4gIH1cblxuICAjYnRuIHtcbiAgICB3aWR0aDogODAlO1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC53cmFwcGVyLWNvbnRhaW5lciB7XG4gICAgem9vbTogOTAlO1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gIH1cblxuICAjYnRuIHtcbiAgICB3aWR0aDogMjAlO1xuICB9XG59XG4jYnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiM2MjUyZWU7XG59XG5cbi5oZWFkIHtcbiAgdGV4dC1hbGlnbjogaW5pdGlhbDtcbn1cblxuaW9uLWlucHV0IHtcbiAgLS1wYWRkaW5nLXN0YXJ0OjE3cHg7XG4gIGhlaWdodDogNDJweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/device-activation/device-activation.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/delar-application/device-activation/device-activation.page.ts ***!
  \*******************************************************************************/
/*! exports provided: DeviceActivationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceActivationPage", function() { return DeviceActivationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm5/ng2-file-upload.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");




//import { Camera,CameraOptions } from '@ionic-native/camera/ngx';







// import { Plugins, CameraResultType, Capacitor, FilesystemDirectory, 
//   CameraPhoto, CameraSource } from '@capacitor/core';
var DeviceActivationPage = /** @class */ (function () {
    function DeviceActivationPage(filePath, formBuilder, http, alertController, router, ajaxService, commonService) {
        this.filePath = filePath;
        this.formBuilder = formBuilder;
        this.http = http;
        this.alertController = alertController;
        this.router = router;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.data = ["AADHAAR NO", "DRIVINGLICENCE NO", "VOTERID NO", "PASSPORT NO"];
        this.mob = "^((\\+91-?)|0)?[0-9]{10}$";
        this.e_Mail = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$';
        this.proof = "";
        this.selectedFile = null;
        this.show = true;
        this.document = false;
        this.assest = false;
        this.showFiles = false;
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_4__["FileUploader"]({});
        this.count = 0;
    }
    // hiding data and progress bar
    DeviceActivationPage.prototype.personDetails = function () {
        this.show = !this.show;
    };
    DeviceActivationPage.prototype.documents = function () {
        this.document = !this.document;
    };
    DeviceActivationPage.prototype.assestDetails = function () {
        this.assest = !this.assest;
    };
    DeviceActivationPage.prototype.showFile = function () {
        this.showFiles = !this.showFiles;
    };
    DeviceActivationPage.prototype.progressbar = function () {
        this.count = 0;
        if (this.device.value.ChassisNo != null && this.device.value.ChassisNo.length >= 5) {
            this.count = this.count + 0.1;
        }
        if (this.device.value.customerName != null && this.device.value.customerName.length >= 3) {
            this.count = this.count + 0.1;
        }
        if (this.device.value.address != null && this.device.value.address.length >= 5) {
            this.count = this.count + 0.1;
        }
        if (this.device.value.imeiNO != null && this.device.value.imeiNO.length === 15) {
            this.count = this.count + 0.1;
        }
        if (this.device.value.EngineNo != null && this.device.value.EngineNo.length >= 4) {
            this.count = this.count + 0.1;
        }
        if (this.device.value.vehNo != null && this.device.value.vehNo.length >= 4) {
            this.count = this.count + 0.1;
        }
        if (this.device.value.proof != null && this.device.value.proof.length >= 4) {
            this.count = this.count + 0.1;
        }
        if (this.device.controls.mail.status === "VALID") {
            this.count = this.count + 0.1;
        }
        if (this.device.value.mobile != null && this.device.value.mobile.length === 10) {
            this.count = this.count + 0.1;
        }
        if ((this.uploader.queue.length != null && this.uploader.queue.length >= 0) || this.photos.length >= 0) {
            this.count = this.count + 0.04;
        }
    };
    //getting document file
    DeviceActivationPage.prototype.onFileChange = function (ev) {
        this.show = false;
        this.file = ev.srcElement.files[0];
    };
    DeviceActivationPage.prototype.submit = function () {
        var _this = this;
        this.commonService.presentToast("please wait until the process is finished");
        this.proof = this.device.value.proof;
        this.deviceActivation = {
            imeiNO: this.device.value.imeiNO,
            customerName: this.device.value.customerName,
            address: this.device.value.address,
            mobile: this.device.value.mobile,
            EngineNo: this.device.value.EngineNo,
            ChassisNo: this.device.value.ChassisNo,
            vehicleNo: this.device.value.vehNo,
        };
        this.deviceActivation[this.proof] = this.device.value.proofNumber;
        // this.commonService.presentToast("Device Activated");
        // starting ngselcet 
        if (this.uploader.queue.length == 0 && this.photos.length == 0) {
            this.commonService.presentToast("please insert one document ");
        }
        else {
            var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + '/site/insert/s3';
            var file = this.uploader;
            // this.uploadFileToUrl = function(file, url){
            var testData = new FormData();
            for (var i = 0; i < file.queue.length; i++) {
                testData.append('file', file.queue[i]._file);
            }
            // if(this.photos > 0){
            // testData.append('file', );
            // for(var i=0;i<this.photos.length;i++){
            // testData.append('file', this.photos[i]);
            //   }
            //  }
            testData.append('data', JSON.stringify(this.deviceActivation));
            // testData.append('data',JSON.stringify(this.deviceActivation));
            this.ajaxService.ajaxPutMethod(url, testData)
                .subscribe(function (res) {
                console.log(res);
                res = JSON.parse(res);
                if (res.message == "Success") {
                    _this.commonService.presentToast("device activated");
                    _this.router.navigateByUrl('/dashboard');
                }
                else {
                    _this.commonService.presentToast("please give an valid details to activate");
                }
                // this.commonService.presentToast("please insert one document ");
            });
            // }
            //   console.log(this.deviceActivation);
            //    
            // this.ajaxService.ajaxPutMethod(url,this.deviceActivation)
            // .subscribe(res=>{
            //   console.log(res);
            //   // this.commonService.presentToast("please insert one document ");
            //   })
        }
        // const data= new FormData();
        // data.append('image',this.selectedFile)
        //  this.datas.push(data);
        // this.http.post('',this.deviceActivation)
        // .subscribe(res=>{
        //   console.log(res);
        // });
    };
    DeviceActivationPage.prototype.deletes = function () {
        this.progressbar();
        if (this.uploader.queue.length >= 0 || this.photos.length >= 0) {
            this.count = this.count - 0.1;
        }
    };
    // getting camera file
    //  image(){
    //  const options: CameraOptions = {
    //   quality: 100,
    //   destinationType: this.camera.DestinationType.NATIVE_URI,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE , 
    // }
    // const capturedPhoto =  CameraOptions.getPhoto({
    //   resultType: CameraResultType.Uri, // file-based data; provides best performance
    //   source: CameraSource.Camera, // automatically take a new photo with the camera
    //   quality: 100 // highest quality (0 to 100)
    // });
    // this.camera.getPicture(options).then((imageData) => {
    //  // imageData is either a base64 encoded string or a file URI
    //  // If it's base64 (DATA_URL):
    //  this.imageData = imageData;
    //     this.image1=(<any>window).Ionic.WebView.convertFileSrc(imageData);
    //  this.myPic = 'data:image/jpeg;base64,' + imageData;
    //  this.photos.push(this.image1);
    //  this.photos.reverse();
    //  this.filePath.resolveNativePath( this.imageData)
    //  .then(filePath =>
    // this.url=filePath)
    //  .catch(err => console.log(err));
    //  const tempImage =  this.url;
    //  const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
    //  const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
    //  const newBaseFilesystemPath = this.file.dataDirectory;
    //   this.file.copyFile(tempBaseFilesystemPath, tempFilename, 
    //                           newBaseFilesystemPath, tempFilename);
    //  const storedPhoto = newBaseFilesystemPath + tempFilename;
    // //  const displayImage = window.Ionic.WebView.convertFileSrc(storedPhoto);
    // }, (err) => {
    //  // Handle error
    // this.commonService.presentToast("camera not available");
    // });
    // }
    DeviceActivationPage.prototype.deletePhoto = function (index) {
        this.photos.splice(index, 1);
    };
    DeviceActivationPage.prototype.delete = function (index) {
        this.uploader.queue.splice(index, 1);
    };
    DeviceActivationPage.prototype.ionViewWillEnter = function () {
        this.device.reset();
    };
    DeviceActivationPage.prototype.ngOnInit = function () {
        this.device = this.formBuilder.group({
            imeiNO: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(15)]],
            customerName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            mobile: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.mob)]],
            mail: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
            proof: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            proofNumber: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8)]],
            vehNo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4)]],
            ChassisNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            EngineNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
        this.photos = [];
    };
    DeviceActivationPage.ctorParameters = function () { return [
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_10__["FilePath"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
        { type: _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] }
    ]; };
    DeviceActivationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-device-activation',
            template: __webpack_require__(/*! raw-loader!./device-activation.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/device-activation/device-activation.page.html"),
            styles: [__webpack_require__(/*! ./device-activation.page.scss */ "./src/app/delar-application/device-activation/device-activation.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_10__["FilePath"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"], _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"], _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]])
    ], DeviceActivationPage);
    return DeviceActivationPage;
}());



/***/ })

}]);
//# sourceMappingURL=device-activation-device-activation-module-es5.js.map