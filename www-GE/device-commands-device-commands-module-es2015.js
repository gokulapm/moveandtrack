(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["device-commands-device-commands-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/device-commands/device-commands.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/device-commands/device-commands.page.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header >\n  <ion-toolbar mode=\"md\" class=\"dealerHeader\">\n    <ion-grid class='background' style='padding: 0px;'>\n      <ion-row style='padding: 0px;'>\n        <ion-col size=\"2\" style='padding: 0px;'>\n          <ion-menu-button></ion-menu-button>\n        </ion-col>\n        <ion-col size=\"10\" style='padding: 0px;align-self: center;'>\n          <ion-title>Device command</ion-title>\n        </ion-col>\n        <!-- <ion-col size=\"1\" style='padding: 0px;align-self: center;'>\n          <ion-icon ios=\"md-search\" md=\"md-search\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"2\" style='padding: 0px;'>\n          <ion-button class='buttonData' size='small' (click) ='openAddModule(\"Add\", \"\")'>+ADD</ion-button>\n        </ion-col> -->\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class='background' >\n  <!-- <div *ngIf = 'vehicleShow === \"apm\"'> -->\n    <form class='formPadding' [formGroup]=\"deviceCommand\">\n      <ion-item class=\"appLightBackground labelSpacing\">\n        <ion-label class='textcolor'>Company Name</ion-label>\n        <!-- <ion-select formControlName=\"companyId\" (ionChange)=\"getVehiclelist(companyList)\" placeHolder = 'Company list'>\n          <ion-select-option  *ngFor=\"let companyList of companyList\" [value]=\"companyList.companyName\">\n            {{companyList.companyName}}\n          </ion-select-option>\n        </ion-select> -->\n        <ionic-selectable  class=\"maxWidth border-item select\"  formControlName=\"companyId\"\n        [items]=\"companyList\" itemTextField=\"companyName\" [canSearch]=\"true\" (onChange)=\"getVehiclelist($event)\"\n                      [hasVirtualScroll]=\"true\" \n                      placeholder=\"Company List\" closeButtonSlot=\"end\">\n                      <ng-template ionicSelectableCloseButtonTemplate>\n                        <ion-icon name=\"close-circle\"></ion-icon>\n                      </ng-template>\n                    </ionic-selectable>\n      </ion-item> \n      \n      <ion-item  class=\"appLightBackground labelSpacing\">\n        <ion-label class='textcolor'>Vehicle list</ion-label>\n        <ion-select  [disabled] = \"!getCompany\" (ionChange)=\"getCommands($event)\" formControlName=\"vehicle\" placeHolder =  'Vehicle list'>\n          <ion-select-option *ngFor=\"let companyVehicle of companyVehicle\"  [value]=\"companyVehicle\">\n            {{companyVehicle.imei}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item> \n      \n      <ion-item  class=\"appLightBackground labelSpacing\" style=\"overflow-y: scroll;\">\n        <ion-label class='textcolor'>Select command</ion-label>\n        \n        <ion-select  [disabled] = \"!getCompany\" (ionChange)=\"validateCommand()\"  formControlName=\"commandsType\" placeHolder = 'select command' interface=\"popover\">\n          <ion-select-option *ngFor=\"let commands of commands\" [value]=\"commands.type\">\n            {{commands.name}}\n          </ion-select-option>\n        </ion-select>\n    \n        <!-- <select  [disabled] = \"!getCompany\" (ionChange)=\"validateCommand()\" class=\"input-item input-new\" formControlName=\"commandsType\" placeHolder = 'select command'>\n          <option value=\"\">Vehicle type </option>\n          <option *ngFor=\"let Vehicle of vehicleTypes\" [value]=\"Vehicle\">{{Vehicle}}</option>\n        </select> -->\n      </ion-item> \n\n      <ion-item class=\"appLightBackground labelSpacing\" *ngIf='showTextbox'>\n        <ion-textarea   formControlName=\"commandBox\" placeholder=\"Enter command here\"></ion-textarea>\n      </ion-item>  \n    </form>\n    <!-- <ion-row>\n      <ion-col size='6' >\n        <ion-row>\n          <ion-col size='3'></ion-col>\n          <ion-col size='2'>\n            <button style=\"padding-left: 25%;\" class=\" dealerHeader button\"  (click)=\"clearMsgBox()\">\n              <ion-icon  name=\"calendar-clear\" class=\"iconsize dealerHeader\"></ion-icon>\n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n      \n      <ion-col size='6' >\n        <ion-row>\n          <ion-col size='4'></ion-col>\n          <ion-col size='2'>\n            <button class=\" appHeader button\"(click)=\"sendCommand()\">\n             <ion-icon name=\"arrow-forward\"  class=\"iconsize dealerHeader\" ></ion-icon>\n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row> -->\n  <!-- </div> -->\n  <ion-row >\n    <ion-col size=\"12\" class=\"ion-text-center\"> <ion-button id=\"btn\"  (click)=\"sendCommand()\"   [disabled]=\"!deviceCommand.valid\"  \n     >Submit</ion-button></ion-col></ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/delar-application/device-commands/device-commands-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/delar-application/device-commands/device-commands-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: DeviceCommandsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceCommandsPageRoutingModule", function() { return DeviceCommandsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _device_commands_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./device-commands.page */ "./src/app/delar-application/device-commands/device-commands.page.ts");




const routes = [
    {
        path: '',
        component: _device_commands_page__WEBPACK_IMPORTED_MODULE_3__["DeviceCommandsPage"]
    }
];
let DeviceCommandsPageRoutingModule = class DeviceCommandsPageRoutingModule {
};
DeviceCommandsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DeviceCommandsPageRoutingModule);



/***/ }),

/***/ "./src/app/delar-application/device-commands/device-commands.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/delar-application/device-commands/device-commands.module.ts ***!
  \*****************************************************************************/
/*! exports provided: DeviceCommandsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceCommandsPageModule", function() { return DeviceCommandsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/esm2015/ionic-selectable.min.js");
/* harmony import */ var _device_commands_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./device-commands-routing.module */ "./src/app/delar-application/device-commands/device-commands-routing.module.ts");
/* harmony import */ var _device_commands_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./device-commands.page */ "./src/app/delar-application/device-commands/device-commands.page.ts");








let DeviceCommandsPageModule = class DeviceCommandsPageModule {
};
DeviceCommandsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _device_commands_routing_module__WEBPACK_IMPORTED_MODULE_6__["DeviceCommandsPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            ionic_selectable__WEBPACK_IMPORTED_MODULE_5__["IonicSelectableModule"]
        ],
        declarations: [_device_commands_page__WEBPACK_IMPORTED_MODULE_7__["DeviceCommandsPage"]]
    })
], DeviceCommandsPageModule);



/***/ }),

/***/ "./src/app/delar-application/device-commands/device-commands.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/delar-application/device-commands/device-commands.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".iconsize {\n  font-size: 30px;\n}\n\n#btn {\n  --background:#6252ee;\n}\n\n.maxWidth {\n  width: 100%;\n}\n\n.border-item {\n  height: 26px;\n  border-radius: 3px;\n  color: #989898;\n  background: white;\n  text-align: end;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vZGV2aWNlLWNvbW1hbmRzL2RldmljZS1jb21tYW5kcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL2RldmljZS1jb21tYW5kcy9kZXZpY2UtY29tbWFuZHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVFBO0VBQ0ksZUFBQTtBQ1BKOztBRFVBO0VBQ0ksb0JBQUE7QUNQSjs7QURVRTtFQUNFLFdBQUE7QUNQSjs7QURTQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUVBLGVBQUE7QUNQSiIsImZpbGUiOiJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL2RldmljZS1jb21tYW5kcy9kZXZpY2UtY29tbWFuZHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLmJ1dHRvbiB7XHJcbi8vICAgICBjb2xvcjogI2ZkZWVlZjtcclxuLy8gICAgIGhlaWdodDogNTZweDtcclxuLy8gICAgIHdpZHRoOiA1NnB4O1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czo1MHB4O1xyXG4vLyAgICAgYmFja2dyb3VuZC1jb2xvcjojN2M2OGY4IDtcclxuLy8gICAgIHBhZGRpbmctYm90dG9tOiB1bnNldDtcclxuLy8gfVxyXG4uaWNvbnNpemUge1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG59XHJcblxyXG4jYnRue1xyXG4gICAgLS1iYWNrZ3JvdW5kOiM2MjUyZWU7XHJcbiAgXHJcbiAgfVxyXG4gIC5tYXhXaWR0aCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG4uYm9yZGVyLWl0ZW0ge1xyXG4gICAgaGVpZ2h0OiAyNnB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czozcHg7XHJcbiAgICBjb2xvcjojOTg5ODk4O1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAvLyAtLWljb24tY29sb3I6d2hpdGU7XHJcbiAgICB0ZXh0LWFsaWduOiBlbmQ7XHJcbiBcclxufSIsIi5pY29uc2l6ZSB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuI2J0biB7XG4gIC0tYmFja2dyb3VuZDojNjI1MmVlO1xufVxuXG4ubWF4V2lkdGgge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmJvcmRlci1pdGVtIHtcbiAgaGVpZ2h0OiAyNnB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGNvbG9yOiAjOTg5ODk4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjogZW5kO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/delar-application/device-commands/device-commands.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/delar-application/device-commands/device-commands.page.ts ***!
  \***************************************************************************/
/*! exports provided: DeviceCommandsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceCommandsPage", function() { return DeviceCommandsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/esm2015/ionic-selectable.min.js");








let DeviceCommandsPage = class DeviceCommandsPage {
    constructor(formBuilder, ajaxService, commonService, router) {
        this.formBuilder = formBuilder;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.router = router;
        this.getCompany = false;
        this.commandValid = false;
        this.showTextbox = false;
    }
    onCompanyTrigger($event, companyList) {
        this.getCompany = true;
        const selectedCompany = this.deviceCommand.value.companyId;
        const userId = {
            companyid: selectedCompany.companyId
        };
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/api/vts/superadmin/company/assets/' + JSON.stringify(userId);
        this.ajaxService.ajaxGet(url)
            .subscribe(res => {
            this.companyVehicle = res;
        });
    }
    clearMsgBox() {
        if (this.deviceCommand.value.commandBox.length > 0) {
            this.deviceCommand = this.formBuilder.group({
                commandBox: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
            this.commonService.presentToast('Your command box is clear');
        }
        else {
            this.commonService.presentToast('Nothing to clear in command box');
        }
    }
    sendCommand() {
        var commandValue = '';
        if (this.deviceCommand.value.commandsType == "other") {
            commandValue = this.deviceCommand.value.commandBox;
        }
        else {
            commandValue = this.deviceCommand.value.commandsType;
        }
        if (commandValue != '') {
            const deviceConfig = {
                "imeiNo": this.deviceCommand.value.vehicle.imei,
                "manufacturer": this.deviceCommand.value.vehicle.devicetype,
                "command": commandValue,
                "pass": "1234",
                "model": this.deviceCommand.value.vehicle.model,
            };
            // const url = serverUrl.Admin + '/device/initial/smsCommands';
            const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].Admin + '/api/device/command';
            this.commonService.presentLoader();
            this.ajaxService.ajaxPostMethod(url, JSON.stringify(deviceConfig))
                .subscribe(res => {
                if (res.error.text == "pending" || "Sended") {
                    this.commonService.presentToast('Command sent');
                    this.router.navigateByUrl('/tabs-login/new-dashboard');
                    this.commonService.dismissLoader();
                }
                else {
                    this.commonService.presentToast('Command not worked!');
                    this.commonService.dismissLoader();
                }
            });
        }
        else {
            this.commonService.presentToast('Enter the Commands');
        }
    }
    getCompanyList() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/global/getcompanylist?suffix=' + localStorage.companySuffix;
        this.ajaxService.ajaxGet(url)
            .subscribe(res => {
            this.companyList = res;
        });
    }
    getVehiclelist() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/global/getlistofvehiclesinfo?companyId=' + this.deviceCommand.value.companyId.companyName;
        this.ajaxService.ajaxGet(url)
            .subscribe(res => {
            this.companyVehicle = res;
            if (res.length > 0) {
                this.getCompany = true;
            }
            else {
                this.commonService.presentToast('No vehicle in this company');
            }
        });
    }
    getCommands(commands) {
        //this.commands ={"Concox":[{"name":"SWITCHING Emergency","type":"SET EO"},{"name": "SWITCHING Emergency","type": "SET EO"}]}
        this.commands = this.commandsData[commands.detail.value.model];
        this.commandValid = true;
    }
    validateCommand() {
        if (this.deviceCommand.value.commandsType == "other") {
            this.showTextbox = true;
        }
        else {
            this.showTextbox = false;
        }
    }
    getDefaultcommands() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/login/getPreferences?key=commands&companyId=' + this.companyDetail.companyID;
        this.ajaxService.ajaxGetPerference(url)
            .subscribe(res => {
            this.commandsData = res;
        });
    }
    ngOnInit() {
        this.companyDetail = {
            companyID: localStorage.getItem('companyId'),
            userId: localStorage.getItem('userId')
        };
        this.getDefaultcommands();
        this.getCompanyList();
        this.companyList = JSON.parse(localStorage.getItem('dashboardData'));
        this.deviceCommand = this.formBuilder.group({
            companyId: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            vehicle: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            commandsType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            commandBox: ['']
        });
    }
};
DeviceCommandsPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('selectComponent', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ionic_selectable__WEBPACK_IMPORTED_MODULE_7__["IonicSelectableComponent"])
], DeviceCommandsPage.prototype, "selectComponent", void 0);
DeviceCommandsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-device-commands',
        template: __webpack_require__(/*! raw-loader!./device-commands.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/device-commands/device-commands.page.html"),
        styles: [__webpack_require__(/*! ./device-commands.page.scss */ "./src/app/delar-application/device-commands/device-commands.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
], DeviceCommandsPage);



/***/ })

}]);
//# sourceMappingURL=device-commands-device-commands-module-es2015.js.map