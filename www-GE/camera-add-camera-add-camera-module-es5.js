(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["camera-add-camera-add-camera-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/camera/add-camera/add-camera.page.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/camera/add-camera/add-camera.page.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class='header'>\n  <ion-toolbar>\n      <ion-grid>\n          <ion-row></ion-row>\n          <ion-row>\n            <ion-col size=2>\n              <ion-menu-button></ion-menu-button>\n            </ion-col>\n              <ion-col size='8.5' style='align-self: center;'>\n                  <ion-row>\n                      <ion-label> ADD DEVICE </ion-label>\n                  </ion-row>\n              </ion-col>\n              <!-- <ion-col size='1.5'>\n                      <ion-row><ion-icon style='font-size: 25px;' ios=\"md-search\" (click) ='searchStatus()' md=\"md-search\"></ion-icon></ion-row>\n                  </ion-col> -->\n              <ion-col size='1.5' style=\"padding: 0px; align-self: center;\">\n                <!-- <ion-icon (click) = \"doRefresh()\" name=\"refresh\"></ion-icon> -->\n              </ion-col>\n          </ion-row>\n      </ion-grid>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content force-overscroll=\"false\">\n  <!-- <video autoplay loop src=\"assets/Login Video/loginCCTV.mp4\"></video> -->\n  <div class=\"page-centered-element\">\n    <div class=\"content\">\n      <ion-row>\n    <form [formGroup]=\"addDevice\" >\n      <ion-item  class=\" labelSpacing\">\n          <ion-icon slot=\"start\" name=\"person\"></ion-icon>\n          <ion-input formControlName=\"deviceId\" placeholder=\"Serial Number\"></ion-input>\n      </ion-item>\n      <!-- <ion-item  class=\" labelSpacing\">\n        <ion-icon slot=\"start\" name=\"person\"></ion-icon>\n        <ion-input formControlName=\"UserName\" placeholder=\"User Name\"></ion-input>\n    </ion-item>\n      <ion-item class=\" labelSpacing\">\n          <ion-icon slot=\"start\" name=\"key\"></ion-icon>\n          <ion-input formControlName=\"password\" placeholder=\"Password\" (keyup.enter)=\"submitLogin()\" [type]=\"password_type\">\n          </ion-input>\n          <ion-icon slot=\"end\" [name]=\"eye_icon\" (click)=\"showHidePass()\"></ion-icon>\n      </ion-item> -->\n  </form>\n</ion-row>\n\n<div class=\"buttons-container\">\n  <ion-row class=\"center-button\">\n        <ion-button (click) = \"addNewDevice()\"\n        shape=\"round\" expand=\"block\">\n        <ion-icon name=\"log-in\" class=\"iconSize\"></ion-icon>\n    </ion-button>\n  </ion-row>\n  </div>\n  </div>\n  </div>\n\n \n  \n</ion-content>\n"

/***/ }),

/***/ "./src/app/camera/add-camera/add-camera.module.ts":
/*!********************************************************!*\
  !*** ./src/app/camera/add-camera/add-camera.module.ts ***!
  \********************************************************/
/*! exports provided: AddCameraPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCameraPageModule", function() { return AddCameraPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_camera_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-camera.page */ "./src/app/camera/add-camera/add-camera.page.ts");








var routes = [
    {
        path: '',
        component: _add_camera_page__WEBPACK_IMPORTED_MODULE_6__["AddCameraPage"]
    }
];
var AddCameraPageModule = /** @class */ (function () {
    function AddCameraPageModule() {
    }
    AddCameraPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_camera_page__WEBPACK_IMPORTED_MODULE_6__["AddCameraPage"]]
        })
    ], AddCameraPageModule);
    return AddCameraPageModule;
}());



/***/ }),

/***/ "./src/app/camera/add-camera/add-camera.page.scss":
/*!********************************************************!*\
  !*** ./src/app/camera/add-camera/add-camera.page.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page-centered-element {\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\nvideo {\n  height: 100%;\n}\n\nion-button {\n  --background: linear-gradient(197deg, rgb(255 0 0 / 82%) 0%, rgb(197 26 26 / 86%) 13.5%, rgb(177 24 24 / 64%) 33.33%, rgb(193 14 14 / 80%) 100%) !important;\n}\n\n.content {\n  text-align: center;\n}\n\n.content .buttons-container {\n  margin-top: 30px;\n}\n\n.labelSpacing {\n  width: 100%;\n}\n\n.iconSize {\n  width: 50px;\n  height: 35px;\n}\n\n.center-button {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.bottomLink {\n  position: absolute;\n  bottom: 0;\n  left: 25%;\n  font-size: 10px;\n}\n\n.register {\n  font-size: 13px;\n  color: #2196F3;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvY2FtZXJhL2FkZC1jYW1lcmEvYWRkLWNhbWVyYS5wYWdlLnNjc3MiLCJzcmMvYXBwL2NhbWVyYS9hZGQtY2FtZXJhL2FkZC1jYW1lcmEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0Esb0JBQUE7RUFBQSxhQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7QUNDSjs7QURDQTtFQUNJLFlBQUE7QUNFSjs7QURBQTtFQUNJLDJKQUFBO0FDR0o7O0FEREU7RUFDRSxrQkFBQTtBQ0lKOztBREZJO0VBQ0UsZ0JBQUE7QUNJTjs7QURDRTtFQUNFLFdBQUE7QUNFSjs7QURDQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDRUo7O0FEQ0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDRUo7O0FEQ0E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ0VKOztBRENBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL2NhbWVyYS9hZGQtY2FtZXJhL2FkZC1jYW1lcmEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBhZ2UtY2VudGVyZWQtZWxlbWVudCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbnZpZGVve1xuICAgIGhlaWdodDogMTAwJVxufVxuaW9uLWJ1dHRvbntcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxOTdkZWcsIHJnYigyNTUgMCAwIC8gODIlKSAwJSwgcmdiKDE5NyAyNiAyNiAvIDg2JSkgMTMuNSUsIHJnYigxNzcgMjQgMjQgLyA2NCUpIDMzLjMzJSwgcmdiKDE5MyAxNCAxNCAvIDgwJSkgMTAwJSkgIWltcG9ydGFudDtcbn1cbiAgLmNvbnRlbnQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgXG4gICAgLmJ1dHRvbnMtY29udGFpbmVyIHtcbiAgICAgIG1hcmdpbi10b3A6IDMwcHg7XG4gICAgICBcbiAgICB9XG4gIH1cblxuICAubGFiZWxTcGFjaW5ne1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uaWNvblNpemV7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgaGVpZ2h0OiAzNXB4O1xufVxuXG4uY2VudGVyLWJ1dHRvbntcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmJvdHRvbUxpbmt7XG4gICAgcG9zaXRpb246YWJzb2x1dGU7XG4gICAgYm90dG9tOjA7XG4gICAgbGVmdDogMjUlO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLnJlZ2lzdGVye1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogIzIxOTZGMztcbn0iLCIucGFnZS1jZW50ZXJlZC1lbGVtZW50IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG52aWRlbyB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuaW9uLWJ1dHRvbiB7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDE5N2RlZywgcmdiKDI1NSAwIDAgLyA4MiUpIDAlLCByZ2IoMTk3IDI2IDI2IC8gODYlKSAxMy41JSwgcmdiKDE3NyAyNCAyNCAvIDY0JSkgMzMuMzMlLCByZ2IoMTkzIDE0IDE0IC8gODAlKSAxMDAlKSAhaW1wb3J0YW50O1xufVxuXG4uY29udGVudCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jb250ZW50IC5idXR0b25zLWNvbnRhaW5lciB7XG4gIG1hcmdpbi10b3A6IDMwcHg7XG59XG5cbi5sYWJlbFNwYWNpbmcge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmljb25TaXplIHtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogMzVweDtcbn1cblxuLmNlbnRlci1idXR0b24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmJvdHRvbUxpbmsge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMjUlO1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbi5yZWdpc3RlciB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgY29sb3I6ICMyMTk2RjM7XG59Il19 */"

/***/ }),

/***/ "./src/app/camera/add-camera/add-camera.page.ts":
/*!******************************************************!*\
  !*** ./src/app/camera/add-camera/add-camera.page.ts ***!
  \******************************************************/
/*! exports provided: AddCameraPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCameraPage", function() { return AddCameraPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AddCameraPage = /** @class */ (function () {
    function AddCameraPage(formBuilder, router) {
        this.formBuilder = formBuilder;
        this.router = router;
    }
    AddCameraPage.prototype.addNewDevice = function () {
        var _this = this;
        Android.addDeviceInAdmin(this.addDevice.value.deviceId, "admin", "inside123");
        setTimeout(function () {
            _this.router.navigateByUrl('camera-list');
        }, 2000);
    };
    AddCameraPage.prototype.ngOnInit = function () {
        this.addDevice = this.formBuilder.group({
            deviceId: ['vvhg', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    AddCameraPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    AddCameraPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-camera',
            template: __webpack_require__(/*! raw-loader!./add-camera.page.html */ "./node_modules/raw-loader/index.js!./src/app/camera/add-camera/add-camera.page.html"),
            styles: [__webpack_require__(/*! ./add-camera.page.scss */ "./src/app/camera/add-camera/add-camera.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AddCameraPage);
    return AddCameraPage;
}());



/***/ })

}]);
//# sourceMappingURL=camera-add-camera-add-camera-module-es5.js.map