(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["alerts-tab-alerts-tab-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/alerts-tab/alerts-tab.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/alerts-tab/alerts-tab.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\r\n  <ion-grid class=\"appHeaderTitle ion-padding\"  style=\"padding-bottom:8px\">\r\n    <ion-row [@inOutAnimation] *ngIf=\"!showSearch\">\r\n      <ion-col size=\"2\">\r\n        <ion-menu-button></ion-menu-button>\r\n      </ion-col>\r\n      <ion-col size=\"8\">\r\n        <ion-searchbar [(ngModel)]=\"searchInput\" debounce=\"500\" placeholder=\"Search\" animated class=\"noPad\">\r\n        </ion-searchbar>\r\n      </ion-col>\r\n      <ion-col size=\"2\" style=\"align-self: center;\" class=\"ion-align-self-end\"  class=\"ion-text-center\">\r\n        <ion-badge color=\"danger\">{{totalAlert}}</ion-badge>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-header> -->\r\n<ion-header class='header'>\r\n  <ion-toolbar>\r\n      <ion-grid>\r\n          <ion-row *ngIf='!showSearch'  style=\"align-items: center;\">\r\n              <ion-col size='2'>\r\n                  <ion-menu-button></ion-menu-button>\r\n              </ion-col>\r\n              <ion-col size='7.5' style='align-self: center;'>\r\n                  <ion-row>\r\n                      <ion-label> Alerts </ion-label>\r\n                  </ion-row>\r\n              </ion-col>\r\n              <ion-col (click)=\"clickShowSearch()\" style=\"text-align: right;padding: 4px;\r\n              padding-top: 9px;\" size='1'>\r\n                <ion-icon style=\"font-size: 27px;\" name=\"search\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size='1.5' style=\"padding: 0px;\">\r\n                  <ion-row (click)=\"bellRouter()\">\r\n                      <ion-col size='3' style=\"align-self: center;\"> \r\n                          <ion-img src=\"assets/dashboard_background/bell.png\" style=\"width:22px; height:22px\"></ion-img>\r\n                          <ion-badge color=\"danger\" style=\"    position: absolute;font-size: 7px;\r\n                              font-size: 9px;\r\n                              top: 6px;\r\n                              left: 17px;\r\n                              padding: 2px;\r\n                              color: white;\r\n                              font-weight: bold;\">\r\n                              {{totalAlert}}\r\n                          </ion-badge>\r\n                      </ion-col>\r\n                      \r\n                     <ion-col *ngIf='myPlatform ==\"desktop\"' size='9'>\r\n                      <ion-img [src]=\"app.logo\"></ion-img>\r\n                     </ion-col>\r\n                  </ion-row>\r\n              </ion-col>\r\n          </ion-row>\r\n          <ion-row  *ngIf='showSearch' [@inOutAnimation]> \r\n            <ion-col size=11>\r\n              <ion-searchbar [(ngModel)]=\"searchInput\" debounce=\"500\" placeholder=\"Search\" animated class=\"noPad\">  </ion-searchbar>\r\n            </ion-col>\r\n            <ion-col (click)=\"clickShowSearch()\" size=1>\r\n              <ion-icon *ngIf='showSearch' style=\"font-size: 28px;\" name=\"close\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n      </ion-grid>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content >\r\n  \r\n \r\n\r\n  <app-alerts *ngIf=\"isShowsAlert == true\" [searchInput]=\"searchInput\" mode=\"all\" (total)=\"totalAlerts($event)\"></app-alerts>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/alerts-tab/alerts-tab.module.ts":
/*!*************************************************!*\
  !*** ./src/app/alerts-tab/alerts-tab.module.ts ***!
  \*************************************************/
/*! exports provided: AlertsTabPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertsTabPageModule", function() { return AlertsTabPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _alerts_tab_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./alerts-tab.page */ "./src/app/alerts-tab/alerts-tab.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _alerts_tab_page__WEBPACK_IMPORTED_MODULE_6__["AlertsTabPage"]
    }
];
let AlertsTabPageModule = class AlertsTabPageModule {
};
AlertsTabPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_alerts_tab_page__WEBPACK_IMPORTED_MODULE_6__["AlertsTabPage"]]
    })
], AlertsTabPageModule);



/***/ }),

/***/ "./src/app/alerts-tab/alerts-tab.page.scss":
/*!*************************************************!*\
  !*** ./src/app/alerts-tab/alerts-tab.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-badge {\n  --padding-top: 5px;\n  --padding-bottom: 5px;\n  --padding-start: 5px;\n  --padding-end: 5px;\n  border-radius: 20px;\n}\n\n.ion-padding,\n[padding] {\n  padding-left: unset;\n  padding-right: unset;\n  -webkit-padding-start: var(--ion-padding, 16px);\n  padding-inline-start: var(--ion-padding, 0px);\n  -webkit-padding-end: var(--ion-padding, -16px);\n  padding-inline-end: var(--ion-padding, 13px);\n}\n\n.botPad {\n  padding-bottom: 4px;\n}\n\n.noPad {\n  padding: 0;\n}\n\n.botNoPad {\n  padding-bottom: 0;\n}\n\n.iconSize {\n  width: 22px;\n  height: 22px;\n}\n\n.fontLabel {\n  font-size: 12px;\n}\n\n.search {\n  position: fixed;\n  z-index: 13;\n  width: 40%;\n  left: 30%;\n  padding: 4px;\n  background: white;\n  /* top: 0px; */\n  /* height: 7px; */\n  margin: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvYWxlcnRzLXRhYi9hbGVydHMtdGFiLnBhZ2Uuc2NzcyIsInNyYy9hcHAvYWxlcnRzLXRhYi9hbGVydHMtdGFiLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNDRjs7QURFQTs7RUFFRSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsK0NBQUE7RUFDQSw2Q0FBQTtFQUNBLDhDQUFBO0VBQ0EsNENBQUE7QUNDRjs7QURDQTtFQUNFLG1CQUFBO0FDRUY7O0FEQUE7RUFDRSxVQUFBO0FDR0Y7O0FEQUE7RUFDRSxpQkFBQTtBQ0dGOztBREFBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNHRjs7QURBQTtFQUNFLGVBQUE7QUNHRjs7QURBQTtFQUNFLGVBQUE7RUFDRSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FDR0oiLCJmaWxlIjoic3JjL2FwcC9hbGVydHMtdGFiL2FsZXJ0cy10YWIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWJhZGdlIHtcclxuICAtLXBhZGRpbmctdG9wOiA1cHg7XHJcbiAgLS1wYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gIC0tcGFkZGluZy1zdGFydDogNXB4O1xyXG4gIC0tcGFkZGluZy1lbmQ6IDVweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG59XHJcblxyXG4uaW9uLXBhZGRpbmcsXHJcbltwYWRkaW5nXSB7XHJcbiAgcGFkZGluZy1sZWZ0OiB1bnNldDtcclxuICBwYWRkaW5nLXJpZ2h0OiB1bnNldDtcclxuICAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcclxuICBwYWRkaW5nLWlubGluZS1zdGFydDogdmFyKC0taW9uLXBhZGRpbmcsIDBweCk7XHJcbiAgLXdlYmtpdC1wYWRkaW5nLWVuZDogdmFyKC0taW9uLXBhZGRpbmcsIC0xNnB4KTtcclxuICBwYWRkaW5nLWlubGluZS1lbmQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxM3B4KTtcclxufVxyXG4uYm90UGFke1xyXG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XHJcbn1cclxuLm5vUGFkIHtcclxuICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4uYm90Tm9QYWQge1xyXG4gIHBhZGRpbmctYm90dG9tOiAwO1xyXG59XHJcblxyXG4uaWNvblNpemUge1xyXG4gIHdpZHRoOiAyMnB4O1xyXG4gIGhlaWdodDogMjJweDtcclxufVxyXG5cclxuLmZvbnRMYWJlbCB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG4uc2VhcmNoe1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHotaW5kZXg6IDEzO1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICAgIGxlZnQ6IDMwJTtcclxuICAgIHBhZGRpbmc6IDRweDtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgLyogdG9wOiAwcHg7ICovXHJcbiAgICAvKiBoZWlnaHQ6IDdweDsgKi9cclxuICAgIG1hcmdpbjogMHB4O1xyXG59IiwiaW9uLWJhZGdlIHtcbiAgLS1wYWRkaW5nLXRvcDogNXB4O1xuICAtLXBhZGRpbmctYm90dG9tOiA1cHg7XG4gIC0tcGFkZGluZy1zdGFydDogNXB4O1xuICAtLXBhZGRpbmctZW5kOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG59XG5cbi5pb24tcGFkZGluZyxcbltwYWRkaW5nXSB7XG4gIHBhZGRpbmctbGVmdDogdW5zZXQ7XG4gIHBhZGRpbmctcmlnaHQ6IHVuc2V0O1xuICAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAwcHgpO1xuICAtd2Via2l0LXBhZGRpbmctZW5kOiB2YXIoLS1pb24tcGFkZGluZywgLTE2cHgpO1xuICBwYWRkaW5nLWlubGluZS1lbmQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxM3B4KTtcbn1cblxuLmJvdFBhZCB7XG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XG59XG5cbi5ub1BhZCB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5ib3ROb1BhZCB7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xufVxuXG4uaWNvblNpemUge1xuICB3aWR0aDogMjJweDtcbiAgaGVpZ2h0OiAyMnB4O1xufVxuXG4uZm9udExhYmVsIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uc2VhcmNoIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB6LWluZGV4OiAxMztcbiAgd2lkdGg6IDQwJTtcbiAgbGVmdDogMzAlO1xuICBwYWRkaW5nOiA0cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICAvKiB0b3A6IDBweDsgKi9cbiAgLyogaGVpZ2h0OiA3cHg7ICovXG4gIG1hcmdpbjogMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/alerts-tab/alerts-tab.page.ts":
/*!***********************************************!*\
  !*** ./src/app/alerts-tab/alerts-tab.page.ts ***!
  \***********************************************/
/*! exports provided: AlertsTabPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertsTabPage", function() { return AlertsTabPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




let AlertsTabPage = class AlertsTabPage {
    constructor(platform, menuController) {
        this.platform = platform;
        this.menuController = menuController;
        this.app = { logo: 'logo.png' };
        this.mode = "all";
        this.totalAlert = 0;
        this.showSearch = false;
        this.initialHeader = true;
        this.isShowsAlert = false;
    }
    searchActivity(task) {
        if (task == "open") {
            this.showSearch = true;
        }
        else {
            this.showSearch = false;
            this.searchInput = '';
        }
    }
    clickShowSearch() {
        this.showSearch = !this.showSearch;
    }
    scroll(event) {
        if (event.detail.scrollTop > 0) {
            this.initialHeader = false;
        }
        else {
            this.initialHeader = true;
        }
    }
    totalAlerts(event) {
        this.totalAlert = event;
    }
    ionViewWillEnter() {
        this.isShowsAlert = false;
    }
    ionViewDidEnter() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.searchInput = '';
            this.app["logo"] = localStorage.companyLogo;
            this.myPlatform = this.platform.platforms()[0];
            if (this.myPlatform == 'tablet') {
                this.myPlatform = 'desktop';
            }
            this.subscription = this.platform.backButton.subscribe(() => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (this.menuController.isOpen()) {
                    this.menuController.close();
                }
            }));
            this.isShowsAlert = true;
        });
    }
};
AlertsTabPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
];
AlertsTabPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-alerts-tab',
        template: __webpack_require__(/*! raw-loader!./alerts-tab.page.html */ "./node_modules/raw-loader/index.js!./src/app/alerts-tab/alerts-tab.page.html"),
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('inOutAnimation', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])(':enter', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0.7 }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('0.7s ease-out')
                ])
            ])
        ],
        styles: [__webpack_require__(/*! ./alerts-tab.page.scss */ "./src/app/alerts-tab/alerts-tab.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
], AlertsTabPage);



/***/ })

}]);
//# sourceMappingURL=alerts-tab-alerts-tab-module-es2015.js.map