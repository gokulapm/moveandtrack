(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/login/login.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/login/login.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content style=\"background-color:#fffdfd ;\"\n  [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic', 'appLightBackground': appName === 'upcot'}\">\n\n  <div *ngIf='myPlatform != \"desktop\"' class=\"moveAndTrackSpacing\" style=\"background-color:#fffdfd ;\">\n    <ion-grid>\n      <ion-row>\n          <ion-col size=\"8\" offset=\"2\" class=\"ion-align-items-center ion-justify-content-center\">\n              <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n                  <ion-img style=\"width:70%;\" [src]=\"logo\"></ion-img>\n              </ion-row>\n          </ion-col>\n      </ion-row>\n  </ion-grid>\n  <form [formGroup]=\"login\">\n      <ion-item [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic', 'appLightBackground': appName === 'upcot'}\" class=\" labelSpacing\">\n          <ion-icon slot=\"start\" name=\"person\"></ion-icon>\n          <ion-input formControlName=\"userName\" placeholder=\"Username\"></ion-input>\n      </ion-item>\n      <ion-item [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic', 'appLightBackground': appName === 'upcot'}\" class=\" labelSpacing\">\n          <ion-icon slot=\"start\" name=\"key\"></ion-icon>\n          <ion-input formControlName=\"password\" placeholder=\"Password\" (keyup.enter)=\"submitLogin()\" [type]=\"password_type\">\n          </ion-input>\n          <ion-icon slot=\"end\" [name]=\"eye_icon\" (click)=\"showHidePass()\"></ion-icon>\n      </ion-item>\n  </form>\n\n  <ion-row>\n      <ion-col size=\"6\" offset=\"3\">    \n          <ion-button color=\"primary\" (click)=\"submitLogin()\" \n          shape=\"round\" expand=\"block\"\n          [disabled]=\"!login.valid\">\n          <ion-icon name=\"log-in\" class=\"iconSize\"></ion-icon>\n      </ion-button>\n  </ion-col>\n</ion-row>\n    \n  \n  </div>\n\n  <div style=\"padding-top: 7%;\" *ngIf='myPlatform == \"desktop\"' class=\"container-wrapper\">\n    <div style=\"position: absolute;\n    z-index: 14;\n    bottom: 0px;\n    right: 0px;\n    background: white;\n    padding: 11px;\"  class=\"forgot\">\n      <span style=\"color: red;\" (click)=\"changeModule('user')\"> I'm a User </span> | <span (click)=\"changeModule('dealer')\"> I'm a Dealer\n      </span>\n    </div>\n    <div class=\"moveAndTrackSpacing ion-align-items-center ion-justify-content-center\" size-md=\"6 \" size-lg=\"6\"\n      size-xs=\"12\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"8\" offset=\"2\" class=\"ion-align-items-center ion-justify-content-center\">\n            <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n              <ion-img style=\"width:70%;\" [src]=\"logo\"></ion-img>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <form [formGroup]=\"login\">\n\n        <ion-item\n          [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\n          class=\"labelSpacing label-shape\">\n          <ion-img src=\"assets/company.svg\" slot=\"end\" class=\"svg_img\"></ion-img>\n          <ion-input formControlName=\"userName\" placeholder=\"User Name\" class=\"web-input\"></ion-input>\n        </ion-item>\n        <ion-item\n          [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\n          class=\" labelSpacing label-shape\">\n          <ion-input formControlName=\"password\" placeholder=\"Password\" (keyup.enter)=\"submitLogin()\"\n            [type]=\"password_type\" class=\"web-input\">\n          </ion-input>\n          <ion-icon slot=\"end\" [name]=\"eye_icon\" (click)=\"showHidePass()\"></ion-icon>\n        </ion-item>\n      </form>\n      <ion-grid class=\"appLightBackground\">\n        <ion-row style=\"padding: 0px 0 0 20px;\">\n          <ion-col size=\"6\" [class]=\"appName\">\n            <ion-label (click)=\"openFpassModal()\" class=\"forgot\">Forgot Password?</ion-label>\n          </ion-col>\n          <ion-col size=\"6\">\n            <ion-item lines=\"none\" style=\"--background: transparent;margin: -15px 0 0 0px;\n       font-size: 12px;\">\n              <ion-checkbox class=\"check\" [(ngModel)]=\"isChecked\" slot=\"start\"></ion-checkbox>\n              <ion-label>Remember Me</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <!-- <ion-row></ion-row> -->\n\n      </ion-grid>\n      <ion-row>\n        <ion-col size=\"6\" offset=\"3\">\n          <ion-button [class]=\"appName\" (click)=\"submitLogin()\" shape=\"round\" expand=\"block\" [disabled]=\"!login.valid\">\n            <ion-icon name=\"log-in\" class=\"iconSize\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </div>\n\n  </div>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/forgotpass-modal/forgotpass-modal.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/forgotpass-modal/forgotpass-modal.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\r\n  <ion-toolbar class=\"appHeader\">\r\n    <ion-grid >\r\n      <ion-row>\r\n        <ion-col size=\"2\">\r\n          <ion-icon slot=\"start\" name=\"arrow-back\" (click)=\"getBack()\"></ion-icon>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row >\r\n        <ion-col>\r\n          <ion-title class=\"appHeader\">\r\n            Forgot Password\r\n          </ion-title>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-content class=\"appLightBackground\">\r\n  \r\n  <ion-row class=\"margin\"> \r\n    <ion-row>\r\n      <ion-icon  name=\"arrow-back\" (click)=\"getBack()\"></ion-icon>\r\n  </ion-row>\r\n    <ion-col size=\"4\" offset=\"4\">\r\n      <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\"\r\n      id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512;\">\r\n      <path [attr.fill]=\"color[appName]\"\r\n      d=\"M127.937,326c11.046,0,20,8.954,20,20l0,0c0,11.046-8.954,20-20,20s-20-8.954-20-20l0,0  C107.937,334.954,116.891,326,127.937,326z M182.937,346L182.937,346c0,11.046,8.954,20,20,20s20-8.954,20-20l0,0  c0-11.046-8.954-20-20-20S182.937,334.954,182.937,346z M257.937,346L257.937,346c0,11.046,8.954,20,20,20s20-8.954,20-20l0,0  c0-11.046-8.954-20-20-20S257.937,334.954,257.937,346z M364.937,492c0,11.046-8.954,20-20,20h-249c-44.112,0-80-35.888-80-80V268  c0-44.112,35.888-80,80-80H119.9v-70.534C119.9,52.695,173.732,0,239.9,0s120,52.695,120,117.466V188h24.037  c8.334,0,15.551,0.907,22.064,2.773c10.618,3.043,16.76,14.117,13.718,24.735c-3.043,10.618-14.117,16.76-24.735,13.718  c-2.88-0.825-6.493-1.227-11.047-1.227h-288c-22.056,0-40,17.944-40,40v164c0,22.056,17.944,40,40,40h249  C355.983,472,364.937,480.954,364.937,492z M159.9,188h160v-70.534c0-42.715-35.888-77.466-80-77.466s-80,34.751-80,77.466V188z   M424.937,472L424.937,472c-11.046,0-20,8.954-20,20s8.954,20,20,20l0,0c11.046,0,20-8.954,20-20S435.983,472,424.937,472z   M496.046,331.21c-0.438-38.789-32.13-70.21-71.021-70.21C385.863,261,354,292.862,354,332.025c0,11.046,8.954,20,20,20  s20-8.954,20-20C394,314.918,407.918,301,425.026,301s31.025,13.918,31.025,31.025c0,0.182,0.002,0.363,0.007,0.543  c-0.206,12.247-7.563,23.211-18.864,28.035c-19.541,8.345-32.168,27.618-32.168,49.101V427c0,11.046,8.954,20,20,20s20-8.954,20-20  v-17.296c0-5.438,3.092-10.271,7.875-12.313c26.227-11.196,43.169-36.855,43.162-65.37  C496.063,331.749,496.057,331.479,496.046,331.21z\">\r\n    </path>\r\n  </svg>\r\n</ion-col>\r\n</ion-row>\r\n<form [formGroup]=\"forgetPass\">\r\n  <ion-list>\r\n    <ion-item class=\"appLightBackground labelSpacing\">\r\n      <ion-icon slot=\"start\" name=\"contacts\"></ion-icon>\r\n      <ion-input formControlName=\"compId\" placeholder=\"Company id\"></ion-input>\r\n    </ion-item>\r\n    <ion-item class=\"appLightBackground labelSpacing\">\r\n      <ion-icon slot=\"start\" name=\"person\"></ion-icon>\r\n      <ion-input formControlName=\"compName\" placeholder=\"User name\"></ion-input>\r\n    </ion-item>\r\n    <ion-item class=\"appLightBackground labelSpacing\">\r\n      <ion-icon slot=\"start\" name=\"keypad\"></ion-icon>\r\n      <ion-input formControlName=\"mobileNo\" placeholder=\"Mobile no\"></ion-input>\r\n    </ion-item>\r\n    <ion-item class=\"appLightBackground labelSpacing\">\r\n      <ion-icon slot=\"start\" name=\"mail\"></ion-icon>\r\n      <ion-input formControlName=\"emailId\" placeholder=\"Email id (Optional)\"></ion-input>\r\n    </ion-item>\r\n   \r\n  </ion-list>\r\n  <ion-row>\r\n    <ion-col size=\"6\" offset=\"3\">\r\n      <ion-button (click)=\"submitPass()\"\r\n      [class]=\"appName\" shape=\"round\" expand=\"block\"\r\n      [disabled]=\"!forgetPass.valid\">Submit</ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n</form>\r\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.page.html":
/*!*****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content\r\n    [ngClass]=\"{'background':appName =='Armoron','appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic', 'appLightBackground': appName === 'upcot'}\">\r\n    <div *ngIf='myPlatform != \"desktop\" && appName != \"Armoron\" && appName != \"ParentApp\"' class=\"moveAndTrackSpacing\"\r\n        style=\"background-color:#fffdfd ;\">\r\n        <ion-grid>\r\n            <ion-row>\r\n                <ion-col size=\"8\" offset=\"2\" class=\"ion-align-items-center ion-justify-content-center\">\r\n                    <ion-row class=\"ion-align-items-center ion-justify-content-center\">\r\n                        <ion-img style=\"width:70%;\" [src]=\"logo\"></ion-img>\r\n                    </ion-row>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <form [formGroup]=\"login\">\r\n            <ion-item\r\n                [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic', 'appLightBackground': appName === 'upcot'}\"\r\n                class=\"labelSpacing\">\r\n                <ion-icon slot=\"start\" name=\"contacts\"></ion-icon>\r\n                <ion-input formControlName=\"compId\" placeholder=\"Company id\"></ion-input>\r\n            </ion-item>\r\n            <ion-item\r\n                [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic', 'appLightBackground': appName === 'upcot'}\"\r\n                class=\" labelSpacing\">\r\n                <ion-icon slot=\"start\" name=\"person\"></ion-icon>\r\n                <ion-input formControlName=\"compName\" placeholder=\"User name\"></ion-input>\r\n            </ion-item>\r\n            <ion-item\r\n                [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC','appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic', 'appLightBackground': appName === 'upcot'}\"\r\n                class=\" labelSpacing\">\r\n                <ion-icon slot=\"start\" name=\"key\"></ion-icon>\r\n                <ion-input formControlName=\"password\" placeholder=\"Password\" (keyup.enter)=\"submitLogin()\"\r\n                    [type]=\"password_type\">\r\n                </ion-input>\r\n                <ion-icon slot=\"end\" [name]=\"eye_icon\" (click)=\"showHidePass()\"></ion-icon>\r\n            </ion-item>\r\n        </form>\r\n        <ion-grid class=\"appLightBackground\">\r\n            <ion-row>\r\n                <ion-col offset=\"7\" size=\"5\" class=\"forgotPass\" [class]=\"appName\">\r\n                    <ion-label class=\"forgotPass\" (click)=\"openFpassModal()\">Forgot Password?</ion-label>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row></ion-row>\r\n        </ion-grid>\r\n        <!-- <ion-item class=\"appLightBackground noBorder\">\r\n          <ion-checkbox class=\"check\" [(ngModel)]=\"isChecked\" slot=\"start\"></ion-checkbox>\r\n          <ion-label>Remember Me</ion-label>\r\n      </ion-item> -->\r\n        <ion-row>\r\n            <ion-col size=\"6\" offset=\"3\">\r\n                <ion-button [class]=\"appName\" (click)=\"submitLogin()\" shape=\"round\" expand=\"block\"\r\n                    [disabled]=\"!login.valid\">\r\n                    <ion-icon name=\"log-in\" class=\"iconSize\"></ion-icon>\r\n                </ion-button>\r\n            </ion-col>\r\n        </ion-row>\r\n    </div>\r\n    <div *ngIf='myPlatform == \"desktop\" && appName != \"Remoncloud\"   && appName != \"Armoron\" && appName != \"RAC\" && appName != \"GE\"  && entryPoint !=\"TTS\" && appName != \"ParentApp\"'>\r\n    <div   style=\"position: absolute; \r\n    z-index: 14;\r\n    bottom: 0px;\r\n    right: 0px;\r\n    background: white;\r\n   padding: 11px;\" class=\"forgot\">\r\n           <span (click)=\"changeModule('user')\"> I'm a User </span> | <span style=\"color: red;\"\r\n               (click)=\"changeModule('dealer')\"> I'm a Dealer </span>\r\n       </div>\r\n    </div>\r\n    <div *ngIf='myPlatform == \"desktop\" && appName != \"Remoncloud\"   && appName != \"Armoron\" && appName != \"GE\" && appName != \"RAC\"  && entryPoint !=\"TTS\" && appName != \"ParentApp\"'\r\n        class=\"container-wrapper\">\r\n       \r\n        <div class=\"moveAndTrackSpacing ion-align-items-center ion-justify-content-center\" size-md=\"6 \" size-lg=\"6\"\r\n            size-xs=\"12\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col size=\"8\" offset=\"2\" class=\"ion-align-items-center ion-justify-content-center\">\r\n                        <ion-row class=\"ion-align-items-center ion-justify-content-center\">\r\n                            <ion-img style=\"width:70%;\" [src]=\"logo\"></ion-img>\r\n                        </ion-row>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n            <form [formGroup]=\"login\">\r\n\r\n                <ion-item\r\n                    [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\r\n                    class=\"labelSpacing label-shape\">\r\n                    <ion-img src=\"assets/company.svg\" slot=\"end\" class=\"svg_img\"></ion-img>\r\n                    <ion-input formControlName=\"compId\" placeholder=\"Company id\" class=\"web-input\"></ion-input>\r\n                </ion-item>\r\n                <ion-item\r\n                    [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\r\n                    class=\" labelSpacing label-shape\">\r\n                    <ion-icon slot=\"end\" name=\"contacts\"></ion-icon>\r\n                    <ion-input formControlName=\"compName\" placeholder=\"User name\" class=\"web-input\"></ion-input>\r\n                </ion-item>\r\n                <ion-item\r\n                    [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\r\n                    class=\" labelSpacing label-shape\">\r\n                    <ion-input formControlName=\"password\" placeholder=\"Password\" (keyup.enter)=\"submitLogin()\"\r\n                        [type]=\"password_type\" class=\"web-input\">\r\n                    </ion-input>\r\n                    <ion-icon slot=\"end\" [name]=\"eye_icon\" (click)=\"showHidePass()\"></ion-icon>\r\n                </ion-item>\r\n            </form>\r\n            <ion-grid class=\"appLightBackground\">\r\n                <ion-row style=\"padding: 0px 0 0 20px;\">\r\n                    <ion-col size=\"6\" [class]=\"appName\">\r\n                        <ion-label (click)=\"openFpassModal()\" class=\"forgot\">Forgot Password?</ion-label>\r\n                    </ion-col>\r\n                    <ion-col size=\"6\">\r\n                        <ion-item lines=\"none\" style=\"--background: transparent;margin: -15px 0 0 0px;\r\n        font-size: 12px;\">\r\n                            <ion-checkbox class=\"check\" [(ngModel)]=\"isChecked\" slot=\"start\"></ion-checkbox>\r\n                            <ion-label>Remember Me</ion-label>\r\n                        </ion-item>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <!-- <ion-row></ion-row> -->\r\n\r\n            </ion-grid>\r\n            <ion-row>\r\n                <ion-col size=\"6\" offset=\"3\">\r\n                    <ion-button [class]=\"appName\" (click)=\"submitLogin()\" shape=\"round\" expand=\"block\"\r\n                        [disabled]=\"!login.valid\">\r\n                        <ion-icon name=\"log-in\" class=\"iconSize\"></ion-icon>\r\n                    </ion-button>\r\n                </ion-col>\r\n            </ion-row>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <div  [style.background]=\"'url('+ bgImage+')'\" [style.background-repeat]=\"'no-repeat'\"  [style.background-size]= \"'cover'\" class='container '\r\n        *ngIf='myPlatform == \"desktop\"  && (appName === \"GE\" || appName == \"Remoncloud\" || appName == \"RAC\") && appName != \"ParentApp\"'>\r\n        <!-- <ion-grid style=\"position: fixed; width: 20%;\">\r\n        <ion-row>\r\n            <ion-col style=\"padding: 0px;\" size=\"12\" >\r\n                <ion-row>\r\n                    <ion-img style=\"width:100%;\" [src]=\"logo\"></ion-img>\r\n                </ion-row>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid> -->\r\n\r\n        <div class=\"moveAndTrackSpacingNew ion-align-items-center ion-justify-content-center\" size-md=\"6 \" size-lg=\"6\"\r\n            size-xs=\"12\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col size=\"8\" offset=\"2\" class=\"ion-align-items-center ion-justify-content-center\">\r\n                        <ion-row class=\"ion-align-items-center ion-justify-content-center\">\r\n                            <ion-img style=\"width:70%;\" [src]=\"logo\"></ion-img>\r\n                        </ion-row>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n            <form [formGroup]=\"login\">\r\n                <ion-item\r\n                    [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\r\n                    class=\"labelSpacing\">\r\n                    <ion-icon slot=\"start\" name=\"contacts\"></ion-icon>\r\n                    <ion-input formControlName=\"compId\" placeholder=\"CompanyID\"></ion-input>\r\n                </ion-item>\r\n                <ion-item\r\n                    [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\r\n                    class=\" labelSpacing\">\r\n                    <ion-icon slot=\"start\" name=\"person\"></ion-icon>\r\n                    <ion-input formControlName=\"compName\" placeholder=\"Username\"></ion-input>\r\n                </ion-item>\r\n                <ion-item\r\n                    [ngClass]=\"{'appLightBackground': appName === 'Moveandtrack', 'appLightBackground': appName === 'RAC', 'appImageBackground': appName=== 'GE', 'appDarkBackground': appName === 'Tracalogic'}\"\r\n                    class=\" labelSpacing\">\r\n                    <ion-icon slot=\"start\" name=\"key\"></ion-icon>\r\n                    <ion-input formControlName=\"password\" placeholder=\"Password\" (keyup.enter)=\"submitLogin()\"\r\n                        [type]=\"password_type\">\r\n                    </ion-input>\r\n                    <ion-icon slot=\"end\" [name]=\"eye_icon\" (click)=\"showHidePass()\"></ion-icon>\r\n                </ion-item>\r\n            </form>\r\n            <ion-grid class=\"appLightBackground\">\r\n                <ion-row>\r\n                    <ion-col offset=\"5\" size=\"7\" [class]=\"appName\">\r\n                        <ion-label (click)=\"openFpassModal()\" class=\"forgotPass\">Forgot Password?</ion-label>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row></ion-row>\r\n            </ion-grid>\r\n            <ion-item style=\"--background: transparent;\" class=\"appLightBackground noBorder\">\r\n                <ion-checkbox class=\"check\" [(ngModel)]=\"isChecked\" slot=\"start\"></ion-checkbox>\r\n                <ion-label>Remember Me</ion-label>\r\n            </ion-item>\r\n            <ion-row>\r\n                <ion-col size=\"6\" offset=\"3\">\r\n                    <ion-button [class]=\"appName\" (click)=\"submitLogin()\" shape=\"round\" expand=\"block\"\r\n                        [disabled]=\"!login.valid\">\r\n                        <ion-icon name=\"log-in\" class=\"iconSize\"></ion-icon>\r\n                    </ion-button>\r\n                </ion-col>\r\n            </ion-row>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf='myPlatform != \"desktop\" && appName === \"Armoron\" && appName != \"ParentApp\"'>\r\n        <div class=\"row row-center\">\r\n            <div class=\" col text-center\" align-title=\"center\">\r\n                <ion-img src=\"assets/Armoron/APM KT LOGO.png\" alt=\"\"\r\n                    style=\"width: 100%; height: 74px; margin-top: 82px;margin-bottom: 12px;background-repeat: no-repeat;background-size: cover;\">\r\n                </ion-img>\r\n            </div>\r\n        </div>\r\n\r\n        <ion-grid style=\"font-size:12px\">\r\n            <ion-row class=\"rowstyle\" style=\"font-size:10px\">\r\n                <ion-col size=\"1.3\" id=\"countryIcon\" class=\"iconShift\">\r\n                    <div style=\"padding-top: 15px;text-align: right;\">\r\n                        <ion-icon name=\"globe\" style=\"color:#ba7203\"></ion-icon>\r\n                    </div>\r\n                </ion-col>\r\n                <ion-col size=\"5.7\" style=\"color:#ba7203\">\r\n                    <div style=\"padding-top: 15px\">Country</div>\r\n                </ion-col>\r\n                <ion-col size=\"5\" style=\"color:#ba7203;\">\r\n                    <ion-select selectedText=\"India\" [interfaceOptions]=\"customPopoverOptions\" interface=\"popover\"\r\n                        (change)=\"countryChanged\">\r\n                        <ion-select-option *ngFor=\"let y of countryList\">{{y}}</ion-select-option>\r\n                    </ion-select>\r\n                    <!-- <div style=\"padding-top: 10px;text-align: right;\">\r\n                    <select style=\"background-color: transparent;border:none;\"><option selected>India</option><option>Srilanka</option></select>\r\n                </div> -->\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"rowstyle\" style=\"font-size:10px\">\r\n                <ion-col size=\"1.3\" id=\"languageIcon\" class=\"iconShift\">\r\n                    <div style=\"padding-top: 3px;text-align: right;\">\r\n                        <ion-icon name=\"chatboxes\" style=\"color:#ba7203\"></ion-icon>\r\n                    </div>\r\n                </ion-col>\r\n                <ion-col size=\"5.7\" style=\"color:#ba7203\">\r\n                    <ion-label style=\"padding-top: 3px\">Language</ion-label>\r\n                </ion-col>\r\n                <ion-col size=\"5\" style=\"color:#ba7203;\">\r\n                    <!-- <div style=\"padding-top: 3px;text-align: right;\"> -->\r\n                    <ion-select [(ngModel)]=\"armoronApp.selectedlanguage\" (ngModelChange)=\"langChged($event)\"\r\n                        selectedText=\"{{armoronApp.selectedlanguage}}\" [interfaceOptions]=\"customPopoverOptions\"\r\n                        interface=\"popover\">\r\n                        <ion-select-option *ngFor=\"let x of languageOptions\">{{x}}</ion-select-option>\r\n                    </ion-select>\r\n                    <!-- <ion-select-option *ngFor=\"let x of languageOptions\" >{{x}}</ion-select-option> -->\r\n                    <!-- <select style=\"background-color: transparent;border:none;\"><option selected>ENGLISH</option><option>ARABIC</option></select> -->\r\n                    <!-- </div> -->\r\n                </ion-col>\r\n            </ion-row>\r\n            <form [formGroup]=\"login\">\r\n                <ion-row>\r\n                    <ion-col size=\"2\">\r\n                        <div style=\"padding-top: 22px;padding-bottom: unset;text-align: right;\">\r\n                            <ion-icon name=\"phone-portrait\" style=\"color:whitesmoke\" class=\"iconsize\"></ion-icon>\r\n                        </div>\r\n                    </ion-col>\r\n                    <ion-col size=\"1.3\" ngDefaultControl class=\"contryCodeStyle\">\r\n                        <div style=\"border-right:1px solid whitesmoke \">\r\n                            <ion-label ngDefaultControl>{{armoronApp.countryCode}}</ion-label>\r\n                        </div>\r\n                    </ion-col>\r\n                    <ion-col size=\"5.7\"\r\n                        style=\"border-bottom:1px solid whitesmoke;padding-top: 22px;padding-bottom: unset\">\r\n                        <div>\r\n                            <ion-input type=\"text\" placeholder=\"Phone number\" style=\"color:white\"\r\n                                formControlName=\"compName\"></ion-input>\r\n                        </div>\r\n                    </ion-col>\r\n                    <ion-col size=\"2\" offset=\"1\" style=\"color:#ba7203;position: relative;padding-top:1px;\">\r\n                        <button class=\"buttonArmoron\"\r\n                            style=\"border-radius:50px;background-color:#6abd51;position:relative; top:24px;\"\r\n                            (click)=\"generateOTP()\">\r\n                            <ion-icon name=\"send\" class=\"iconsize\"></ion-icon>\r\n                        </button>\r\n                    </ion-col>\r\n                </ion-row>\r\n\r\n                <ion-row style=\"margin-top: 32px;\">\r\n                    <ion-col size=\"2\">\r\n                        <div style=\"padding-bottom: unset;text-align: right;padding-top: 10px;\">\r\n                            <ion-icon name=\"mail\" style=\"color:whitesmoke\" class=\"iconsize\"></ion-icon>\r\n                        </div>\r\n                    </ion-col>\r\n                    <ion-col size=\"7\" style=\"padding-bottom: unset;\">\r\n                        <div>\r\n                            <ion-input type=\"text\" placeholder=\"OTP\" formControlName=\"password\"\r\n                                style=\"color:white;border-bottom:1px solid whitesmoke;\"></ion-input>\r\n                        </div>\r\n                    </ion-col>\r\n                    <ion-col size=\"2\" offset=\"1\" style=\"color:#ba7203;\">\r\n                        <button class=\"buttonArmoron\"\r\n                            style=\"border-radius:50px;background-color:#6abd51;padding-bottom: unset;\"\r\n                            (click)=\"verifyOTPMethod(login.value.password, login.value.compName)\">\r\n                            <ion-icon name=\"checkmark\" class=\"iconsize\"></ion-icon>\r\n                        </button>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </form>\r\n        </ion-grid>\r\n    </div>\r\n\r\n    <div *ngIf='myPlatform != \"desktop\" && appName != \"Armoron\" && appName === \"ParentApp\"'>\r\n        <app-student-login></app-student-login>\r\n    </div>\r\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/parent-app/student-login/student-login.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/parent-app/student-login/student-login.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"login\">\n  <ion-card>\n    <ion-item>\n      <ion-icon class=\"placeholder-label\" slot=\"start\" name=\"phone-portrait\"></ion-icon>\n      <ion-label style=\"color: #b1b1b1;\" position=\"stacked\">MOBILE NO</ion-label>\n      <ion-input type=\"number\" formControlName=\"mobileNo\"></ion-input>\n    </ion-item>\n  </ion-card>\n  <ion-card *ngIf=\"button =='Verify'\">\n    <ion-item>\n      <ion-icon class=\"placeholder-label\" slot=\"start\" name=\"key\"></ion-icon>\n      <ion-label style=\"color: #b1b1b1;\" position=\"stacked\">OTP</ion-label>\n      <ion-input formControlName=\"otp\"></ion-input>\n    </ion-item>\n  </ion-card>\n  <ion-row *ngIf=\"button =='Verify'\" class=\"resend-otp\" (click)=\"sendOtp()\"> \n    Resend OTP?\n  </ion-row>\n</form>\n<ion-row style=\"justify-content: center;\">\n  <ion-button class=\"button-color\" shape=\"round\" (click)=\"loginSubmit()\">{{button}}</ion-button>\n</ion-row>\n<!-- <ion-row id=\"divOuter\">\n  <ion-col id=\"divInner\">\n     <ion-input formControlName=\"otp\" id=\"partitioned\" type=\"text\" maxlength=\"4\"\n          oninput=\"this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\\..*)\\./g, '$1');\"\n          onKeyPress=\"if(this.value.length==) return false;\"></ion-input>\n  </ion-col>\n</ion-row> -->"

/***/ }),

/***/ "./src/app/delar-application/login/login-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/delar-application/login/login-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/delar-application/login/login.page.ts");




var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
var LoginPageRoutingModule = /** @class */ (function () {
    function LoginPageRoutingModule() {
    }
    LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], LoginPageRoutingModule);
    return LoginPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/delar-application/login/login.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/delar-application/login/login.module.ts ***!
  \*********************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/delar-application/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login.page */ "./src/app/delar-application/login/login.page.ts");








var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _login_routing_module__WEBPACK_IMPORTED_MODULE_6__["LoginPageRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["IonicStorageModule"].forRoot()
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_7__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/delar-application/login/login.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/delar-application/login/login.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".moveAndTrackSpacing {\n  margin-top: 12%;\n}\n\n.labelSpacing {\n  margin-bottom: 3%;\n}\n\n.rememberPass {\n  text-align: right;\n  position: absolute;\n  right: 5px;\n  top: -3px;\n}\n\n.forgetPass {\n  text-align: right;\n  position: absolute;\n  right: 0px;\n  top: 1px;\n}\n\n.check {\n  width: 14px;\n  height: 14px;\n  position: absolute;\n  right: 85px;\n  top: -11px;\n}\n\n.noWhiteSpace {\n  padding: 0px;\n}\n\n.inputText {\n  padding: 0px;\n  margin: 0px;\n  font-size: 12px;\n  background-color: \"none\";\n}\n\nion-input .native-input {\n  border-radius: 25px;\n}\n\n.Tracalogic {\n  --background:rgb(255, 174, 0);\n}\n\n.Moveandtrack {\n  --background:rgb(237, 27, 36);\n}\n\n.GE {\n  --background:rgb(255, 174, 0);\n}\n\n.Upcot-mvt {\n  --background:rgb(237, 27, 36);\n}\n\n.Remoncloud {\n  --background:rgb(237, 27, 36);\n}\n\n.moveAndTrackSpacing {\n  margin-top: 12%;\n}\n\n.labelSpacing {\n  margin-bottom: 3%;\n  margin-left: 6%;\n  width: 85%;\n}\n\n.check {\n  width: 12px;\n  height: 12px;\n  margin-right: 10px;\n}\n\n.backgroundCol {\n  background: #32384b;\n}\n\n.noWhiteSpace {\n  padding: 0px;\n}\n\n.iconSize {\n  width: 50px;\n  height: 35px;\n}\n\n.appDarkBackground {\n  background: #32384b;\n}\n\n.label-shape {\n  border-radius: 40px;\n  background-color: none;\n  height: 40px;\n}\n\n.label-shape-box {\n  background-color: none;\n  height: 40px;\n  margin-bottom: 3%;\n}\n\n.input-spacing {\n  top: 5vh;\n  right: 2vh;\n}\n\n.input-spacing {\n  float: right;\n}\n\n.web-input {\n  --padding-top: 4px;\n  font-size: 14px;\n  font-weight: 600;\n  font-family: system-ui;\n}\n\nion-input .native-input {\n  border-radius: 25px;\n}\n\n.Tracalogic {\n  --background:rgb(255, 174, 0);\n}\n\n.Moveandtrack {\n  --background:rgb(237, 27, 36);\n}\n\n.GE {\n  --background:rgb(255, 174, 0);\n}\n\n.moveAndTrackSpacing {\n  opacity: 0.9;\n  width: 300px;\n  margin: auto;\n  position: relative;\n  top: 50px;\n  border-radius: 5px;\n}\n\n.container {\n  margin: 0px;\n  height: 100vh;\n  display: block;\n}\n\n.left-side-form-grp {\n  width: 20%;\n  /* height: 6%; */\n  /* float: right; */\n  position: fixed;\n  top: 10vh;\n  right: 2vh;\n}\n\n.container-wrapper {\n  margin: 0px;\n  height: 100vh;\n  display: block;\n  background: url(\"https://kingstrackimages.s3.amazonaws.com/loginimages/track_apmkingstrack_com_background.jpg\") center center/cover no-repeat fixed;\n}\n\n.labelSpacing {\n  margin-bottom: 3%;\n  margin-left: 6%;\n  width: 85%;\n}\n\n.forgotPass {\n  padding-left: 50px;\n  font-size: 12px;\n}\n\n.forgot {\n  font-size: 12px;\n}\n\n.svg_img {\n  width: 20px;\n  padding: 0px 0px 6px;\n}\n\n.iconSize {\n  width: 50px;\n  height: 35px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9kZWxhci1hcHBsaWNhdGlvbi9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBYUE7RUFDSSxlQUFBO0FDWko7O0FEY0k7RUFDSSxpQkFBQTtBQ1hSOztBRGNJO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FDWFI7O0FEYUk7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUNWUjs7QURhSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQ1ZSOztBRGFJO0VBQ0ksWUFBQTtBQ1ZSOztBRFlJO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esd0JBQUE7QUNUSjs7QURjSTtFQUNJLG1CQUFBO0FDWFI7O0FEY0E7RUFDSSw2QkFBQTtBQ1hKOztBRGFBO0VBQ0ksNkJBQUE7QUNWSjs7QURZQTtFQUNJLDZCQUFBO0FDVEo7O0FEV0E7RUFDSSw2QkFBQTtBQ1JKOztBRFVBO0VBQ0ksNkJBQUE7QUNQSjs7QURTQTtFQUNJLGVBQUE7QUNOSjs7QURRQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNMSjs7QURZQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNUSjs7QURZQTtFQUNJLG1CQUFBO0FDVEo7O0FEWUE7RUFDSSxZQUFBO0FDVEo7O0FEWUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1RKOztBRGNBO0VBQ0ksbUJBQUE7QUNYSjs7QURrQkE7RUFDSSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtBQ2ZKOztBRGtCQTtFQUNJLHNCQUFBO0VBQ0EsWUFBQTtFQUVBLGlCQUFBO0FDaEJKOztBRGtCQTtFQUNJLFFBQUE7RUFDQSxVQUFBO0FDZko7O0FEaUJBO0VBQ0ksWUFBQTtBQ2RKOztBRGdCQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUNiSjs7QURnQkk7RUFDSSxtQkFBQTtBQ2JSOztBRGdCQTtFQUNJLDZCQUFBO0FDYko7O0FEZUE7RUFDSSw2QkFBQTtBQ1pKOztBRGNBO0VBQ0ksNkJBQUE7QUNYSjs7QURlQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FDWko7O0FEZ0JBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDYko7O0FEbUJBO0VBQ0ksVUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUNoQko7O0FEa0JBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUpBQUE7QUNmSjs7QURpQkE7RUFDSSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDZEo7O0FEaUJBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FDZEo7O0FEZ0JBO0VBQ0ksZUFBQTtBQ2JKOztBRGVBO0VBQ0ksV0FBQTtFQUNBLG9CQUFBO0FDWko7O0FEY0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1hKIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi8vIGlvbi1pbnB1dCB7XHJcbi8vICAgICAubmF0aXZlLWlucHV0IHtcclxuLy8gICAgICAgICBib3JkZXItcmFkaXVzOiAyNXB4XHJcbi8vICAgICB9XHJcbi8vIH1cclxuLy8gLnBhZGRpbmd7XHJcbi8vICAgICBwYWRkaW5nLXRvcDogNjBweDtcclxuLy8gfVxyXG4vLyAudGV4dEJveHtcclxuLy8gICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbi8vIH1cclxuXHJcbi5tb3ZlQW5kVHJhY2tTcGFjaW5ne1xyXG4gICAgbWFyZ2luLXRvcDogMTIlO1xyXG59XHJcbiAgICAubGFiZWxTcGFjaW5ne1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDMlO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAucmVtZW1iZXJQYXNze1xyXG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDogNXB4O1xyXG4gICAgICAgIHRvcDogLTNweDtcclxuICAgIH1cclxuICAgIC5mb3JnZXRQYXNze1xyXG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDowcHg7XHJcbiAgICAgICAgdG9wOjFweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmNoZWNrIHtcclxuICAgICAgICB3aWR0aDogMTRweDtcclxuICAgICAgICBoZWlnaHQ6IDE0cHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHJpZ2h0Ojg1cHg7XHJcbiAgICAgICAgdG9wOi0xMXB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubm9XaGl0ZVNwYWNle1xyXG4gICAgICAgIHBhZGRpbmc6MHB4O1xyXG4gICAgfVxyXG4gICAgLmlucHV0VGV4dHtcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogXCJub25lXCI7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbmlvbi1pbnB1dCB7XHJcbiAgICAubmF0aXZlLWlucHV0IHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAyNXB4XHJcbiAgICB9XHJcbn1cclxuLlRyYWNhbG9naWN7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcclxufVxyXG4uTW92ZWFuZHRyYWNre1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XHJcbn1cclxuLkdFe1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYigyNTUsIDE3NCwgMCk7XHJcbn1cclxuLlVwY290LW12dHtcclxuICAgIC0tYmFja2dyb3VuZDpyZ2IoMjM3LCAyNywgMzYpO1xyXG59XHJcbi5SZW1vbmNsb3Vke1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XHJcbn1cclxuLm1vdmVBbmRUcmFja1NwYWNpbmd7XHJcbiAgICBtYXJnaW4tdG9wOiAxMiU7XHJcbn1cclxuLmxhYmVsU3BhY2luZ3tcclxuICAgIG1hcmdpbi1ib3R0b206IDMlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDYlO1xyXG4gICAgd2lkdGg6IDg1JTtcclxufVxyXG4vLyAuZm9yZ290UGFzcyB7XHJcbi8vICAgICBwYWRkaW5nLWxlZnQ6IDUwcHg7XHJcbi8vICAgICBmb250LXNpemU6IDExcHg7XHJcbi8vIH1cclxuXHJcbi5jaGVjayB7XHJcbiAgICB3aWR0aDogMTJweDtcclxuICAgIGhlaWdodDogMTJweDtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweFxyXG59XHJcblxyXG4uYmFja2dyb3VuZENvbHtcclxuICAgIGJhY2tncm91bmQ6IzMyMzg0YjtcclxufVxyXG5cclxuLm5vV2hpdGVTcGFjZXtcclxuICAgIHBhZGRpbmc6MHB4O1xyXG59XHJcblxyXG4uaWNvblNpemV7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogMzVweDtcclxufVxyXG4vLyAuYXBwSW1hZ2VCYWNrZ3JvdW5ke1xyXG4vLyAgICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2JhY2tncm91bmdJbWFnZS9EZWZhdWx0LVBvcnRyYWl0QH5pcGFkcHJvLnBuZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XHJcbi8vIH1cclxuLmFwcERhcmtCYWNrZ3JvdW5ke1xyXG4gICAgYmFja2dyb3VuZDojMzIzODRiO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL3dlYi8vLy8vLy8vLy8vLy8vLy8vLy9AYXQtcm9vdFxyXG4ubGFiZWwtc2hhcGV7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbm9uZTtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIC8vd2lkdGg6IDQyMHB4OyAgXHJcbn1cclxuLmxhYmVsLXNoYXBlLWJveHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IG5vbmU7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAvLyB3aWR0aDoyMCU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzJTtcclxufVxyXG4uaW5wdXQtc3BhY2luZ3tcclxuICAgIHRvcDogNXZoO1xyXG4gICAgcmlnaHQ6IDJ2aDtcclxufVxyXG4uaW5wdXQtc3BhY2luZ3tcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG4ud2ViLWlucHV0e1xyXG4gICAgLS1wYWRkaW5nLXRvcDogNHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBzeXN0ZW0tdWk7XHJcbn1cclxuaW9uLWlucHV0IHtcclxuICAgIC5uYXRpdmUtaW5wdXQge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHhcclxuICAgIH1cclxufVxyXG4uVHJhY2Fsb2dpY3tcclxuICAgIC0tYmFja2dyb3VuZDpyZ2IoMjU1LCAxNzQsIDApO1xyXG59XHJcbi5Nb3ZlYW5kdHJhY2t7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiKDIzNywgMjcsIDM2KTtcclxufVxyXG4uR0V7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcclxufVxyXG5cclxuXHJcbi5tb3ZlQW5kVHJhY2tTcGFjaW5ne1xyXG4gICAgb3BhY2l0eTogMC45O1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiA1MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgXHJcbn1cclxuXHJcbi5jb250YWluZXJ7XHJcbiAgICBtYXJnaW46IDBweDtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIC8vIGJhY2tncm91bmQ6IHVybChcImh0dHBzOi8va2luZ3N0cmFja2ltYWdlcy5zMy5hbWF6b25hd3MuY29tL2xvZ2luaW1hZ2VzL3RyYWNrX2FwbWtpbmdzdHJhY2tfY29tX2JhY2tncm91bmQuanBnXCIpIGNlbnRlciBjZW50ZXIgLyBjb3ZlciBuby1yZXBlYXQgZml4ZWQ7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOnVybChcIi4uLy4uL2Fzc2V0cy9iYWNrZ3JvdW5kX2ltZy93b29kaG91c2UuanBnXCIpIGNlbnRlciBjZW50ZXIgLyBjb3ZlciBuby1yZXBlYXQgZml4ZWQ7XHJcbiAgIFxyXG59XHJcblxyXG4ubGVmdC1zaWRlLWZvcm0tZ3Jwe1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIC8qIGhlaWdodDogNiU7ICovXHJcbiAgICAvKiBmbG9hdDogcmlnaHQ7ICovXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB0b3A6IDEwdmg7XHJcbiAgICByaWdodDogMnZoO1xyXG59XHJcbi5jb250YWluZXItd3JhcHBlcntcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgYmFja2dyb3VuZDogdXJsKFwiaHR0cHM6Ly9raW5nc3RyYWNraW1hZ2VzLnMzLmFtYXpvbmF3cy5jb20vbG9naW5pbWFnZXMvdHJhY2tfYXBta2luZ3N0cmFja19jb21fYmFja2dyb3VuZC5qcGdcIikgY2VudGVyIGNlbnRlciAvIGNvdmVyIG5vLXJlcGVhdCBmaXhlZDsgIFxyXG59XHJcbi5sYWJlbFNwYWNpbmd7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzJTtcclxuICAgIG1hcmdpbi1sZWZ0OiA2JTtcclxuICAgIHdpZHRoOiA4NSU7XHJcbn1cclxuXHJcbi5mb3Jnb3RQYXNzIHtcclxuICAgIHBhZGRpbmctbGVmdDogNTBweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG4uZm9yZ290e1xyXG4gICAgZm9udC1zaXplOiAxMnB4OyBcclxufVxyXG4uc3ZnX2ltZ3tcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDBweCA2cHg7XHJcbn1cclxuLmljb25TaXple1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDM1cHg7XHJcbn0iLCIubW92ZUFuZFRyYWNrU3BhY2luZyB7XG4gIG1hcmdpbi10b3A6IDEyJTtcbn1cblxuLmxhYmVsU3BhY2luZyB7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xufVxuXG4ucmVtZW1iZXJQYXNzIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDVweDtcbiAgdG9wOiAtM3B4O1xufVxuXG4uZm9yZ2V0UGFzcyB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwcHg7XG4gIHRvcDogMXB4O1xufVxuXG4uY2hlY2sge1xuICB3aWR0aDogMTRweDtcbiAgaGVpZ2h0OiAxNHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiA4NXB4O1xuICB0b3A6IC0xMXB4O1xufVxuXG4ubm9XaGl0ZVNwYWNlIHtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4uaW5wdXRUZXh0IHtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBcIm5vbmVcIjtcbn1cblxuaW9uLWlucHV0IC5uYXRpdmUtaW5wdXQge1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG4uVHJhY2Fsb2dpYyB7XG4gIC0tYmFja2dyb3VuZDpyZ2IoMjU1LCAxNzQsIDApO1xufVxuXG4uTW92ZWFuZHRyYWNrIHtcbiAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XG59XG5cbi5HRSB7XG4gIC0tYmFja2dyb3VuZDpyZ2IoMjU1LCAxNzQsIDApO1xufVxuXG4uVXBjb3QtbXZ0IHtcbiAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XG59XG5cbi5SZW1vbmNsb3VkIHtcbiAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XG59XG5cbi5tb3ZlQW5kVHJhY2tTcGFjaW5nIHtcbiAgbWFyZ2luLXRvcDogMTIlO1xufVxuXG4ubGFiZWxTcGFjaW5nIHtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG4gIG1hcmdpbi1sZWZ0OiA2JTtcbiAgd2lkdGg6IDg1JTtcbn1cblxuLmNoZWNrIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4uYmFja2dyb3VuZENvbCB7XG4gIGJhY2tncm91bmQ6ICMzMjM4NGI7XG59XG5cbi5ub1doaXRlU3BhY2Uge1xuICBwYWRkaW5nOiAwcHg7XG59XG5cbi5pY29uU2l6ZSB7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDM1cHg7XG59XG5cbi5hcHBEYXJrQmFja2dyb3VuZCB7XG4gIGJhY2tncm91bmQ6ICMzMjM4NGI7XG59XG5cbi5sYWJlbC1zaGFwZSB7XG4gIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IG5vbmU7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLmxhYmVsLXNoYXBlLWJveCB7XG4gIGJhY2tncm91bmQtY29sb3I6IG5vbmU7XG4gIGhlaWdodDogNDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG59XG5cbi5pbnB1dC1zcGFjaW5nIHtcbiAgdG9wOiA1dmg7XG4gIHJpZ2h0OiAydmg7XG59XG5cbi5pbnB1dC1zcGFjaW5nIHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4ud2ViLWlucHV0IHtcbiAgLS1wYWRkaW5nLXRvcDogNHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtZmFtaWx5OiBzeXN0ZW0tdWk7XG59XG5cbmlvbi1pbnB1dCAubmF0aXZlLWlucHV0IHtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLlRyYWNhbG9naWMge1xuICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcbn1cblxuLk1vdmVhbmR0cmFjayB7XG4gIC0tYmFja2dyb3VuZDpyZ2IoMjM3LCAyNywgMzYpO1xufVxuXG4uR0Uge1xuICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcbn1cblxuLm1vdmVBbmRUcmFja1NwYWNpbmcge1xuICBvcGFjaXR5OiAwLjk7XG4gIHdpZHRoOiAzMDBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogNTBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uY29udGFpbmVyIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGhlaWdodDogMTAwdmg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4ubGVmdC1zaWRlLWZvcm0tZ3JwIHtcbiAgd2lkdGg6IDIwJTtcbiAgLyogaGVpZ2h0OiA2JTsgKi9cbiAgLyogZmxvYXQ6IHJpZ2h0OyAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMTB2aDtcbiAgcmlnaHQ6IDJ2aDtcbn1cblxuLmNvbnRhaW5lci13cmFwcGVyIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGhlaWdodDogMTAwdmg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCJodHRwczovL2tpbmdzdHJhY2tpbWFnZXMuczMuYW1hem9uYXdzLmNvbS9sb2dpbmltYWdlcy90cmFja19hcG1raW5nc3RyYWNrX2NvbV9iYWNrZ3JvdW5kLmpwZ1wiKSBjZW50ZXIgY2VudGVyL2NvdmVyIG5vLXJlcGVhdCBmaXhlZDtcbn1cblxuLmxhYmVsU3BhY2luZyB7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xuICBtYXJnaW4tbGVmdDogNiU7XG4gIHdpZHRoOiA4NSU7XG59XG5cbi5mb3Jnb3RQYXNzIHtcbiAgcGFkZGluZy1sZWZ0OiA1MHB4O1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5mb3Jnb3Qge1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5zdmdfaW1nIHtcbiAgd2lkdGg6IDIwcHg7XG4gIHBhZGRpbmc6IDBweCAwcHggNnB4O1xufVxuXG4uaWNvblNpemUge1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiAzNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/delar-application/login/login.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/delar-application/login/login.page.ts ***!
  \*******************************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");








var LoginPage = /** @class */ (function () {
    function LoginPage(router, platform, formBuilder, alertController, menuController, ajaxService, commonService) {
        var _this = this;
        this.router = router;
        this.platform = platform;
        this.formBuilder = formBuilder;
        this.alertController = alertController;
        this.menuController = menuController;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.logo = 'assets/APM KT LOGO.png';
        this.eye_icon = "eye-off";
        this.password_type = "password";
        this.showHidePass = function () {
            _this.password_type = _this.password_type === "text" ? "password" : "text";
            _this.eye_icon = _this.eye_icon === "eye" ? "eye-off" : "eye";
        };
    }
    LoginPage.prototype.changeModule = function (data) {
        if (data == "user") {
            this.router.navigateByUrl("tabs-login/members/login");
        }
        else {
            this.router.navigateByUrl("tabs-login/dealarlogin/login");
        }
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        this.yudsegment = "sample1";
        localStorage.clear();
    };
    LoginPage.prototype.ngOnInit = function () {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        this.logo = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["app"].loginImgUrl;
        localStorage.clear();
        this.menuController.enable(false);
        this.login = this.formBuilder.group({
            userName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    LoginPage.prototype.submitLogin = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var url;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.commonService.presentLoader();
                this.loginCrd = {
                    userId: this.login.value.userName,
                    password: this.login.value.password,
                    version: "v2"
                };
                // tslint:disable-next-line: max-line-length
                if (this.loginCrd.userId == null || this.loginCrd.userId === '' || this.loginCrd.userId === undefined && this.loginCrd.password == null || this.loginCrd.password === undefined) {
                    // this.commonService.dismissLoader();
                    this.commonService.presentToast('User id and password is empty');
                }
                else {
                    url = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["serverUrl"].web + '/global/checkAuthenticateDealer';
                    this.ajaxService.ajaxPostWithBody(url, this.loginCrd)
                        .subscribe(function (res) {
                        if (res !== undefined) {
                            if (res.message == "Invalid User") {
                                if (_this.commonService.isLoading)
                                    _this.commonService.dismissLoader();
                                _this.commonService.presentToast('Invalid User name and password');
                            }
                            else {
                                console.log(res);
                                _this.commonService.updateLogo(res);
                                localStorage.setItem('dealerLoginData', JSON.stringify(res));
                                localStorage.setItem('companyId', res.companyId);
                                localStorage.setItem('companySuffix', res.companySuffix);
                                localStorage.setItem('corpId', res.companyId);
                                localStorage.setItem('userName', res.userId);
                                localStorage.setItem('userId', res.userId);
                                localStorage.setItem('password', res.password);
                                // localStorage.setItem('mainMenu', res.mainMenu);
                                localStorage.setItem('mainMenu', res.mainmenu);
                                // if(this.myPlatform == "desktop")
                                // this.router.navigateByUrl('/dashboard');
                                // else
                                _this.router.navigateByUrl('/tabs-login/new-dashboard');
                                //this.authService.login();
                            }
                        }
                        else if (res.message == "Invalid User") {
                            if (_this.commonService.isLoading)
                                _this.commonService.dismissLoader();
                            _this.commonService.presentToast('Invalid User name and password');
                        }
                        else if (res.error) {
                            if (_this.commonService.isLoading)
                                _this.commonService.dismissLoader();
                            var error = res.error;
                            if (error.text === "Invalid") {
                                _this.commonService.presentToast('Invalid User name and password');
                            }
                            else if (error.text === "Update") {
                                _this.commonService.presentToast('Get Lastest Application');
                            }
                            else if (error.text === "Error") {
                                _this.commonService.presentToast('Something Wrong Please Contact Support Team');
                            }
                            else {
                                _this.commonService.presentToast('Please check your internet connection!');
                            }
                        }
                    });
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"] },
        { type: _services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/delar-application/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"],
            _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"],
            _services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/login/forgotpass-modal/forgotpass-modal.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/login/forgotpass-modal/forgotpass-modal.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".labelSpacing {\n  margin-bottom: 3%;\n  margin-left: 6%;\n  width: 85%;\n}\n\n.margin {\n  margin-top: 3%;\n}\n\n.Tracalogic {\n  --background:rgb(255, 174, 0);\n}\n\n.Moveandtrack {\n  --background:rgb(237, 27, 36);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvbG9naW4vZm9yZ290cGFzcy1tb2RhbC9mb3Jnb3RwYXNzLW1vZGFsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbG9naW4vZm9yZ290cGFzcy1tb2RhbC9mb3Jnb3RwYXNzLW1vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNDSjs7QURDQTtFQUNJLGNBQUE7QUNFSjs7QURBQTtFQUNJLDZCQUFBO0FDR0o7O0FEREE7RUFDSSw2QkFBQTtBQ0lKIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vZm9yZ290cGFzcy1tb2RhbC9mb3Jnb3RwYXNzLW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sYWJlbFNwYWNpbmd7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzJTtcclxuICAgIG1hcmdpbi1sZWZ0OiA2JTtcclxuICAgIHdpZHRoOiA4NSU7XHJcbn1cclxuLm1hcmdpbntcclxuICAgIG1hcmdpbi10b3A6IDMlXHJcbn1cclxuLlRyYWNhbG9naWN7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcclxufVxyXG4uTW92ZWFuZHRyYWNre1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XHJcbn0iLCIubGFiZWxTcGFjaW5nIHtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG4gIG1hcmdpbi1sZWZ0OiA2JTtcbiAgd2lkdGg6IDg1JTtcbn1cblxuLm1hcmdpbiB7XG4gIG1hcmdpbi10b3A6IDMlO1xufVxuXG4uVHJhY2Fsb2dpYyB7XG4gIC0tYmFja2dyb3VuZDpyZ2IoMjU1LCAxNzQsIDApO1xufVxuXG4uTW92ZWFuZHRyYWNrIHtcbiAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XG59Il19 */"

/***/ }),

/***/ "./src/app/login/forgotpass-modal/forgotpass-modal.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/login/forgotpass-modal/forgotpass-modal.page.ts ***!
  \*****************************************************************/
/*! exports provided: ForgotpassModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpassModalPage", function() { return ForgotpassModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");







var ForgotpassModalPage = /** @class */ (function () {
    function ForgotpassModalPage(formBuilder, modalController, ajaxService, commonService) {
        this.formBuilder = formBuilder;
        this.modalController = modalController;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.color = {
            "Tracalogic": 'rgb(255, 174, 0)',
            "Moveandtrack": 'rgb(237, 27, 36)'
        };
        this.showorhideInput = true;
    }
    ForgotpassModalPage.prototype.getBack = function () {
        this.modalController.dismiss();
    };
    ForgotpassModalPage.prototype.submitPass = function () {
        var _this = this;
        var body = {
            "username": this.forgetPass.value.compName,
            "corpid": this.forgetPass.value.compId,
            "emailId": this.forgetPass.value.emailId,
            "mobileNo": this.forgetPass.value.mobileNo
        };
        var url = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + "/login/company/branch/user/forgetpassword";
        this.ajaxService.ajaxPostWithString(url, body)
            .subscribe(function (res) {
            console.log(res);
            if (res) {
                if (res == "sucess")
                    _this.commonService.presentToast("Password send your represent mobile number");
                _this.modalController.dismiss();
            }
        });
    };
    ForgotpassModalPage.prototype.ngOnInit = function () {
        this.appName = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["app"].appName.replace(/ /g, "");
        this.forgetPass = this.formBuilder.group({
            emailId: [''],
            mobileNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            compName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            compId: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    ForgotpassModalPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"] },
        { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"] }
    ]; };
    ForgotpassModalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgotpass-modal',
            template: __webpack_require__(/*! raw-loader!./forgotpass-modal.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/forgotpass-modal/forgotpass-modal.page.html"),
            styles: [__webpack_require__(/*! ./forgotpass-modal.page.scss */ "./src/app/login/forgotpass-modal/forgotpass-modal.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"],
            src_app_services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"]])
    ], ForgotpassModalPage);
    return ForgotpassModalPage;
}());



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule, ForgotpassModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpassModalPageModule", function() { return ForgotpassModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_forgotpass_modal_forgotpass_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/forgotpass-modal/forgotpass-modal.page */ "./src/app/login/forgotpass-modal/forgotpass-modal.page.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");
/* harmony import */ var _parent_app_student_login_student_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../parent-app/student-login/student-login.component */ "./src/app/parent-app/student-login/student-login.component.ts");









var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_7__["LoginPage"]
    }
];
var fPassModal = [
    {
        path: '',
        component: _login_forgotpass_modal_forgotpass_modal_page__WEBPACK_IMPORTED_MODULE_6__["ForgotpassModalPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(fPassModal)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_7__["LoginPage"], _login_forgotpass_modal_forgotpass_modal_page__WEBPACK_IMPORTED_MODULE_6__["ForgotpassModalPage"], _parent_app_student_login_student_login_component__WEBPACK_IMPORTED_MODULE_8__["StudentLoginComponent"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

var ForgotpassModalPageModule = /** @class */ (function () {
    function ForgotpassModalPageModule() {
    }
    return ForgotpassModalPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-input .native-input {\n  border-radius: 25px;\n}\n\n.Tracalogic {\n  --background:rgb(255, 174, 0);\n}\n\n.Moveandtrack {\n  --background:rgb(237, 27, 36);\n}\n\n.GE {\n  --background:rgb(255, 174, 0);\n}\n\n.Upcot-mvt {\n  --background:rgb(237, 27, 36);\n}\n\n.Remoncloud {\n  --background:rgb(237, 27, 36);\n}\n\n.moveAndTrackSpacing {\n  margin-top: 12%;\n}\n\n.labelSpacing {\n  margin-bottom: 3%;\n  margin-left: 6%;\n  width: 85%;\n}\n\n.check {\n  width: 12px;\n  height: 12px;\n  margin-right: 10px;\n}\n\n.backgroundCol {\n  background: #32384b;\n}\n\n.noWhiteSpace {\n  padding: 0px;\n}\n\n.iconSize {\n  width: 50px;\n  height: 35px;\n}\n\n.appDarkBackground {\n  background: #32384b;\n}\n\n.label-shape {\n  border-radius: 40px;\n  background-color: none;\n  height: 40px;\n}\n\n.label-shape-box {\n  background-color: none;\n  height: 40px;\n  margin-bottom: 3%;\n}\n\n.input-spacing {\n  top: 5vh;\n  right: 2vh;\n}\n\n.input-spacing {\n  float: right;\n}\n\n.web-input {\n  --padding-top: 4px;\n  font-size: 14px;\n  font-weight: 600;\n  font-family: system-ui;\n}\n\nion-input .native-input {\n  border-radius: 25px;\n}\n\n.Tracalogic {\n  --background:rgb(255, 174, 0);\n}\n\n.Moveandtrack {\n  --background:rgb(237, 27, 36);\n}\n\n.GE {\n  --background:rgb(255, 174, 0);\n}\n\n.moveAndTrackSpacing {\n  opacity: 0.9;\n  width: 300px;\n  margin: auto;\n  position: relative;\n  top: 50px;\n  border-radius: 5px;\n}\n\n@media only screen and (min-width: 1024px) {\n  .moveAndTrackSpacing {\n    margin: auto;\n    position: relative;\n    top: 17vh;\n  }\n\n  .moveAndTrackSpacingNew {\n    position: absolute;\n    right: 8px;\n    top: 4vh;\n    background: #f1f1f170;\n  }\n}\n\n@media only screen and (max-width: 550px) {\n  .moveAndTrackSpacing {\n    position: relative;\n    top: 50px;\n  }\n}\n\n@media only screen and (min-width: 551px) {\n  .moveAndTrackSpacing {\n    width: 360px;\n  }\n\n  .moveAndTrackSpacingNew {\n    width: 360px;\n  }\n}\n\n.container {\n  margin: 0px;\n  height: 100vh;\n  display: block;\n}\n\n.text-box-spacing {\n  height: 60px;\n}\n\n.labelSpacing-web {\n  margin-bottom: 3%;\n}\n\n.left-side-form-grp {\n  width: 20%;\n  /* height: 6%; */\n  /* float: right; */\n  position: fixed;\n  top: 10vh;\n  right: 2vh;\n}\n\n.container-wrapper {\n  margin: 0px;\n  height: 100vh;\n  display: block;\n  background: url(\"https://kingstrackimages.s3.amazonaws.com/loginimages/track_apmkingstrack_com_background.jpg\") center center/cover no-repeat fixed;\n}\n\n.labelSpacing {\n  margin-bottom: 3%;\n  margin-left: 6%;\n  width: 85%;\n}\n\n.forgotPass {\n  font-size: 13px;\n}\n\n.forgot {\n  font-size: 12px;\n}\n\n.svg_img {\n  width: 20px;\n  padding: 0px 0px 6px;\n}\n\n.check {\n  width: 12px;\n  height: 12px;\n  margin-right: 10px;\n}\n\n.backgroundCol {\n  background: #32384b;\n}\n\n.noWhiteSpace {\n  padding: 0px;\n}\n\n.iconSize {\n  width: 50px;\n  height: 35px;\n}\n\n.appDarkBackground {\n  background: #32384b;\n}\n\n.background {\n  --background: url('background.jpg');\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.text-center {\n  text-align: center;\n}\n\n.buttonArmoron {\n  color: blanchedalmond;\n  height: 56px;\n  width: 56px;\n}\n\n.iconsize {\n  font-size: 30px;\n}\n\n.rowstyle {\n  border-bottom: 1px solid #ba7203;\n  margin-top: 10px;\n  margin-bottom: 20px;\n  font-size: 14px;\n}\n\n#languageIcon, #countryIcon {\n  font-size: 20px;\n}\n\n.contryCodeStyle {\n  color: whitesmoke;\n  border-bottom: 1px solid whitesmoke;\n  padding-top: 32px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxtQkFBQTtBQ0RSOztBRElBO0VBQ0ksNkJBQUE7QUNESjs7QURHQTtFQUNJLDZCQUFBO0FDQUo7O0FERUE7RUFDSSw2QkFBQTtBQ0NKOztBRENBO0VBQ0ksNkJBQUE7QUNFSjs7QURBQTtFQUNJLDZCQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0FDSUo7O0FERkE7RUFDSSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDS0o7O0FERUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNDSjs7QURJQTtFQUNJLG1CQUFBO0FDREo7O0FEUUE7RUFDSSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtBQ0xKOztBRFFBO0VBQ0ksc0JBQUE7RUFDQSxZQUFBO0VBRUEsaUJBQUE7QUNOSjs7QURRQTtFQUNJLFFBQUE7RUFDQSxVQUFBO0FDTEo7O0FET0E7RUFDSSxZQUFBO0FDSko7O0FETUE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0FDSEo7O0FETUk7RUFDSSxtQkFBQTtBQ0hSOztBRE1BO0VBQ0ksNkJBQUE7QUNISjs7QURLQTtFQUNJLDZCQUFBO0FDRko7O0FESUE7RUFDSSw2QkFBQTtBQ0RKOztBREtBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7QUNGSjs7QURNQTtFQUNJO0lBQ0ksWUFBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtFQ0hOOztFREtFO0lBQ0ksa0JBQUE7SUFDQSxVQUFBO0lBQ0EsUUFBQTtJQUNBLHFCQUFBO0VDRk47QUFDRjs7QURLRTtFQUNFO0lBQ0ksa0JBQUE7SUFDQSxTQUFBO0VDSE47QUFDRjs7QURNRTtFQUNFO0lBQ0ssWUFBQTtFQ0pQOztFRE1FO0lBQ0ksWUFBQTtFQ0hOO0FBQ0Y7O0FET0E7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNMSjs7QURXQTtFQUNBLFlBQUE7QUNSQTs7QURXQTtFQUNJLGlCQUFBO0FDUko7O0FEV0E7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQ1JKOztBRFVBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUpBQUE7QUNQSjs7QURTQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNOSjs7QURTQTtFQUVJLGVBQUE7QUNQSjs7QURTQTtFQUNJLGVBQUE7QUNOSjs7QURRQTtFQUNJLFdBQUE7RUFDQSxvQkFBQTtBQ0xKOztBRE9BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0pKOztBRE9BO0VBQ0ksbUJBQUE7QUNKSjs7QURPQTtFQUNJLFlBQUE7QUNKSjs7QURPQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDSko7O0FET0E7RUFDSSxtQkFBQTtBQ0pKOztBRFNBO0VBQ0ksbUNBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDTko7O0FEU0E7RUFDSSxrQkFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ05KOztBRFNBO0VBQ0ksZUFBQTtBQ05KOztBRFNBO0VBQ0ksZ0NBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ05KOztBRFNBO0VBQ0ksZUFBQTtBQ05KOztBRFFBO0VBQ0ksaUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGlCQUFBO0FDTEoiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW9uLWlucHV0IHtcclxuICAgIC5uYXRpdmUtaW5wdXQge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHhcclxuICAgIH1cclxufVxyXG4uVHJhY2Fsb2dpY3tcclxuICAgIC0tYmFja2dyb3VuZDpyZ2IoMjU1LCAxNzQsIDApO1xyXG59XHJcbi5Nb3ZlYW5kdHJhY2t7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiKDIzNywgMjcsIDM2KTtcclxufVxyXG4uR0V7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcclxufVxyXG4uVXBjb3QtbXZ0e1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYigyMzcsIDI3LCAzNik7XHJcbn1cclxuLlJlbW9uY2xvdWR7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiKDIzNywgMjcsIDM2KTtcclxufVxyXG4ubW92ZUFuZFRyYWNrU3BhY2luZ3tcclxuICAgIG1hcmdpbi10b3A6IDEyJTtcclxufVxyXG4ubGFiZWxTcGFjaW5ne1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMyU7XHJcbiAgICBtYXJnaW4tbGVmdDogNiU7XHJcbiAgICB3aWR0aDogODUlO1xyXG59XHJcbi8vIC5mb3Jnb3RQYXNzIHtcclxuLy8gICAgIHBhZGRpbmctbGVmdDogNTBweDtcclxuLy8gICAgIGZvbnQtc2l6ZTogMTFweDtcclxuLy8gfVxyXG5cclxuLmNoZWNrIHtcclxuICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgaGVpZ2h0OiAxMnB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4XHJcbn1cclxuXHJcbi5iYWNrZ3JvdW5kQ29se1xyXG4gICAgYmFja2dyb3VuZDojMzIzODRiO1xyXG59XHJcblxyXG4ubm9XaGl0ZVNwYWNle1xyXG4gICAgcGFkZGluZzowcHg7XHJcbn1cclxuXHJcbi5pY29uU2l6ZXtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG59XHJcbi8vIC5hcHBJbWFnZUJhY2tncm91bmR7XHJcbi8vICAgICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvYmFja2dyb3VuZ0ltYWdlL0RlZmF1bHQtUG9ydHJhaXRAfmlwYWRwcm8ucG5nKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuLy8gfVxyXG4uYXBwRGFya0JhY2tncm91bmR7XHJcbiAgICBiYWNrZ3JvdW5kOiMzMjM4NGI7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vd2ViLy8vLy8vLy8vLy8vLy8vLy8vL0BhdC1yb290XHJcbi5sYWJlbC1zaGFwZXtcclxuICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBub25lO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgLy93aWR0aDogNDIwcHg7ICBcclxufVxyXG4ubGFiZWwtc2hhcGUtYm94e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbm9uZTtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIC8vIHdpZHRoOjIwJTtcclxuICAgIG1hcmdpbi1ib3R0b206IDMlO1xyXG59XHJcbi5pbnB1dC1zcGFjaW5ne1xyXG4gICAgdG9wOiA1dmg7XHJcbiAgICByaWdodDogMnZoO1xyXG59XHJcbi5pbnB1dC1zcGFjaW5ne1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi53ZWItaW5wdXR7XHJcbiAgICAtLXBhZGRpbmctdG9wOiA0cHg7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgZm9udC1mYW1pbHk6IHN5c3RlbS11aTtcclxufVxyXG5pb24taW5wdXQge1xyXG4gICAgLm5hdGl2ZS1pbnB1dCB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjVweFxyXG4gICAgfVxyXG59XHJcbi5UcmFjYWxvZ2lje1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYigyNTUsIDE3NCwgMCk7XHJcbn1cclxuLk1vdmVhbmR0cmFja3tcclxuICAgIC0tYmFja2dyb3VuZDpyZ2IoMjM3LCAyNywgMzYpO1xyXG59XHJcbi5HRXtcclxuICAgIC0tYmFja2dyb3VuZDpyZ2IoMjU1LCAxNzQsIDApO1xyXG59XHJcblxyXG5cclxuLm1vdmVBbmRUcmFja1NwYWNpbmd7XHJcbiAgICBvcGFjaXR5OiAwLjk7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDUwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcclxuICAgIC5tb3ZlQW5kVHJhY2tTcGFjaW5nIHtcclxuICAgICAgICBtYXJnaW46YXV0bztcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOjE3dmg7XHJcbiAgICB9XHJcbiAgICAubW92ZUFuZFRyYWNrU3BhY2luZ05ldyB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHJpZ2h0OiA4cHg7XHJcbiAgICAgICAgdG9wOiA0dmg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2YxZjFmMTcwO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgfVxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTUwcHgpICB7XHJcbiAgICAubW92ZUFuZFRyYWNrU3BhY2luZyB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDogNTBweDtcclxuICAgIH1cclxuICAgIFxyXG4gIH1cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NTUxcHgpe1xyXG4gICAgLm1vdmVBbmRUcmFja1NwYWNpbmcge1xyXG4gICAgICAgICB3aWR0aDogMzYwcHg7XHJcbiAgICB9IFxyXG4gICAgLm1vdmVBbmRUcmFja1NwYWNpbmdOZXcge1xyXG4gICAgICAgIHdpZHRoOiAzNjBweDtcclxuICAgfSBcclxuICB9XHJcblxyXG5cclxuLmNvbnRhaW5lcntcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICBcclxuICAgIC8vIGJhY2tncm91bmQ6IHVybChcImh0dHBzOi8va2luZ3N0cmFja2ltYWdlcy5zMy5hbWF6b25hd3MuY29tL2xvZ2luaW1hZ2VzL3RyYWNrX2FwbWtpbmdzdHJhY2tfY29tX2JhY2tncm91bmQuanBnXCIpIGNlbnRlciBjZW50ZXIgLyBjb3ZlciBuby1yZXBlYXQgZml4ZWQ7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOnVybChcIi4uLy4uL2Fzc2V0cy9iYWNrZ3JvdW5kX2ltZy93b29kaG91c2UuanBnXCIpIGNlbnRlciBjZW50ZXIgLyBjb3ZlciBuby1yZXBlYXQgZml4ZWQ7XHJcbiAgIFxyXG59XHJcbi50ZXh0LWJveC1zcGFjaW5ne1xyXG5oZWlnaHQ6IDYwcHg7XHJcbn1cclxuXHJcbi5sYWJlbFNwYWNpbmctd2ViIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDMlO1xyXG59XHJcblxyXG4ubGVmdC1zaWRlLWZvcm0tZ3Jwe1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIC8qIGhlaWdodDogNiU7ICovXHJcbiAgICAvKiBmbG9hdDogcmlnaHQ7ICovXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB0b3A6IDEwdmg7XHJcbiAgICByaWdodDogMnZoO1xyXG59XHJcbi5jb250YWluZXItd3JhcHBlcntcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgYmFja2dyb3VuZDogdXJsKFwiaHR0cHM6Ly9raW5nc3RyYWNraW1hZ2VzLnMzLmFtYXpvbmF3cy5jb20vbG9naW5pbWFnZXMvdHJhY2tfYXBta2luZ3N0cmFja19jb21fYmFja2dyb3VuZC5qcGdcIikgY2VudGVyIGNlbnRlciAvIGNvdmVyIG5vLXJlcGVhdCBmaXhlZDsgIFxyXG59XHJcbi5sYWJlbFNwYWNpbmd7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzJTtcclxuICAgIG1hcmdpbi1sZWZ0OiA2JTtcclxuICAgIHdpZHRoOiA4NSU7XHJcbn1cclxuXHJcbi5mb3Jnb3RQYXNzIHtcclxuICAgIC8vIHBhZGRpbmctbGVmdDogNTBweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG4uZm9yZ290e1xyXG4gICAgZm9udC1zaXplOiAxMnB4OyBcclxufVxyXG4uc3ZnX2ltZ3tcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDBweCA2cHg7XHJcbn1cclxuLmNoZWNrIHtcclxuICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgaGVpZ2h0OiAxMnB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4XHJcbn1cclxuXHJcbi5iYWNrZ3JvdW5kQ29se1xyXG4gICAgYmFja2dyb3VuZDojMzIzODRiO1xyXG59XHJcblxyXG4ubm9XaGl0ZVNwYWNle1xyXG4gICAgcGFkZGluZzowcHg7XHJcbn1cclxuXHJcbi5pY29uU2l6ZXtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG59XHJcblxyXG4uYXBwRGFya0JhY2tncm91bmR7XHJcbiAgICBiYWNrZ3JvdW5kOiMzMjM4NGI7XHJcbn1cclxuXHJcbi8vLy9Bcm1vcm9uXHJcblxyXG4uYmFja2dyb3VuZCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi4uLy4uL2Fzc2V0cy9Bcm1vcm9uL2JhY2tncm91bmQuanBnXCIpO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn1cclxuXHJcbi50ZXh0LWNlbnRlciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5idXR0b25Bcm1vcm9uIHtcclxuICAgIGNvbG9yOiBibGFuY2hlZGFsbW9uZDtcclxuICAgIGhlaWdodDogNTZweDtcclxuICAgIHdpZHRoOiA1NnB4O1xyXG59XHJcblxyXG4uaWNvbnNpemUge1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG59XHJcblxyXG4ucm93c3R5bGUge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNiYTcyMDM7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuI2xhbmd1YWdlSWNvbiwgI2NvdW50cnlJY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG4uY29udHJ5Q29kZVN0eWxle1xyXG4gICAgY29sb3I6d2hpdGVzbW9rZTtcclxuICAgIGJvcmRlci1ib3R0b206MXB4IHNvbGlkIHdoaXRlc21va2U7XHJcbiAgICBwYWRkaW5nLXRvcDogMzJweDtcclxufSIsImlvbi1pbnB1dCAubmF0aXZlLWlucHV0IHtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLlRyYWNhbG9naWMge1xuICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcbn1cblxuLk1vdmVhbmR0cmFjayB7XG4gIC0tYmFja2dyb3VuZDpyZ2IoMjM3LCAyNywgMzYpO1xufVxuXG4uR0Uge1xuICAtLWJhY2tncm91bmQ6cmdiKDI1NSwgMTc0LCAwKTtcbn1cblxuLlVwY290LW12dCB7XG4gIC0tYmFja2dyb3VuZDpyZ2IoMjM3LCAyNywgMzYpO1xufVxuXG4uUmVtb25jbG91ZCB7XG4gIC0tYmFja2dyb3VuZDpyZ2IoMjM3LCAyNywgMzYpO1xufVxuXG4ubW92ZUFuZFRyYWNrU3BhY2luZyB7XG4gIG1hcmdpbi10b3A6IDEyJTtcbn1cblxuLmxhYmVsU3BhY2luZyB7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xuICBtYXJnaW4tbGVmdDogNiU7XG4gIHdpZHRoOiA4NSU7XG59XG5cbi5jaGVjayB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLmJhY2tncm91bmRDb2wge1xuICBiYWNrZ3JvdW5kOiAjMzIzODRiO1xufVxuXG4ubm9XaGl0ZVNwYWNlIHtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4uaWNvblNpemUge1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiAzNXB4O1xufVxuXG4uYXBwRGFya0JhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kOiAjMzIzODRiO1xufVxuXG4ubGFiZWwtc2hhcGUge1xuICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBub25lO1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi5sYWJlbC1zaGFwZS1ib3gge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBub25lO1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xufVxuXG4uaW5wdXQtc3BhY2luZyB7XG4gIHRvcDogNXZoO1xuICByaWdodDogMnZoO1xufVxuXG4uaW5wdXQtc3BhY2luZyB7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLndlYi1pbnB1dCB7XG4gIC0tcGFkZGluZy10b3A6IDRweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LWZhbWlseTogc3lzdGVtLXVpO1xufVxuXG5pb24taW5wdXQgLm5hdGl2ZS1pbnB1dCB7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbi5UcmFjYWxvZ2ljIHtcbiAgLS1iYWNrZ3JvdW5kOnJnYigyNTUsIDE3NCwgMCk7XG59XG5cbi5Nb3ZlYW5kdHJhY2sge1xuICAtLWJhY2tncm91bmQ6cmdiKDIzNywgMjcsIDM2KTtcbn1cblxuLkdFIHtcbiAgLS1iYWNrZ3JvdW5kOnJnYigyNTUsIDE3NCwgMCk7XG59XG5cbi5tb3ZlQW5kVHJhY2tTcGFjaW5nIHtcbiAgb3BhY2l0eTogMC45O1xuICB3aWR0aDogMzAwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcbiAgLm1vdmVBbmRUcmFja1NwYWNpbmcge1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAxN3ZoO1xuICB9XG5cbiAgLm1vdmVBbmRUcmFja1NwYWNpbmdOZXcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogOHB4O1xuICAgIHRvcDogNHZoO1xuICAgIGJhY2tncm91bmQ6ICNmMWYxZjE3MDtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1NTBweCkge1xuICAubW92ZUFuZFRyYWNrU3BhY2luZyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogNTBweDtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA1NTFweCkge1xuICAubW92ZUFuZFRyYWNrU3BhY2luZyB7XG4gICAgd2lkdGg6IDM2MHB4O1xuICB9XG5cbiAgLm1vdmVBbmRUcmFja1NwYWNpbmdOZXcge1xuICAgIHdpZHRoOiAzNjBweDtcbiAgfVxufVxuLmNvbnRhaW5lciB7XG4gIG1hcmdpbjogMHB4O1xuICBoZWlnaHQ6IDEwMHZoO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLnRleHQtYm94LXNwYWNpbmcge1xuICBoZWlnaHQ6IDYwcHg7XG59XG5cbi5sYWJlbFNwYWNpbmctd2ViIHtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG59XG5cbi5sZWZ0LXNpZGUtZm9ybS1ncnAge1xuICB3aWR0aDogMjAlO1xuICAvKiBoZWlnaHQ6IDYlOyAqL1xuICAvKiBmbG9hdDogcmlnaHQ7ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAxMHZoO1xuICByaWdodDogMnZoO1xufVxuXG4uY29udGFpbmVyLXdyYXBwZXIge1xuICBtYXJnaW46IDBweDtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6IHVybChcImh0dHBzOi8va2luZ3N0cmFja2ltYWdlcy5zMy5hbWF6b25hd3MuY29tL2xvZ2luaW1hZ2VzL3RyYWNrX2FwbWtpbmdzdHJhY2tfY29tX2JhY2tncm91bmQuanBnXCIpIGNlbnRlciBjZW50ZXIvY292ZXIgbm8tcmVwZWF0IGZpeGVkO1xufVxuXG4ubGFiZWxTcGFjaW5nIHtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG4gIG1hcmdpbi1sZWZ0OiA2JTtcbiAgd2lkdGg6IDg1JTtcbn1cblxuLmZvcmdvdFBhc3Mge1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbi5mb3Jnb3Qge1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5zdmdfaW1nIHtcbiAgd2lkdGg6IDIwcHg7XG4gIHBhZGRpbmc6IDBweCAwcHggNnB4O1xufVxuXG4uY2hlY2sge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5iYWNrZ3JvdW5kQ29sIHtcbiAgYmFja2dyb3VuZDogIzMyMzg0Yjtcbn1cblxuLm5vV2hpdGVTcGFjZSB7XG4gIHBhZGRpbmc6IDBweDtcbn1cblxuLmljb25TaXplIHtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogMzVweDtcbn1cblxuLmFwcERhcmtCYWNrZ3JvdW5kIHtcbiAgYmFja2dyb3VuZDogIzMyMzg0Yjtcbn1cblxuLmJhY2tncm91bmQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi4uLy4uL2Fzc2V0cy9Bcm1vcm9uL2JhY2tncm91bmQuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4udGV4dC1jZW50ZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5idXR0b25Bcm1vcm9uIHtcbiAgY29sb3I6IGJsYW5jaGVkYWxtb25kO1xuICBoZWlnaHQ6IDU2cHg7XG4gIHdpZHRoOiA1NnB4O1xufVxuXG4uaWNvbnNpemUge1xuICBmb250LXNpemU6IDMwcHg7XG59XG5cbi5yb3dzdHlsZSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYmE3MjAzO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbiNsYW5ndWFnZUljb24sICNjb3VudHJ5SWNvbiB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLmNvbnRyeUNvZGVTdHlsZSB7XG4gIGNvbG9yOiB3aGl0ZXNtb2tlO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGVzbW9rZTtcbiAgcGFkZGluZy10b3A6IDMycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_forgotpass_modal_forgotpass_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/forgotpass-modal/forgotpass-modal.page */ "./src/app/login/forgotpass-modal/forgotpass-modal.page.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _services_websocket_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/websocket.service */ "./src/app/services/websocket.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");













var LoginPage = /** @class */ (function () {
    function LoginPage(formBuilder, ajaxService, platform, router, modalController, commonService, websocketService, menuController, alertController, authService) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.ajaxService = ajaxService;
        this.platform = platform;
        this.router = router;
        this.modalController = modalController;
        this.commonService = commonService;
        this.websocketService = websocketService;
        this.menuController = menuController;
        this.alertController = alertController;
        this.authService = authService;
        this.isChecked = false;
        this.eye_icon = "eye-off";
        this.password_type = "password";
        this.exitPopup = false;
        this.armoronApp = {
            countryCode: "+91",
            selectedlanguage: "English",
            "country": "India"
        };
        this.languageOptions = [];
        this.showHidePass = function () {
            _this.password_type = _this.password_type === "text" ? "password" : "text";
            _this.eye_icon = _this.eye_icon === "eye" ? "eye-off" : "eye";
        };
        this.submitLogin = function () {
            // if (app.entryPoint == 'TTS') {
            _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint = "unknown";
            // }
            _this.commonService.presentLoader();
            var body = {
                "username": _this.login.value.compName,
                "password": _this.login.value.password,
                "corpid": _this.login.value.compId,
                "loginMode": "mobile",
                "entryPoint": _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint,
                "appsetting": "vts_mobile"
            };
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + "/login/company/login";
            _this.ajaxService.ajaxPostMethod(url, body)
                .subscribe(function (res) {
                console.log(res);
                if (res != undefined) {
                    if (res.length > 1) {
                        if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint == 'unknown') {
                            _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint = res[1].entryPoint;
                        }
                        if (_this.isChecked) {
                            document.cookie = "rememberme=yes;domain=" + window.location.hostname + ";path=/";
                            var remValue = { "compName": _this.login.value.compName, "password": _this.login.value.password, "corpid": _this.login.value.compId, "checked": _this.isChecked };
                            sessionStorage.setItem('rememberMe', JSON.stringify(remValue));
                        }
                        else {
                            document.cookie = "rememberme=no;domain=" + window.location.hostname + ";path=/";
                            if (localStorage.rememberMe) {
                                localStorage.removeItem('rememberMe');
                            }
                        }
                        var dashboardInput = {
                            "companyID": _this.login.value.compId,
                            "branchID": _this.login.value.compId,
                            "emailId": _this.login.value.compName,
                            "Check": false,
                            "entryPoint": _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint,
                            "pollingDuration": JSON.parse(res[1]["applicationSettings"]).pollingDuration,
                            "mode": "dashboardData",
                            "dashboardVin": "",
                            "defaultInterval": res[1]["applicationSettings"].liveTrackingDelay,
                            "make": "",
                            "model": "",
                            "delay": res[1]["applicationSettings"].liveTrackingDelay,
                            "ImeiNo": "",
                        };
                        var messagingServiceData = {
                            "companyId": _this.login.value.compId,
                            "logo": res[1]["logo"],
                            "entryPoint": res[1].entryPoint
                        };
                        _this.commonService.updateLogo(messagingServiceData);
                        _this.websocketService.connectSocket(dashboardInput, 'livetrack');
                        localStorage.setItem('companyLogo', res[1]["logo"]);
                        localStorage.setItem("mapAllowed", res[1]["mapAllowed"]);
                        localStorage.setItem('mainMenu', res[1]["mainmenu"]);
                        localStorage.setItem('dashboardWebSocketData', JSON.stringify(dashboardInput));
                        localStorage.setItem('loginData', JSON.stringify(res));
                        localStorage.setItem('staticIOData', JSON.stringify(res[0]["staticIODatas"]));
                        localStorage.setItem('appSettings', res[1]["applicationSettings"]);
                        // localStorage.setItem('map', JSON.parse(res[1]["applicationSettings"]).mapview);
                        localStorage.setItem('map', res[1]["mapview"]);
                        localStorage.setItem('corpId', _this.login.value.compId);
                        localStorage.setItem('userName', _this.login.value.compName);
                        localStorage.setItem('password', _this.login.value.password);
                        localStorage.setItem('commandsData', res[1]["CommandsData"]);
                        // this.router.navigateByUrl("/dashboard");
                        if (_this.myPlatform != "desktop") {
                            localStorage.setItem('inItPage', res[1]["initialPage"]);
                            sessionStorage.setItem('login', "true");
                            localStorage.setItem('login', "true");
                            _this.authService.login();
                            _this.commonService.dismissLoader();
                        }
                        else {
                            localStorage.setItem('inItPage', res[1]["initialPage"]);
                            if (res[1]["initialPage"] == 'dashboard' || res[1]["initialPage"] == undefined) {
                                _this.router.navigateByUrl("/tabs/members/dashboard");
                            }
                            else if (res[1]["initialPage"] == 'gridView') {
                                _this.router.navigateByUrl('tabs/gridview/All');
                            }
                            sessionStorage.setItem('login', "false");
                            localStorage.setItem('login', "false");
                        }
                        var url_1 = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/login/getPreferences?key=pdfDownloadLogo&companyId=' + _this.login.value.compId;
                        _this.ajaxService.ajaxGetPerference(url_1)
                            .subscribe(function (res) {
                            localStorage.setItem("pdfDownloadLogo", res);
                        });
                    }
                    else {
                        sessionStorage.setItem('login', "false");
                        localStorage.setItem('login', "false");
                        _this.commonService.dismissLoader();
                        _this.commonService.presentToast('Invalid credential');
                    }
                }
                else {
                    _this.commonService.dismissLoader();
                    var data = navigator.onLine;
                    if (data == false) {
                        _this.commonService.networkChecker();
                    }
                    else if (data == true) {
                        _this.ajaxService.ajaxGetWithString(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + "/login/test")
                            .subscribe(function (res) {
                            if (res == '["Hi Web....!"]')
                                console.log("server run");
                            else {
                                _this.commonService.dismissLoader();
                                _this.commonService.presentAlert("Server maintanance error", "Sorry for the inconvenience please try after some times");
                            }
                        });
                    }
                }
            });
        };
        this.generateOTP = function (cntrl) {
            _this.generatedOTP = Math
                .floor(Math.random() * 9000) + 1000;
            if (/(android|iPhone|iPad|iPod)/i.test(navigator.userAgent)) {
                if (_this.login.value.compName === '9600696008' || _this.login.value.compName === '7010017783') {
                    _this.verifyOTPMethod(_this.generatedOTP, _this.login.value.compName);
                }
                else {
                    _this.sendOTP();
                    //this.verifyOTPMethod(this.generatedOTP, this.login.value.compName);
                }
            }
            else {
                _this.verifyOTPMethod(_this.generatedOTP, _this.login.value.compName);
            }
        };
        this.sendOTP = function () {
            // this.platform.ready().then(() => {
            //   this.commonService.presentLoader();
            //   const messageData = this.OTPmessage + encodeURIComponent(this.generatedOTP);
            //   let smsAPI = localStorage.getItem('SMSAPI');
            //   smsAPI = smsAPI.replace('smsAPIMobileNumber',
            //     encodeURIComponent(this.login.value.compName)).replace('smsAPIMessageContent', messageData);
            //   this.ajaxService.ajaxGet(smsAPI)
            //     .subscribe(res => {
            //       console.log(res);
            //       this.commonService.presentToast('Enter your Otp');
            //       this.commonService.dismissLoader();
            //     }, err => {
            //       console.log(err);
            //       this.commonService.dismissLoader();
            //     });
            // });
            _this.platform.ready().then(function () {
                _this.commonService.presentLoader();
                var url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/parentapp/otp?message=your otp is ' + _this.generatedOTP + '&contact=' + _this.login.value.compName;
                _this.ajaxService.ajaxGet(url)
                    .subscribe(function (res) {
                    console.log(res);
                    _this.commonService.presentToast('Enter your Otp');
                    _this.commonService.dismissLoader();
                }, function (err) {
                    console.log(err);
                    _this.commonService.dismissLoader();
                });
            });
        };
        this.verifyOTPMethod = function (userotp, phoneNum) {
            if (phoneNum === undefined || phoneNum === '') {
                _this.commonService.presentAlert('Phone_Number', 'Please enter valid phone number');
            }
            else if (phoneNum !== undefined || phoneNum !== '') {
                if (userotp === '' || userotp === undefined) {
                    _this.commonService.presentAlert('OTP', 'Please enter the OTP');
                }
                else if (_this.generatedOTP == userotp
                    && _this.generatedOTP !== '' && _this.entryPoint !== undefined) {
                    _this.commonService.presentLoader();
                    if (phoneNum.charAt(0) === '0') {
                        phoneNum = phoneNum.substr(1);
                    }
                    //  this.armoronLogin();
                    var url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/login/checkcompany/' + _this.login.value.compName;
                    //const url = this._global.getUrlValue() + '/api/company/checkCompany/' + phoneNum;
                    _this.ajaxService.ajaxGetWithBody(url)
                        .subscribe(function (res) {
                        if (res == 'Unchanged') {
                            _this.armoronLogin(res);
                            // this.commonService.presentToast('contact support team');
                        }
                        else {
                            //  this.generateOTP('cnrl');
                            _this.newCompName = _this.login.value.compName;
                            _this.login.patchValue({
                                compName: res
                            });
                            _this.armoronLogin(res);
                        }
                        console.log(res);
                        // this.identifyCompany(res);
                    }, function (err) {
                        console.log(err);
                        _this.commonService.dismissLoader();
                    });
                }
                else {
                    _this.commonService.presentAlert('OTP', 'Invalid OTP');
                }
            }
        };
        this.identifyCompany = function (data) {
            if (data.indexOf('Error') !== -1 || data !== null || data !== 'null') {
                var dataConverted = Number(data);
                if (isNaN(dataConverted) === false) {
                    // localStorage.setItem('changedNumber', this.login.compName.toString());
                    // localStorage.setItem('PhoneNumber', data);
                    _this.login.compName = data;
                }
                var credentials = {
                    'companyName': _this.login.compName.toString(),
                    'password': _this.login.password.toString(),
                    'entryPoint': _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint
                };
                var url = '';
                // const url = serverUrl.web + '/login/checkcompany/'+this.login.value.compName;
                // const url = this._global.getUrlValue() + '/api/company/user';
                _this.ajaxService.ajaxGetWithString(url)
                    .subscribe(function (res) {
                    if (res == 'Unchanged') {
                        _this.commonService.presentToast('contact support team');
                    }
                    else {
                        _this.generateOTP('cnrl');
                    }
                    console.log(res);
                    _this.generateOTP('cnrl');
                }, function (err) {
                    console.log(err);
                    _this.commonService.dismissLoader();
                });
            }
            else {
                _this.commonService.presentAlert('Error', 'Try again after sometime.');
                _this.commonService.dismissLoader();
            }
        };
        this.armoronLogin = function (data) {
            var body = {
                "username": _this.login.value.compName,
                "password": _this.login.value.compName,
                "corpid": _this.login.value.compName,
                "loginMode": "mobile",
                "entryPoint": _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint,
                "appsetting": "vts_mobile"
            };
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + "/login/company/login";
            _this.ajaxService.ajaxPostMethod(url, body)
                .subscribe(function (res) {
                if (res.length > 1) {
                    localStorage.setItem('inItPage', res[1]["initialPage"]);
                    sessionStorage.setItem('login', "true");
                    localStorage.setItem('login', "true");
                    _this.authService.login();
                    var dashboardInput = {
                        "companyID": _this.login.value.compName,
                        "branchID": _this.login.value.compName,
                        "emailId": _this.login.value.compName,
                        "Check": false,
                        "entryPoint": _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint,
                        "pollingDuration": JSON.parse(res[1]["applicationSettings"]).pollingDuration,
                        "mode": "dashboardData",
                        "dashboardVin": "",
                        "defaultInterval": res[1]["applicationSettings"].liveTrackingDelay,
                        "make": "",
                        "model": "",
                        "delay": res[1]["applicationSettings"].liveTrackingDelay,
                        "ImeiNo": "",
                    };
                    var messagingServiceData = {
                        "companyId": _this.login.value.compName,
                        "logo": res[1]["logo"],
                        "entryPoint": res[1].entryPoint
                    };
                    if (data != 'Unchanged') {
                        messagingServiceData["companyId"] = _this.newCompName;
                    }
                    _this.commonService.updateLogo(messagingServiceData);
                    _this.websocketService.connectSocket(dashboardInput, 'livetrack');
                    localStorage.setItem('companyLogo', res[1]["logo"]);
                    localStorage.setItem("mapAllowed", res[1]["mapAllowed"]);
                    localStorage.setItem('mainMenu', res[1]["mainmenu"]);
                    localStorage.setItem('dashboardWebSocketData', JSON.stringify(dashboardInput));
                    localStorage.setItem('loginData', JSON.stringify(res));
                    localStorage.setItem('staticIOData', JSON.stringify(res[0]["staticIODatas"]));
                    localStorage.setItem('appSettings', res[1]["applicationSettings"]);
                    //     localStorage.setItem('map', JSON.parse(res[1]["applicationSettings"]).mapview);
                    localStorage.setItem('map', res[1]["mapview"]);
                    localStorage.setItem('corpId', _this.login.value.compName);
                    localStorage.setItem('userName', _this.login.value.compName);
                    localStorage.setItem('password', _this.login.value.compName);
                    localStorage.setItem('commandsData', res[1]["CommandsData"]);
                }
                else {
                    localStorage.setItem('PhoneNumber', _this.login.value.compName);
                    _this.router.navigateByUrl('/tabs-login/dashboard/add-company');
                }
                _this.commonService.dismissLoader();
            });
        };
        this.verifyLogin = function (data) {
            if (data === null) {
                _this.commonService.presentAlert('Error', 'Try again after sometime.');
                _this.commonService.dismissLoader();
            }
            else {
                // $ionicHistory.nextViewOptions({
                //    disableAnimate: true,
                //    disableBack: true
                // });
                // localStorage.setItem('PhoneNumber', this.user.phoneNumber);
                // this.getAlertConfigurationData();
                // this.getCommandsData();
            }
        };
        this.getAlertConfigurationData = function () {
            // this.commonService.getAlertsConfig()
            //   .subscribe(res => {
            //     localStorage.setItem('alertsData', JSON.stringify(res));
            //   }, err => {
            //     console.log(err);
            //     this.commonService.dismiss();
            //   });
        };
        this.getCommandsData = function () {
            // this.commonService.getCommonApplicationSettingsData('CommandsData', 'obj')
            //   .subscribe(res => {
            //     localStorage.setItem('commandsData', JSON.stringify(res));
            //     this.getPersonalTrackerCommandsData();
            //   }, err => {
            //     console.log(err);
            //     this.commonService.dismiss();
            //   });
        };
    }
    LoginPage.prototype.changeModule = function (data) {
        if (data == "user") {
            this.router.navigateByUrl("tabs-login/members/login");
        }
        else {
            this.router.navigateByUrl("tabs-login/dealarlogin/login");
        }
    };
    LoginPage.prototype.openFpassModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _login_forgotpass_modal_forgotpass_modal_page__WEBPACK_IMPORTED_MODULE_6__["ForgotpassModalPage"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        this.menuController.enable(false);
        if (this.commonService.isLoading)
            this.commonService.dismissLoader();
        this.backButtonExit();
        if (this.websocketService.isAlive('livetrack')) {
            this.websocketService.disConnectSocket('livetrack');
        }
    };
    LoginPage.prototype.backButtonExit = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        // this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
        var thisKey = this;
        var alertController = this.alertController;
        var websocketService = this.websocketService;
        document.addEventListener('backbutton', function (event) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                var alert_1;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(window.location.hash == "#/tabs-login" || window.location.hash == "#/tabs-login/members/login")) return [3 /*break*/, 4];
                            event.preventDefault();
                            // event.stopImmediatePropagation()
                            // event.stopPropagation();
                            console.log('hello');
                            if (!(thisKey.exitPopup === false)) return [3 /*break*/, 3];
                            thisKey.exitPopup = true;
                            localStorage.setItem("exitPopup", "true");
                            return [4 /*yield*/, thisKey.alertController.create({
                                    header: 'Exit',
                                    backdropDismiss: false,
                                    message: "Are you sure? You want to exit!",
                                    buttons: [{
                                            text: 'Cancel',
                                            role: 'cancel',
                                            handler: function (data) {
                                                thisKey.exitPopup = false;
                                                localStorage.setItem("exitPopup", "false");
                                            }
                                        },
                                        {
                                            text: 'Ok',
                                            handler: function (data) {
                                                navigator['app'].exitApp();
                                                // thisKey.websocketService.disConnectSocket("livetrack");
                                            }
                                        }]
                                })];
                        case 1:
                            alert_1 = _a.sent();
                            return [4 /*yield*/, alert_1.present()];
                        case 2:
                            _a.sent();
                            _a.label = 3;
                        case 3: return [3 /*break*/, 5];
                        case 4:
                            if (this.subscription)
                                this.subscription.unsubscribe();
                            _a.label = 5;
                        case 5: return [2 /*return*/];
                    }
                });
            });
        }, false);
    };
    LoginPage.prototype.backButton = function () {
        this.subscription = this.platform.backButton.subscribeWithPriority(9999, function () {
            document.addEventListener('backbutton', function (event) {
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                    var alert_2;
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                event.preventDefault();
                                if (!(localStorage.getItem('exitPopup') == "false" || localStorage.getItem('exitPopup') == null)) return [3 /*break*/, 3];
                                localStorage.setItem("exitPopup", "true");
                                return [4 /*yield*/, this.alertController.create({
                                        header: 'Are you sure?',
                                        backdropDismiss: false,
                                        message: "You want to exit!",
                                        buttons: [{
                                                text: 'Cancel',
                                                role: 'cancel',
                                                handler: function (data) {
                                                    localStorage.setItem("exitPopup", "false");
                                                }
                                            },
                                            {
                                                text: 'Ok',
                                                handler: function (data) {
                                                    navigator['app'].exitApp();
                                                }
                                            }]
                                    })];
                            case 1:
                                alert_2 = _a.sent();
                                return [4 /*yield*/, alert_2.present()];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            });
        });
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.ionViewWillLeave = function () {
        if (this.subscription)
            this.subscription.unsubscribe();
    };
    LoginPage.prototype.ngOnInit = function () {
        var _this = this;
        if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].appName == "Armoron") {
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/login/getPreferences?key=CountrySettings&companyId=""';
            this.ajaxService.ajaxGetPerference(url)
                .subscribe(function (res) {
                _this.countryList = Object.keys(res);
                _this.countryList = Object.keys(res);
                var smsAPI = res[_this.armoronApp.country]['smsAPI'];
                _this.OTPmessage = res[_this.armoronApp.country]['OTPmessage'];
                _this.armoronApp.countryCode = res[_this.armoronApp.country]['countryCode'];
                _this.entryPoint = res[_this.armoronApp.country]['entryPoint'];
                localStorage.setItem("entryPoint", _this.entryPoint);
                for (var x in res[_this.armoronApp.country]['languages']) {
                    _this.languageOptions.push(decodeURI(res[_this.armoronApp.country]['languages'][x]));
                    _this.armoronApp.selectedlanguage = decodeURI(res[_this.armoronApp.country]['defaultLanguage']);
                    // this.translateLangService.setLanguage(this.app.selectedlanguage);
                }
                localStorage.setItem('SMSAPI', smsAPI);
                console.log(res);
            });
        }
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        if (this.myPlatform != 'desktop') {
            localStorage.clear();
        }
        else {
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["serverUrl"].web + '/login/getPreferences?key=TrackVersion_mvt&companyId=""';
            this.ajaxService.ajaxGetPerference(url)
                .subscribe(function (res) {
                console.log(res);
                if (res) {
                    if (localStorage.TrackVersionMvt < res[0] || localStorage.TrackVersionMvt == null || localStorage.TrackVersionMvt == undefined) {
                        localStorage.setItem('TrackVersionMvt', res[0]);
                        window.location.reload();
                    }
                    else {
                        localStorage.clear();
                        localStorage.setItem('TrackVersionMvt', res[0]);
                    }
                }
            });
        }
        this.bgImage = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].bgImage;
        this.menuController.enable(false);
        this.appName = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].appName.replace(/ /g, "");
        this.logo = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].loginImgUrl;
        this.login = this.formBuilder.group({
            compName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            compId: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        if (this.appName == "RAC") {
            this.login.patchValue({
                compId: "rac"
            });
        }
        if (sessionStorage.rememberMe) {
            var details = JSON.parse(sessionStorage.rememberMe);
            this.login.patchValue({
                compName: details["compName"],
                password: details["password"],
                compId: details["corpid"],
                isChecked: details["checked"]
            });
            this.isChecked = details["checked"];
            // this.submitLogin();
        }
        this.router.events.subscribe(function () {
            // if (window.location.hash == "#/tabs-login/members/login")
            // if (this.subscription)
            // this.subscription.unsubscribe();
        });
        this.entryPoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["app"].entryPoint;
    };
    LoginPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
        { type: _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
        { type: _services_websocket_service__WEBPACK_IMPORTED_MODULE_8__["WebsocketService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_9__["AuthenticationService"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_ajax_service__WEBPACK_IMPORTED_MODULE_4__["AjaxService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"],
            _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
            _services_websocket_service__WEBPACK_IMPORTED_MODULE_8__["WebsocketService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_9__["AuthenticationService"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/parent-app/student-login/student-login.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/parent-app/student-login/student-login.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#partitioned {\n  padding-left: 15px;\n  letter-spacing: 42px;\n  border: 0;\n  background-image: -webkit-gradient(linear, right top, left top, color-stop(70%, #e90000), color-stop(0%, rgba(255, 255, 255, 0)));\n  background-image: linear-gradient(to left, #e90000 70%, rgba(255, 255, 255, 0) 0%);\n  background-position: bottom;\n  background-size: 50px 1px;\n  background-repeat: repeat-x;\n  background-position-x: 35px;\n  width: 220px;\n  min-width: 220px;\n}\n\n.background-gradient {\n  background-image: -webkit-gradient(linear, left bottom, right top, from(#f51e2e), to(#fd6035));\n  background-image: linear-gradient(to top right, #f51e2e, #fd6035);\n  width: 100%;\n  height: 100%;\n  position: fixed;\n}\n\n#divInner {\n  left: 0;\n  position: -webkit-sticky;\n  position: sticky;\n}\n\n#divOuter {\n  width: 190px;\n  overflow: hidden;\n}\n\n.item-interactive.ion-valid {\n  --highlight-background: none;\n}\n\n.placeholder-label {\n  color: #1aba7e;\n  font-size: 25px;\n  align-self: center;\n}\n\n.button-color {\n  --background: #1aba7e;\n  --background-focused: #1aba7e;\n  --background-hover: #1aba7e;\n  --background-activated: #1aba7e;\n}\n\n.font-label {\n  font-size: 21px;\n  font-weight: bold;\n  font-family: sans-serif;\n  color: #1aba7e;\n}\n\n.resend-otp {\n  font-size: 14px;\n  font-weight: bold;\n  padding: 0px 11px 7px 3px;\n  color: #1aba7e;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvcGFyZW50LWFwcC9zdHVkZW50LWxvZ2luL3N0dWRlbnQtbG9naW4uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhcmVudC1hcHAvc3R1ZGVudC1sb2dpbi9zdHVkZW50LWxvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxvQkFBQTtFQUNBLFNBQUE7RUFDQSxpSUFBQTtFQUFBLGtGQUFBO0VBQ0EsMkJBQUE7RUFDQSx5QkFBQTtFQUNBLDJCQUFBO0VBQ0EsMkJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLDhGQUFBO0VBQUEsaUVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNDRjs7QURFQTtFQUNFLE9BQUE7RUFDQSx3QkFBQTtFQUFBLGdCQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLDRCQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsMkJBQUE7RUFDQSwrQkFBQTtBQ0NGOztBREVBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0FDQ0Y7O0FERUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9wYXJlbnQtYXBwL3N0dWRlbnQtbG9naW4vc3R1ZGVudC1sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNwYXJ0aXRpb25lZCB7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDQycHg7XG4gIGJvcmRlcjogMDtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIGxlZnQsIHJnYigyMzMsIDAsIDApIDcwJSwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwKSAwJSk7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGJvdHRvbTtcbiAgYmFja2dyb3VuZC1zaXplOiA1MHB4IDFweDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IHJlcGVhdC14O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDM1cHg7XG4gIHdpZHRoOiAyMjBweDtcbiAgbWluLXdpZHRoOiAyMjBweDtcbn1cblxuLmJhY2tncm91bmQtZ3JhZGllbnQge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gdG9wIHJpZ2h0LCAjZjUxZTJlLCAjZmQ2MDM1KTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuXG4jZGl2SW5uZXIge1xuICBsZWZ0OiAwO1xuICBwb3NpdGlvbjogc3RpY2t5O1xufVxuXG4jZGl2T3V0ZXIge1xuICB3aWR0aDogMTkwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5pdGVtLWludGVyYWN0aXZlLmlvbi12YWxpZCB7XG4gIC0taGlnaGxpZ2h0LWJhY2tncm91bmQ6IG5vbmU7XG59XG5cbi5wbGFjZWhvbGRlci1sYWJlbCB7XG4gIGNvbG9yOiAjMWFiYTdlO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLmJ1dHRvbi1jb2xvciB7XG4gIC0tYmFja2dyb3VuZDogIzFhYmE3ZTtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMxYWJhN2U7XG4gIC0tYmFja2dyb3VuZC1ob3ZlcjogIzFhYmE3ZTtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzFhYmE3ZTtcbn1cblxuLmZvbnQtbGFiZWwge1xuICBmb250LXNpemU6IDIxcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgY29sb3I6ICMxYWJhN2U7XG59XG5cbi5yZXNlbmQtb3Rwe1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nOiAwcHggMTFweCA3cHggM3B4O1xuICBjb2xvcjogIzFhYmE3ZTtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn0iLCIjcGFydGl0aW9uZWQge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIGxldHRlci1zcGFjaW5nOiA0MnB4O1xuICBib3JkZXI6IDA7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBsZWZ0LCAjZTkwMDAwIDcwJSwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwKSAwJSk7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGJvdHRvbTtcbiAgYmFja2dyb3VuZC1zaXplOiA1MHB4IDFweDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IHJlcGVhdC14O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDM1cHg7XG4gIHdpZHRoOiAyMjBweDtcbiAgbWluLXdpZHRoOiAyMjBweDtcbn1cblxuLmJhY2tncm91bmQtZ3JhZGllbnQge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gdG9wIHJpZ2h0LCAjZjUxZTJlLCAjZmQ2MDM1KTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuXG4jZGl2SW5uZXIge1xuICBsZWZ0OiAwO1xuICBwb3NpdGlvbjogc3RpY2t5O1xufVxuXG4jZGl2T3V0ZXIge1xuICB3aWR0aDogMTkwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5pdGVtLWludGVyYWN0aXZlLmlvbi12YWxpZCB7XG4gIC0taGlnaGxpZ2h0LWJhY2tncm91bmQ6IG5vbmU7XG59XG5cbi5wbGFjZWhvbGRlci1sYWJlbCB7XG4gIGNvbG9yOiAjMWFiYTdlO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLmJ1dHRvbi1jb2xvciB7XG4gIC0tYmFja2dyb3VuZDogIzFhYmE3ZTtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMxYWJhN2U7XG4gIC0tYmFja2dyb3VuZC1ob3ZlcjogIzFhYmE3ZTtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzFhYmE3ZTtcbn1cblxuLmZvbnQtbGFiZWwge1xuICBmb250LXNpemU6IDIxcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgY29sb3I6ICMxYWJhN2U7XG59XG5cbi5yZXNlbmQtb3RwIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgcGFkZGluZzogMHB4IDExcHggN3B4IDNweDtcbiAgY29sb3I6ICMxYWJhN2U7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/parent-app/student-login/student-login.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/parent-app/student-login/student-login.component.ts ***!
  \*********************************************************************/
/*! exports provided: StudentLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentLoginComponent", function() { return StudentLoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");









var StudentLoginComponent = /** @class */ (function () {
    function StudentLoginComponent(formBuilder, ajaxService, platform, router, modalController, commonService, menuController, alertController, authService) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.ajaxService = ajaxService;
        this.platform = platform;
        this.router = router;
        this.modalController = modalController;
        this.commonService = commonService;
        this.menuController = menuController;
        this.alertController = alertController;
        this.authService = authService;
        this.button = "Send OTP";
        this.loginOtp = "";
        this.generateOTP = function (cntrl) {
            _this.generatedOTP = Math
                .floor(Math.random() * 9000) + 1000;
            if (/(android|iPhone|iPad|iPod)/i.test(navigator.userAgent)) {
                if (_this.login.value.mobileNo === '9600696008' || _this.login.value.mobileNo === '9962139968') {
                    _this.button = "Verify";
                }
                else {
                    _this.sendOtp();
                    //this.verifyOTPMethod(this.generatedOTP, this.login.value.compName);
                }
            }
            else {
                _this.commonService.presentToast('Contact support team');
            }
        };
    }
    StudentLoginComponent.prototype.generateOtp = function () {
        this.loginOtp = Math.floor(Math.random() * 9000) + 1000;
    };
    StudentLoginComponent.prototype.getLoginData = function () {
        this.router.navigateByUrl('student-dashboard');
    };
    StudentLoginComponent.prototype.verifyPhoneNo = function () {
        var _this = this;
        var url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + '/parentapp/checkUser?parentId=' + this.login.value.mobileNo;
        this.ajaxService.ajaxGetWithString(url).subscribe(function (res) {
            console.log(res);
            if (res == "Not Available") {
                _this.commonService.presentToast("Enter a Valid PhoneNumber");
            }
            else {
                _this.button = "Verify";
                _this.sendOtp();
            }
        });
    };
    StudentLoginComponent.prototype.sendOtp = function () {
        var _this = this;
        this.generateOtp();
        localStorage.setItem('userName', JSON.stringify(this.login.value.mobileNo));
        var url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + '/parentapp/otp?message=your otp is ' + this.loginOtp + '&contact=' + this.login.value.mobileNo;
        this.ajaxService.ajaxGet(url).subscribe(function (res) {
            if (res.message == "sent") {
                _this.button = "Verify";
                _this.commonService.presentToast('Enter your otp');
            }
            else {
                _this.commonService.presentToast('Enter a valid phonenumber to login');
            }
        });
    };
    StudentLoginComponent.prototype.checkUser = function () {
        var _this = this;
        if ((this.login.value.mobileNo).toString().length == 10) {
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + '/parentapp/checkUser?parentId=' + this.login.value.mobileNo;
            this.ajaxService.ajaxGetWithString(url).subscribe(function (res) {
                console.log(res);
                if (JSON.parse(res).message == "Available") {
                    _this.sendOtp();
                }
                else {
                    _this.commonService.presentToast('Enter a Valid PhoneNumber');
                }
            });
        }
        else {
            this.commonService.presentToast('Enter 10 digits PhoneNumber');
        }
    };
    StudentLoginComponent.prototype.sideMenus = function () {
        // this.authService.login();
        var _this = this;
        var url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["serverUrl"].web + '/parentapp/login?parentId=' + this.login.value.mobileNo;
        this.ajaxService.ajaxGet(url).subscribe(function (res) {
            console.log(res);
            _this.commonService.updateLogo(res);
            localStorage.setItem('mainMenu', res[0].mainmenu);
        });
    };
    // localStorage.setItem('companyLogo', res[1]["logo"])
    // localStorage.setItem("mapAllowed", res[1]["mapAllowed"])
    // localStorage.setItem('mainMenu', res[1]["mainmenu"]);
    StudentLoginComponent.prototype.loginSubmit = function () {
        // this.generateOtp();
        if (this.button == "Send OTP") {
            // this.verifyPhoneNo();
            // this.button = "Verify"
            // this.sendOtp()
            this.checkUser();
        }
        else if (this.button == "Verify") {
            if (this.loginOtp == this.login.value.otp) {
                localStorage.setItem('corpId', this.login.value.mobileNo);
                this.sideMenus();
                this.router.navigateByUrl('student-dashboard');
            }
            else {
                this.commonService.presentToast('Otp is wrong, try again');
            }
        }
    };
    StudentLoginComponent.prototype.ionViewWillEnter = function () {
        this.login.reset();
    };
    StudentLoginComponent.prototype.ngOnChanges = function () {
        console.log('value changed', this.data);
    };
    StudentLoginComponent.prototype.ngOnInit = function () {
        this.loginOtp = '';
        this.login = this.formBuilder.group({
            mobileNo: [''],
            otp: ['']
        });
        this.login.reset();
    };
    StudentLoginComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
        { type: _app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
        { type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], StudentLoginComponent.prototype, "data", void 0);
    StudentLoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-student-login',
            template: __webpack_require__(/*! raw-loader!./student-login.component.html */ "./node_modules/raw-loader/index.js!./src/app/parent-app/student-login/student-login.component.html"),
            styles: [__webpack_require__(/*! ./student-login.component.scss */ "./src/app/parent-app/student-login/student-login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            _app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"]])
    ], StudentLoginComponent);
    return StudentLoginComponent;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module-es5.js.map