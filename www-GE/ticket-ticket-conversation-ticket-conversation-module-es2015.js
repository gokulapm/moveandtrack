(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ticket-ticket-conversation-ticket-conversation-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/ticket/image-conversation/image-conversation.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/ticket/image-conversation/image-conversation.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-row  class=\"img_wrapper\">\n    <ion-col  size=\"12\" >\n      <ion-img src=\"imageData\"   \n       (click)=\"getBack()\"></ion-img>\n    </ion-col>\n  </ion-row> \n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/ticket/ticket-conversation/ticket-conversation.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/ticket/ticket-conversation/ticket-conversation.page.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-row *ngIf=\"messages\" class=\"main-wrapper\" >\n  <div class=\"ticket-id\" *ngIf=\"customerId\">Ticket ID : {{id}}</div>\n   <ion-col  *ngFor=\"let items of conversation;let i=index;\" size=\"12\" style=\"margin:1px 0px\">\n           <ion-row   *ngIf=\"items.body.length == items.body_text.length+11\" \n           [ngClass]=\"items.body_text ==  '.' ? 'imgBlock':'from-wrapper' \">\n             <ion-col size=12 class=\"wrapper-container\" >\n                {{items.body_text}}\n                 </ion-col>\n           </ion-row>\n       <ion-row  style=\"clear: both;overflow: hidden;\n           white-space: nowrap;\" *ngIf='items.attachments[0] != undefined'>   \n             <ion-col size=12 >\n              <img src= {{items.attachments[0].attachment_url}}  onerror=\"this.style.display='none'\"  \n              [ngClass]=\"items.attachments[0]['content_type'] !== 'application/octet-stream'   ? 'dataBlock':'imgBlock'\" class=\"thum_nail\"  (click)=\"enlargeImage(items.attachments[0].attachment_url)\"/>\n             </ion-col>\n             <ion-col size=\"12\" class=\"date_label\" [ngClass]=\"items.attachments[0]['content_type'] !== 'application/octet-stream'   ? 'dataBlock':'imgBlock'\"   >\n              <ion-row >\n                <ion-col  class=\"date_block\">{{ items.created_at | date:'medium' }}</ion-col>\n              </ion-row>\n            </ion-col>\n                </ion-row>\n                <ion-row *ngIf='items.attachments[0] != undefined ' >\n                  <ion-col size=12>\n                    <audio controls=\"\"   style=\"float:right;outline:none;\" [ngClass]=\"items.attachments[0]['content_type'] === 'application/octet-stream'   ? 'dataBlock':'imgBlock'\" >\n                      <source [src]=\"items.attachments[0].attachment_url\"     type=\"audio/wav\" >\n                        </audio>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"date_label\" *ngIf='items.attachments[0] != undefined ' [ngClass]=\"items.attachments[0]['content_type'] === 'application/octet-stream'   ? 'dataBlock':'imgBlock'\">\n                    <ion-row>\n                      <ion-col class=\"date_block\">{{ items.created_at | date:'medium' }}</ion-col>\n                    </ion-row>\n                  </ion-col>\n                 </ion-row>\n          <div  style=\"margin: 0;\" *ngIf=\"items.body.length != items.body_text.length+11\" \n           [ngClass]=\"items.body_text == ''? 'display-wrapper':'to-wrapper'\">\n             <div class=\"wrapper-container\">\n               {{items.body_text}}\n          </div>\n       </div> \n       \n      \n     </ion-col>\n     <ion-row id=\"0\">\n     <ion-col  size=\"6\" size-sm=\"10\" size-lg=\"11\" size-md=\"11\">  \n      \n    <ion-item class=\"tickets-item\">\n         <ion-input type=\"text\" placeholder=\"Write your queries\" style=\"--padding-top:0;\n             font-size: 13px;\" [(ngModel)]='image'    (keyup)=\"enableSendbtn($event)\" ></ion-input>\n  \n        <ion-icon  id=\"yourBtn\"  (click)=\"getFile()\" name=\"attach\" class=\"attach-iconc\" (click)=\"enableSendbtn($event)\"></ion-icon>\n        <div style='height: 0px;width: 0px; overflow:hidden;outline: none;'>\n          <input id=\"upfile\" type=\"file\" value=\"upload\" [(ngModel)]='image'  ng2FileSelect  [uploader]=\"uploader\" (change)=\"subject($event)\" multiple  />\n        </div>\n       \n      </ion-item>\n      \n         </ion-col>\n        \n             <ion-col size-sm=\"2\" size-lg=\"1\" size-md=\"1\" class=\"send-col\">\n         <a >\n          <ion-icon name=\"mic\" class=\"send-icon\"  *ngIf=\"!recording && !sendbtn\"  (click)=\"initiateRecording()\" ></ion-icon> \n           <ion-icon name=\"mic-off\" class=\"send-icon\" *ngIf=\"recording && !sendbtn\"  (click)=\"stopRecording()\" ></ion-icon>\n          <ion-icon name=\"send\"   class=\"send-icon\" *ngIf=\"sendbtn\" (click)=\"sendReplyMsg();\"  ></ion-icon> \n          </a>\n          \n           </ion-col>\n         </ion-row> \n    </ion-row>\n \n\n"

/***/ }),

/***/ "./src/app/ticket/image-conversation/image-conversation.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/ticket/image-conversation/image-conversation.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img_wrapper {\n  margin: 24px 0px 0px 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvdGlja2V0L2ltYWdlLWNvbnZlcnNhdGlvbi9pbWFnZS1jb252ZXJzYXRpb24uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3RpY2tldC9pbWFnZS1jb252ZXJzYXRpb24vaW1hZ2UtY29udmVyc2F0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3RpY2tldC9pbWFnZS1jb252ZXJzYXRpb24vaW1hZ2UtY29udmVyc2F0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZ193cmFwcGVye1xyXG4gICAgbWFyZ2luOiAyNHB4IDBweCAwcHggMHB4O1xyXG59IiwiLmltZ193cmFwcGVyIHtcbiAgbWFyZ2luOiAyNHB4IDBweCAwcHggMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/ticket/image-conversation/image-conversation.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/ticket/image-conversation/image-conversation.component.ts ***!
  \***************************************************************************/
/*! exports provided: ImageConversationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageConversationComponent", function() { return ImageConversationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let ImageConversationComponent = class ImageConversationComponent {
    constructor(modalController) {
        this.modalController = modalController;
    }
    getBack() {
        this.modalController.dismiss();
    }
    ngOnInit() {
        this.imageData = this.image;
        console.log(this.imageData, 'iam image popup');
    }
    ngOnChanges() {
        this.imageData = this.image;
        console.log(this.imageData, 'iam image popup');
    }
};
ImageConversationComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ImageConversationComponent.prototype, "image", void 0);
ImageConversationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-image-conversation',
        template: __webpack_require__(/*! raw-loader!./image-conversation.component.html */ "./node_modules/raw-loader/index.js!./src/app/ticket/image-conversation/image-conversation.component.html"),
        styles: [__webpack_require__(/*! ./image-conversation.component.scss */ "./src/app/ticket/image-conversation/image-conversation.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], ImageConversationComponent);



/***/ }),

/***/ "./src/app/ticket/ticket-conversation/ticket-conversation.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/ticket/ticket-conversation/ticket-conversation.module.ts ***!
  \**************************************************************************/
/*! exports provided: TicketConversationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketConversationPageModule", function() { return TicketConversationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ticket_conversation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ticket-conversation.page */ "./src/app/ticket/ticket-conversation/ticket-conversation.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm2015/ng2-file-upload.js");









const routes = [
    {
        path: '',
        component: _ticket_conversation_page__WEBPACK_IMPORTED_MODULE_6__["TicketConversationPage"]
    }
];
let TicketConversationPageModule = class TicketConversationPageModule {
};
TicketConversationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_ticket_conversation_page__WEBPACK_IMPORTED_MODULE_6__["TicketConversationPage"]]
    })
], TicketConversationPageModule);



/***/ }),

/***/ "./src/app/ticket/ticket-conversation/ticket-conversation.page.scss":
/*!**************************************************************************!*\
  !*** ./src/app/ticket/ticket-conversation/ticket-conversation.page.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 1px;\n}\n* ion-card {\n  background: white;\n  cursor: pointer;\n}\n.mobileCard {\n  display: none;\n}\n.getConversationFail {\n  display: inline-block;\n}\n.border-pending {\n  border-left: 3px solid orange;\n}\n.border-closed {\n  border-left: 3px solid #008060;\n}\n.border-open {\n  border-left: 3px solid #0084ff;\n}\n.main-wrapper {\n  background: url('light_bg.png') repeat center center/cover;\n  height: 82vh;\n  overflow-y: scroll;\n}\n.to-wrapper {\n  background: url('leftSide_bg.jpg') no-repeat center center/cover;\n  height: auto;\n  padding: 5px;\n  border-radius: 5px;\n  float: left;\n  margin: 0;\n}\n.from-wrapper {\n  background: url('rightSide_bg.jpg') no-repeat center center/cover;\n  height: auto;\n  padding: 5px;\n  border-radius: 5px;\n  margin: 0px;\n  float: right;\n}\n.date_label {\n  float: right;\n  font-size: 10px;\n  text-align: right;\n  font-weight: bold;\n}\n.date_block {\n  color: #595959;\n}\n.dataBlock {\n  display: block !important;\n}\n.imgBlock {\n  display: none !important;\n}\n.ticket-title {\n  font-size: 18px;\n}\n.display-wrapper {\n  display: none;\n}\n.ticket-id {\n  border: 1px solid #e9e8e8;\n  border-radius: 0px 0px 5px 5px;\n  background: #9C27B0;\n  font-size: 15px;\n  height: 25px;\n  padding: 2px 8px;\n  position: -webkit-sticky;\n  position: sticky;\n  z-index: 1;\n  top: -5px;\n  margin: -5px auto 0;\n  color: white;\n}\n.tickets-item {\n  height: 40px;\n  border: 1px solid #e2e0e0;\n  border-radius: 20px;\n  position: fixed;\n  bottom: 6px;\n}\n.send-col {\n  width: 15%;\n  position: fixed;\n  bottom: 0px;\n  right: 0;\n}\n.send-icon {\n  font-size: 20px;\n  color: white;\n  background: #0ba08c;\n  border-radius: 16px;\n  padding: 7px;\n}\n.wrapper-container {\n  font-size: 12px;\n  padding: 0px 25px;\n}\n.contact-panal {\n  background-color: #f8f8fb;\n  height: 90vh;\n  overflow-y: scroll;\n}\n.contact-panal::-webkit-scrollbar {\n  width: 4px;\n}\n.contact-panal::-webkit-scrollbar-track {\n  background: #f6f6f6;\n}\n.contact-panal::-webkit-scrollbar-thumb {\n  background: #b6b5b5;\n}\n@media only screen and (min-width: 320px) and (max-width: 767px) {\n  .second-col {\n    display: block;\n  }\n\n  .tickets-item {\n    width: 80%;\n    left: 11px;\n  }\n\n  .container {\n    height: 89vh;\n    overflow-y: hidden;\n  }\n\n  ion-card {\n    margin: 5px 10px;\n    padding-left: 7px;\n  }\n}\n@media only screen and (min-width: 767px) {\n  .main-wrapper::-webkit-scrollbar {\n    width: 10px;\n  }\n\n  .main-wrapper::-webkit-scrollbar-track {\n    background: #f6f6f6;\n  }\n\n  .main-wrapper::-webkit-scrollbar-thumb {\n    background: #afadad;\n  }\n\n  .main-wrapper::-webkit-scrollbar-thumb:hover {\n    background: #b6b5b5;\n  }\n}\n@media only screen and (min-width: 767px) and (max-width: 1022px) {\n  .tickets-item {\n    width: 350px;\n    position: fixed;\n    bottom: 7px;\n    right: 8%;\n  }\n\n  ion-card {\n    margin: 5px;\n    padding-left: 7px;\n  }\n}\n@media only screen and (min-width: 1023px) {\n  .tickets-item {\n    width: 500px;\n    position: fixed;\n    bottom: 6px;\n    right: 9%;\n  }\n\n  ion-card {\n    margin: 5px 0px;\n    padding-left: 7px;\n  }\n}\n@media only screen and (min-width: 1350px) {\n  .tickets-item {\n    right: 15%;\n  }\n\n  .container {\n    height: 91vh;\n    overflow-y: hidden;\n  }\n\n  ion-card {\n    margin: 5px 10px;\n    padding-left: 7px;\n  }\n\n  .send-icon {\n    position: relative;\n    right: 68%;\n    bottom: 5px;\n  }\n}\n.thum_nail {\n  float: right;\n  zoom: 10%;\n  border: 20px solid #d7fb9f;\n}\n.img_wrapper {\n  margin: 24px 0px 0px 0px;\n}\n#yourBtn {\n  position: relative;\n  font-size: 30px;\n  bottom: 5px;\n  padding: 0px 10px;\n  -webkit-transform: rotate(20deg);\n          transform: rotate(20deg);\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvdGlja2V0L3RpY2tldC1jb252ZXJzYXRpb24vdGlja2V0LWNvbnZlcnNhdGlvbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3RpY2tldC90aWNrZXQtY29udmVyc2F0aW9uL3RpY2tldC1jb252ZXJzYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNDSjtBREFJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FDRVI7QURFQTtFQUNJLGFBQUE7QUNDSjtBRENBO0VBQ0UscUJBQUE7QUNFRjtBRENBO0VBQ0ksNkJBQUE7QUNFSjtBRENBO0VBQ0ksOEJBQUE7QUNFSjtBRENBO0VBQ0ksOEJBQUE7QUNFSjtBRENBO0VBQ0ksMERBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7QUNDSjtBRENBO0VBQ0ksZ0VBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUNFSjtBRENBO0VBQ0ksaUVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNFSjtBREFFO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDR0o7QURERTtFQUNFLGNBQUE7QUNJSjtBRERBO0VBQ0kseUJBQUE7QUNJSjtBREZBO0VBQ0ksd0JBQUE7QUNLSjtBREhBO0VBQ0ksZUFBQTtBQ01KO0FESkE7RUFDSSxhQUFBO0FDT0o7QURMQTtFQUNJLHlCQUFBO0VBQ0EsOEJBQUE7RUFFQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSx3QkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtFQUVBLFlBQUE7QUNNSjtBREpBO0VBQ0ksWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ09KO0FESUE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0FDREo7QURHQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNBSjtBREVBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0FDQ0o7QURDQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDRUo7QURBQTtFQUNJLFVBQUE7QUNHSjtBREFFO0VBQ0UsbUJBQUE7QUNHSjtBREFFO0VBQ0UsbUJBQUE7QUNHSjtBREdBO0VBQ0k7SUFDQyxjQUFBO0VDQUg7O0VERUU7SUFDSSxVQUFBO0lBQ0EsVUFBQTtFQ0NOOztFRElFO0lBQ0ksWUFBQTtJQUNBLGtCQUFBO0VDRE47O0VER0U7SUFDSSxnQkFBQTtJQUNBLGlCQUFBO0VDQU47QUFDRjtBREdHO0VBQ0M7SUFDSSxXQUFBO0VDRE47O0VER0k7SUFDRSxtQkFBQTtFQ0FOOztFREdJO0lBQ0UsbUJBQUE7RUNBTjs7RURHSTtJQUNFLG1CQUFBO0VDQU47QUFDRjtBRElBO0VBQ0k7SUFDQSxZQUFBO0lBQ0EsZUFBQTtJQUNBLFdBQUE7SUFDQSxTQUFBO0VDRkY7O0VESUU7SUFDSSxXQUFBO0lBQ0EsaUJBQUE7RUNETjtBQUNGO0FER0E7RUFDSTtJQUNBLFlBQUE7SUFDQSxlQUFBO0lBQ0EsV0FBQTtJQUNBLFNBQUE7RUNERjs7RURHRTtJQUNJLGVBQUE7SUFDQSxpQkFBQTtFQ0FOO0FBQ0Y7QURFQTtFQUNJO0lBQ0MsVUFBQTtFQ0FIOztFREVFO0lBQ0ksWUFBQTtJQUNBLGtCQUFBO0VDQ047O0VEQ0U7SUFDSSxnQkFBQTtJQUNBLGlCQUFBO0VDRU47O0VEQUU7SUFDSSxrQkFBQTtJQUNBLFVBQUE7SUFDQSxXQUFBO0VDR047QUFDRjtBRERBO0VBQ0ksWUFBQTtFQUNBLFNBQUE7RUFDQSwwQkFBQTtBQ0dKO0FEQUE7RUFDSSx3QkFBQTtBQ0dKO0FEQUE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFFQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0EsZUFBQTtBQ0dKIiwiZmlsZSI6InNyYy9hcHAvdGlja2V0L3RpY2tldC1jb252ZXJzYXRpb24vdGlja2V0LWNvbnZlcnNhdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbiAgICBpb24tY2FyZHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tb2JpbGVDYXJke1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uZ2V0Q29udmVyc2F0aW9uRmFpbHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5ib3JkZXItcGVuZGluZ3tcclxuICAgIGJvcmRlci1sZWZ0OjNweCBzb2xpZCBvcmFuZ2U7XHJcbiAgICBcclxufVxyXG4uYm9yZGVyLWNsb3NlZHtcclxuICAgIGJvcmRlci1sZWZ0OjNweCBzb2xpZCByZ2IoMCwgMTI4LCA5Nik7XHJcbiAgICBcclxufVxyXG4uYm9yZGVyLW9wZW57XHJcbiAgICBib3JkZXItbGVmdDozcHggc29saWQgcmdiKDAsIDEzMiwgMjU1KTtcclxuICBcclxufVxyXG4ubWFpbi13cmFwcGVye1xyXG4gICAgYmFja2dyb3VuZDp1cmwoXCIuLi8uLi8uLi9hc3NldHMvdGlja2V0c19pbWcvbGlnaHRfYmcucG5nXCIpcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3ZlcjtcclxuICAgLy8gaGVpZ2h0Ojkwdmggbm92MTIgYXNtYTtcclxuICAgIGhlaWdodDo4MnZoO1xyXG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG59XHJcbi50by13cmFwcGVye1xyXG4gICAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL3RpY2tldHNfaW1nL2xlZnRTaWRlX2JnLmpwZ1wiKW5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIC8gY292ZXI7XHJcbiAgICBoZWlnaHQ6YXV0bztcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgXHJcbn1cclxuLmZyb20td3JhcHBlcntcclxuICAgIGJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy90aWNrZXRzX2ltZy9yaWdodFNpZGVfYmcuanBnXCIpbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3ZlcjtcclxuICAgIGhlaWdodDphdXRvO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbWFyZ2luOiAwcHggO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gIH1cclxuICAuZGF0ZV9sYWJlbHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgfVxyXG4gIC5kYXRlX2Jsb2Nre1xyXG4gICAgY29sb3I6ICM1OTU5NTk7XHJcbiAgfVxyXG5cclxuLmRhdGFCbG9ja3tcclxuICAgIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XHJcbn1cclxuLmltZ0Jsb2Nre1xyXG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi50aWNrZXQtdGl0bGV7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuLmRpc3BsYXktd3JhcHBlcntcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLnRpY2tldC1pZHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlOWU4ZTg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwcHggMHB4IDVweCA1cHg7XHJcbiAgICAvL2JhY2tncm91bmQ6ICNkOWVjZjI7bm92MzBhc21hXHJcbiAgICBiYWNrZ3JvdW5kOiAjOUMyN0IwO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgaGVpZ2h0OjI1cHg7XHJcbiAgICBwYWRkaW5nOiAycHggOHB4O1xyXG4gICAgcG9zaXRpb246IC13ZWJraXQtc3RpY2t5O1xyXG4gICAgcG9zaXRpb246IHN0aWNreTtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICB0b3A6IC01cHg7XHJcbiAgICBtYXJnaW46LTVweCBhdXRvIDA7XHJcbiAgICAvL2NvbG9yOiAjNjk2ODY4O25vdjMwYXNtYVxyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi50aWNrZXRzLWl0ZW17XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBib3JkZXI6MXB4IHNvbGlkIHJnYigyMjYsIDIyNCwgMjI0KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7IFxyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgYm90dG9tOiA2cHg7XHJcbiAgICBcclxufVxyXG4vLyAuYXR0YWNoLWljb25je1xyXG4vLyAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4vLyAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4vLyAgICAgYm90dG9tOiAxMHB4O1xyXG4vLyAgICAgcGFkZGluZzogMHB4IDEwcHg7XHJcbi8vICAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjBkZWcpOyBcclxuLy8gICAgIHRyYW5zZm9ybTogcm90YXRlKDIwZGVnKTsgIFxyXG4vLyB9XHJcbi5zZW5kLWNvbHtcclxuICAgIHdpZHRoOjE1JTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGJvdHRvbTogMHB4O1xyXG4gICAgcmlnaHQ6IDA7XHJcbn1cclxuLnNlbmQtaWNvbntcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJhY2tncm91bmQ6ICMwYmEwOGM7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gICAgcGFkZGluZzogN3B4O1xyXG59XHJcbi53cmFwcGVyLWNvbnRhaW5lcntcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHBhZGRpbmc6IDBweCAyNXB4O1xyXG59XHJcbi5jb250YWN0LXBhbmFse1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y4ZjhmYjtcclxuICAgIGhlaWdodDogOTB2aDtcclxuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcclxufVxyXG4uY29udGFjdC1wYW5hbDo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgd2lkdGg6NHB4O1xyXG4gIH1cclxuICBcclxuICAuY29udGFjdC1wYW5hbDo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gICAgYmFja2dyb3VuZDogICNmNmY2ZjY7XHJcbiAgfVxyXG4gIFxyXG4gIC5jb250YWN0LXBhbmFsOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAgcmdiKDE4MiwgMTgxLCAxODEpO1xyXG4gIH1cclxuICBcclxuXHJcbiAgXHJcbiAgXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDozMjBweClhbmQobWF4LXdpZHRoOjc2N3B4KXtcclxuICAgIC5zZWNvbmQtY29se1xyXG4gICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgfVxyXG4gICAgLnRpY2tldHMtaXRlbXsgXHJcbiAgICAgICAgd2lkdGg6ODAlO1xyXG4gICAgICAgIGxlZnQ6IDExcHg7IFxyXG4gICAgfVxyXG4gICAgLy8gLmNvbnRhY3QtcGFuYWx7XHJcbiAgICAvLyAgICAgZGlzcGxheTogbm9uZTtcclxuICAgIC8vIH1cclxuICAgIC5jb250YWluZXJ7XHJcbiAgICAgICAgaGVpZ2h0Ojg5dmg7XHJcbiAgICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xyXG4gICAgfVxyXG4gICAgaW9uLWNhcmR7XHJcbiAgICAgICAgbWFyZ2luOjVweCAxMHB4O1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogN3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG4gICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NzY3cHgpe1xyXG4gICAgLm1haW4td3JhcHBlcjo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgICAgIHdpZHRoOjEwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLm1haW4td3JhcHBlcjo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICAjZjZmNmY2O1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAubWFpbi13cmFwcGVyOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogICNhZmFkYWQ7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIC5tYWluLXdyYXBwZXI6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiByZ2IoMTgyLCAxODEsIDE4MSk7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICBcclxuICAgfVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NzY3cHgpYW5kIChtYXgtd2lkdGg6MTAyMnB4KXtcclxuICAgIC50aWNrZXRzLWl0ZW17IFxyXG4gICAgd2lkdGg6IDM1MHB4O1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgYm90dG9tOiA3cHg7XHJcbiAgICByaWdodDogOCU7XHJcbiAgICB9XHJcbiAgICBpb24tY2FyZHtcclxuICAgICAgICBtYXJnaW46NXB4O1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogN3B4O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDoxMDIzcHgpe1xyXG4gICAgLnRpY2tldHMtaXRlbXsgXHJcbiAgICB3aWR0aDogNTAwcHg7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBib3R0b206IDZweDtcclxuICAgIHJpZ2h0OiA5JTtcclxuICAgIH1cclxuICAgIGlvbi1jYXJke1xyXG4gICAgICAgIG1hcmdpbjo1cHggMHB4O1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogN3B4O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDoxMzUwcHgpe1xyXG4gICAgLnRpY2tldHMtaXRlbXsgXHJcbiAgICAgcmlnaHQ6IDE1JTtcclxuICAgIH1cclxuICAgIC5jb250YWluZXJ7XHJcbiAgICAgICAgaGVpZ2h0Ojkxdmg7XHJcbiAgICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xyXG4gICAgfVxyXG4gICAgaW9uLWNhcmR7XHJcbiAgICAgICAgbWFyZ2luOjVweCAxMHB4O1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogN3B4O1xyXG4gICAgfVxyXG4gICAgLnNlbmQtaWNvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHJpZ2h0OiA2OCU7XHJcbiAgICAgICAgYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcbn1cclxuLnRodW1fbmFpbHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIHpvb206MTAlO1xyXG4gICAgYm9yZGVyOiAyMHB4IHNvbGlkICNkN2ZiOWY7XHJcbn1cclxuXHJcbi5pbWdfd3JhcHBlcntcclxuICAgIG1hcmdpbjogMjRweCAwcHggMHB4IDBweDtcclxufVxyXG4gIFxyXG4jeW91ckJ0biB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICBib3R0b206IDVweDtcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4O1xyXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDIwZGVnKTsgXHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyMGRlZyk7ICBcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcblxyXG5cclxuICIsIioge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMXB4O1xufVxuKiBpb24tY2FyZCB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5tb2JpbGVDYXJkIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLmdldENvbnZlcnNhdGlvbkZhaWwge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5ib3JkZXItcGVuZGluZyB7XG4gIGJvcmRlci1sZWZ0OiAzcHggc29saWQgb3JhbmdlO1xufVxuXG4uYm9yZGVyLWNsb3NlZCB7XG4gIGJvcmRlci1sZWZ0OiAzcHggc29saWQgIzAwODA2MDtcbn1cblxuLmJvcmRlci1vcGVuIHtcbiAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCAjMDA4NGZmO1xufVxuXG4ubWFpbi13cmFwcGVyIHtcbiAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL3RpY2tldHNfaW1nL2xpZ2h0X2JnLnBuZ1wiKSByZXBlYXQgY2VudGVyIGNlbnRlci9jb3ZlcjtcbiAgaGVpZ2h0OiA4MnZoO1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG59XG5cbi50by13cmFwcGVyIHtcbiAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL3RpY2tldHNfaW1nL2xlZnRTaWRlX2JnLmpwZ1wiKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlci9jb3ZlcjtcbiAgaGVpZ2h0OiBhdXRvO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbjogMDtcbn1cblxuLmZyb20td3JhcHBlciB7XG4gIGJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy90aWNrZXRzX2ltZy9yaWdodFNpZGVfYmcuanBnXCIpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyL2NvdmVyO1xuICBoZWlnaHQ6IGF1dG87XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW46IDBweDtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uZGF0ZV9sYWJlbCB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxMHB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5kYXRlX2Jsb2NrIHtcbiAgY29sb3I6ICM1OTU5NTk7XG59XG5cbi5kYXRhQmxvY2sge1xuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xufVxuXG4uaW1nQmxvY2sge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi50aWNrZXQtdGl0bGUge1xuICBmb250LXNpemU6IDE4cHg7XG59XG5cbi5kaXNwbGF5LXdyYXBwZXIge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4udGlja2V0LWlkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2U5ZThlODtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDBweCA1cHggNXB4O1xuICBiYWNrZ3JvdW5kOiAjOUMyN0IwO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGhlaWdodDogMjVweDtcbiAgcGFkZGluZzogMnB4IDhweDtcbiAgcG9zaXRpb246IC13ZWJraXQtc3RpY2t5O1xuICBwb3NpdGlvbjogc3RpY2t5O1xuICB6LWluZGV4OiAxO1xuICB0b3A6IC01cHg7XG4gIG1hcmdpbjogLTVweCBhdXRvIDA7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnRpY2tldHMtaXRlbSB7XG4gIGhlaWdodDogNDBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZTBlMDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDZweDtcbn1cblxuLnNlbmQtY29sIHtcbiAgd2lkdGg6IDE1JTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDBweDtcbiAgcmlnaHQ6IDA7XG59XG5cbi5zZW5kLWljb24ge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogIzBiYTA4YztcbiAgYm9yZGVyLXJhZGl1czogMTZweDtcbiAgcGFkZGluZzogN3B4O1xufVxuXG4ud3JhcHBlci1jb250YWluZXIge1xuICBmb250LXNpemU6IDEycHg7XG4gIHBhZGRpbmc6IDBweCAyNXB4O1xufVxuXG4uY29udGFjdC1wYW5hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmOGY4ZmI7XG4gIGhlaWdodDogOTB2aDtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xufVxuXG4uY29udGFjdC1wYW5hbDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogNHB4O1xufVxuXG4uY29udGFjdC1wYW5hbDo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xuICBiYWNrZ3JvdW5kOiAjZjZmNmY2O1xufVxuXG4uY29udGFjdC1wYW5hbDo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjYjZiNWI1O1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDMyMHB4KSBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgLnNlY29uZC1jb2wge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG5cbiAgLnRpY2tldHMtaXRlbSB7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBsZWZ0OiAxMXB4O1xuICB9XG5cbiAgLmNvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiA4OXZoO1xuICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgfVxuXG4gIGlvbi1jYXJkIHtcbiAgICBtYXJnaW46IDVweCAxMHB4O1xuICAgIHBhZGRpbmctbGVmdDogN3B4O1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2N3B4KSB7XG4gIC5tYWluLXdyYXBwZXI6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICB3aWR0aDogMTBweDtcbiAgfVxuXG4gIC5tYWluLXdyYXBwZXI6Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjZmNmY2O1xuICB9XG5cbiAgLm1haW4td3JhcHBlcjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICAgIGJhY2tncm91bmQ6ICNhZmFkYWQ7XG4gIH1cblxuICAubWFpbi13cmFwcGVyOjotd2Via2l0LXNjcm9sbGJhci10aHVtYjpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2I2YjViNTtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjdweCkgYW5kIChtYXgtd2lkdGg6IDEwMjJweCkge1xuICAudGlja2V0cy1pdGVtIHtcbiAgICB3aWR0aDogMzUwcHg7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvdHRvbTogN3B4O1xuICAgIHJpZ2h0OiA4JTtcbiAgfVxuXG4gIGlvbi1jYXJkIHtcbiAgICBtYXJnaW46IDVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDdweDtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDIzcHgpIHtcbiAgLnRpY2tldHMtaXRlbSB7XG4gICAgd2lkdGg6IDUwMHB4O1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBib3R0b206IDZweDtcbiAgICByaWdodDogOSU7XG4gIH1cblxuICBpb24tY2FyZCB7XG4gICAgbWFyZ2luOiA1cHggMHB4O1xuICAgIHBhZGRpbmctbGVmdDogN3B4O1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEzNTBweCkge1xuICAudGlja2V0cy1pdGVtIHtcbiAgICByaWdodDogMTUlO1xuICB9XG5cbiAgLmNvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiA5MXZoO1xuICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgfVxuXG4gIGlvbi1jYXJkIHtcbiAgICBtYXJnaW46IDVweCAxMHB4O1xuICAgIHBhZGRpbmctbGVmdDogN3B4O1xuICB9XG5cbiAgLnNlbmQtaWNvbiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHJpZ2h0OiA2OCU7XG4gICAgYm90dG9tOiA1cHg7XG4gIH1cbn1cbi50aHVtX25haWwge1xuICBmbG9hdDogcmlnaHQ7XG4gIHpvb206IDEwJTtcbiAgYm9yZGVyOiAyMHB4IHNvbGlkICNkN2ZiOWY7XG59XG5cbi5pbWdfd3JhcHBlciB7XG4gIG1hcmdpbjogMjRweCAwcHggMHB4IDBweDtcbn1cblxuI3lvdXJCdG4ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgYm90dG9tOiA1cHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4O1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgyMGRlZyk7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/ticket/ticket-conversation/ticket-conversation.page.ts":
/*!************************************************************************!*\
  !*** ./src/app/ticket/ticket-conversation/ticket-conversation.page.ts ***!
  \************************************************************************/
/*! exports provided: TicketConversationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketConversationPage", function() { return TicketConversationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_ticket_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/ticket-service.service */ "./src/app/services/ticket-service.service.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm2015/ng2-file-upload.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var recordrtc__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! recordrtc */ "./node_modules/recordrtc/RecordRTC.js");
/* harmony import */ var recordrtc__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(recordrtc__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _image_conversation_image_conversation_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../image-conversation/image-conversation.component */ "./src/app/ticket/image-conversation/image-conversation.component.ts");












let TicketConversationPage = class TicketConversationPage {
    constructor(platform, location, TicketService, ajaxService, commonService, router, modalController) {
        this.platform = platform;
        this.location = location;
        this.TicketService = TicketService;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.router = router;
        this.modalController = modalController;
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploader"]({});
        this.data = [];
        this.data1 = [];
        this.image = "";
        this.countCoversation = 2;
        this.fabButton = false;
        this.responceData = 0;
        this.messages = false;
        this.customerId = false;
        this.sendbtn = false;
        //New****
        this.title = 'micRecorder';
        this.recording = false;
    }
    sanitize(url) {
        return this.domSanitizer.bypassSecurityTrustUrl(url);
    }
    initiateRecording() {
        this.recording = true;
        let mediaConstraints = {
            audio: true
        };
        navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallback.bind(this), this.errorCallback.bind(this));
    }
    successCallback(stream) {
        var options = {
            mimeType: "audio/wav",
        };
        var StereoAudioRecorder = recordrtc__WEBPACK_IMPORTED_MODULE_10__["StereoAudioRecorder"];
        this.record = new StereoAudioRecorder(stream, options);
        this.record.record();
    }
    stopRecording() {
        this.recording = false;
        this.record.stop(this.processRecording.bind(this));
    }
    /**
    * processRecording Do what ever you want with blob
    * @param  {any} blob Blog
    */
    processRecording(blob, fileName) {
        console.log(new File([blob], fileName, { lastModified: new Date().getTime(), type: blob.type }));
        this.audioUrl = new File([blob], fileName, { lastModified: new Date().getTime(), type: blob.type });
        this.sendAudioMsg();
    }
    errorCallback(error) {
        this.error = 'Can not play audio in your browser';
        this.commonService.presentToast("Audio device not supported");
    }
    sendAudioMsg() {
        var imgData = new FormData();
        imgData.append("attachments[]", this.audioUrl);
        imgData.append("body", ".");
        console.log(imgData);
        this.uploader.queue.length = 0;
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].ticketUrl + "/api/v2/tickets/" + this.id + "/reply";
        this.TicketService.ticketPostImage(url, imgData)
            .subscribe(res => {
            console.log(res);
            if (res.ticket_id + '' == this.id) {
                this.commonService.presentToast('Sent successfully');
                this.image = '';
            }
            else {
                this.commonService.presentToast('Please try again');
            }
            this.getChats(this.id);
            this.answer = '';
        });
    }
    getBack() {
        this.location.back();
    }
    sendReplyMsg() {
        this.sendbtn = false;
        if (this.uploader.queue.length == 0) {
            this.replyObj = { "body": this.image };
            if (this.image["body"] !== "") {
                const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].ticketUrl + "/api/v2/tickets/" + this.id + "/reply";
                this.TicketService.ticketPost(url, this.replyObj)
                    .subscribe(res => {
                    console.log(res);
                    if (res.ticket_id + '' == this.id) {
                        this.commonService.presentToast('Sent successfully');
                    }
                    else {
                        this.commonService.presentToast('Please try again');
                    }
                    this.getChats(this.id);
                    this.image = '';
                });
            }
        }
        else {
            var currentdate = new Date();
            var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth() + 1) + "/"
                + currentdate.getFullYear() + "  "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();
            this.sendbtn = false;
            if (this.uploader.queue.length == 0) {
                this.commonService.presentToast("please insert one document ");
            }
            else {
                const file = this.uploader;
                var imgData = new FormData();
                // this.replyObj = { "attachments[]": imgData};
                for (let i = 0; i < file.queue.length; i++) {
                    imgData.append("attachments[]", file.queue[i]._file);
                }
                imgData.append("body", ".");
                //imgData.append('data',JSON.stringify(this.image));
                //const data=JSON.stringify(imgData)
                console.log(imgData);
                // this.replyObj = { "attachments[]": imgData};
                //if(this.answer["body"] !== ""){
                this.uploader.queue.length = 0;
                const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].ticketUrl + "/api/v2/tickets/" + this.id + "/reply";
                this.TicketService.ticketPostImage(url, imgData)
                    .subscribe(res => {
                    console.log(res);
                    if (res.ticket_id + '' == this.id) {
                        this.commonService.presentToast('Sent successfully');
                        this.image = '';
                    }
                    else {
                        this.commonService.presentToast('Please try again');
                    }
                    this.getChats(this.id);
                    this.answer = '';
                });
                //}
            }
        }
    }
    getFile() {
        document.getElementById("upfile").click();
    }
    enableSendbtn(data) {
        if (data.target.value == '') {
            this.sendbtn = false;
        }
        else {
            this.sendbtn = true;
        }
    }
    getChats(id) {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].ticketUrl + "/api/v2/tickets/" + id + "/conversations";
        this.TicketService.ticketGet(url)
            .subscribe(res => {
            console.log(res);
            this.conversation = res;
            if (this.conversation.length == 30) {
                this.countCoversation = 2;
                this.getMoreChats(id);
            }
        });
    }
    enlargeImage(image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _image_conversation_image_conversation_component__WEBPACK_IMPORTED_MODULE_11__["ImageConversationComponent"],
                componentProps: {
                    value: image,
                }
            });
            return yield modal.present();
        });
    }
    getMoreChats(id) {
        this.commonService.presentLoader();
        const url1 = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].ticketUrl + "/api/v2/tickets/" + id + "/conversations?page=" + this.countCoversation;
        this.TicketService.ticketGet(url1).subscribe(res => {
            this.countCoversation++;
            this.conversation = this.conversation.concat(res);
            this.commonService.dismissLoader();
            if (res.length == 30) {
                this.getMoreChats(id);
            }
        });
    }
    subject(obj) {
        this.file = obj.target.value;
        this.fileName = this.file.split("\\");
        document.getElementById("yourBtn").innerHTML = this.fileName[this.fileName.length - 1];
        console.log(this.file);
        //document.myForm.submit();
        //event.preventDefault();
        console.log(this.uploader.queue[0].file.name);
    }
    ngOnInit() {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        //this.messages = true;
        this.sub = localStorage.getItem('subject');
        this.id = localStorage.getItem('coversationId');
        if (this.id.length >= 1) {
            this.getChats(this.id);
        }
    }
};
TicketConversationPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] },
    { type: _services_ticket_service_service__WEBPACK_IMPORTED_MODULE_7__["TicketServiceService"] },
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
TicketConversationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ticket-conversation',
        template: __webpack_require__(/*! raw-loader!./ticket-conversation.page.html */ "./node_modules/raw-loader/index.js!./src/app/ticket/ticket-conversation/ticket-conversation.page.html"),
        styles: [__webpack_require__(/*! ./ticket-conversation.page.scss */ "./src/app/ticket/ticket-conversation/ticket-conversation.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"],
        _services_ticket_service_service__WEBPACK_IMPORTED_MODULE_7__["TicketServiceService"],
        _services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], TicketConversationPage);



/***/ })

}]);
//# sourceMappingURL=ticket-ticket-conversation-ticket-conversation-module-es2015.js.map