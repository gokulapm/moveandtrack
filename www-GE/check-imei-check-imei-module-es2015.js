(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["check-imei-check-imei-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/check-imei/check-imei.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/check-imei/check-imei.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar class=\"dealerHeader\">\r\n     <ion-row>\r\n      <ion-icon class=\"iconSize25px\" (click)=\"closePage()\" name=\"arrow-back\"></ion-icon>\r\n      <ion-title>Check Imei</ion-title>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n<div class=\"padding25px\">\r\n <ion-row> \r\n    <ion-col size=\"12\" id=\"radio-wrapper\">\r\n      <ion-radio-group allow-empty-selection=\"false\" name=\"radio-group\"  value=\"Imei Number\" (ionChange)=\"getImeiMobileNumber($event)\"  >\r\n        <ion-col size='8' *ngFor=\"let items of imeiMobileSearch\">\r\n          <ion-col size='3'>\r\n            <ion-radio id=\"radio-btn\" checked [value]=\"items\"></ion-radio>\r\n          </ion-col>\r\n          <ion-col size='8'>\r\n            <ion-label>{{items}}</ion-label>\r\n          </ion-col>\r\n        </ion-col>\r\n      </ion-radio-group>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <form [formGroup]=\"numberSearch\">\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n          <ion-row class=\"paddingBottom10px\">\r\n            <ion-input type=\"tel\" [maxlength]=\"maxNum\" class=\"imeibox\"   [placeholder]=\"holder\"  formControlName=\"commonNumber\"></ion-input>\r\n          </ion-row>\r\n        </ion-col>\r\n      \r\n      </ion-row>\r\n      <ion-row >\r\n        <ion-col style=\"text-align: center;\">\r\n        <ion-button class=\"buttonWidth\" (click)='submit()' [disabled]=\"!numberSearch.valid || numberSearch.value.commonNumber.length != maxNum\">Submit</ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n    \r\n  </form>\r\n    </div>\r\n   <ion-row *ngIf=\"reportData\">\r\n      <ion-col size=12 size-sm=\"12\" size-lg=\"4\" size-md=\"6\" *ngFor=\"let ImeiDetail of reportData\" style=\"margin: auto;\">\r\n        <ion-card style=\"border-left: 4px solid #7c68f8; margin: 0px;\">\r\n          <ion-card-content>\r\n            <ion-row >\r\n              <ion-col size=12>\r\n                <ion-row>\r\n                  <ion-col size=8 class=\"ImeiStyle\">\r\n                    {{ImeiDetail.plateNo}}\r\n                  </ion-col>\r\n                  <ion-col size=4 class=\"ImeiStyle Expirystyle\">\r\n                    {{ImeiDetail.warrantyExpiryDate}}\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                  <ion-col size=12 class=\"ImeiNo\">\r\n                    {{ImeiDetail.imei}}\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                  <ion-col size=12 class=\"ImeiStyle\">\r\n                    <ion-icon class=\"iconColor\" name=\"call\"></ion-icon> {{ImeiDetail.simcardNo}}\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                  <ion-col size=12 class=\"ImeiStyle\">\r\n                    <ion-icon class=\"iconColor\" name=\"business\"></ion-icon> {{ImeiDetail.companyId}} ({{ImeiDetail.currentAgency}})\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"timestampRow\">\r\n              <ion-col size=12 class=\"ImeiStyle timestampCol\">\r\n                {{ImeiDetail.serverTimeStamp}}\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n      </ion-row>\r\n \r\n\r\n      <ion-row class=\"company-wrapper\" *ngIf=\"mobileNumber\">\r\n        <ion-col size=12 size-sm=\"12\" size-lg=\"4\" size-md=\"6\" style=\"margin: auto;\">\r\n          <ion-card style=\"border-left: 4px solid #7c68f8; margin: 0px;\" >\r\n            <ion-card-content>\r\n              <ion-row>\r\n                <ion-col size=9>\r\n                  <ion-row class=\"companyName uppercase\">\r\n                    {{mobileNumber.companyName}}\r\n                  </ion-row>\r\n                  <ion-row class=\"centerAlign\">\r\n                    <ion-icon class=\"iconColor\" name=\"call\"></ion-icon> <label> {{mobileNumber.contact}}</label>\r\n                  </ion-row>\r\n                  <ion-row class=\"centerAlign\">\r\n                    <ion-icon class=\"iconColor\" name=\"mail\"></ion-icon> <label> {{mobileNumber.emailId}}</label>\r\n                  </ion-row>\r\n                </ion-col>\r\n                <ion-col size=3 class=\"centerColNum\">\r\n                  <ion-label>\r\n                    {{mobileNumber.vehicle_Count}}\r\n                  </ion-label>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-card-content>\r\n          </ion-card>\r\n        </ion-col>\r\n      </ion-row>\r\n     \r\n  </ion-content>\r\n  \r\n"

/***/ }),

/***/ "./src/app/delar-application/check-imei/check-imei.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/delar-application/check-imei/check-imei.module.ts ***!
  \*******************************************************************/
/*! exports provided: CheckImeiPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckImeiPageModule", function() { return CheckImeiPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _check_imei_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./check-imei.page */ "./src/app/delar-application/check-imei/check-imei.page.ts");







const routes = [
    {
        path: '',
        component: _check_imei_page__WEBPACK_IMPORTED_MODULE_6__["CheckImeiPage"]
    }
];
let CheckImeiPageModule = class CheckImeiPageModule {
};
CheckImeiPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_check_imei_page__WEBPACK_IMPORTED_MODULE_6__["CheckImeiPage"]]
    })
], CheckImeiPageModule);



/***/ }),

/***/ "./src/app/delar-application/check-imei/check-imei.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/delar-application/check-imei/check-imei.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".iconSize25px {\n  font-size: 25px;\n}\n\n.image-size {\n  height: 35px;\n  width: 35px;\n  cursor: pointer;\n  display: inline-block;\n  margin: 5px;\n}\n\n.download-wrapper {\n  padding: 6px;\n  background: #eaeaea;\n}\n\n.download-wrapper:hover {\n  background-color: #f6f6f6;\n}\n\n.subscription-title {\n  font-size: 20px;\n}\n\n.reportInfo {\n  color: #000000;\n  font-size: 14px;\n  font-family: sans-serif;\n}\n\n.paddingBottom10px {\n  padding-bottom: 4px;\n}\n\n.padding25px {\n  padding: 20px;\n}\n\n.buttonWidth {\n  width: 33%;\n  height: 30px;\n  --ion-color-primary: #7c68f8;\n}\n\n.imeibox {\n  background-color: #e8e8e8;\n  color: #676464;\n  width: 100%;\n}\n\n.padding0px {\n  padding: 0px;\n}\n\n.ImeiStyle {\n  font-family: sans-serif;\n  font-size: 11px;\n  text-transform: uppercase;\n  padding: 0px;\n  align-self: center;\n}\n\n.ImeiNo {\n  font-family: sans-serif;\n  font-size: x-large;\n  font-weight: bold;\n  color: #3c3c3c;\n  padding: 0px;\n}\n\n.Expirystyle {\n  padding: 3px;\n  background: #dadada;\n  color: #3c3c3c;\n  text-align: center;\n  border-radius: 29px;\n}\n\n.timestampRow {\n  text-align: center;\n  border-top: 1px solid #dadada;\n}\n\n.timestampCol {\n  padding: 6px;\n}\n\n.textCenter {\n  text-align: center;\n}\n\n.iconColor {\n  color: #7c68f8;\n  padding-right: 7px;\n}\n\n#radio-wrapper {\n  padding: 10px;\n  text-align: center;\n}\n\n#radio-btn {\n  vertical-align: middle;\n}\n\n.companyName {\n  color: #545454;\n  font-family: sans-serif;\n  font-size: 16px;\n  font-weight: bold;\n}\n\n.uppercase {\n  text-transform: uppercase;\n}\n\n.iconColor {\n  color: #7c68f8;\n  padding-right: 7px;\n}\n\n.centerAlign {\n  -webkit-box-align: center;\n          align-items: center;\n  font-family: sans-serif;\n  font-size: small;\n}\n\n.centerColNum {\n  align-self: center;\n  font-size: x-large;\n  text-align: center;\n  border: 2px solid #7c68f8;\n  border-radius: 100px;\n  background: #f1f1f1;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vY2hlY2staW1laS9jaGVjay1pbWVpLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vY2hlY2staW1laS9jaGVjay1pbWVpLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7QUNDRjs7QURDQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBRUEsZUFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBQ0NGOztBRENBO0VBQ0UsWUFBQTtFQUNBLG1CQUFBO0FDRUY7O0FEQUE7RUFDRSx5QkFBQTtBQ0dGOztBRERBO0VBQ0UsZUFBQTtBQ0lGOztBREZBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtBQ0tGOztBREZBO0VBQ0UsbUJBQUE7QUNLRjs7QURGQTtFQUNFLGFBQUE7QUNLRjs7QURGQTtFQUNFLFVBQUE7RUFDQSxZQUFBO0VBQ0EsNEJBQUE7QUNLRjs7QURGQTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUNLRjs7QURIQTtFQUNFLFlBQUE7QUNNRjs7QURIQTtFQUNFLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDTUY7O0FESEE7RUFDRSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ01GOztBREhBO0VBQ0UsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNNRjs7QURIQTtFQUNFLGtCQUFBO0VBQ0EsNkJBQUE7QUNNRjs7QURIQTtFQUNFLFlBQUE7QUNNRjs7QURIQTtFQUNFLGtCQUFBO0FDTUY7O0FESEE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7QUNNRjs7QURIQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtBQ01GOztBREpBO0VBQ0Usc0JBQUE7QUNPRjs7QURIQTtFQUNFLGNBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ01GOztBREhBO0VBQ0UseUJBQUE7QUNNRjs7QURKQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtBQ09GOztBREpBO0VBQ0UseUJBQUE7VUFBQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7QUNPRjs7QURMQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNRRiIsImZpbGUiOiJzcmMvYXBwL2RlbGFyLWFwcGxpY2F0aW9uL2NoZWNrLWltZWkvY2hlY2staW1laS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaWNvblNpemUyNXB4IHtcclxuICBmb250LXNpemU6IDI1cHg7XHJcbn1cclxuLmltYWdlLXNpemUge1xyXG4gIGhlaWdodDogMzVweDtcclxuICB3aWR0aDogMzVweDtcclxuXHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBtYXJnaW46IDVweDtcclxufVxyXG4uZG93bmxvYWQtd3JhcHBlciB7XHJcbiAgcGFkZGluZzogNnB4O1xyXG4gIGJhY2tncm91bmQ6ICNlYWVhZWE7XHJcbn1cclxuLmRvd25sb2FkLXdyYXBwZXI6aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XHJcbn1cclxuLnN1YnNjcmlwdGlvbi10aXRsZSB7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcbi5yZXBvcnRJbmZvIHtcclxuICBjb2xvcjogIzAwMDAwMDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi5wYWRkaW5nQm90dG9tMTBweCB7XHJcbiAgcGFkZGluZy1ib3R0b206IDRweDtcclxufVxyXG5cclxuLnBhZGRpbmcyNXB4IHtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG59XHJcblxyXG4uYnV0dG9uV2lkdGgge1xyXG4gIHdpZHRoOiAzMyU7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIC0taW9uLWNvbG9yLXByaW1hcnk6ICM3YzY4Zjg7XHJcbn1cclxuXHJcbi5pbWVpYm94IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gIGNvbG9yOiAjNjc2NDY0O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5wYWRkaW5nMHB4IHtcclxuICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbi5JbWVpU3R5bGUge1xyXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc2l6ZTogMTFweDtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIHBhZGRpbmc6IDBweDtcclxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5JbWVpTm8ge1xyXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBjb2xvcjogIzNjM2MzYztcclxuICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbi5FeHBpcnlzdHlsZSB7XHJcbiAgcGFkZGluZzogM3B4O1xyXG4gIGJhY2tncm91bmQ6ICNkYWRhZGE7XHJcbiAgY29sb3I6ICMzYzNjM2M7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGJvcmRlci1yYWRpdXM6IDI5cHg7XHJcbn1cclxuXHJcbi50aW1lc3RhbXBSb3cge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2RhZGFkYTtcclxufVxyXG5cclxuLnRpbWVzdGFtcENvbCB7XHJcbiAgcGFkZGluZzogNnB4O1xyXG59XHJcblxyXG4udGV4dENlbnRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uaWNvbkNvbG9yIHtcclxuICBjb2xvcjogIzdjNjhmODtcclxuICBwYWRkaW5nLXJpZ2h0OiA3cHg7XHJcbn1cclxuXHJcbiNyYWRpby13cmFwcGVyIHtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4jcmFkaW8tYnRuIHtcclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG4vL01vYmlsZSBjYXJkXHJcbi5jb21wYW55TmFtZSB7XHJcbiAgY29sb3I6ICM1NDU0NTQ7XHJcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4udXBwZXJjYXNlIHtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcbi5pY29uQ29sb3Ige1xyXG4gIGNvbG9yOiAjN2M2OGY4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDdweDtcclxufVxyXG5cclxuLmNlbnRlckFsaWduIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc2l6ZTogc21hbGw7XHJcbn1cclxuLmNlbnRlckNvbE51bXtcclxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBib3JkZXI6IDJweCBzb2xpZCAjN2M2OGY4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xyXG4gIGJhY2tncm91bmQ6ICNmMWYxZjE7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59IiwiLmljb25TaXplMjVweCB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmltYWdlLXNpemUge1xuICBoZWlnaHQ6IDM1cHg7XG4gIHdpZHRoOiAzNXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiA1cHg7XG59XG5cbi5kb3dubG9hZC13cmFwcGVyIHtcbiAgcGFkZGluZzogNnB4O1xuICBiYWNrZ3JvdW5kOiAjZWFlYWVhO1xufVxuXG4uZG93bmxvYWQtd3JhcHBlcjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XG59XG5cbi5zdWJzY3JpcHRpb24tdGl0bGUge1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5yZXBvcnRJbmZvIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG59XG5cbi5wYWRkaW5nQm90dG9tMTBweCB7XG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XG59XG5cbi5wYWRkaW5nMjVweCB7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG5cbi5idXR0b25XaWR0aCB7XG4gIHdpZHRoOiAzMyU7XG4gIGhlaWdodDogMzBweDtcbiAgLS1pb24tY29sb3ItcHJpbWFyeTogIzdjNjhmODtcbn1cblxuLmltZWlib3gge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xuICBjb2xvcjogIzY3NjQ2NDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5wYWRkaW5nMHB4IHtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4uSW1laVN0eWxlIHtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgcGFkZGluZzogMHB4O1xuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG59XG5cbi5JbWVpTm8ge1xuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMzYzNjM2M7XG4gIHBhZGRpbmc6IDBweDtcbn1cblxuLkV4cGlyeXN0eWxlIHtcbiAgcGFkZGluZzogM3B4O1xuICBiYWNrZ3JvdW5kOiAjZGFkYWRhO1xuICBjb2xvcjogIzNjM2MzYztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiAyOXB4O1xufVxuXG4udGltZXN0YW1wUm93IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2RhZGFkYTtcbn1cblxuLnRpbWVzdGFtcENvbCB7XG4gIHBhZGRpbmc6IDZweDtcbn1cblxuLnRleHRDZW50ZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5pY29uQ29sb3Ige1xuICBjb2xvcjogIzdjNjhmODtcbiAgcGFkZGluZy1yaWdodDogN3B4O1xufVxuXG4jcmFkaW8td3JhcHBlciB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI3JhZGlvLWJ0biB7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbi5jb21wYW55TmFtZSB7XG4gIGNvbG9yOiAjNTQ1NDU0O1xuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnVwcGVyY2FzZSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5pY29uQ29sb3Ige1xuICBjb2xvcjogIzdjNjhmODtcbiAgcGFkZGluZy1yaWdodDogN3B4O1xufVxuXG4uY2VudGVyQWxpZ24ge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuLmNlbnRlckNvbE51bSB7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlcjogMnB4IHNvbGlkICM3YzY4Zjg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xuICBjb2xvcjogYmxhY2s7XG59Il19 */"

/***/ }),

/***/ "./src/app/delar-application/check-imei/check-imei.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/delar-application/check-imei/check-imei.page.ts ***!
  \*****************************************************************/
/*! exports provided: CheckImeiPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckImeiPage", function() { return CheckImeiPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _services_ajax_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");








let CheckImeiPage = class CheckImeiPage {
    constructor(commonService, ajaxService, location, platform, formBuilder) {
        this.commonService = commonService;
        this.ajaxService = ajaxService;
        this.location = location;
        this.platform = platform;
        this.formBuilder = formBuilder;
        this.imeiMobileSearch = ['Imei Number', 'Mobile Number'];
        this.holder = "Enter the Imei Number ";
        this.maxNum = 15;
        this.disabled = true;
    }
    closePage() {
        this.location.back();
    }
    submit() {
        if (this.numberSearch.value.commonNumber.length == 15) {
            const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/global/searchimei?imeiNo=' + this.numberSearch.value.commonNumber;
            this.ajaxService.ajaxGet(url).subscribe(res => {
                this.reportData = res;
                this.numberSearch.reset();
                if (res.length == 0) {
                    this.commonService.presentToast("Imei not available");
                }
            });
        }
        else {
            const url2 = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/global/searchcontact?contactNo=' + this.numberSearch.value.commonNumber;
            this.ajaxService.ajaxGet(url2).subscribe(res => {
                if (res.contact == '-') {
                    this.commonService.presentToast("Mobile number not available");
                    this.mobileNumber = '';
                }
                else {
                    this.mobileNumber = res;
                }
                this.numberSearch.reset();
            });
        }
    }
    getImeiMobileNumber(data) {
        this.numberSearch.reset();
        if (data.target.value == 'Imei Number') {
            this.holder = "Enter the Imei Number ";
            this.maxNum = 15;
            this.mobileNumber = '';
        }
        else if (data.target.value == 'Mobile Number') {
            this.holder = "Enter the Mobile Number ";
            this.maxNum = 10;
            this.reportData = '';
        }
    }
    ngOnInit() {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
        this.numberSearch = this.formBuilder.group({
            commonNumber: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required]
        });
    }
};
CheckImeiPage.ctorParameters = () => [
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"] },
    { type: _services_ajax_service__WEBPACK_IMPORTED_MODULE_3__["AjaxService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"] }
];
CheckImeiPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-check-imei',
        template: __webpack_require__(/*! raw-loader!./check-imei.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/check-imei/check-imei.page.html"),
        styles: [__webpack_require__(/*! ./check-imei.page.scss */ "./src/app/delar-application/check-imei/check-imei.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _services_ajax_service__WEBPACK_IMPORTED_MODULE_3__["AjaxService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"]])
], CheckImeiPage);



/***/ })

}]);
//# sourceMappingURL=check-imei-check-imei-module-es2015.js.map