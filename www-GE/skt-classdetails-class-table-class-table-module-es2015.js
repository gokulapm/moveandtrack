(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["skt-classdetails-class-table-class-table-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/skt/classdetails/class-table/class-table.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/skt/classdetails/class-table/class-table.page.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <div class=\"class-wrapper\">\n     <ion-row  class=\"header-section\">\n      <ion-col size=\"2\" size-sm=\"2\" size-md=\"2\" size-lg=\"0\"> \n        <ion-menu-button></ion-menu-button>\n      </ion-col>  \n      <ion-col size=\"10\" size-sm=\"10\" size-md=\"10\" size-lg=\"12\" id=\"btn-wrapper\">\n        Class Details\n      </ion-col>\n    </ion-row>\n   <ion-row>\n    <ion-col>\n      <ion-button color='primary' class=\"class-btn\" (click)='openparentDetailModel()'>Add</ion-button>\n      <ion-button color='primary' class=\"class-btn\" (click)='parentEditModel()'>Edit</ion-button>\n      <ion-button color='primary' class=\"class-btn\" (click)='deletebtn()'>Delete</ion-button>\n    </ion-col>\n    </ion-row> \n  </div> \n  <div id=\"export-wrapper\">\n    <!-- <ion-img src=\"assets/student_Details/print.svg\"  class=\"toolbar-row\" (click)=\"btnOnClick()\"></ion-img> -->\n    <ion-img src=\"assets/student_Details/pdf.svg\" class=\"toolbar-row\" (click)=\"createPdf()\"></ion-img>\n    <ion-img src=\"assets/student_Details/excel.svg\" class=\"toolbar-row\"  (click)=\"exportToExcel()\"> </ion-img>\n          <!-- <ion-img src=\"assets/student_Details/refresher.svg\" class=\"toolbar-row\"></ion-img> -->\n    </div> \n   </ion-toolbar>\n</ion-header>\n<ion-content>\n <div class=\"grid_Container\">\n  <jqxGrid\n  (onRowselect)=\"myGridOnRowSelect($event)\"\n  [pageable]=\"true\"\n  [selectionmode]=\"'singlerow'\"\n  [showfilterrow]=\"true\"\n  [filterable]=\"true\"\n  [sortable]=\"true\"\n  [width]=\"'100%'\"\n  [source]=\"dataAdapter\" \n  [columns]=\"columns\" \n  [autoheight]=\"true\"\n  [theme]=\"'material'\"\n  #myGrid \n  style=\"font-size:16px;text-align: center !important;\">\n  </jqxGrid>\n</div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/skt/classdetails/class-table/class-table.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/skt/classdetails/class-table/class-table.module.ts ***!
  \********************************************************************/
/*! exports provided: ClassTablePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassTablePageModule", function() { return ClassTablePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _class_table_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./class-table.page */ "./src/app/skt/classdetails/class-table/class-table.page.ts");
/* harmony import */ var _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../sktcomponents.module */ "./src/app/skt/sktcomponents.module.ts");
/* harmony import */ var src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared-mod/shared-mod.module */ "./src/app/shared-mod/shared-mod.module.ts");









const routes = [
    {
        path: '',
        component: _class_table_page__WEBPACK_IMPORTED_MODULE_6__["ClassTablePage"]
    }
];
let ClassTablePageModule = class ClassTablePageModule {
};
ClassTablePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _sktcomponents_module__WEBPACK_IMPORTED_MODULE_7__["SktComponentsModule"],
            src_app_shared_mod_shared_mod_module__WEBPACK_IMPORTED_MODULE_8__["SharedModModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [
            _class_table_page__WEBPACK_IMPORTED_MODULE_6__["ClassTablePage"],
        ],
    })
], ClassTablePageModule);



/***/ }),

/***/ "./src/app/skt/classdetails/class-table/class-table.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/skt/classdetails/class-table/class-table.page.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".class-wrapper, .grid_Container {\n  border: 1px solid #c7c3c3;\n  margin: 5px 10px 0;\n}\n\n.grid_Container {\n  margin: 0px 10px 0px;\n}\n\n.header-section {\n  background-color: #e8e8e8;\n  border: 1px solid #cac2c2;\n}\n\n#export-wrapper {\n  text-align: right;\n  background-color: #e8e8e8;\n  height: 42px;\n  border-top: 1px solid gainsboro;\n  margin: 0px 10px 0px;\n  border-left: 2px solid #c7c7c7;\n}\n\n#btn-wrapper {\n  font-weight: 700;\n  text-align: center;\n  background-color: #e8e8e8;\n  height: 40px;\n  padding: 0px;\n  font-size: 20px;\n  border-bottom: 1px solid #cac2c2;\n}\n\n.toolbar-row {\n  height: 40px;\n  width: 40px;\n  display: inline-block;\n  margin: 0px;\n  border: 1px solid #b9b7b7;\n  padding: 5px;\n}\n\n.toolbar-row:hover {\n  background-color: whitesmoke;\n}\n\n.class-btn {\n  width: 80px;\n  height: 30px;\n  margin: 5px 10px;\n  font-size: 12px;\n}\n\n.mytable:hover {\n  background-color: blue;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvc2t0L2NsYXNzZGV0YWlscy9jbGFzcy10YWJsZS9jbGFzcy10YWJsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3NrdC9jbGFzc2RldGFpbHMvY2xhc3MtdGFibGUvY2xhc3MtdGFibGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENDO0VBQ0csb0JBQUE7QUNFSjs7QURBQztFQUNDLHlCQUFBO0VBQ0EseUJBQUE7QUNHRjs7QUREQztFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsK0JBQUE7RUFDQSxvQkFBQTtFQUNBLDhCQUFBO0FDSUg7O0FERkM7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQ0FBQTtBQ0tIOztBREhDO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUNNSDs7QURKQTtFQUNJLDRCQUFBO0FDT0o7O0FETEE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ1FKOztBRE5DO0VBQ0Usc0JBQUE7QUNTSCIsImZpbGUiOiJzcmMvYXBwL3NrdC9jbGFzc2RldGFpbHMvY2xhc3MtdGFibGUvY2xhc3MtdGFibGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNsYXNzLXdyYXBwZXIsLmdyaWRfQ29udGFpbmVye1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDE5OSwgMTk1LCAxOTUpO1xyXG4gICAgbWFyZ2luOiA1cHggMTBweCAwO1xyXG4gfVxyXG4gLmdyaWRfQ29udGFpbmVye1xyXG4gICAgbWFyZ2luOiAwcHggMTBweCAwcHg7ICBcclxuIH1cclxuIC5oZWFkZXItc2VjdGlvbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjYWMyYzI7XHJcbn1cclxuICNleHBvcnQtd3JhcHBlcntcclxuICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XHJcbiAgIGhlaWdodDogNDJweDtcclxuICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGdhaW5zYm9ybztcclxuICAgbWFyZ2luOiAwcHggMTBweCAwcHg7XHJcbiAgIGJvcmRlci1sZWZ0OiAycHggc29saWQgI2M3YzdjNztcclxuIH1cclxuICNidG4td3JhcHBlcntcclxuICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xyXG4gICBoZWlnaHQ6IDQwcHg7XHJcbiAgIHBhZGRpbmc6IDBweDtcclxuICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICBib3JkZXItYm90dG9tOjFweCBzb2xpZCAjY2FjMmMyO1xyXG4gIH1cclxuIC50b29sYmFyLXJvd3tcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gICB3aWR0aDogNDBweDtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBtYXJnaW46MHB4O1xyXG4gICBib3JkZXI6IDFweCBzb2xpZCAjYjliN2I3O1xyXG4gICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuLnRvb2xiYXItcm93OmhvdmVye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcclxufVxyXG4uY2xhc3MtYnRue1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBtYXJnaW46IDVweCAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gfVxyXG4gLm15dGFibGU6aG92ZXIge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xyXG4gfVxyXG4gIiwiLmNsYXNzLXdyYXBwZXIsIC5ncmlkX0NvbnRhaW5lciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjN2MzYzM7XG4gIG1hcmdpbjogNXB4IDEwcHggMDtcbn1cblxuLmdyaWRfQ29udGFpbmVyIHtcbiAgbWFyZ2luOiAwcHggMTBweCAwcHg7XG59XG5cbi5oZWFkZXItc2VjdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZTg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjYWMyYzI7XG59XG5cbiNleHBvcnQtd3JhcHBlciB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4O1xuICBoZWlnaHQ6IDQycHg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBnYWluc2Jvcm87XG4gIG1hcmdpbjogMHB4IDEwcHggMHB4O1xuICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNjN2M3Yzc7XG59XG5cbiNidG4td3JhcHBlciB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlODtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjYWMyYzI7XG59XG5cbi50b29sYmFyLXJvdyB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNiOWI3Yjc7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLnRvb2xiYXItcm93OmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcbn1cblxuLmNsYXNzLWJ0biB7XG4gIHdpZHRoOiA4MHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIG1hcmdpbjogNXB4IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm15dGFibGU6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/skt/classdetails/class-table/class-table.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/skt/classdetails/class-table/class-table.page.ts ***!
  \******************************************************************/
/*! exports provided: ClassTablePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassTablePage", function() { return ClassTablePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jqwidgets-ng/jqxgrid */ "./node_modules/jqwidgets-ng/fesm2015/jqwidgets-ng-jqxgrid.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _class_additional_class_additional_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../class-additional/class-additional.component */ "./src/app/skt/classdetails/class-additional/class-additional.component.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");
/* harmony import */ var src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/export-excel.service */ "./src/app/services/export-excel.service.ts");





//import { StudentClassAdditionalComponent } from '../../student/student-class-additional/student-class-additional.component';





let ClassTablePage = class ClassTablePage {
    constructor(modalController, ajaxService, commonService, platform, alertController, ete) {
        this.modalController = modalController;
        this.ajaxService = ajaxService;
        this.commonService = commonService;
        this.platform = platform;
        this.alertController = alertController;
        this.ete = ete;
        this.head = ['Class Id', 'Section Id', 'Tutor', 'Class Description'];
        this.pdfdatas = [];
        this.exportTitle = "Class-details report";
    }
    deletebtn() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.selectedRow) {
                const alert = yield this.alertController.create({
                    header: 'Delete ',
                    backdropDismiss: false,
                    message: "Are you sure you want to delete?",
                    buttons: [{
                            text: 'Cancel',
                            role: 'cancel',
                            handler: data => {
                            }
                        },
                        {
                            text: 'Ok',
                            handler: data => {
                                const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/class/deleteClass?schoolId=' + localStorage.getItem('corpId') + '&branchId=' + localStorage.getItem('corpId') + '&sectionId=' + this.selectedRow.sectionId + '&classId=' + this.selectedRow.classId;
                                this.ajaxService.ajaxDeleteWithString(url).subscribe(res => {
                                    if (res.statusText == "OK") {
                                        this.myGrid.clearselection();
                                        this.getDatas();
                                    }
                                });
                            }
                        }]
                });
                yield alert.present();
            }
            else {
                this.commonService.presentToast('Please select a row to delete');
            }
        });
    }
    btnOnClick() {
        let gridContent = this.myGrid.exportdata('html');
        let newWindow = window.open('', '', 'width=800, height=500'), document = newWindow.document.open(), pageContent = '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '<meta charset="utf-8" />\n' +
            '<title>Parent Details</title>\n' +
            '</head>\n' +
            '<body>\n' + gridContent + '\n</body>\n</html>';
        document.write(pageContent);
        document.close();
        newWindow.print();
    }
    ;
    openparentDetailModel() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _class_additional_class_additional_component__WEBPACK_IMPORTED_MODULE_4__["ClassAdditionalComponent"],
                cssClass: 'my-class-css'
            });
            modal.onDidDismiss().then(() => {
                if (this.myPlatform == "desktop") {
                    this.myGrid.clearselection();
                }
                this.getDatas();
            });
            return yield modal.present();
        });
    }
    parentEditModel() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.selectedRow) {
                const modal = yield this.modalController.create({
                    component: _class_additional_class_additional_component__WEBPACK_IMPORTED_MODULE_4__["ClassAdditionalComponent"],
                    cssClass: 'my-class-css',
                    componentProps: {
                        value: this.selectedRow
                    }
                });
                modal.onDidDismiss().then(() => {
                    if (this.myPlatform == "desktop") {
                        this.myGrid.clearselection();
                    }
                    this.getDatas();
                });
                return yield modal.present();
            }
            else {
                this.commonService.presentToast('Please select a row to edit');
            }
        });
    }
    myGridOnRowSelect(event) {
        this.selectedRow = event.args.row;
    }
    createPdf() {
        this.commonService.createPdf(this.head, this.pdfdatas, this.exportTitle, this.myPlatform, 'Class-details report');
    }
    exportToExcel() {
        let reportData = {
            title: 'Class-details report',
            data: this.pdfdatas,
            headers: this.head
        };
        this.ete.exportExcel(reportData);
    }
    getDatas() {
        const url = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["serverUrl"].web + '/class/getClass?schoolId=' + localStorage.getItem('corpId') + '&branchId=' + localStorage.getItem('corpId');
        this.ajaxService.ajaxGet(url).subscribe(res => {
            var detail = res;
            this.pdfdatas = [];
            for (var i = 0; i < detail.length; i++) {
                this.pdfdatas.push([detail[i].classId, detail[i].sectionId, detail[i].tutor, detail[i].classDescription]);
            }
            this.renderer = (row, column, value) => {
                if (value == "" || null || undefined) {
                    return "----";
                }
                else {
                    return '<span  style="line-height:32px;font-size:11px;color:darkblue;margin:auto"  >' + value + '</span>';
                }
            };
            this.source =
                {
                    localdata: res,
                };
            this.dataAdapter = new jqx.dataAdapter(this.source);
            this.columns =
                [
                    { text: 'Class Id', datafield: 'classId', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Section Id', datafield: 'sectionId', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Tutor', datafield: 'tutor', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                    { text: 'Class Description', datafield: 'classDescription', cellsrenderer: this.renderer, cellsalign: 'center', align: 'center' },
                ];
        });
        this.myGrid.updatebounddata();
        this.myGrid.unselectrow;
    }
    ngAfterViewInit() {
        if (this.myPlatform == 'desktop') {
            this.myGrid.showloadelement();
        }
        this.getDatas();
    }
    ngOnInit() {
        this.myPlatform = this.platform.platforms()[0];
        if (this.myPlatform == 'tablet') {
            this.myPlatform = 'desktop';
        }
    }
};
ClassTablePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"] },
    { type: src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myGrid', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_ng_jqxgrid__WEBPACK_IMPORTED_MODULE_2__["jqxGridComponent"])
], ClassTablePage.prototype, "myGrid", void 0);
ClassTablePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-class-table',
        template: __webpack_require__(/*! raw-loader!./class-table.page.html */ "./node_modules/raw-loader/index.js!./src/app/skt/classdetails/class-table/class-table.page.html"),
        styles: [__webpack_require__(/*! ./class-table.page.scss */ "./src/app/skt/classdetails/class-table/class-table.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_6__["AjaxService"],
        src_app_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        src_app_services_export_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExportExcelService"]])
], ClassTablePage);



/***/ })

}]);
//# sourceMappingURL=skt-classdetails-class-table-class-table-module-es2015.js.map