(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-detail-profile-detail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/delar-application/profile-detail/profile-detail.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/delar-application/profile-detail/profile-detail.page.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n<ion-row class=\"backgroundGrd\"  style=\"height: 35%;\">\n  <ion-row>\n    <ion-icon class=\"iconSize25px\" (click)=\"closePage()\" name=\"arrow-back\"></ion-icon>\n  </ion-row>\n <!--  <ion-col size=12>\n    <ion-row  class=\"rowCenter\">\n      <ion-icon class=\"iconSize\" name=\"contact\"></ion-icon>\n    </ion-row>\n    <ion-row class=\"rowCenter\">\n      <ion-label class=\"capitial userFontName\">User Name</ion-label>\n    </ion-row>\n  </ion-col> -->\n</ion-row> \n<ion-row class=\"iconPlace\">\n  <ion-icon class=\"iconStyle\" style=\"font-size: 120px;\" name= \"contact\"></ion-icon>\n</ion-row>\n<ion-row class=\"rowCenter\"> \n  <ion-card class=\"card\">\n    <ion-card-content class=\"cardContent\">\n      <ion-row style=\"border-bottom: 2px solid #d9d9d9;\" class=\"rowCenter\">\n        <ion-label style=\"    padding: 19px;  \" class=\"userFontName dealerfont capitial\"> {{detail.firstName}} {{detail.lastName}}</ion-label>\n      </ion-row>\n      <ion-row class=\"rowPadding\">\n        <ion-col size=\"2\">\n          <ion-icon class=\"detailIcon\" name=\"phone-portrait\"></ion-icon> \n        </ion-col>\n        <ion-col>\n          <ion-label class=\"label\">\n            {{detail.contactNo}}\n          </ion-label> \n        </ion-col>\n      </ion-row>\n      <ion-row class=\"rowPadding\">\n        <ion-col size=\"2\">\n        <ion-icon class=\"detailIcon\" name=\"mail\"></ion-icon> \n      </ion-col>\n      <ion-col>\n        <ion-label class=\"label\">\n          {{detail.fax}}\n        </ion-label> \n        </ion-col>\n      </ion-row>\n      <ion-row class=\"rowPadding\">\n        <ion-col size=\"2\">\n        <ion-icon class=\"detailIcon\" name=\"compass\"></ion-icon> \n      </ion-col>\n      <ion-col>\n        <ion-label class=\"label\">\n          {{detail.addressLine1}}, {{detail.addressLine2}}, {{detail.addressCity}}, {{detail.countryCode}}\n        </ion-label> \n      </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-row>\n<ion-row class=\"bottomButton\">\n  <ion-button [routerLink]=\"['/manage-fleet/Profile']\" class=\"buttonWidth\">\n    Edit profile\n  </ion-button>\n</ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/delar-application/profile-detail/profile-detail.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/delar-application/profile-detail/profile-detail.module.ts ***!
  \***************************************************************************/
/*! exports provided: ProfileDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileDetailPageModule", function() { return ProfileDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile-detail.page */ "./src/app/delar-application/profile-detail/profile-detail.page.ts");







var routes = [
    {
        path: '',
        component: _profile_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProfileDetailPage"]
    }
];
var ProfileDetailPageModule = /** @class */ (function () {
    function ProfileDetailPageModule() {
    }
    ProfileDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_profile_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProfileDetailPage"]]
        })
    ], ProfileDetailPageModule);
    return ProfileDetailPageModule;
}());



/***/ }),

/***/ "./src/app/delar-application/profile-detail/profile-detail.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/delar-application/profile-detail/profile-detail.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".iconSize {\n  font-size: 140px;\n}\n\n.rowCenter {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.padding15px {\n  padding: 15px;\n}\n\n.iconSize25px {\n  font-size: 25px;\n}\n\n.userFontName {\n  font-size: large;\n  font-weight: bolder;\n  font-family: sans-serif;\n  font-style: italic;\n}\n\n.capitial {\n  text-transform: uppercase;\n}\n\n.backgroundGrd {\n  background-color: #7c68f8;\n  color: white;\n  border-bottom-right-radius: 43px;\n}\n\n.card {\n  width: 90%;\n  margin: -49px;\n  background: white;\n  border-radius: 33px;\n  height: 64vh;\n}\n\n.cardContent {\n  margin: 0px;\n  padding-top: 15%;\n}\n\n.iconStyle {\n  background-color: #fdfdff;\n  border-radius: 100%;\n}\n\n.iconPlace {\n  position: absolute;\n  z-index: 13;\n  top: 17%;\n  left: 35%;\n}\n\n.detailIcon {\n  font-size: 22px;\n  color: black;\n}\n\n.rowPadding {\n  padding: 8px;\n}\n\n.label {\n  font-size: 15px;\n  font-family: sans-serif;\n  color: black;\n}\n\n.buttonWidth {\n  width: 100%;\n  height: 30px;\n  --ion-color-primary: #7c68f8;\n}\n\n.bottomButton {\n  position: fixed;\n  bottom: 0px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWZpcS9EZXNrdG9wL0FUUyBDYXBhY2l0b3IgTmV3IGFwaSBiYWNrZW5kL3NyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vcHJvZmlsZS1kZXRhaWwvcHJvZmlsZS1kZXRhaWwucGFnZS5zY3NzIiwic3JjL2FwcC9kZWxhci1hcHBsaWNhdGlvbi9wcm9maWxlLWRldGFpbC9wcm9maWxlLWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQSxnQkFBQTtBQ0NBOztBRENBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtBQ0VBOztBREFBO0VBQ0ksYUFBQTtBQ0dKOztBRERBO0VBQ0ksZUFBQTtBQ0lKOztBRERBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUNJSjs7QURGRTtFQUNFLHlCQUFBO0FDS0o7O0FERkU7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxnQ0FBQTtBQ0tKOztBREZFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0tKOztBREZFO0VBQ0UsV0FBQTtFQUdBLGdCQUFBO0FDR0o7O0FEREE7RUFDQyx5QkFBQTtFQUNBLG1CQUFBO0FDSUQ7O0FEREU7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtBQ0lKOztBREFFO0VBQ0UsZUFBQTtFQUNBLFlBQUE7QUNHSjs7QURBRTtFQUNFLFlBQUE7QUNHSjs7QURBRTtFQUNFLGVBQUE7RUFHQSx1QkFBQTtFQUNBLFlBQUE7QUNDSjs7QURFRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsNEJBQUE7QUNDSjs7QURFRTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvZGVsYXItYXBwbGljYXRpb24vcHJvZmlsZS1kZXRhaWwvcHJvZmlsZS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmljb25TaXple1xuZm9udC1zaXplOiAxNDBweDtcbn1cbi5yb3dDZW50ZXJ7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5wYWRkaW5nMTVweHtcbiAgICBwYWRkaW5nOiAxNXB4O1xufVxuLmljb25TaXplMjVweHtcbiAgICBmb250LXNpemU6IDI1cHg7XG59XG5cbi51c2VyRm9udE5hbWV7XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgfVxuICAuY2FwaXRpYWx7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgfVxuXG4gIC5iYWNrZ3JvdW5kR3Jke1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM3YzY4Zjg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA0M3B4O1xuICB9XG5cbiAgLmNhcmR7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBtYXJnaW46IC00OXB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDMzcHg7XG4gICAgaGVpZ2h0OiA2NHZoO1xuICB9XG5cbiAgLmNhcmRDb250ZW50e1xuICAgIG1hcmdpbjogMHB4O1xuICAgIC8vIHBhZGRpbmc6IDBweDtcbiAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6IDE1JTtcbiAgfVxuLmljb25TdHlsZXtcbiBiYWNrZ3JvdW5kLWNvbG9yOiAjZmRmZGZmO1xuIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gXG59XG4gIC5pY29uUGxhY2V7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDEzO1xuICAgIHRvcDogMTclO1xuICAgIGxlZnQ6IDM1JTtcbiAgICBcbiAgfVxuXG4gIC5kZXRhaWxJY29ue1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBjb2xvcjogYmxhY2s7XG4gIH1cblxuICAucm93UGFkZGluZ3tcbiAgICBwYWRkaW5nOiA4cHg7XG4gIH1cblxuICAubGFiZWx7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIC8vIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgLy8gcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xuICAgIGNvbG9yOiBibGFjaztcbiAgfVxuXG4gIC5idXR0b25XaWR0aHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgLS1pb24tY29sb3ItcHJpbWFyeTogIzdjNjhmODtcbiAgfVxuXG4gIC5ib3R0b21CdXR0b257XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvdHRvbTogMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9IiwiLmljb25TaXplIHtcbiAgZm9udC1zaXplOiAxNDBweDtcbn1cblxuLnJvd0NlbnRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ucGFkZGluZzE1cHgge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuXG4uaWNvblNpemUyNXB4IHtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4udXNlckZvbnROYW1lIHtcbiAgZm9udC1zaXplOiBsYXJnZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbn1cblxuLmNhcGl0aWFsIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmJhY2tncm91bmRHcmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjN2M2OGY4O1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA0M3B4O1xufVxuXG4uY2FyZCB7XG4gIHdpZHRoOiA5MCU7XG4gIG1hcmdpbjogLTQ5cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAzM3B4O1xuICBoZWlnaHQ6IDY0dmg7XG59XG5cbi5jYXJkQ29udGVudCB7XG4gIG1hcmdpbjogMHB4O1xuICBwYWRkaW5nLXRvcDogMTUlO1xufVxuXG4uaWNvblN0eWxlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZkZmRmZjtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbn1cblxuLmljb25QbGFjZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMTM7XG4gIHRvcDogMTclO1xuICBsZWZ0OiAzNSU7XG59XG5cbi5kZXRhaWxJY29uIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5yb3dQYWRkaW5nIHtcbiAgcGFkZGluZzogOHB4O1xufVxuXG4ubGFiZWwge1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5idXR0b25XaWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIC0taW9uLWNvbG9yLXByaW1hcnk6ICM3YzY4Zjg7XG59XG5cbi5ib3R0b21CdXR0b24ge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJvdHRvbTogMHB4O1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/delar-application/profile-detail/profile-detail.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/delar-application/profile-detail/profile-detail.page.ts ***!
  \*************************************************************************/
/*! exports provided: ProfileDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileDetailPage", function() { return ProfileDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/ajax.service */ "./src/app/services/ajax.service.ts");






var ProfileDetailPage = /** @class */ (function () {
    function ProfileDetailPage(router, location, ajaxService) {
        this.router = router;
        this.location = location;
        this.ajaxService = ajaxService;
        this.detail = { firstName: "", lastName: "" };
    }
    ProfileDetailPage.prototype.closePage = function () {
        this.location.back();
    };
    ProfileDetailPage.prototype.getProfile = function () {
        var _this = this;
        var jsonData = {
            "companyId": this.companyDetail.companyID,
            "userName": this.companyDetail.userId
        };
        var url = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["serverUrl"].web + '/user/getUserDetails';
        this.ajaxService.ajaxPostWithBody(url, jsonData).subscribe(function (res) {
            console.log(res);
            _this.detail = res;
        });
    };
    ProfileDetailPage.prototype.ionViewWillEnter = function () {
        this.getProfile();
    };
    ProfileDetailPage.prototype.ngOnInit = function () {
        this.companyDetail = {
            branchID: localStorage.getItem('corpId'),
            companyID: localStorage.getItem('corpId'),
            userId: localStorage.getItem('userName')
        };
    };
    ProfileDetailPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] },
        { type: src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"] }
    ]; };
    ProfileDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-detail',
            template: __webpack_require__(/*! raw-loader!./profile-detail.page.html */ "./node_modules/raw-loader/index.js!./src/app/delar-application/profile-detail/profile-detail.page.html"),
            styles: [__webpack_require__(/*! ./profile-detail.page.scss */ "./src/app/delar-application/profile-detail/profile-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"],
            src_app_services_ajax_service__WEBPACK_IMPORTED_MODULE_5__["AjaxService"]])
    ], ProfileDetailPage);
    return ProfileDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=profile-detail-profile-detail-module-es5.js.map